import { Column, Entity, Index } from "typeorm";

@Index("PK_GJ_SM02Tbl", ["codeId"], { unique: true })
@Entity("GJ_SM02Tbl", { schema: "dbo" })
export class GjSm02Tbl {
  @Column("varchar", { primary: true, name: "CodeID", length: 20 })
  codeId: string;

  @Column("varchar", { name: "GroupID", length: 13 })
  groupId: string;

  @Column("nvarchar", { name: "CodeName", nullable: true, length: 200 })
  codeName: string | null;

  @Column("nvarchar", { name: "CodeName2", nullable: true, length: 200 })
  codeName2: string | null;

  @Column("nvarchar", { name: "AccountID", nullable: true, length: 100 })
  accountId: string | null;

  @Column("bit", { name: "PrintTF", nullable: true })
  printTf: boolean | null;

  @Column("bit", { name: "isBold", nullable: true })
  isBold: boolean | null;

  @Column("nvarchar", { name: "Formular", nullable: true, length: 200 })
  formular: string | null;

  @Column("bit", { name: "Asset", nullable: true })
  asset: boolean | null;

  @Column("int", { name: "Type", nullable: true })
  type: number | null;

  @Column("bit", { name: "isObject", nullable: true })
  isObject: boolean | null;

  @Column("bit", { name: "Negative", nullable: true })
  negative: boolean | null;

  @Column("decimal", {
    name: "BeginAmount",
    nullable: true,
    precision: 28,
    scale: 0
  })
  beginAmount: number | null;

  @Column("decimal", {
    name: "EndAmount",
    nullable: true,
    precision: 28,
    scale: 0
  })
  endAmount: number | null;
}
