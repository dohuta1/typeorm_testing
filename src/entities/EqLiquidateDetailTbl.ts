import { Column, Entity, Index, JoinColumn, ManyToOne } from "typeorm";
import { EqEquipTbl } from "./EqEquipTbl";
import { EqLiquidateTbl } from "./EqLiquidateTbl";

@Index("PK_EQ_LiquidateDetailTbl", ["userAutoId"], { unique: true })
@Entity("EQ_LiquidateDetailTbl", { schema: "dbo" })
export class EqLiquidateDetailTbl {
  @Column("varchar", { primary: true, name: "UserAutoID", length: 40 })
  userAutoId: string;

  @Column("decimal", {
    name: "RemainAmount",
    nullable: true,
    precision: 28,
    scale: 0
  })
  remainAmount: number | null;

  @Column("decimal", {
    name: "LiquidateAmount",
    nullable: true,
    precision: 28,
    scale: 0
  })
  liquidateAmount: number | null;

  @Column("varchar", { name: "IncomeAccID", nullable: true, length: 50 })
  incomeAccId: string | null;

  @Column("nvarchar", { name: "Notes", nullable: true, length: 150 })
  notes: string | null;

  @Column("decimal", {
    name: "Quantity",
    nullable: true,
    precision: 18,
    scale: 2,
    default: () => "1"
  })
  quantity: number | null;

  @ManyToOne(
    () => EqEquipTbl,
    eqEquipTbl => eqEquipTbl.eqLiquidateDetailTbls,
    { onUpdate: "CASCADE" }
  )
  @JoinColumn([{ name: "EquipID", referencedColumnName: "equipId" }])
  equip: EqEquipTbl;

  @ManyToOne(
    () => EqLiquidateTbl,
    eqLiquidateTbl => eqLiquidateTbl.eqLiquidateDetailTbls,
    { onDelete: "CASCADE", onUpdate: "CASCADE" }
  )
  @JoinColumn([{ name: "DocumentID", referencedColumnName: "documentId" }])
  document: EqLiquidateTbl;
}
