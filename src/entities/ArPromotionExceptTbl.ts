import { Column, Entity, Index, JoinColumn, ManyToOne } from "typeorm";
import { ArPromotionTbl } from "./ArPromotionTbl";

@Index("PK_AR_PromotionExceptTbl", ["userAutoId"], { unique: true })
@Entity("AR_PromotionExceptTbl", { schema: "dbo" })
export class ArPromotionExceptTbl {
  @Column("varchar", {
    primary: true,
    name: "UserAutoID",
    length: 50,
    default: () => "newid()"
  })
  userAutoId: string;

  @Column("varchar", { name: "PromotionExceptID", length: 30 })
  promotionExceptId: string;

  @Column("nvarchar", { name: "Notes", nullable: true, length: 250 })
  notes: string | null;

  @ManyToOne(
    () => ArPromotionTbl,
    arPromotionTbl => arPromotionTbl.arPromotionExceptTbls,
    { onDelete: "CASCADE", onUpdate: "CASCADE" }
  )
  @JoinColumn([{ name: "DocumentID", referencedColumnName: "documentId" }])
  document: ArPromotionTbl;
}
