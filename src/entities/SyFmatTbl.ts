import { Column, Entity, Index } from "typeorm";

@Index("PK_SY_Format", ["formatId"], { unique: true })
@Entity("SY_FmatTbl", { schema: "dbo" })
export class SyFmatTbl {
  @Column("varchar", { primary: true, name: "FormatID", length: 2 })
  formatId: string;

  @Column("nvarchar", { name: "FormatName", nullable: true, length: 50 })
  formatName: string | null;

  @Column("int", { name: "NumberDecimal", nullable: true })
  numberDecimal: number | null;

  @Column("nvarchar", { name: "FormatString", nullable: true, length: 100 })
  formatString: string | null;

  @Column("varchar", { name: "MaskString", nullable: true, length: 50 })
  maskString: string | null;

  @Column("smallint", { name: "MaxLength", nullable: true })
  maxLength: number | null;

  @Column("char", { name: "Type", length: 1, default: () => "'S'" })
  type: string;

  @Column("varchar", { name: "Params", nullable: true, length: 200 })
  params: string | null;

  @Column("char", { name: "Align", length: 1, default: () => "'L'" })
  align: string;

  @Column("bit", { name: "IsComplex", default: () => "0" })
  isComplex: boolean;

  @Column("float", { name: "MinValue", nullable: true, precision: 53 })
  minValue: number | null;

  @Column("float", { name: "MaxValue", nullable: true, precision: 53 })
  maxValue: number | null;
}
