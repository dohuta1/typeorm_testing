import { Column, Entity, Index } from "typeorm";

@Index("PK_CS_CSTypeTbl", ["csType"], { unique: true })
@Entity("CS_CSTypeTbl", { schema: "dbo" })
export class CsCsTypeTbl {
  @Column("varchar", { primary: true, name: "CSType", length: 50 })
  csType: string;

  @Column("nvarchar", { name: "CSTypeName", length: 150 })
  csTypeName: string;

  @Column("bit", { name: "isPayment", default: () => "0" })
  isPayment: boolean;

  @Column("bit", { name: "isCashTranfer" })
  isCashTranfer: boolean;

  @Column("varchar", { name: "AccountID", nullable: true, length: 50 })
  accountId: string | null;
}
