import { Column, Entity, Index } from "typeorm";

@Index("PK_SY_RPDTbl", ["userAutoId"], { unique: true })
@Entity("SY_RpDTbl", { schema: "dbo" })
export class SyRpDTbl {
  @Column("varchar", { primary: true, name: "UserAutoID", length: 100 })
  userAutoId: string;

  @Column("varchar", { name: "RpName", length: 100 })
  rpName: string;

  @Column("varchar", { name: "PName", length: 50 })
  pName: string;

  @Column("bit", { name: "isSub", default: () => "0" })
  isSub: boolean;

  @Column("bit", { name: "isReq", default: () => "0" })
  isReq: boolean;

  @Column("nvarchar", { name: "DefaultValueVN", nullable: true, length: 1000 })
  defaultValueVn: string | null;

  @Column("nvarchar", { name: "DefaultValueEN", nullable: true, length: 1000 })
  defaultValueEn: string | null;

  @Column("nvarchar", { name: "DefaultSQL", nullable: true, length: 150 })
  defaultSql: string | null;

  @Column("varchar", { name: "Link", nullable: true, length: 50 })
  link: string | null;

  @Column("varchar", { name: "SourceType", nullable: true, length: 50 })
  sourceType: string | null;

  @Column("int", { name: "Width", nullable: true, default: () => "0" })
  width: number | null;

  @Column("int", { name: "Height", nullable: true, default: () => "0" })
  height: number | null;

  @Column("nvarchar", { name: "Caption", nullable: true, length: 250 })
  caption: string | null;

  @Column("bit", { name: "isMultiSelect", nullable: true })
  isMultiSelect: boolean | null;

  @Column("nvarchar", { name: "LockExpression", nullable: true, length: 250 })
  lockExpression: string | null;

  @Column("int", { name: "UserType", nullable: true })
  userType: number | null;

  @Column("nvarchar", { name: "SourceNew", nullable: true })
  sourceNew: string | null;

  @Column("nvarchar", { name: "ParaArr", nullable: true, length: 250 })
  paraArr: string | null;

  @Column("int", { name: "EditType", nullable: true })
  editType: number | null;

  @Column("nvarchar", {
    name: "LockExpressionSQL",
    nullable: true,
    length: 500
  })
  lockExpressionSql: string | null;
}
