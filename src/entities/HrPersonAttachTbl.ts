import { Column, Entity, Index, JoinColumn, ManyToOne } from "typeorm";
import { HrPersonTbl } from "./HrPersonTbl";

@Index("PK_HR_PersonAttachTbl", ["userAutoId"], { unique: true })
@Entity("HR_PersonAttachTbl", { schema: "dbo" })
export class HrPersonAttachTbl {
  @Column("varchar", { primary: true, name: "UserAutoID", length: 50 })
  userAutoId: string;

  @Column("nvarchar", { name: "FileName", nullable: true, length: 250 })
  fileName: string | null;

  @Column("int", { name: "FileType", nullable: true, default: () => "(0)" })
  fileType: number | null;

  @Column("nvarchar", { name: "STT", nullable: true, length: 50 })
  stt: string | null;

  @Column("image", { name: "Content", nullable: true })
  content: Buffer | null;

  @Column("image", { name: "Content2", nullable: true })
  content2: Buffer | null;

  @ManyToOne(
    () => HrPersonTbl,
    hrPersonTbl => hrPersonTbl.hrPersonAttachTbls,
    { onDelete: "CASCADE", onUpdate: "CASCADE" }
  )
  @JoinColumn([{ name: "PersonID", referencedColumnName: "personId" }])
  person: HrPersonTbl;
}
