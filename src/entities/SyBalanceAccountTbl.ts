import { Column, Entity, Index, JoinColumn, ManyToOne } from "typeorm";
import { CfCostCenterTbl } from "./CfCostCenterTbl";

@Index("IX_SY_BalanceAccountTbl", ["periodId"], {})
@Index("PK_SY_BalanceAccountTbl", ["userAutoId"], { unique: true })
@Entity("SY_BalanceAccountTbl", { schema: "dbo" })
export class SyBalanceAccountTbl {
  @Column("varchar", { primary: true, name: "UserAutoID", length: 50 })
  userAutoId: string;

  @Column("varchar", { name: "AccountID", length: 50 })
  accountId: string;

  @Column("decimal", {
    name: "SourceAmount",
    precision: 28,
    scale: 2,
    default: () => "0"
  })
  sourceAmount: number;

  @Column("varchar", { name: "CurrencyID", length: 3, default: () => "'VND'" })
  currencyId: string;

  @Column("float", { name: "RateExchange", precision: 53, default: () => "1" })
  rateExchange: number;

  @Column("decimal", {
    name: "Amount",
    precision: 28,
    scale: 0,
    default: () => "0"
  })
  amount: number;

  @Column("bit", { name: "isDebit", default: () => "1" })
  isDebit: boolean;

  @Column("varchar", { name: "PeriodID", length: 10, default: () => "201005" })
  periodId: string;

  @Column("bit", { name: "IsBalance", default: () => "0" })
  isBalance: boolean;

  @Column("varchar", { name: "BranchID", nullable: true, length: 50 })
  branchId: string | null;

  @Column("decimal", {
    name: "SourceDebit",
    nullable: true,
    precision: 18,
    scale: 4
  })
  sourceDebit: number | null;

  @Column("decimal", {
    name: "SourceCredit",
    nullable: true,
    precision: 18,
    scale: 4
  })
  sourceCredit: number | null;

  @Column("decimal", {
    name: "AmountDebit",
    nullable: true,
    precision: 18,
    scale: 0
  })
  amountDebit: number | null;

  @Column("decimal", {
    name: "AmountCredit",
    nullable: true,
    precision: 18,
    scale: 0
  })
  amountCredit: number | null;

  @ManyToOne(
    () => CfCostCenterTbl,
    cfCostCenterTbl => cfCostCenterTbl.syBalanceAccountTbls,
    { onUpdate: "CASCADE" }
  )
  @JoinColumn([{ name: "CostCenterID", referencedColumnName: "costCenterId" }])
  costCenter: CfCostCenterTbl;
}
