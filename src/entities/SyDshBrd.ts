import { Column, Entity, Index } from "typeorm";

@Index("PK_SY_DshBrd", ["userAutoId"], { unique: true })
@Entity("SY_DshBrd", { schema: "dbo" })
export class SyDshBrd {
  @Column("varchar", { primary: true, name: "UserAutoID", length: 50 })
  userAutoId: string;

  @Column("varchar", { name: "KeyID", length: 50 })
  keyId: string;

  @Column("nvarchar", { name: "KeyName", length: 150 })
  keyName: string;

  @Column("nvarchar", { name: "ItemName", nullable: true, length: 100 })
  itemName: string | null;

  @Column("decimal", {
    name: "GiaTri",
    nullable: true,
    precision: 28,
    scale: 4
  })
  giaTri: number | null;

  @Column("decimal", {
    name: "GiaTri2",
    nullable: true,
    precision: 28,
    scale: 4
  })
  giaTri2: number | null;

  @Column("nvarchar", { name: "Notes", nullable: true, length: 100 })
  notes: string | null;

  @Column("int", { name: "Orderby", nullable: true, default: () => "9" })
  orderby: number | null;

  @Column("nvarchar", { name: "Action", nullable: true, length: 100 })
  action: string | null;

  @Column("bit", { name: "IsWanrning", default: () => "0" })
  isWanrning: boolean;

  @Column("varchar", { name: "AccountID", nullable: true, length: 50 })
  accountId: string | null;

  @Column("datetime", {
    name: "LastUpdate",
    nullable: true,
    default: () => "getdate()"
  })
  lastUpdate: Date | null;

  @Column("varchar", { name: "ItemID", nullable: true, length: 50 })
  itemId: string | null;

  @Column("varchar", { name: "ObjectID", nullable: true, length: 50 })
  objectId: string | null;
}
