import { Column, Entity, Index, OneToMany } from "typeorm";
import { ArObjectContractDetailTbl } from "./ArObjectContractDetailTbl";
import { ArObjectContractItemTbl } from "./ArObjectContractItemTbl";

@Index("PK_AR_ObjectContractTbl", ["maHopDong"], { unique: true })
@Entity("AR_ObjectContractTbl", { schema: "dbo" })
export class ArObjectContractTbl {
  @Column("nvarchar", { primary: true, name: "MaHopDong", length: 50 })
  maHopDong: string;

  @Column("nvarchar", { name: "TenHopDong", nullable: true, length: 50 })
  tenHopDong: string | null;

  @Column("nvarchar", { name: "Notes", nullable: true, length: 50 })
  notes: string | null;

  @Column("varchar", { name: "UserCreate", nullable: true, length: 30 })
  userCreate: string | null;

  @Column("varchar", { name: "UserUpdate", nullable: true, length: 30 })
  userUpdate: string | null;

  @Column("datetime", { name: "DateUpdate", nullable: true })
  dateUpdate: Date | null;

  @Column("datetime", { name: "DateCreate", nullable: true })
  dateCreate: Date | null;

  @OneToMany(
    () => ArObjectContractDetailTbl,
    arObjectContractDetailTbl => arObjectContractDetailTbl.maHopDong
  )
  arObjectContractDetailTbls: ArObjectContractDetailTbl[];

  @OneToMany(
    () => ArObjectContractItemTbl,
    arObjectContractItemTbl => arObjectContractItemTbl.maHopDong
  )
  arObjectContractItemTbls: ArObjectContractItemTbl[];
}
