import { Column, Entity, Index, JoinColumn, ManyToOne } from "typeorm";
import { CfContractTbl } from "./CfContractTbl";

@Index("PK_CF_ContractThanhToanTbl", ["userAutoId"], { unique: true })
@Entity("CF_ContractThanhToanTbl", { schema: "dbo" })
export class CfContractThanhToanTbl {
  @Column("varchar", { primary: true, name: "UserAutoID", length: 50 })
  userAutoId: string;

  @Column("varchar", { name: "DotThanhToan", nullable: true, length: 50 })
  dotThanhToan: string | null;

  @Column("decimal", {
    name: "Amount",
    nullable: true,
    precision: 18,
    scale: 0
  })
  amount: number | null;

  @Column("datetime", { name: "NgayThanhToan", nullable: true })
  ngayThanhToan: Date | null;

  @Column("nvarchar", { name: "DienGiai", nullable: true, length: 50 })
  dienGiai: string | null;

  @Column("nvarchar", { name: "Notes", nullable: true, length: 50 })
  notes: string | null;

  @ManyToOne(
    () => CfContractTbl,
    cfContractTbl => cfContractTbl.cfContractThanhToanTbls,
    { onDelete: "CASCADE", onUpdate: "CASCADE" }
  )
  @JoinColumn([{ name: "ContractID", referencedColumnName: "contractId" }])
  contract: CfContractTbl;
}
