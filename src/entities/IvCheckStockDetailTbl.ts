import { Column, Entity, Index, JoinColumn, ManyToOne } from "typeorm";
import { CfItemTbl } from "./CfItemTbl";
import { IvCheckStockTbl } from "./IvCheckStockTbl";

@Index("PK_CheckStockDetailTbl", ["userAutoId"], { unique: true })
@Entity("IV_CheckStockDetailTbl", { schema: "dbo" })
export class IvCheckStockDetailTbl {
  @Column("varchar", { primary: true, name: "UserAutoID", length: 40 })
  userAutoId: string;

  @Column("varchar", { name: "Lot", nullable: true, length: 50 })
  lot: string | null;

  @Column("varchar", { name: "InvAccID", length: 50 })
  invAccId: string;

  @Column("decimal", {
    name: "CheckQuantity",
    nullable: true,
    precision: 28,
    scale: 4
  })
  checkQuantity: number | null;

  @Column("decimal", {
    name: "StockQuantity",
    nullable: true,
    precision: 28,
    scale: 4
  })
  stockQuantity: number | null;

  @Column("decimal", {
    name: "DiffQuantity",
    nullable: true,
    precision: 28,
    scale: 4
  })
  diffQuantity: number | null;

  @Column("decimal", {
    name: "CheckQuantity2",
    nullable: true,
    precision: 28,
    scale: 4
  })
  checkQuantity2: number | null;

  @Column("decimal", {
    name: "StockQuantity2",
    nullable: true,
    precision: 28,
    scale: 4
  })
  stockQuantity2: number | null;

  @Column("decimal", {
    name: "DiffQuantity2",
    nullable: true,
    precision: 28,
    scale: 4
  })
  diffQuantity2: number | null;

  @Column("nvarchar", { name: "Property", nullable: true, length: 50 })
  property: string | null;

  @Column("nvarchar", { name: "Property2", nullable: true, length: 50 })
  property2: string | null;

  @Column("datetime", { name: "ExpireDate", nullable: true })
  expireDate: Date | null;

  @ManyToOne(
    () => CfItemTbl,
    cfItemTbl => cfItemTbl.ivCheckStockDetailTbls,
    { onUpdate: "CASCADE" }
  )
  @JoinColumn([{ name: "ItemID", referencedColumnName: "itemId" }])
  item: CfItemTbl;

  @ManyToOne(
    () => IvCheckStockTbl,
    ivCheckStockTbl => ivCheckStockTbl.ivCheckStockDetailTbls,
    { onDelete: "CASCADE", onUpdate: "CASCADE" }
  )
  @JoinColumn([{ name: "DocumentID", referencedColumnName: "documentId" }])
  document: IvCheckStockTbl;
}
