import { Column, Entity, Index, JoinColumn, ManyToOne } from "typeorm";
import { ArBanHoTbl } from "./ArBanHoTbl";

@Index("PK_AR_BanHoCouponTbl", ["userAutoId"], { unique: true })
@Entity("AR_BanHoCouponTbl", { schema: "dbo" })
export class ArBanHoCouponTbl {
  @Column("varchar", { primary: true, name: "UserAutoID", length: 40 })
  userAutoId: string;

  @Column("varchar", { name: "CouponSeri", nullable: true, length: 100 })
  couponSeri: string | null;

  @Column("decimal", {
    name: "TriGia",
    nullable: true,
    precision: 18,
    scale: 0
  })
  triGia: number | null;

  @Column("nvarchar", { name: "Notes", nullable: true, length: 50 })
  notes: string | null;

  @ManyToOne(
    () => ArBanHoTbl,
    arBanHoTbl => arBanHoTbl.arBanHoCouponTbls,
    { onDelete: "CASCADE", onUpdate: "CASCADE" }
  )
  @JoinColumn([{ name: "DocumentID", referencedColumnName: "documentId" }])
  document: ArBanHoTbl;
}
