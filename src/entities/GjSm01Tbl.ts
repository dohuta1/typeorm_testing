import { Column, Entity, Index } from "typeorm";

@Index("PK_GJ_SM01Tbl", ["codeId"], { unique: true })
@Entity("GJ_SM01Tbl", { schema: "dbo" })
export class GjSm01Tbl {
  @Column("varchar", { primary: true, name: "CodeID", length: 20 })
  codeId: string;

  @Column("varchar", { name: "GroupID", length: 13 })
  groupId: string;

  @Column("nvarchar", { name: "CodeName", nullable: true, length: 200 })
  codeName: string | null;

  @Column("nvarchar", { name: "CodeName2", nullable: true, length: 200 })
  codeName2: string | null;

  @Column("int", { name: "CodeLevel", nullable: true })
  codeLevel: number | null;

  @Column("bit", { name: "isBold", nullable: true })
  isBold: boolean | null;

  @Column("int", { name: "ID", nullable: true })
  id: number | null;

  @Column("int", { name: "Type", nullable: true })
  type: number | null;

  @Column("bit", { name: "PrintTF", nullable: true })
  printTf: boolean | null;
}
