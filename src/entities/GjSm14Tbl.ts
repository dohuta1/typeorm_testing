import { Column, Entity, Index } from "typeorm";

@Index("PK_GJ_SM14Tbl", ["codeId"], { unique: true })
@Entity("GJ_SM14Tbl", { schema: "dbo" })
export class GjSm14Tbl {
  @Column("varchar", { primary: true, name: "CodeID", length: 50 })
  codeId: string;

  @Column("varchar", { name: "GroupID", length: 13 })
  groupId: string;

  @Column("nvarchar", { name: "CodeName", nullable: true, length: 200 })
  codeName: string | null;

  @Column("nvarchar", { name: "CodeName2", nullable: true, length: 200 })
  codeName2: string | null;

  @Column("bit", { name: "PrintTF", nullable: true })
  printTf: boolean | null;

  @Column("bit", { name: "isBold", nullable: true })
  isBold: boolean | null;

  @Column("int", { name: "AccountLevel", nullable: true })
  accountLevel: number | null;

  @Column("decimal", { name: "ID", nullable: true, precision: 28, scale: 4 })
  id: number | null;

  @Column("int", { name: "Type", nullable: true })
  type: number | null;

  @Column("decimal", {
    name: "BeginAmount",
    nullable: true,
    precision: 28,
    scale: 0
  })
  beginAmount: number | null;

  @Column("decimal", {
    name: "EndAmount",
    nullable: true,
    precision: 28,
    scale: 0
  })
  endAmount: number | null;
}
