import { Column, Entity, Index } from "typeorm";

@Index("PK_SY_Res", ["resId"], { unique: true })
@Entity("SY_Res", { schema: "dbo" })
export class SyRes {
  @Column("varchar", { primary: true, name: "ResID", length: 50 })
  resId: string;

  @Column("image", { name: "ResValue", nullable: true })
  resValue: Buffer | null;

  @Column("nvarchar", { name: "Notes", nullable: true, length: 150 })
  notes: string | null;
}
