import { Column, Entity, Index } from "typeorm";

@Index("PK_CF_PaymentTypeTbl", ["paymentTypeId"], { unique: true })
@Entity("CF_PaymentTypeTbl", { schema: "dbo" })
export class CfPaymentTypeTbl {
  @Column("varchar", { primary: true, name: "PaymentTypeID", length: 50 })
  paymentTypeId: string;

  @Column("nvarchar", { name: "PaymentTypeName", length: 100 })
  paymentTypeName: string;

  @Column("nvarchar", { name: "PaymentTypeName2", nullable: true, length: 100 })
  paymentTypeName2: string | null;

  @Column("bit", { name: "isDefault", nullable: true })
  isDefault: boolean | null;

  @Column("varchar", { name: "UserCreate", nullable: true, length: 30 })
  userCreate: string | null;

  @Column("varchar", { name: "UserUpdate", nullable: true, length: 30 })
  userUpdate: string | null;

  @Column("datetime", { name: "DateUpdate", nullable: true })
  dateUpdate: Date | null;

  @Column("datetime", { name: "DateCreate", nullable: true })
  dateCreate: Date | null;
}
