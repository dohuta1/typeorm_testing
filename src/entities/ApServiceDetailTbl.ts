import { Column, Entity, Index, JoinColumn, ManyToOne } from "typeorm";
import { CfCostCenterTbl } from "./CfCostCenterTbl";
import { CfItemTbl } from "./CfItemTbl";
import { CfCostTbl } from "./CfCostTbl";
import { ApServiceTbl } from "./ApServiceTbl";

@Index("IX_AP_ServiceDetailTbl_DocumentID", ["documentId"], {})
@Index("PK_ServiceDetailTbl", ["userAutoId"], { unique: true })
@Entity("AP_ServiceDetailTbl", { schema: "dbo" })
export class ApServiceDetailTbl {
  @Column("varchar", { primary: true, name: "UserAutoID", length: 40 })
  userAutoId: string;

  @Column("varchar", { name: "DocumentID", length: 30 })
  documentId: string;

  @Column("nvarchar", { name: "Memo", nullable: true, length: 150 })
  memo: string | null;

  @Column("nvarchar", { name: "Unit", nullable: true, length: 50 })
  unit: string | null;

  @Column("decimal", {
    name: "Quantity",
    nullable: true,
    precision: 28,
    scale: 2
  })
  quantity: number | null;

  @Column("decimal", {
    name: "UnitPrice",
    nullable: true,
    precision: 28,
    scale: 4
  })
  unitPrice: number | null;

  @Column("decimal", {
    name: "SourceAmount",
    nullable: true,
    precision: 28,
    scale: 2
  })
  sourceAmount: number | null;

  @Column("decimal", {
    name: "Amount",
    nullable: true,
    precision: 28,
    scale: 0
  })
  amount: number | null;

  @Column("varchar", { name: "VATID", nullable: true, length: 50 })
  vatid: string | null;

  @Column("decimal", {
    name: "VATPercent",
    nullable: true,
    precision: 18,
    scale: 2
  })
  vatPercent: number | null;

  @Column("decimal", {
    name: "VATAmount",
    nullable: true,
    precision: 28,
    scale: 0
  })
  vatAmount: number | null;

  @Column("decimal", {
    name: "TotalAmount",
    nullable: true,
    precision: 28,
    scale: 0
  })
  totalAmount: number | null;

  @Column("varchar", { name: "ExpenseAccID", length: 50 })
  expenseAccId: string;

  @Column("varchar", { name: "ObjectID", nullable: true, length: 50 })
  objectId: string | null;

  @Column("varchar", { name: "ParentID", nullable: true, length: 50 })
  parentId: string | null;

  @Column("decimal", {
    name: "ImportPercent",
    nullable: true,
    precision: 28,
    scale: 2
  })
  importPercent: number | null;

  @Column("decimal", {
    name: "ImportAmount",
    nullable: true,
    precision: 28,
    scale: 2
  })
  importAmount: number | null;

  @Column("decimal", {
    name: "UnitPriceV",
    nullable: true,
    precision: 28,
    scale: 2
  })
  unitPriceV: number | null;

  @ManyToOne(
    () => CfCostCenterTbl,
    cfCostCenterTbl => cfCostCenterTbl.apServiceDetailTbls,
    { onUpdate: "CASCADE" }
  )
  @JoinColumn([{ name: "CostCenterID", referencedColumnName: "costCenterId" }])
  costCenter: CfCostCenterTbl;

  @ManyToOne(
    () => CfItemTbl,
    cfItemTbl => cfItemTbl.apServiceDetailTbls,
    { onUpdate: "CASCADE" }
  )
  @JoinColumn([{ name: "ItemID", referencedColumnName: "itemId" }])
  item: CfItemTbl;

  @ManyToOne(
    () => CfCostTbl,
    cfCostTbl => cfCostTbl.apServiceDetailTbls,
    { onUpdate: "CASCADE" }
  )
  @JoinColumn([{ name: "CostID", referencedColumnName: "costId" }])
  cost: CfCostTbl;

  @ManyToOne(
    () => ApServiceTbl,
    apServiceTbl => apServiceTbl.apServiceDetailTbls,
    { onDelete: "CASCADE", onUpdate: "CASCADE" }
  )
  @JoinColumn([{ name: "DocumentID", referencedColumnName: "documentId" }])
  document: ApServiceTbl;
}
