import { Column, Entity, Index, OneToMany } from "typeorm";
import { HrPersonAllowanceTbl } from "./HrPersonAllowanceTbl";
import { HrPersonAttachTbl } from "./HrPersonAttachTbl";
import { HrPersonKtklTbl } from "./HrPersonKtklTbl";
import { HrPersonNghiPhepTbl } from "./HrPersonNghiPhepTbl";
import { HrPersonSalaryTbl } from "./HrPersonSalaryTbl";

@Index("PK_HR_PersonTbl", ["personId"], { unique: true })
@Entity("HR_PersonTbl", { schema: "dbo" })
export class HrPersonTbl {
  @Column("varchar", { primary: true, name: "PersonID", length: 50 })
  personId: string;

  @Column("nvarchar", { name: "PersonName", length: 50 })
  personName: string;

  @Column("nvarchar", { name: "PersonName2", nullable: true, length: 50 })
  personName2: string | null;

  @Column("nvarchar", { name: "PhongBan", nullable: true, length: 50 })
  phongBan: string | null;

  @Column("nvarchar", { name: "DienThoai", nullable: true, length: 50 })
  dienThoai: string | null;

  @Column("nvarchar", { name: "GioiTinh", nullable: true, length: 20 })
  gioiTinh: string | null;

  @Column("nvarchar", { name: "TitleName", nullable: true, length: 50 })
  titleName: string | null;

  @Column("nvarchar", { name: "PostionName", nullable: true, length: 50 })
  postionName: string | null;

  @Column("nvarchar", { name: "ProvineName", nullable: true, length: 150 })
  provineName: string | null;

  @Column("nvarchar", { name: "NationName", nullable: true, length: 150 })
  nationName: string | null;

  @Column("nvarchar", { name: "JobName", nullable: true, length: 50 })
  jobName: string | null;

  @Column("datetime", { name: "NgayVaoLam", nullable: true })
  ngayVaoLam: Date | null;

  @Column("varchar", { name: "ShiftID", nullable: true, length: 50 })
  shiftId: string | null;

  @Column("int", { name: "EnrollNumber", nullable: true })
  enrollNumber: number | null;

  @Column("varchar", { name: "CardNo", nullable: true, length: 10 })
  cardNo: string | null;

  @Column("varchar", { name: "CMND", nullable: true, length: 50 })
  cmnd: string | null;

  @Column("varchar", { name: "CMNDNgayCap", nullable: true, length: 50 })
  cmndNgayCap: string | null;

  @Column("nvarchar", { name: "CMNDNoiCap", nullable: true, length: 150 })
  cmndNoiCap: string | null;

  @Column("datetime", { name: "NgaySinh", nullable: true })
  ngaySinh: Date | null;

  @Column("int", { name: "NamSinh", nullable: true })
  namSinh: number | null;

  @Column("nvarchar", { name: "NoiSinh", nullable: true, length: 150 })
  noiSinh: string | null;

  @Column("nvarchar", { name: "DiaChiThuongTru", nullable: true, length: 250 })
  diaChiThuongTru: string | null;

  @Column("nvarchar", { name: "Email", nullable: true, length: 50 })
  email: string | null;

  @Column("datetime", { name: "NgayNghiViec", nullable: true })
  ngayNghiViec: Date | null;

  @Column("nvarchar", { name: "WorkingGroupName", nullable: true, length: 50 })
  workingGroupName: string | null;

  @Column("nvarchar", { name: "PeoplesName", nullable: true, length: 150 })
  peoplesName: string | null;

  @Column("nvarchar", { name: "ReligionName", nullable: true, length: 50 })
  religionName: string | null;

  @Column("nvarchar", { name: "BankName", nullable: true, length: 150 })
  bankName: string | null;

  @Column("nvarchar", { name: "BankAccountNo", nullable: true, length: 150 })
  bankAccountNo: string | null;

  @Column("nvarchar", { name: "CareerName", nullable: true, length: 50 })
  careerName: string | null;

  @Column("nvarchar", { name: "EducationName", nullable: true, length: 50 })
  educationName: string | null;

  @Column("nvarchar", { name: "HospitalName", nullable: true, length: 150 })
  hospitalName: string | null;

  @Column("nvarchar", { name: "HonNhan", nullable: true, length: 150 })
  honNhan: string | null;

  @Column("nvarchar", { name: "SocialID", nullable: true, length: 50 })
  socialId: string | null;

  @Column("datetime", { name: "SocialDate", nullable: true })
  socialDate: Date | null;

  @Column("varchar", { name: "FingerData", nullable: true })
  fingerData: string | null;

  @Column("varchar", { name: "FingerData2", nullable: true })
  fingerData2: string | null;

  @Column("varchar", { name: "UserCreate", nullable: true, length: 30 })
  userCreate: string | null;

  @Column("varchar", { name: "UserUpdate", nullable: true, length: 30 })
  userUpdate: string | null;

  @Column("datetime", { name: "DateUpdate", nullable: true })
  dateUpdate: Date | null;

  @Column("datetime", { name: "DateCreate", nullable: true })
  dateCreate: Date | null;

  @Column("datetime", { name: "NgayThuViec", nullable: true })
  ngayThuViec: Date | null;

  @Column("nvarchar", { name: "SoHopDong", nullable: true, length: 50 })
  soHopDong: string | null;

  @Column("nvarchar", { name: "LoaiHopDong", nullable: true, length: 50 })
  loaiHopDong: string | null;

  @Column("datetime", { name: "NgayHopDong", nullable: true })
  ngayHopDong: Date | null;

  @Column("datetime", { name: "NgayHetHopDong", nullable: true })
  ngayHetHopDong: Date | null;

  @Column("nvarchar", { name: "DungCuLamViec", nullable: true, length: 150 })
  dungCuLamViec: string | null;

  @Column("nvarchar", { name: "GhiChu", nullable: true, length: 150 })
  ghiChu: string | null;

  @Column("int", { name: "PersonStatus", default: () => "(1)" })
  personStatus: number;

  @Column("nvarchar", { name: "SoTheBHYT", nullable: true, length: 50 })
  soTheBhyt: string | null;

  @Column("nvarchar", { name: "ThoiGianHuongBHYT", nullable: true, length: 50 })
  thoiGianHuongBhyt: string | null;

  @Column("nvarchar", { name: "NoiDangKyBHYT", nullable: true, length: 50 })
  noiDangKyBhyt: string | null;

  @OneToMany(
    () => HrPersonAllowanceTbl,
    hrPersonAllowanceTbl => hrPersonAllowanceTbl.person
  )
  hrPersonAllowanceTbls: HrPersonAllowanceTbl[];

  @OneToMany(
    () => HrPersonAttachTbl,
    hrPersonAttachTbl => hrPersonAttachTbl.person
  )
  hrPersonAttachTbls: HrPersonAttachTbl[];

  @OneToMany(
    () => HrPersonKtklTbl,
    hrPersonKtklTbl => hrPersonKtklTbl.person
  )
  hrPersonKtklTbls: HrPersonKtklTbl[];

  @OneToMany(
    () => HrPersonNghiPhepTbl,
    hrPersonNghiPhepTbl => hrPersonNghiPhepTbl.person
  )
  hrPersonNghiPhepTbls: HrPersonNghiPhepTbl[];

  @OneToMany(
    () => HrPersonSalaryTbl,
    hrPersonSalaryTbl => hrPersonSalaryTbl.person
  )
  hrPersonSalaryTbls: HrPersonSalaryTbl[];
}
