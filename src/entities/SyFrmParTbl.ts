import { Column, Entity, Index } from "typeorm";

@Index("PK_SY_FrmParTbl", ["userAutoId"], { unique: true })
@Entity("SY_FrmParTbl", { schema: "dbo" })
export class SyFrmParTbl {
  @Column("varchar", { primary: true, name: "UserAutoID", length: 50 })
  userAutoId: string;

  @Column("varchar", { name: "FormID", nullable: true, length: 250 })
  formId: string | null;

  @Column("varchar", { name: "GridName", nullable: true, length: 100 })
  gridName: string | null;

  @Column("varchar", { name: "DataName", nullable: true, length: 100 })
  dataName: string | null;

  @Column("varchar", { name: "Type", length: 100 })
  type: string;

  @Column("varchar", { name: "ColumnID", nullable: true, length: 50 })
  columnId: string | null;

  @Column("nvarchar", { name: "Value", nullable: true, length: 150 })
  value: string | null;

  @Column("nvarchar", { name: "Value2", nullable: true, length: 150 })
  value2: string | null;

  @Column("varchar", { name: "Target", nullable: true, length: 50 })
  target: string | null;

  @Column("varchar", { name: "MsgID", nullable: true, length: 50 })
  msgId: string | null;

  @Column("bit", { name: "isUser", default: () => "0" })
  isUser: boolean;

  @Column("bit", { name: "isDisable", default: () => "0" })
  isDisable: boolean;
}
