import { Column, Entity, Index, JoinColumn, ManyToOne } from "typeorm";
import { ArItemGroupTbl } from "./ArItemGroupTbl";

@Index("PK_AR_ItemGroupDetailTbl", ["userAutoId"], { unique: true })
@Entity("AR_ItemGroupDetailTbl", { schema: "dbo" })
export class ArItemGroupDetailTbl {
  @Column("varchar", { primary: true, name: "UserAutoID", length: 50 })
  userAutoId: string;

  @Column("varchar", { name: "ItemID", length: 50 })
  itemId: string;

  @ManyToOne(
    () => ArItemGroupTbl,
    arItemGroupTbl => arItemGroupTbl.arItemGroupDetailTbls,
    { onDelete: "CASCADE", onUpdate: "CASCADE" }
  )
  @JoinColumn([
    { name: "ItemGroupCode", referencedColumnName: "itemGroupCode" }
  ])
  itemGroupCode: ArItemGroupTbl;
}
