import { Column, Entity, Index, JoinColumn, ManyToOne } from "typeorm";
import { EqEquipTbl } from "./EqEquipTbl";

@Index("PK_EQ_EquipPeriodSetupTbl", ["userAutoId"], { unique: true })
@Entity("EQ_EquipPeriodSetupTbl", { schema: "dbo" })
export class EqEquipPeriodSetupTbl {
  @Column("varchar", { primary: true, name: "UserAutoID", length: 50 })
  userAutoId: string;

  @Column("varchar", { name: "PeriodID", length: 50 })
  periodId: string;

  @Column("bit", { name: "isStopAllocate", nullable: true })
  isStopAllocate: boolean | null;

  @Column("decimal", {
    name: "FixAllocateAmount",
    nullable: true,
    precision: 28,
    scale: 0
  })
  fixAllocateAmount: number | null;

  @Column("varchar", { name: "CostID", nullable: true, length: 50 })
  costId: string | null;

  @Column("varchar", { name: "CostCenterID", nullable: true, length: 50 })
  costCenterId: string | null;

  @Column("varchar", { name: "ObjectID", nullable: true, length: 50 })
  objectId: string | null;

  @Column("varchar", { name: "ExpenseAccID", nullable: true, length: 50 })
  expenseAccId: string | null;

  @Column("varchar", { name: "PrepayAccountID", nullable: true, length: 50 })
  prepayAccountId: string | null;

  @Column("nvarchar", { name: "Notes", nullable: true, length: 150 })
  notes: string | null;

  @ManyToOne(
    () => EqEquipTbl,
    eqEquipTbl => eqEquipTbl.eqEquipPeriodSetupTbls,
    { onDelete: "CASCADE", onUpdate: "CASCADE" }
  )
  @JoinColumn([{ name: "EquipID", referencedColumnName: "equipId" }])
  equip: EqEquipTbl;
}
