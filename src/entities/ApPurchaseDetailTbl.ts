import { Column, Entity, Index, JoinColumn, ManyToOne } from "typeorm";
import { CfCostTbl } from "./CfCostTbl";
import { CfItemTbl } from "./CfItemTbl";
import { CfCostCenterTbl } from "./CfCostCenterTbl";
import { ApPurchaseTbl } from "./ApPurchaseTbl";
import { CfStoreHouseTbl } from "./CfStoreHouseTbl";

@Index("IX_AP_PurchaseDetailTbl_DocumentID", ["documentId"], {})
@Index("PK_PurchaseDetailTbl", ["userAutoId"], { unique: true })
@Entity("AP_PurchaseDetailTbl", { schema: "dbo" })
export class ApPurchaseDetailTbl {
  @Column("varchar", { primary: true, name: "UserAutoID", length: 40 })
  userAutoId: string;

  @Column("varchar", { name: "DocumentID", length: 30 })
  documentId: string;

  @Column("varchar", { name: "Lot", nullable: true, length: 50 })
  lot: string | null;

  @Column("datetime", { name: "ExpireDate", nullable: true })
  expireDate: Date | null;

  @Column("decimal", {
    name: "Quantity",
    precision: 28,
    scale: 4,
    default: () => "(1)"
  })
  quantity: number;

  @Column("decimal", {
    name: "UnitPrice",
    nullable: true,
    precision: 28,
    scale: 4
  })
  unitPrice: number | null;

  @Column("decimal", {
    name: "SourceAmount",
    nullable: true,
    precision: 28,
    scale: 2
  })
  sourceAmount: number | null;

  @Column("decimal", {
    name: "Amount",
    nullable: true,
    precision: 28,
    scale: 0
  })
  amount: number | null;

  @Column("decimal", {
    name: "ImportPercent",
    nullable: true,
    precision: 28,
    scale: 2
  })
  importPercent: number | null;

  @Column("decimal", {
    name: "ImportAmount",
    nullable: true,
    precision: 28,
    scale: 0
  })
  importAmount: number | null;

  @Column("varchar", { name: "VATID", nullable: true, length: 50 })
  vatid: string | null;

  @Column("decimal", {
    name: "VATPercent",
    nullable: true,
    precision: 28,
    scale: 2
  })
  vatPercent: number | null;

  @Column("decimal", {
    name: "VATAmount",
    nullable: true,
    precision: 28,
    scale: 0
  })
  vatAmount: number | null;

  @Column("decimal", {
    name: "TotalAmount",
    nullable: true,
    precision: 28,
    scale: 0
  })
  totalAmount: number | null;

  @Column("nvarchar", { name: "Notes", nullable: true, length: 100 })
  notes: string | null;

  @Column("varchar", { name: "InvAccID", nullable: true, length: 50 })
  invAccId: string | null;

  @Column("decimal", {
    name: "PurchaseCost",
    nullable: true,
    precision: 28,
    scale: 0,
    default: () => "(0)"
  })
  purchaseCost: number | null;

  @Column("decimal", {
    name: "Quantity2",
    nullable: true,
    precision: 28,
    scale: 4
  })
  quantity2: number | null;

  @Column("nvarchar", { name: "Property", nullable: true, length: 50 })
  property: string | null;

  @Column("nvarchar", { name: "Property2", nullable: true, length: 50 })
  property2: string | null;

  @Column("varchar", { name: "ObjectID", nullable: true, length: 50 })
  objectId: string | null;

  @Column("varchar", { name: "ParentID", nullable: true, length: 50 })
  parentId: string | null;

  @Column("decimal", {
    name: "UnitPriceV",
    nullable: true,
    precision: 28,
    scale: 2
  })
  unitPriceV: number | null;

  @Column("varchar", { name: "CostAccID", length: 50, default: () => "'1562'" })
  costAccId: string;

  @Column("float", { name: "UnitFactor", nullable: true, precision: 53 })
  unitFactor: number | null;

  @Column("decimal", {
    name: "PhiKhac",
    nullable: true,
    precision: 18,
    scale: 0
  })
  phiKhac: number | null;

  @Column("varchar", { name: "TKPhiKhac", nullable: true, length: 50 })
  tkPhiKhac: string | null;

  @Column("decimal", {
    name: "ImportPercent2",
    nullable: true,
    precision: 18,
    scale: 1
  })
  importPercent2: number | null;

  @Column("decimal", {
    name: "ImportAmount2",
    nullable: true,
    precision: 18,
    scale: 0
  })
  importAmount2: number | null;

  @Column("decimal", {
    name: "DiscountPercent",
    nullable: true,
    precision: 18,
    scale: 2
  })
  discountPercent: number | null;

  @Column("decimal", {
    name: "DiscountAmount",
    nullable: true,
    precision: 18,
    scale: 0
  })
  discountAmount: number | null;

  @ManyToOne(
    () => CfCostTbl,
    cfCostTbl => cfCostTbl.apPurchaseDetailTbls,
    { onUpdate: "CASCADE" }
  )
  @JoinColumn([{ name: "CostID", referencedColumnName: "costId" }])
  cost: CfCostTbl;

  @ManyToOne(
    () => CfItemTbl,
    cfItemTbl => cfItemTbl.apPurchaseDetailTbls,
    { onUpdate: "CASCADE" }
  )
  @JoinColumn([{ name: "ItemID", referencedColumnName: "itemId" }])
  item: CfItemTbl;

  @ManyToOne(
    () => CfCostCenterTbl,
    cfCostCenterTbl => cfCostCenterTbl.apPurchaseDetailTbls,
    { onUpdate: "CASCADE" }
  )
  @JoinColumn([{ name: "CostCenterID", referencedColumnName: "costCenterId" }])
  costCenter: CfCostCenterTbl;

  @ManyToOne(
    () => ApPurchaseTbl,
    apPurchaseTbl => apPurchaseTbl.apPurchaseDetailTbls,
    { onDelete: "CASCADE", onUpdate: "CASCADE" }
  )
  @JoinColumn([{ name: "DocumentID", referencedColumnName: "documentId" }])
  document: ApPurchaseTbl;

  @ManyToOne(
    () => CfStoreHouseTbl,
    cfStoreHouseTbl => cfStoreHouseTbl.apPurchaseDetailTbls,
    { onUpdate: "CASCADE" }
  )
  @JoinColumn([{ name: "StoreHouseID", referencedColumnName: "storeHouseId" }])
  storeHouse: CfStoreHouseTbl;
}
