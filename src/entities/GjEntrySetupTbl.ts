import { Column, Entity, Index } from "typeorm";

@Index("PK_GJ_EntrySetupTbl", ["userAutoId"], { unique: true })
@Entity("GJ_EntrySetupTbl", { schema: "dbo" })
export class GjEntrySetupTbl {
  @Column("varchar", { primary: true, name: "UserAutoID", length: 50 })
  userAutoId: string;

  @Column("varchar", { name: "SourceID", nullable: true, length: 50 })
  sourceId: string | null;

  @Column("varchar", { name: "MaSo", nullable: true, length: 50 })
  maSo: string | null;

  @Column("varchar", { name: "DebitAccID", length: 200 })
  debitAccId: string;

  @Column("varchar", { name: "CreditAccID", length: 200 })
  creditAccId: string;

  @Column("nvarchar", { name: "Memo", nullable: true, length: 150 })
  memo: string | null;

  @Column("varchar", { name: "OtherAccID", nullable: true, length: 50 })
  otherAccId: string | null;

  @Column("bit", { name: "isObject", default: () => "0" })
  isObject: boolean;

  @Column("bit", { name: "isCost", default: () => "0" })
  isCost: boolean;

  @Column("bit", { name: "isCostCenter", default: () => "0" })
  isCostCenter: boolean;

  @Column("bit", { name: "isItem", default: () => "0" })
  isItem: boolean;

  @Column("bit", { name: "isRefDoc", default: () => "0" })
  isRefDoc: boolean;
}
