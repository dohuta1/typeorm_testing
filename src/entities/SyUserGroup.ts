import { Column, Entity, Index, OneToMany } from "typeorm";
import { SyUserGroupDetail } from "./SyUserGroupDetail";

@Index("IX_SY_UserGroup", ["userGroupName"], { unique: true })
@Index("PK_SY_UserGroup", ["userGroupId"], { unique: true })
@Entity("SY_UserGroup", { schema: "dbo" })
export class SyUserGroup {
  @Column("varchar", { primary: true, name: "UserGroupID", length: 20 })
  userGroupId: string;

  @Column("nvarchar", { name: "UserGroupName", length: 100 })
  userGroupName: string;

  @Column("bit", { name: "IsDisable" })
  isDisable: boolean;

  @OneToMany(
    () => SyUserGroupDetail,
    syUserGroupDetail => syUserGroupDetail.userGroup
  )
  syUserGroupDetails: SyUserGroupDetail[];
}
