import {
  Column,
  Entity,
  Index,
  JoinColumn,
  ManyToOne,
  OneToMany
} from "typeorm";
import { ArInvoiceOrderDetailTbl } from "./ArInvoiceOrderDetailTbl";
import { CfObjectTbl } from "./CfObjectTbl";

@Index("PK_AR_InvoiceOrderTbl", ["documentId"], { unique: true })
@Entity("AR_InvoiceOrderTbl", { schema: "dbo" })
export class ArInvoiceOrderTbl {
  @Column("varchar", { primary: true, name: "DocumentID", length: 30 })
  documentId: string;

  @Column("datetime", { name: "DocumentDate" })
  documentDate: Date;

  @Column("varchar", { name: "EmployeeID", nullable: true, length: 50 })
  employeeId: string | null;

  @Column("nvarchar", { name: "Memo", nullable: true, length: 200 })
  memo: string | null;

  @Column("nvarchar", { name: "Notes", nullable: true, length: 200 })
  notes: string | null;

  @Column("datetime", { name: "DeliverDate", nullable: true })
  deliverDate: Date | null;

  @Column("varchar", { name: "CurrencyID", length: 3, default: () => "'VND'" })
  currencyId: string;

  @Column("float", { name: "RateExchange", precision: 53, default: () => "1" })
  rateExchange: number;

  @Column("decimal", {
    name: "BaseTotal",
    nullable: true,
    precision: 28,
    scale: 0
  })
  baseTotal: number | null;

  @Column("nvarchar", { name: "SearchField", nullable: true, length: 2000 })
  searchField: string | null;

  @Column("nvarchar", { name: "Vehicle", nullable: true, length: 100 })
  vehicle: string | null;

  @Column("bit", { name: "isLock", default: () => "0" })
  isLock: boolean;

  @Column("varchar", { name: "UserCreate", nullable: true, length: 30 })
  userCreate: string | null;

  @Column("varchar", { name: "UserUpdate", nullable: true, length: 30 })
  userUpdate: string | null;

  @Column("datetime", { name: "DateUpdate", nullable: true })
  dateUpdate: Date | null;

  @Column("datetime", { name: "DateCreate", nullable: true })
  dateCreate: Date | null;

  @Column("varchar", { name: "LinkID", nullable: true, length: 50 })
  linkId: string | null;

  @Column("varchar", { name: "ContractID", nullable: true, length: 50 })
  contractId: string | null;

  @Column("decimal", {
    name: "DepositAmount",
    nullable: true,
    precision: 28,
    scale: 0
  })
  depositAmount: number | null;

  @OneToMany(
    () => ArInvoiceOrderDetailTbl,
    arInvoiceOrderDetailTbl => arInvoiceOrderDetailTbl.document
  )
  arInvoiceOrderDetailTbls: ArInvoiceOrderDetailTbl[];

  @ManyToOne(
    () => CfObjectTbl,
    cfObjectTbl => cfObjectTbl.arInvoiceOrderTbls,
    { onUpdate: "CASCADE" }
  )
  @JoinColumn([{ name: "ObjectID", referencedColumnName: "objectId" }])
  object: CfObjectTbl;
}
