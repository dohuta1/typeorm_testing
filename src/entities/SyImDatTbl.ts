import { Column, Entity, Index } from "typeorm";

@Index("PK_SY_ImportDataTbl", ["userAutoId"], { unique: true })
@Entity("SY_ImDatTbl", { schema: "dbo" })
export class SyImDatTbl {
  @Column("varchar", {
    primary: true,
    name: "UserAutoID",
    length: 50,
    default: () => "newid()"
  })
  userAutoId: string;

  @Column("varchar", { name: "Source", length: 150 })
  source: string;

  @Column("nvarchar", { name: "ImportName", length: 100 })
  importName: string;

  @Column("nvarchar", { name: "ImportSQL", length: 1000 })
  importSql: string;

  @Column("nvarchar", { name: "DeleteSQL", nullable: true, length: 1000 })
  deleteSql: string | null;

  @Column("int", { name: "STT", default: () => "50" })
  stt: number;

  @Column("varchar", { name: "TableName", length: 150 })
  tableName: string;

  @Column("nvarchar", { name: "ImportLocalSQL", nullable: true, length: 1000 })
  importLocalSql: string | null;
}
