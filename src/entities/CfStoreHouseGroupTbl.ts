import { Column, Entity, Index } from "typeorm";

@Index("PK_CF_StoreHouseGroupTbl", ["storeHouseGroup"], { unique: true })
@Entity("CF_StoreHouseGroupTbl", { schema: "dbo" })
export class CfStoreHouseGroupTbl {
  @Column("nvarchar", { primary: true, name: "StoreHouseGroup", length: 50 })
  storeHouseGroup: string;

  @Column("varchar", { name: "Notes", nullable: true, length: 50 })
  notes: string | null;
}
