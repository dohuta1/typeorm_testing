import { Column, Entity, Index, OneToMany } from "typeorm";
import { CfContractDetailTbl } from "./CfContractDetailTbl";
import { CfContractThanhToanTbl } from "./CfContractThanhToanTbl";

@Index("PK_CF_ContractTbl", ["contractId"], { unique: true })
@Entity("CF_ContractTbl", { schema: "dbo" })
export class CfContractTbl {
  @Column("varchar", { primary: true, name: "ContractID", length: 50 })
  contractId: string;

  @Column("varchar", { name: "ObjectID", length: 50 })
  objectId: string;

  @Column("nvarchar", { name: "ContractName", length: 100 })
  contractName: string;

  @Column("nvarchar", { name: "ContractName2", nullable: true, length: 100 })
  contractName2: string | null;

  @Column("datetime", { name: "ContractDate", nullable: true })
  contractDate: Date | null;

  @Column("varchar", { name: "CurrencyID", nullable: true, length: 3 })
  currencyId: string | null;

  @Column("decimal", {
    name: "ContractAmount",
    nullable: true,
    precision: 18,
    scale: 2
  })
  contractAmount: number | null;

  @Column("nvarchar", { name: "Notes", nullable: true, length: 150 })
  notes: string | null;

  @Column("varchar", { name: "UserCreate", nullable: true, length: 30 })
  userCreate: string | null;

  @Column("varchar", { name: "UserUpdate", nullable: true, length: 30 })
  userUpdate: string | null;

  @Column("datetime", { name: "DateUpdate", nullable: true })
  dateUpdate: Date | null;

  @Column("datetime", { name: "DateCreate", nullable: true })
  dateCreate: Date | null;

  @OneToMany(
    () => CfContractDetailTbl,
    cfContractDetailTbl => cfContractDetailTbl.contract
  )
  cfContractDetailTbls: CfContractDetailTbl[];

  @OneToMany(
    () => CfContractThanhToanTbl,
    cfContractThanhToanTbl => cfContractThanhToanTbl.contract
  )
  cfContractThanhToanTbls: CfContractThanhToanTbl[];
}
