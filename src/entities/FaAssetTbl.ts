import {
  Column,
  Entity,
  Index,
  JoinColumn,
  ManyToOne,
  OneToMany
} from "typeorm";
import { FaAdjustDetailTbl } from "./FaAdjustDetailTbl";
import { FaAssetAttachTbl } from "./FaAssetAttachTbl";
import { FaAssetDepreTbl } from "./FaAssetDepreTbl";
import { FaAssetLogTbl } from "./FaAssetLogTbl";
import { FaAssetPeriodSetupTbl } from "./FaAssetPeriodSetupTbl";
import { FaCapitalTbl } from "./FaCapitalTbl";
import { CfDepartmentTbl } from "./CfDepartmentTbl";
import { FaMoveDetailTbl } from "./FaMoveDetailTbl";
import { FaRepairDetailTbl } from "./FaRepairDetailTbl";

@Index("PK_FA_AssetTbl", ["assetId"], { unique: true })
@Entity("FA_AssetTbl", { schema: "dbo" })
export class FaAssetTbl {
  @Column("varchar", { primary: true, name: "AssetID", length: 50 })
  assetId: string;

  @Column("nvarchar", { name: "AssetName", nullable: true, length: 200 })
  assetName: string | null;

  @Column("nvarchar", { name: "AssetName2", nullable: true, length: 200 })
  assetName2: string | null;

  @Column("nvarchar", { name: "Unit", nullable: true, length: 50 })
  unit: string | null;

  @Column("varchar", { name: "AssetTypeID", length: 50 })
  assetTypeId: string;

  @Column("varchar", { name: "CardNo", nullable: true, length: 50 })
  cardNo: string | null;

  @Column("varchar", { name: "AssetAccID", length: 30 })
  assetAccId: string;

  @Column("varchar", { name: "DepreAccID", length: 30 })
  depreAccId: string;

  @Column("varchar", { name: "ExpenseAccID", length: 50 })
  expenseAccId: string;

  @Column("datetime", { name: "UsingDate" })
  usingDate: Date;

  @Column("datetime", { name: "DepreDate" })
  depreDate: Date;

  @Column("decimal", {
    name: "YearUse",
    nullable: true,
    precision: 18,
    scale: 2,
    default: () => "(3)"
  })
  yearUse: number | null;

  @Column("decimal", {
    name: "DeprePercent",
    nullable: true,
    precision: 18,
    scale: 4
  })
  deprePercent: number | null;

  @Column("datetime", { name: "StopDate", nullable: true })
  stopDate: Date | null;

  @Column("float", {
    name: "SourceAmount",
    precision: 53,
    default: () => "(0)"
  })
  sourceAmount: number;

  @Column("varchar", { name: "CurrencyID", length: 3, default: () => "'VND'" })
  currencyId: string;

  @Column("float", {
    name: "RateExchange",
    precision: 53,
    default: () => "(1)"
  })
  rateExchange: number;

  @Column("float", { name: "Amount", precision: 53, default: () => "(0)" })
  amount: number;

  @Column("float", { name: "OpenDepre", nullable: true, precision: 53 })
  openDepre: number | null;

  @Column("nvarchar", { name: "Status", nullable: true, length: 100 })
  status: string | null;

  @Column("varchar", { name: "CostCenterID", nullable: true, length: 50 })
  costCenterId: string | null;

  @Column("varchar", { name: "CostID", nullable: true, length: 50 })
  costId: string | null;

  @Column("nvarchar", { name: "Manufacturer", nullable: true, length: 100 })
  manufacturer: string | null;

  @Column("nvarchar", { name: "BuyFrom", nullable: true, length: 150 })
  buyFrom: string | null;

  @Column("nvarchar", { name: "WarrantyAddress", nullable: true, length: 100 })
  warrantyAddress: string | null;

  @Column("nvarchar", { name: "Notes", nullable: true, length: 100 })
  notes: string | null;

  @Column("varchar", { name: "RefDoc", nullable: true, length: 50 })
  refDoc: string | null;

  @Column("varchar", { name: "RefSourceID", nullable: true, length: 3 })
  refSourceId: string | null;

  @Column("datetime", { name: "DateCreate", nullable: true })
  dateCreate: Date | null;

  @Column("datetime", { name: "DateUpdate", nullable: true })
  dateUpdate: Date | null;

  @Column("varchar", { name: "UserCreate", nullable: true, length: 20 })
  userCreate: string | null;

  @Column("varchar", { name: "UserUpdate", nullable: true, length: 20 })
  userUpdate: string | null;

  @Column("varchar", { name: "ObjectID", nullable: true, length: 50 })
  objectId: string | null;

  @Column("int", { name: "MonthUse", nullable: true })
  monthUse: number | null;

  @Column("varchar", { name: "LinkID", nullable: true, length: 50 })
  linkId: string | null;

  @Column("datetime", { name: "LiquidationDate", nullable: true })
  liquidationDate: Date | null;

  @Column("varchar", { name: "BranchID", length: 50 })
  branchId: string;

  @Column("datetime", { name: "NgayChungTu", nullable: true })
  ngayChungTu: Date | null;

  @Column("decimal", {
    name: "Quantity",
    nullable: true,
    precision: 18,
    scale: 0,
    default: () => "(1)"
  })
  quantity: number | null;

  @OneToMany(
    () => FaAdjustDetailTbl,
    faAdjustDetailTbl => faAdjustDetailTbl.asset
  )
  faAdjustDetailTbls: FaAdjustDetailTbl[];

  @OneToMany(
    () => FaAssetAttachTbl,
    faAssetAttachTbl => faAssetAttachTbl.asset
  )
  faAssetAttachTbls: FaAssetAttachTbl[];

  @OneToMany(
    () => FaAssetDepreTbl,
    faAssetDepreTbl => faAssetDepreTbl.asset
  )
  faAssetDepreTbls: FaAssetDepreTbl[];

  @OneToMany(
    () => FaAssetLogTbl,
    faAssetLogTbl => faAssetLogTbl.asset
  )
  faAssetLogTbls: FaAssetLogTbl[];

  @OneToMany(
    () => FaAssetPeriodSetupTbl,
    faAssetPeriodSetupTbl => faAssetPeriodSetupTbl.asset
  )
  faAssetPeriodSetupTbls: FaAssetPeriodSetupTbl[];

  @ManyToOne(
    () => FaCapitalTbl,
    faCapitalTbl => faCapitalTbl.faAssetTbls,
    { onUpdate: "CASCADE" }
  )
  @JoinColumn([{ name: "CapitalID", referencedColumnName: "capitalId" }])
  capital: FaCapitalTbl;

  @ManyToOne(
    () => CfDepartmentTbl,
    cfDepartmentTbl => cfDepartmentTbl.faAssetTbls,
    { onUpdate: "CASCADE" }
  )
  @JoinColumn([{ name: "DepartmentID", referencedColumnName: "departmentId" }])
  department: CfDepartmentTbl;

  @OneToMany(
    () => FaMoveDetailTbl,
    faMoveDetailTbl => faMoveDetailTbl.asset
  )
  faMoveDetailTbls: FaMoveDetailTbl[];

  @OneToMany(
    () => FaRepairDetailTbl,
    faRepairDetailTbl => faRepairDetailTbl.asset
  )
  faRepairDetailTbls: FaRepairDetailTbl[];
}
