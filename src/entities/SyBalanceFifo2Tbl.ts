import { Column, Entity, Index, JoinColumn, ManyToOne } from "typeorm";
import { CfStoreHouseTbl } from "./CfStoreHouseTbl";
import { CfItemTbl } from "./CfItemTbl";

@Index("IX_SY_BalanceFIFO2Tbl_DocumentDate", ["documentDate"], {})
@Index("PK_SY_BalanceFIFO2Tbl", ["userAutoId"], { unique: true })
@Entity("SY_BalanceFIFO2Tbl", { schema: "dbo" })
export class SyBalanceFifo2Tbl {
  @Column("varchar", { primary: true, name: "UserAutoID", length: 50 })
  userAutoId: string;

  @Column("varchar", { name: "Lot", nullable: true, length: 50 })
  lot: string | null;

  @Column("varchar", { name: "InvAccID", length: 50 })
  invAccId: string;

  @Column("decimal", { name: "Quantity", precision: 28, scale: 8 })
  quantity: number;

  @Column("decimal", {
    name: "Quantity2",
    nullable: true,
    precision: 28,
    scale: 8
  })
  quantity2: number | null;

  @Column("nvarchar", { name: "Property", nullable: true, length: 50 })
  property: string | null;

  @Column("nvarchar", { name: "Property2", nullable: true, length: 50 })
  property2: string | null;

  @Column("decimal", {
    name: "UnitPrice",
    nullable: true,
    precision: 28,
    scale: 8
  })
  unitPrice: number | null;

  @Column("decimal", {
    name: "Amount",
    nullable: true,
    precision: 28,
    scale: 0
  })
  amount: number | null;

  @Column("varchar", { name: "DocumentID", nullable: true, length: 50 })
  documentId: string | null;

  @Column("datetime", { name: "DocumentDate", nullable: true })
  documentDate: Date | null;

  @Column("decimal", {
    name: "QuantityOgrin",
    nullable: true,
    precision: 28,
    scale: 8
  })
  quantityOgrin: number | null;

  @Column("varchar", { name: "OutputID", nullable: true, length: 50 })
  outputId: string | null;

  @Column("varchar", { name: "InputID", nullable: true, length: 50 })
  inputId: string | null;

  @ManyToOne(
    () => CfStoreHouseTbl,
    cfStoreHouseTbl => cfStoreHouseTbl.syBalanceFifo2Tbls,
    { onUpdate: "CASCADE" }
  )
  @JoinColumn([{ name: "StorehouseID", referencedColumnName: "storeHouseId" }])
  storehouse: CfStoreHouseTbl;

  @ManyToOne(
    () => CfItemTbl,
    cfItemTbl => cfItemTbl.syBalanceFifo2Tbls,
    { onUpdate: "CASCADE" }
  )
  @JoinColumn([{ name: "ItemID", referencedColumnName: "itemId" }])
  item: CfItemTbl;
}
