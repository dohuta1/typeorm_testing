import { Column, Entity, Index } from "typeorm";

@Index("PK_SY_link", ["linkCode"], { unique: true })
@Entity("SY_Link", { schema: "dbo" })
export class SyLink {
  @Column("varchar", { primary: true, name: "LinkCode", length: 50 })
  linkCode: string;

  @Column("varchar", { name: "ServerName", nullable: true, length: 50 })
  serverName: string | null;

  @Column("varchar", { name: "DatabaseName", nullable: true, length: 50 })
  databaseName: string | null;

  @Column("varchar", { name: "UserName", nullable: true, length: 50 })
  userName: string | null;

  @Column("varchar", { name: "Password", nullable: true, length: 50 })
  password: string | null;

  @Column("varchar", { name: "LinkType", nullable: true, length: 50 })
  linkType: string | null;
}
