import { Column, Entity, Index, JoinColumn, ManyToOne } from "typeorm";
import { ArItemGroupTbl } from "./ArItemGroupTbl";
import { CfObjectTbl } from "./CfObjectTbl";

@Index("PK_AR_ItemGroupEmployeeTbl", ["userAutoId"], { unique: true })
@Entity("AR_ItemGroupEmployeeTbl", { schema: "dbo" })
export class ArItemGroupEmployeeTbl {
  @Column("varchar", { primary: true, name: "UserAutoID", length: 50 })
  userAutoId: string;

  @Column("nvarchar", { name: "Note", nullable: true, length: 50 })
  note: string | null;

  @ManyToOne(
    () => ArItemGroupTbl,
    arItemGroupTbl => arItemGroupTbl.arItemGroupEmployeeTbls,
    { onDelete: "CASCADE", onUpdate: "CASCADE" }
  )
  @JoinColumn([
    { name: "ItemGroupCode", referencedColumnName: "itemGroupCode" }
  ])
  itemGroupCode: ArItemGroupTbl;

  @ManyToOne(
    () => CfObjectTbl,
    cfObjectTbl => cfObjectTbl.arItemGroupEmployeeTbls,
    { onUpdate: "CASCADE" }
  )
  @JoinColumn([{ name: "EmployeeID", referencedColumnName: "objectId" }])
  employee: CfObjectTbl;
}
