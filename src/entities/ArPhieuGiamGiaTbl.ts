import { Column, Entity, Index, OneToMany } from "typeorm";
import { ArPhieuGiamGiaDetailTbl } from "./ArPhieuGiamGiaDetailTbl";

@Index("PK_AR_PhieuGiamGiaTbl", ["documentId"], { unique: true })
@Entity("AR_PhieuGiamGiaTbl", { schema: "dbo" })
export class ArPhieuGiamGiaTbl {
  @Column("varchar", { primary: true, name: "DocumentID", length: 50 })
  documentId: string;

  @Column("datetime", { name: "DocumentDate" })
  documentDate: Date;

  @Column("nvarchar", { name: "Memo", nullable: true, length: 200 })
  memo: string | null;

  @Column("nvarchar", { name: "Notes", nullable: true, length: 200 })
  notes: string | null;

  @Column("varchar", { name: "KyTuDau", nullable: true, length: 50 })
  kyTuDau: string | null;

  @Column("varchar", { name: "KyTuCuoi", nullable: true, length: 50 })
  kyTuCuoi: string | null;

  @Column("decimal", { name: "TuSo", nullable: true, precision: 28, scale: 0 })
  tuSo: number | null;

  @Column("decimal", { name: "DenSo", nullable: true, precision: 28, scale: 0 })
  denSo: number | null;

  @Column("decimal", {
    name: "ChieuDai",
    nullable: true,
    precision: 28,
    scale: 0,
    default: () => "(4)"
  })
  chieuDai: number | null;

  @Column("decimal", {
    name: "TriGiaPhieu",
    nullable: true,
    precision: 28,
    scale: 0,
    default: () => "(50000)"
  })
  triGiaPhieu: number | null;

  @Column("nvarchar", { name: "BangChu", nullable: true, length: 500 })
  bangChu: string | null;

  @Column("datetime", { name: "NgayHetHan", nullable: true })
  ngayHetHan: Date | null;

  @Column("decimal", {
    name: "TongGiaTri",
    nullable: true,
    precision: 28,
    scale: 0
  })
  tongGiaTri: number | null;

  @Column("bit", { name: "isLock", default: () => "(0)" })
  isLock: boolean;

  @Column("varchar", { name: "UserCreate", nullable: true, length: 30 })
  userCreate: string | null;

  @Column("varchar", { name: "UserUpdate", nullable: true, length: 30 })
  userUpdate: string | null;

  @Column("datetime", { name: "DateUpdate", nullable: true })
  dateUpdate: Date | null;

  @Column("datetime", { name: "DateCreate", nullable: true })
  dateCreate: Date | null;

  @Column("varchar", { name: "ItemID", nullable: true, length: 50 })
  itemId: string | null;

  @OneToMany(
    () => ArPhieuGiamGiaDetailTbl,
    arPhieuGiamGiaDetailTbl => arPhieuGiamGiaDetailTbl.document
  )
  arPhieuGiamGiaDetailTbls: ArPhieuGiamGiaDetailTbl[];
}
