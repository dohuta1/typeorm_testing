import { Column, Entity, Index } from "typeorm";

@Index("PK_GJ_SM11Tbl", ["codeId"], { unique: true })
@Entity("GJ_SM11Tbl", { schema: "dbo" })
export class GjSm11Tbl {
  @Column("varchar", { primary: true, name: "CodeID", length: 50 })
  codeId: string;

  @Column("varchar", { name: "CodeID2", nullable: true, length: 50 })
  codeId2: string | null;

  @Column("varchar", { name: "GroupID", length: 13 })
  groupId: string;

  @Column("nvarchar", { name: "CodeName", nullable: true, length: 200 })
  codeName: string | null;

  @Column("nvarchar", { name: "CodeName2", nullable: true, length: 200 })
  codeName2: string | null;

  @Column("varchar", { name: "AccountID", nullable: true, length: 50 })
  accountId: string | null;

  @Column("varchar", { name: "AccountID2", nullable: true, length: 50 })
  accountId2: string | null;

  @Column("bit", { name: "PrintTF", nullable: true })
  printTf: boolean | null;

  @Column("bit", { name: "isBold", nullable: true })
  isBold: boolean | null;

  @Column("varchar", { name: "AmtType", nullable: true, length: 50 })
  amtType: string | null;

  @Column("bit", { name: "Negative", nullable: true })
  negative: boolean | null;

  @Column("int", { name: "Type", nullable: true })
  type: number | null;

  @Column("decimal", {
    name: "Amount",
    nullable: true,
    precision: 28,
    scale: 0
  })
  amount: number | null;

  @Column("decimal", {
    name: "Amount2",
    nullable: true,
    precision: 28,
    scale: 0
  })
  amount2: number | null;

  @Column("varchar", { name: "Period", nullable: true, length: 50 })
  period: string | null;
}
