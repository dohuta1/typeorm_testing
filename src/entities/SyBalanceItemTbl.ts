import { Column, Entity, Index, JoinColumn, ManyToOne } from "typeorm";
import { CfItemTbl } from "./CfItemTbl";
import { CfCostCenterTbl } from "./CfCostCenterTbl";
import { CfStoreHouseTbl } from "./CfStoreHouseTbl";

@Index("IX_SY_BalanceItemTbl", ["periodId"], {})
@Index("PK_SY_BalanceItemTbl", ["userAutoId"], { unique: true })
@Entity("SY_BalanceItemTbl", { schema: "dbo" })
export class SyBalanceItemTbl {
  @Column("varchar", { primary: true, name: "UserAutoID", length: 40 })
  userAutoId: string;

  @Column("varchar", { name: "Lot", nullable: true, length: 50 })
  lot: string | null;

  @Column("varchar", { name: "InvAccID", length: 50 })
  invAccId: string;

  @Column("decimal", { name: "Quantity", precision: 28, scale: 4 })
  quantity: number;

  @Column("decimal", {
    name: "Quantity2",
    nullable: true,
    precision: 28,
    scale: 4
  })
  quantity2: number | null;

  @Column("nvarchar", { name: "Property", nullable: true, length: 50 })
  property: string | null;

  @Column("nvarchar", { name: "Property2", nullable: true, length: 50 })
  property2: string | null;

  @Column("decimal", {
    name: "UnitPrice",
    precision: 28,
    scale: 4,
    default: () => "0"
  })
  unitPrice: number;

  @Column("decimal", {
    name: "Amount",
    precision: 28,
    scale: 0,
    default: () => "0"
  })
  amount: number;

  @Column("varchar", { name: "PeriodID", length: 50, default: () => "201005" })
  periodId: string;

  @Column("bit", { name: "IsBalance", default: () => "0" })
  isBalance: boolean;

  @Column("varchar", { name: "DocumentID", nullable: true, length: 50 })
  documentId: string | null;

  @Column("datetime", { name: "DocumentDate", nullable: true })
  documentDate: Date | null;

  @Column("datetime", { name: "ExpireDate", nullable: true })
  expireDate: Date | null;

  @Column("varchar", { name: "BranchID", nullable: true, length: 50 })
  branchId: string | null;

  @ManyToOne(
    () => CfItemTbl,
    cfItemTbl => cfItemTbl.syBalanceItemTbls,
    { onUpdate: "CASCADE" }
  )
  @JoinColumn([{ name: "ItemID", referencedColumnName: "itemId" }])
  item: CfItemTbl;

  @ManyToOne(
    () => CfCostCenterTbl,
    cfCostCenterTbl => cfCostCenterTbl.syBalanceItemTbls,
    { onUpdate: "CASCADE" }
  )
  @JoinColumn([{ name: "CostCenterID", referencedColumnName: "costCenterId" }])
  costCenter: CfCostCenterTbl;

  @ManyToOne(
    () => CfStoreHouseTbl,
    cfStoreHouseTbl => cfStoreHouseTbl.syBalanceItemTbls,
    { onUpdate: "CASCADE" }
  )
  @JoinColumn([{ name: "StorehouseID", referencedColumnName: "storeHouseId" }])
  storehouse: CfStoreHouseTbl;
}
