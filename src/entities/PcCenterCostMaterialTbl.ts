import { Column, Entity, Index, JoinColumn, ManyToOne } from "typeorm";
import { PcCenterCostTbl } from "./PcCenterCostTbl";

@Index("PK_PC_CenterCostMaterialTbl", ["userAutoId"], { unique: true })
@Entity("PC_CenterCostMaterialTbl", { schema: "dbo" })
export class PcCenterCostMaterialTbl {
  @Column("varchar", { primary: true, name: "UserAutoID", length: 50 })
  userAutoId: string;

  @Column("varchar", { name: "AccountID", length: 50 })
  accountId: string;

  @Column("varchar", { name: "CostCenterID", nullable: true, length: 50 })
  costCenterId: string | null;

  @Column("varchar", { name: "CostID", nullable: true, length: 50 })
  costId: string | null;

  @Column("varchar", { name: "MaterialID", length: 50 })
  materialId: string;

  @Column("decimal", {
    name: "Quantity",
    nullable: true,
    precision: 28,
    scale: 4
  })
  quantity: number | null;

  @Column("decimal", {
    name: "Amount",
    nullable: true,
    precision: 28,
    scale: 0,
    default: () => "0"
  })
  amount: number | null;

  @Column("decimal", {
    name: "AmountAllocate",
    nullable: true,
    precision: 28,
    scale: 0,
    default: () => "0"
  })
  amountAllocate: number | null;

  @Column("nvarchar", { name: "Notes", nullable: true, length: 50 })
  notes: string | null;

  @ManyToOne(
    () => PcCenterCostTbl,
    pcCenterCostTbl => pcCenterCostTbl.pcCenterCostMaterialTbls,
    { onDelete: "CASCADE", onUpdate: "CASCADE" }
  )
  @JoinColumn([{ name: "PriceID", referencedColumnName: "priceId" }])
  price: PcCenterCostTbl;
}
