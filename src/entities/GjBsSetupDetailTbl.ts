import { Column, Entity, Index, JoinColumn, ManyToOne } from "typeorm";
import { GjBsSetupTbl } from "./GjBsSetupTbl";

@Index("PK_GJ_BalanceSheetSetupDetailTbl", ["userAutoId"], { unique: true })
@Entity("GJ_BSSetupDetailTbl", { schema: "dbo" })
export class GjBsSetupDetailTbl {
  @Column("varchar", { primary: true, name: "UserAutoID", length: 50 })
  userAutoId: string;

  @Column("varchar", { name: "AccountID", nullable: true, length: 50 })
  accountId: string | null;

  @Column("bit", { name: "IsDebitBal", default: () => "0" })
  isDebitBal: boolean;

  @Column("bit", { name: "IsObject", default: () => "0" })
  isObject: boolean;

  @Column("varchar", { name: "SumID", nullable: true, length: 50 })
  sumId: string | null;

  @Column("bit", { name: "SoDuong", default: () => "0" })
  soDuong: boolean;

  @Column("bit", { name: "ReverseSign", default: () => "0" })
  reverseSign: boolean;

  @Column("bit", { name: "SubAccAmount", nullable: true, default: () => "(0)" })
  subAccAmount: boolean | null;

  @ManyToOne(
    () => GjBsSetupTbl,
    gjBsSetupTbl => gjBsSetupTbl.gjBsSetupDetailTbls,
    { onDelete: "CASCADE", onUpdate: "CASCADE" }
  )
  @JoinColumn([{ name: "SetupID", referencedColumnName: "setupId" }])
  setup: GjBsSetupTbl;
}
