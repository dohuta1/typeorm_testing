import { Column, Entity, Index } from "typeorm";

@Index("PK_StockInObject", ["stockInObjectId"], { unique: true })
@Entity("IV_StockInObjectTbl", { schema: "dbo" })
export class IvStockInObjectTbl {
  @Column("varchar", {
    primary: true,
    name: "StockInObjectID",
    length: 50,
    default: () => "newid()"
  })
  stockInObjectId: string;

  @Column("varchar", { name: "ObjectID", length: 50 })
  objectId: string;

  @Column("datetime", { name: "CreateDate", default: () => "getdate()" })
  createDate: Date;

  @Column("varchar", { name: "ItemID", length: 50 })
  itemId: string;

  @Column("int", { name: "Quantity" })
  quantity: number;

  @Column("varchar", { name: "UserCreate", nullable: true, length: 30 })
  userCreate: string | null;
}
