import { Column, Entity, Index } from "typeorm";

@Index("PK_SY_FrmCfg", ["userAutoId"], { unique: true })
@Entity("SY_FrmCfg", { schema: "dbo" })
export class SyFrmCfg {
  @Column("varchar", { primary: true, name: "UserAutoID", length: 50 })
  userAutoId: string;

  @Column("varchar", { name: "FID", nullable: true, length: 250 })
  fid: string | null;

  @Column("varchar", { name: "KeyID", length: 50 })
  keyId: string;

  @Column("varchar", { name: "SubID", nullable: true, length: 50 })
  subId: string | null;

  @Column("nvarchar", { name: "KeyValue", nullable: true })
  keyValue: string | null;

  @Column("nvarchar", { name: "SubValue", nullable: true })
  subValue: string | null;

  @Column("datetime", { name: "VDate", nullable: true })
  vDate: Date | null;
}
