import { Column, Entity, Index, JoinColumn, ManyToOne } from "typeorm";
import { CfItemTbl } from "./CfItemTbl";
import { ApPurchaseTbl } from "./ApPurchaseTbl";

@Index("PK_AP_PurchaseAllocateTbl", ["userAutoId"], { unique: true })
@Entity("AP_PurchaseAllocateTbl", { schema: "dbo" })
export class ApPurchaseAllocateTbl {
  @Column("varchar", { primary: true, name: "UserAutoID", length: 40 })
  userAutoId: string;

  @Column("varchar", { name: "RefDoc", length: 50 })
  refDoc: string;

  @Column("varchar", { name: "SourceID", length: 5 })
  sourceId: string;

  @Column("varchar", { name: "AccountID", length: 50 })
  accountId: string;

  @Column("decimal", { name: "Amount", precision: 28, scale: 0 })
  amount: number;

  @Column("varchar", { name: "AllocateKind", length: 5, default: () => "'ST'" })
  allocateKind: string;

  @ManyToOne(
    () => CfItemTbl,
    cfItemTbl => cfItemTbl.apPurchaseAllocateTbls,
    { onUpdate: "CASCADE" }
  )
  @JoinColumn([{ name: "ItemID", referencedColumnName: "itemId" }])
  item: CfItemTbl;

  @ManyToOne(
    () => ApPurchaseTbl,
    apPurchaseTbl => apPurchaseTbl.apPurchaseAllocateTbls,
    { onDelete: "CASCADE", onUpdate: "CASCADE" }
  )
  @JoinColumn([{ name: "DocumentID", referencedColumnName: "documentId" }])
  document: ApPurchaseTbl;
}
