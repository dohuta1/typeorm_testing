import { Column, Entity, Index } from "typeorm";

@Index("PK_CF_CurrencyRateTbl", ["userAutoId"], { unique: true })
@Entity("CF_CurrencyRateTbl", { schema: "dbo" })
export class CfCurrencyRateTbl {
  @Column("varchar", { primary: true, name: "UserAutoID", length: 50 })
  userAutoId: string;

  @Column("varchar", { name: "CurrencyID", length: 50 })
  currencyId: string;

  @Column("datetime", { name: "RateDate", default: () => "getdate()" })
  rateDate: Date;

  @Column("float", { name: "RateExchange", precision: 53, default: () => "1" })
  rateExchange: number;

  @Column("varchar", { name: "UserCreate", nullable: true, length: 30 })
  userCreate: string | null;

  @Column("varchar", { name: "UserUpdate", nullable: true, length: 30 })
  userUpdate: string | null;

  @Column("datetime", { name: "DateUpdate", nullable: true })
  dateUpdate: Date | null;

  @Column("datetime", { name: "DateCreate", nullable: true })
  dateCreate: Date | null;
}
