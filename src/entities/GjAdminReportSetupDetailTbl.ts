import { Column, Entity, Index, JoinColumn, ManyToOne } from "typeorm";
import { GjAdminReportSetupTbl } from "./GjAdminReportSetupTbl";

@Index("PK_GJ_AdminReportSetupDetailTbl", ["userAutoId"], { unique: true })
@Entity("GJ_AdminReportSetupDetailTbl", { schema: "dbo" })
export class GjAdminReportSetupDetailTbl {
  @Column("varchar", { primary: true, name: "UserAutoID", length: 50 })
  userAutoId: string;

  @Column("varchar", { name: "DebitAccID", nullable: true, length: 50 })
  debitAccId: string | null;

  @Column("varchar", { name: "CreditAccID", nullable: true, length: 50 })
  creditAccId: string | null;

  @Column("varchar", { name: "SumID", nullable: true, length: 50 })
  sumId: string | null;

  @Column("bit", { name: "ReverseSign", nullable: true, default: () => "(0)" })
  reverseSign: boolean | null;

  @Column("varchar", { name: "CostID", nullable: true, length: 50 })
  costId: string | null;

  @Column("varchar", { name: "CostCenterID", nullable: true, length: 50 })
  costCenterId: string | null;

  @Column("bit", { name: "IsAmount2", nullable: true, default: () => "(0)" })
  isAmount2: boolean | null;

  @Column("varchar", { name: "ObjectID", nullable: true, length: 50 })
  objectId: string | null;

  @Column("bit", { name: "IsObjectCT", nullable: true, default: () => "(0)" })
  isObjectCt: boolean | null;

  @Column("bit", { name: "IsCostCT", nullable: true, default: () => "(0)" })
  isCostCt: boolean | null;

  @Column("bit", {
    name: "IsCostCenterCT",
    nullable: true,
    default: () => "(0)"
  })
  isCostCenterCt: boolean | null;

  @Column("bit", { name: "IsAccountCT", nullable: true, default: () => "(0)" })
  isAccountCt: boolean | null;

  @Column("int", { name: "STT", nullable: true, default: () => "(0)" })
  stt: number | null;

  @ManyToOne(
    () => GjAdminReportSetupTbl,
    gjAdminReportSetupTbl => gjAdminReportSetupTbl.gjAdminReportSetupDetailTbls,
    { onDelete: "CASCADE", onUpdate: "CASCADE" }
  )
  @JoinColumn([{ name: "SetupID", referencedColumnName: "setupId" }])
  setup: GjAdminReportSetupTbl;
}
