import { Column, Entity, Index, OneToMany } from "typeorm";
import { EqLiquidateDetailTbl } from "./EqLiquidateDetailTbl";

@Index("PK_EQ_LiquidateTbl", ["documentId"], { unique: true })
@Entity("EQ_LiquidateTbl", { schema: "dbo" })
export class EqLiquidateTbl {
  @Column("varchar", { primary: true, name: "DocumentID", length: 30 })
  documentId: string;

  @Column("datetime", { name: "DocumentDate" })
  documentDate: Date;

  @Column("varchar", { name: "ObjectID", nullable: true, length: 50 })
  objectId: string | null;

  @Column("varchar", { name: "EmployeeID", nullable: true, length: 50 })
  employeeId: string | null;

  @Column("nvarchar", { name: "Memo", nullable: true, length: 150 })
  memo: string | null;

  @Column("nvarchar", { name: "Notes", nullable: true, length: 150 })
  notes: string | null;

  @Column("decimal", {
    name: "BaseTotal",
    nullable: true,
    precision: 18,
    scale: 0
  })
  baseTotal: number | null;

  @Column("bit", { name: "isLock" })
  isLock: boolean;

  @Column("varchar", { name: "RefDoc", nullable: true, length: 50 })
  refDoc: string | null;

  @Column("varchar", { name: "UserCreate", nullable: true, length: 30 })
  userCreate: string | null;

  @Column("varchar", { name: "UserUpdate", nullable: true, length: 30 })
  userUpdate: string | null;

  @Column("datetime", { name: "DateUpdate", nullable: true })
  dateUpdate: Date | null;

  @Column("datetime", { name: "DateCreate", nullable: true })
  dateCreate: Date | null;

  @OneToMany(
    () => EqLiquidateDetailTbl,
    eqLiquidateDetailTbl => eqLiquidateDetailTbl.document
  )
  eqLiquidateDetailTbls: EqLiquidateDetailTbl[];
}
