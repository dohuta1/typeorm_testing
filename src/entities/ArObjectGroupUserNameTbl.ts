import { Column, Entity, Index, JoinColumn, ManyToOne } from "typeorm";
import { ArObjectGroupTbl } from "./ArObjectGroupTbl";

@Index("PK_AR_ObjectGroupUserNameTbl", ["userAutoId"], { unique: true })
@Entity("AR_ObjectGroupUserNameTbl", { schema: "dbo" })
export class ArObjectGroupUserNameTbl {
  @Column("varchar", { primary: true, name: "UserAutoID", length: 50 })
  userAutoId: string;

  @Column("varchar", { name: "UserName", length: 50 })
  userName: string;

  @Column("nvarchar", { name: "Note", nullable: true, length: 50 })
  note: string | null;

  @ManyToOne(
    () => ArObjectGroupTbl,
    arObjectGroupTbl => arObjectGroupTbl.arObjectGroupUserNameTbls,
    { onDelete: "CASCADE", onUpdate: "CASCADE" }
  )
  @JoinColumn([
    { name: "ObjectGroupCode", referencedColumnName: "objectGroupCode" }
  ])
  objectGroupCode: ArObjectGroupTbl;
}
