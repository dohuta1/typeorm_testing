import { Column, Entity, Index, JoinColumn, ManyToOne } from "typeorm";
import { CfItemTbl } from "./CfItemTbl";
import { ArInvoiceOrderTbl } from "./ArInvoiceOrderTbl";

@Index("PK_AR_InvoiceOrderDetailTbl", ["userAutoId"], { unique: true })
@Entity("AR_InvoiceOrderDetailTbl", { schema: "dbo" })
export class ArInvoiceOrderDetailTbl {
  @Column("varchar", { primary: true, name: "UserAutoID", length: 40 })
  userAutoId: string;

  @Column("decimal", {
    name: "Quantity",
    nullable: true,
    precision: 28,
    scale: 2
  })
  quantity: number | null;

  @Column("decimal", {
    name: "UnitPrice",
    nullable: true,
    precision: 28,
    scale: 4
  })
  unitPrice: number | null;

  @Column("decimal", {
    name: "SourceAmount",
    nullable: true,
    precision: 18,
    scale: 2
  })
  sourceAmount: number | null;

  @Column("decimal", {
    name: "Amount",
    nullable: true,
    precision: 28,
    scale: 0
  })
  amount: number | null;

  @Column("nvarchar", { name: "Notes", nullable: true, length: 100 })
  notes: string | null;

  @Column("decimal", {
    name: "Quantity2",
    nullable: true,
    precision: 28,
    scale: 4
  })
  quantity2: number | null;

  @Column("nvarchar", { name: "Property", nullable: true, length: 50 })
  property: string | null;

  @Column("nvarchar", { name: "Property2", nullable: true, length: 50 })
  property2: string | null;

  @Column("varchar", { name: "ParentID", nullable: true, length: 50 })
  parentId: string | null;

  @Column("varchar", { name: "OrderNo", nullable: true, length: 50 })
  orderNo: string | null;

  @ManyToOne(
    () => CfItemTbl,
    cfItemTbl => cfItemTbl.arInvoiceOrderDetailTbls,
    { onUpdate: "CASCADE" }
  )
  @JoinColumn([{ name: "ItemID", referencedColumnName: "itemId" }])
  item: CfItemTbl;

  @ManyToOne(
    () => ArInvoiceOrderTbl,
    arInvoiceOrderTbl => arInvoiceOrderTbl.arInvoiceOrderDetailTbls,
    { onDelete: "CASCADE", onUpdate: "CASCADE" }
  )
  @JoinColumn([{ name: "DocumentID", referencedColumnName: "documentId" }])
  document: ArInvoiceOrderTbl;
}
