import { Column, Entity, Index } from "typeorm";

@Index("PK__AP_OrderStatusTbl", ["statusId"], { unique: true })
@Entity("AP_OrderStatusTbl", { schema: "dbo" })
export class ApOrderStatusTbl {
  @Column("int", { primary: true, name: "StatusID" })
  statusId: number;

  @Column("nvarchar", { name: "StatusName", length: 50 })
  statusName: string;

  @Column("int", { name: "BackColor", nullable: true })
  backColor: number | null;

  @Column("int", { name: "ForeColor", nullable: true })
  foreColor: number | null;
}
