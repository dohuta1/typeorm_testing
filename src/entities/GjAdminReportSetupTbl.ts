import { Column, Entity, Index, OneToMany } from "typeorm";
import { GjAdminReportSetupDetailTbl } from "./GjAdminReportSetupDetailTbl";

@Index("PK_GJ_AdminReportSetupTbl", ["setupId"], { unique: true })
@Entity("GJ_AdminReportSetupTbl", { schema: "dbo" })
export class GjAdminReportSetupTbl {
  @Column("varchar", { primary: true, name: "SetupID", length: 50 })
  setupId: string;

  @Column("varchar", { name: "GroupID", length: 50, default: () => "'1'" })
  groupId: string;

  @Column("varchar", { name: "CodeID", length: 50 })
  codeId: string;

  @Column("nvarchar", { name: "CodeName", length: 250 })
  codeName: string;

  @Column("nvarchar", { name: "CodeName2", nullable: true, length: 250 })
  codeName2: string | null;

  @Column("int", { name: "Priority", default: () => "(5)" })
  priority: number;

  @Column("bit", { name: "IsBold", default: () => "(0)" })
  isBold: boolean;

  @Column("bit", { name: "IsLeft", default: () => "(0)" })
  isLeft: boolean;

  @Column("int", { name: "Type", default: () => "(0)" })
  type: number;

  @Column("bit", { name: "IsAmount3", default: () => "(1)" })
  isAmount3: boolean;

  @Column("bit", { name: "IsNotPrint", default: () => "(0)" })
  isNotPrint: boolean;

  @OneToMany(
    () => GjAdminReportSetupDetailTbl,
    gjAdminReportSetupDetailTbl => gjAdminReportSetupDetailTbl.setup
  )
  gjAdminReportSetupDetailTbls: GjAdminReportSetupDetailTbl[];
}
