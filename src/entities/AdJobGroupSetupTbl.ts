import { Column, Entity, Index, OneToMany } from "typeorm";
import { AdJobGroupSetupDetailTbl } from "./AdJobGroupSetupDetailTbl";

@Index("PK_AD_JobGroupSetupTbl", ["codeId"], { unique: true })
@Entity("AD_JobGroupSetupTbl", { schema: "dbo" })
export class AdJobGroupSetupTbl {
  @Column("varchar", { primary: true, name: "CodeID", length: 50 })
  codeId: string;

  @Column("varchar", { name: "CostGroupID", length: 50 })
  costGroupId: string;

  @Column("nvarchar", { name: "Memo", nullable: true, length: 150 })
  memo: string | null;

  @Column("nvarchar", { name: "Notes", nullable: true, length: 150 })
  notes: string | null;

  @Column("bit", { name: "IsExpenseAllocate", default: () => "0" })
  isExpenseAllocate: boolean;

  @Column("bit", { name: "isDisable", default: () => "0" })
  isDisable: boolean;

  @Column("varchar", { name: "UserCreate", nullable: true, length: 30 })
  userCreate: string | null;

  @Column("varchar", { name: "UserUpdate", nullable: true, length: 30 })
  userUpdate: string | null;

  @Column("datetime", { name: "DateUpdate", nullable: true })
  dateUpdate: Date | null;

  @Column("datetime", { name: "DateCreate", nullable: true })
  dateCreate: Date | null;

  @OneToMany(
    () => AdJobGroupSetupDetailTbl,
    adJobGroupSetupDetailTbl => adJobGroupSetupDetailTbl.code
  )
  adJobGroupSetupDetailTbls: AdJobGroupSetupDetailTbl[];
}
