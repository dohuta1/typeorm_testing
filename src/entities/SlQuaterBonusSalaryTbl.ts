import { Column, Entity, Index, JoinColumn, ManyToOne } from "typeorm";
import { SlSalaryGroupTbl } from "./SlSalaryGroupTbl";

@Index("PK_SL_QuaterBonusSalaryTbl", ["autoId"], { unique: true })
@Entity("SL_QuaterBonusSalaryTbl", { schema: "dbo" })
export class SlQuaterBonusSalaryTbl {
  @Column("varchar", {
    primary: true,
    name: "AutoID",
    length: 50,
    default: () => "newid()"
  })
  autoId: string;

  @Column("decimal", { name: "FromPercent", precision: 3, scale: 0 })
  fromPercent: number;

  @Column("decimal", { name: "ToPercent", precision: 3, scale: 0 })
  toPercent: number;

  @Column("decimal", { name: "BonusRate", precision: 5, scale: 4 })
  bonusRate: number;

  @Column("datetime", { name: "StartDate" })
  startDate: Date;

  @Column("datetime", { name: "EndDate", nullable: true })
  endDate: Date | null;

  @Column("nvarchar", { name: "Notes", nullable: true, length: 200 })
  notes: string | null;

  @ManyToOne(
    () => SlSalaryGroupTbl,
    slSalaryGroupTbl => slSalaryGroupTbl.slQuaterBonusSalaryTbls,
    { onDelete: "CASCADE", onUpdate: "CASCADE" }
  )
  @JoinColumn([
    { name: "SalaryGroupID", referencedColumnName: "salaryGroupId" }
  ])
  salaryGroup: SlSalaryGroupTbl;
}
