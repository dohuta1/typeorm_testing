import { Column, Entity, Index } from "typeorm";

@Index("PK_AR_InvoiceRequestInfoTbl", ["documentId"], { unique: true })
@Entity("AR_InvoiceRequestInfoTbl", { schema: "dbo" })
export class ArInvoiceRequestInfoTbl {
  @Column("varchar", { primary: true, name: "DocumentID", length: 30 })
  documentId: string;

  @Column("datetime", { name: "DocumentDate" })
  documentDate: Date;

  @Column("varchar", { name: "SoBienNhan", nullable: true, length: 50 })
  soBienNhan: string | null;

  @Column("varchar", { name: "ObjectID", nullable: true, length: 50 })
  objectId: string | null;

  @Column("nvarchar", { name: "ObjectName", length: 250 })
  objectName: string;

  @Column("nvarchar", { name: "Address", length: 250 })
  address: string;

  @Column("nvarchar", { name: "TaxCode", length: 50 })
  taxCode: string;

  @Column("datetime", { name: "NgayGiaoHoaDon", nullable: true })
  ngayGiaoHoaDon: Date | null;

  @Column("nvarchar", { name: "Memo", nullable: true, length: 200 })
  memo: string | null;

  @Column("nvarchar", { name: "Notes", nullable: true, length: 200 })
  notes: string | null;

  @Column("bit", { name: "DaXuat", default: () => "0" })
  daXuat: boolean;

  @Column("bit", { name: "isLock", default: () => "0" })
  isLock: boolean;

  @Column("varchar", { name: "UserCreate", nullable: true, length: 30 })
  userCreate: string | null;

  @Column("varchar", { name: "UserUpdate", nullable: true, length: 30 })
  userUpdate: string | null;

  @Column("datetime", { name: "DateUpdate", nullable: true })
  dateUpdate: Date | null;

  @Column("datetime", { name: "DateCreate", nullable: true })
  dateCreate: Date | null;

  @Column("nvarchar", { name: "TenCongTy", nullable: true, length: 250 })
  tenCongTy: string | null;

  @Column("nvarchar", { name: "DiaChiCongTy", nullable: true, length: 250 })
  diaChiCongTy: string | null;
}
