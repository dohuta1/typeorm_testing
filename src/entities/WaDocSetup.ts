import { Column, Entity, Index } from "typeorm";

@Index("PK_WA_DocSetup", ["docType"], { unique: true })
@Entity("WA_DocSetup", { schema: "dbo" })
export class WaDocSetup {
  @Column("varchar", { primary: true, name: "DocType", length: 20 })
  docType: string;

  @Column("nvarchar", { name: "DocName", length: 100 })
  docName: string;

  @Column("varchar", { name: "PreFix", length: 4 })
  preFix: string;

  @Column("bit", { name: "isAuto", default: () => "(1)" })
  isAuto: boolean;

  @Column("varchar", { name: "AccountID", nullable: true, length: 50 })
  accountId: string | null;

  @Column("int", { name: "STT", nullable: true })
  stt: number | null;

  @Column("varchar", { name: "TableName", nullable: true, length: 150 })
  tableName: string | null;

  @Column("varchar", { name: "TableDetail", nullable: true, length: 150 })
  tableDetail: string | null;

  @Column("varchar", { name: "FormID", nullable: true, length: 150 })
  formId: string | null;

  @Column("nvarchar", { name: "DocGroupName", nullable: true, length: 150 })
  docGroupName: string | null;

  @Column("int", { name: "isAcc", nullable: true, default: () => "(1)" })
  isAcc: number | null;
}
