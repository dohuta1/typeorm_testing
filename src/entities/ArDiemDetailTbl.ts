import { Column, Entity, Index, JoinColumn, ManyToOne } from "typeorm";
import { ArDiemTbl } from "./ArDiemTbl";

@Index("PK_AR_DiemDetailTbl", ["userAutoId"], { unique: true })
@Entity("AR_DiemDetailTbl", { schema: "dbo" })
export class ArDiemDetailTbl {
  @Column("varchar", { primary: true, name: "UserAutoID", length: 40 })
  userAutoId: string;

  @Column("varchar", { name: "ItemID", length: 50 })
  itemId: string;

  @Column("decimal", {
    name: "DiemTichLuy",
    nullable: true,
    precision: 28,
    scale: 2,
    default: () => "(0)"
  })
  diemTichLuy: number | null;

  @Column("nvarchar", { name: "Notes", nullable: true, length: 100 })
  notes: string | null;

  @Column("varchar", { name: "ParentID", nullable: true, length: 50 })
  parentId: string | null;

  @ManyToOne(
    () => ArDiemTbl,
    arDiemTbl => arDiemTbl.arDiemDetailTbls,
    { onDelete: "CASCADE", onUpdate: "CASCADE" }
  )
  @JoinColumn([{ name: "DocumentID", referencedColumnName: "documentId" }])
  document: ArDiemTbl;
}
