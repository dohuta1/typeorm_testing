import { Column, Entity, Index, PrimaryGeneratedColumn } from "typeorm";

@Index("IX_SY_FormatFields", ["fieldName", "formName"], { unique: true })
@Index("PK_SY_FmtFldTbl", ["autoId"], { unique: true })
@Entity("SY_FmtFldTbl", { schema: "dbo" })
export class SyFmtFldTbl {
  @PrimaryGeneratedColumn({
    type: "numeric",
    name: "AutoID",
    // precision: 18,
    // scale: 0
  })
  autoId: number;

  @Column("varchar", { name: "FormatID", nullable: true, length: 2 })
  formatId: string | null;

  @Column("varchar", { name: "FieldName", unique: true, length: 50 })
  fieldName: string;

  @Column("varchar", {
    name: "FormName",
    nullable: true,
    unique: true,
    length: 100
  })
  formName: string | null;

  @Column("nvarchar", { name: "CaptionVN", nullable: true, length: 200 })
  captionVn: string | null;

  @Column("nvarchar", { name: "CaptionEN", nullable: true, length: 200 })
  captionEn: string | null;

  @Column("nvarchar", { name: "CaptionCH", nullable: true, length: 200 })
  captionCh: string | null;

  @Column("varchar", { name: "AlignX", nullable: true, length: 2 })
  alignX: string | null;
}
