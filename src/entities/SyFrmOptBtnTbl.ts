import { Column, Entity, Index } from "typeorm";

@Index("PK_SY_FrmOptBtnTbl", ["userAutoId"], { unique: true })
@Entity("SY_FrmOptBtnTbl", { schema: "dbo" })
export class SyFrmOptBtnTbl {
  @Column("varchar", { primary: true, name: "UserAutoID", length: 40 })
  userAutoId: string;

  @Column("varchar", { name: "FormID", nullable: true, length: 250 })
  formId: string | null;

  @Column("varchar", { name: "KeyID", length: 100 })
  keyId: string;

  @Column("varchar", { name: "GridName", nullable: true, length: 100 })
  gridName: string | null;

  @Column("nvarchar", { name: "Caption", length: 150 })
  caption: string;

  @Column("int", { name: "Width", nullable: true })
  width: number | null;

  @Column("varchar", { name: "Action", nullable: true, length: 100 })
  action: string | null;

  @Column("varchar", { name: "Source", nullable: true, length: 200 })
  source: string | null;

  @Column("nvarchar", { name: "WhereStr", nullable: true, length: 100 })
  whereStr: string | null;

  @Column("varchar", { name: "Para", nullable: true, length: 100 })
  para: string | null;

  @Column("varchar", { name: "RequireFieldArr", nullable: true, length: 100 })
  requireFieldArr: string | null;

  @Column("varchar", { name: "BeforeAction", nullable: true, length: 50 })
  beforeAction: string | null;

  @Column("varchar", { name: "AfterAction", nullable: true, length: 50 })
  afterAction: string | null;

  @Column("varchar", { name: "EnableStatus", nullable: true, length: 50 })
  enableStatus: string | null;

  @Column("varchar", { name: "ReturnMasterField", nullable: true, length: 200 })
  returnMasterField: string | null;

  @Column("varchar", { name: "ReturnDetailField", nullable: true, length: 200 })
  returnDetailField: string | null;

  @Column("varchar", { name: "RefTable", nullable: true, length: 100 })
  refTable: string | null;

  @Column("varchar", { name: "RefCompareField", nullable: true, length: 100 })
  refCompareField: string | null;

  @Column("varchar", { name: "RefDisplayField", nullable: true, length: 100 })
  refDisplayField: string | null;

  @Column("varchar", { name: "RefDisplayField2", nullable: true, length: 100 })
  refDisplayField2: string | null;

  @Column("varchar", { name: "MasterHiddenField", nullable: true, length: 500 })
  masterHiddenField: string | null;

  @Column("varchar", { name: "DetailHiddenField", nullable: true, length: 500 })
  detailHiddenField: string | null;

  @Column("bit", { name: "IsDisable", default: () => "0" })
  isDisable: boolean;

  @Column("bit", { name: "IsMultiSelect", nullable: true, default: () => "0" })
  isMultiSelect: boolean | null;

  @Column("bit", { name: "IsRecordCompare", default: () => "0" })
  isRecordCompare: boolean;

  @Column("varchar", { name: "RefCompareField2", nullable: true, length: 100 })
  refCompareField2: string | null;

  @Column("varchar", { name: "HiddenFieldList", nullable: true, length: 250 })
  hiddenFieldList: string | null;
}
