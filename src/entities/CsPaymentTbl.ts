import {
  Column,
  Entity,
  Index,
  JoinColumn,
  ManyToOne,
  OneToMany,
  OneToOne
} from "typeorm";
import { CsOutputTbl } from "./CsOutputTbl";
import { CsPaymentDetailTbl } from "./CsPaymentDetailTbl";
import { CfObjectTbl } from "./CfObjectTbl";
import { CsVatInvoiceTbl } from "./CsVatInvoiceTbl";

@Index("PK_CS_PaymentTbl", ["documentId"], { unique: true })
@Entity("CS_PaymentTbl", { schema: "dbo" })
export class CsPaymentTbl {
  @Column("varchar", { primary: true, name: "DocumentID", length: 30 })
  documentId: string;

  @Column("datetime", { name: "DocumentDate" })
  documentDate: Date;

  @Column("nvarchar", { name: "Memo", nullable: true, length: 200 })
  memo: string | null;

  @Column("nvarchar", { name: "Notes", nullable: true, length: 200 })
  notes: string | null;

  @Column("nvarchar", { name: "Attach", nullable: true, length: 50 })
  attach: string | null;

  @Column("varchar", { name: "CurrencyID", length: 3, default: () => "'VND'" })
  currencyId: string;

  @Column("float", {
    name: "RateExchange",
    precision: 53,
    default: () => "(1)"
  })
  rateExchange: number;

  @Column("decimal", {
    name: "BaseTotal",
    nullable: true,
    precision: 18,
    scale: 0
  })
  baseTotal: number | null;

  @Column("varchar", { name: "CashAccountID", length: 50 })
  cashAccountId: string;

  @Column("varchar", { name: "AccountID2", nullable: true, length: 500 })
  accountId2: string | null;

  @Column("varchar", { name: "VATAccID", length: 50, default: () => "(1331)" })
  vatAccId: string;

  @Column("bit", { name: "isBank", default: () => "(0)" })
  isBank: boolean;

  @Column("bit", { name: "isLock", default: () => "(0)" })
  isLock: boolean;

  @Column("varchar", { name: "InvoiceNo", nullable: true, length: 50 })
  invoiceNo: string | null;

  @Column("nvarchar", { name: "SearchField", nullable: true, length: 2000 })
  searchField: string | null;

  @Column("int", { name: "Status", default: () => "(1)" })
  status: number;

  @Column("nvarchar", { name: "Receiver", nullable: true, length: 150 })
  receiver: string | null;

  @Column("varchar", { name: "AccountNo", nullable: true, length: 50 })
  accountNo: string | null;

  @Column("varchar", { name: "LinkID", nullable: true, length: 50 })
  linkId: string | null;

  @Column("varchar", { name: "UserCreate", nullable: true, length: 30 })
  userCreate: string | null;

  @Column("varchar", { name: "UserUpdate", nullable: true, length: 30 })
  userUpdate: string | null;

  @Column("datetime", { name: "DateUpdate", nullable: true })
  dateUpdate: Date | null;

  @Column("datetime", { name: "DateCreate", nullable: true })
  dateCreate: Date | null;

  @Column("varchar", { name: "DocBatch", nullable: true, length: 50 })
  docBatch: string | null;

  @Column("nvarchar", { name: "ObjectName", nullable: true, length: 250 })
  objectName: string | null;

  @Column("nvarchar", { name: "Address", nullable: true, length: 250 })
  address: string | null;

  @Column("datetime", { name: "AttachDate", nullable: true })
  attachDate: Date | null;

  @Column("varchar", { name: "BranchID", length: 50 })
  branchId: string;

  @Column("nvarchar", { name: "LocationID", nullable: true, length: 50 })
  locationId: string | null;

  @Column("varchar", { name: "EmployeeID", nullable: true, length: 50 })
  employeeId: string | null;

  @Column("varchar", { name: "ManagerID", nullable: true, length: 50 })
  managerId: string | null;

  @OneToOne(
    () => CsOutputTbl,
    csOutputTbl => csOutputTbl.output
  )
  csOutputTbl: CsOutputTbl;

  @OneToMany(
    () => CsPaymentDetailTbl,
    csPaymentDetailTbl => csPaymentDetailTbl.document
  )
  csPaymentDetailTbls: CsPaymentDetailTbl[];

  @ManyToOne(
    () => CfObjectTbl,
    cfObjectTbl => cfObjectTbl.csPaymentTbls,
    { onUpdate: "CASCADE" }
  )
  @JoinColumn([{ name: "ObjectID", referencedColumnName: "objectId" }])
  object: CfObjectTbl;

  @OneToMany(
    () => CsVatInvoiceTbl,
    csVatInvoiceTbl => csVatInvoiceTbl.payment
  )
  csVatInvoiceTbls: CsVatInvoiceTbl[];
}
