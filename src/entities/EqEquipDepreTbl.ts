import { Column, Entity, Index, JoinColumn, ManyToOne } from "typeorm";
import { EqEquipTbl } from "./EqEquipTbl";

@Index("PK_EQ_EquipDepreTbl", ["userAutoId"], { unique: true })
@Entity("EQ_EquipDepreTbl", { schema: "dbo" })
export class EqEquipDepreTbl {
  @Column("varchar", { primary: true, name: "UserAutoID", length: 50 })
  userAutoId: string;

  @Column("varchar", { name: "PeriodID", length: 50 })
  periodId: string;

  @Column("nvarchar", { name: "EquipName", nullable: true, length: 200 })
  equipName: string | null;

  @Column("varchar", { name: "DocumentID", length: 50 })
  documentId: string;

  @Column("datetime", { name: "DocumentDate" })
  documentDate: Date;

  @Column("decimal", { name: "Amount", precision: 28, scale: 0 })
  amount: number;

  @Column("decimal", {
    name: "OpenAllocate",
    nullable: true,
    precision: 28,
    scale: 0
  })
  openAllocate: number | null;

  @Column("decimal", {
    name: "PeriodAmount",
    nullable: true,
    precision: 28,
    scale: 0
  })
  periodAmount: number | null;

  @Column("decimal", {
    name: "EndAllocate",
    nullable: true,
    precision: 28,
    scale: 0
  })
  endAllocate: number | null;

  @Column("decimal", {
    name: "RemainAmount",
    nullable: true,
    precision: 28,
    scale: 0
  })
  remainAmount: number | null;

  @Column("varchar", { name: "AccountID", length: 50 })
  accountId: string;

  @Column("varchar", { name: "ExpenseAccID", length: 50 })
  expenseAccId: string;

  @Column("varchar", { name: "CostCenterID", nullable: true, length: 50 })
  costCenterId: string | null;

  @Column("varchar", { name: "CostID", nullable: true, length: 50 })
  costId: string | null;

  @Column("varchar", { name: "ObjectID", nullable: true, length: 50 })
  objectId: string | null;

  @ManyToOne(
    () => EqEquipTbl,
    eqEquipTbl => eqEquipTbl.eqEquipDepreTbls,
    { onDelete: "CASCADE", onUpdate: "CASCADE" }
  )
  @JoinColumn([{ name: "EquipID", referencedColumnName: "equipId" }])
  equip: EqEquipTbl;
}
