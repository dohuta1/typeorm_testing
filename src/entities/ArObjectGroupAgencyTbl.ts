import { Column, Entity, Index, JoinColumn, ManyToOne } from "typeorm";
import { ArObjectGroupTbl } from "./ArObjectGroupTbl";

@Index("PK_AR_ObjectGroupAgencyTbl", ["userAutoId"], { unique: true })
@Entity("AR_ObjectGroupAgencyTbl", { schema: "dbo" })
export class ArObjectGroupAgencyTbl {
  @Column("varchar", { primary: true, name: "UserAutoID", length: 50 })
  userAutoId: string;

  @Column("varchar", { name: "AgencyID", length: 50 })
  agencyId: string;

  @Column("nvarchar", { name: "Note", nullable: true, length: 50 })
  note: string | null;

  @ManyToOne(
    () => ArObjectGroupTbl,
    arObjectGroupTbl => arObjectGroupTbl.arObjectGroupAgencyTbls,
    { onDelete: "CASCADE", onUpdate: "CASCADE" }
  )
  @JoinColumn([
    { name: "ObjectGroupCode", referencedColumnName: "objectGroupCode" }
  ])
  objectGroupCode: ArObjectGroupTbl;
}
