import { Column, Entity, Index } from "typeorm";

@Index("PK_HR_StatusTbl", ["trangThai"], { unique: true })
@Entity("HR_StatusTbl", { schema: "dbo" })
export class HrStatusTbl {
  @Column("int", { primary: true, name: "TrangThai" })
  trangThai: number;

  @Column("nvarchar", { name: "TenTrangThai", nullable: true, length: 50 })
  tenTrangThai: string | null;
}
