import {
  Column,
  Entity,
  Index,
  OneToMany,
  PrimaryGeneratedColumn
} from "typeorm";
import { CfObjectTbl } from "./CfObjectTbl";

@Index("PK_CF_ObjectGroupList", ["objectGroupId"], { unique: true })
@Entity("CF_ObjectGroupTbl", { schema: "dbo" })
export class CfObjectGroupTbl {
  @Column("varchar", { primary: true, name: "ObjectGroupID", length: 50 })
  objectGroupId: string;

  @Column("nvarchar", { name: "ObjectGroupName", length: 150 })
  objectGroupName: string;

  @Column("varchar", { name: "PaymentTermID", nullable: true, length: 50 })
  paymentTermId: string | null;

  @Column("nvarchar", { name: "LocationID", nullable: true, length: 50 })
  locationId: string | null;

  @Column("bit", { name: "isCustomer", default: () => "(1)" })
  isCustomer: boolean;

  @Column("bit", { name: "isVendor", default: () => "(1)" })
  isVendor: boolean;

  @Column("bit", { name: "isEmployee", default: () => "(1)" })
  isEmployee: boolean;

  @Column("bit", { name: "isManager", default: () => "(0)" })
  isManager: boolean;

  @Column("bit", { name: "isDefault", nullable: true, default: () => "(0)" })
  isDefault: boolean | null;

  @PrimaryGeneratedColumn({ type: "int", name: "STT" })
  stt: number;

  @Column("varchar", { name: "UserCreate", nullable: true, length: 30 })
  userCreate: string | null;

  @Column("varchar", { name: "UserUpdate", nullable: true, length: 30 })
  userUpdate: string | null;

  @Column("datetime", { name: "DateUpdate", nullable: true })
  dateUpdate: Date | null;

  @Column("datetime", { name: "DateCreate", nullable: true })
  dateCreate: Date | null;

  @OneToMany(
    () => CfObjectTbl,
    cfObjectTbl => cfObjectTbl.objectGroup
  )
  cfObjectTbls: CfObjectTbl[];
}
