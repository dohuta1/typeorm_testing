import { Column, Entity, Index, OneToMany } from "typeorm";
import { CsReceiptPlan2Tbl } from "./CsReceiptPlan2Tbl";
import { CsReceiptPlanTbl } from "./CsReceiptPlanTbl";

@Index("PK_CS_ReceiptPlanStatusTbl", ["statusId"], { unique: true })
@Entity("CS_ReceiptPlanStatusTbl", { schema: "dbo" })
export class CsReceiptPlanStatusTbl {
  @Column("int", { primary: true, name: "StatusID" })
  statusId: number;

  @Column("nvarchar", { name: "StatusName", length: 50 })
  statusName: string;

  @Column("int", { name: "BackColor", nullable: true })
  backColor: number | null;

  @Column("int", { name: "ForeColor", nullable: true })
  foreColor: number | null;

  @OneToMany(
    () => CsReceiptPlan2Tbl,
    csReceiptPlan2Tbl => csReceiptPlan2Tbl.status
  )
  csReceiptPlan2Tbls: CsReceiptPlan2Tbl[];

  @OneToMany(
    () => CsReceiptPlanTbl,
    csReceiptPlanTbl => csReceiptPlanTbl.status
  )
  csReceiptPlanTbls: CsReceiptPlanTbl[];
}
