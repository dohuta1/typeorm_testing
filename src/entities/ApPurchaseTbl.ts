import {
  Column,
  Entity,
  Index,
  JoinColumn,
  ManyToOne,
  OneToMany
} from "typeorm";
import { ApPurchaseAllocateTbl } from "./ApPurchaseAllocateTbl";
import { ApPurchaseDetailTbl } from "./ApPurchaseDetailTbl";
import { CfObjectTbl } from "./CfObjectTbl";
import { ApVatInvoiceTbl } from "./ApVatInvoiceTbl";

@Index("PK_PurchaseTbl", ["documentId"], { unique: true })
@Entity("AP_PurchaseTbl", { schema: "dbo" })
export class ApPurchaseTbl {
  @Column("varchar", { primary: true, name: "DocumentID", length: 30 })
  documentId: string;

  @Column("datetime", { name: "DocumentDate" })
  documentDate: Date;

  @Column("nvarchar", { name: "Memo", nullable: true, length: 200 })
  memo: string | null;

  @Column("nvarchar", { name: "Notes", nullable: true, length: 200 })
  notes: string | null;

  @Column("nvarchar", { name: "Deliver", nullable: true, length: 100 })
  deliver: string | null;

  @Column("varchar", { name: "CurrencyID", length: 3, default: () => "'VND'" })
  currencyId: string;

  @Column("float", {
    name: "RateExchange",
    precision: 53,
    default: () => "(1)"
  })
  rateExchange: number;

  @Column("varchar", { name: "PaymentTypeID", nullable: true, length: 50 })
  paymentTypeId: string | null;

  @Column("varchar", { name: "PaymentTermID", nullable: true, length: 50 })
  paymentTermId: string | null;

  @Column("datetime", { name: "DueDate", nullable: true })
  dueDate: Date | null;

  @Column("varchar", { name: "PONo", nullable: true, length: 50 })
  poNo: string | null;

  @Column("varchar", { name: "ContractID", nullable: true, length: 50 })
  contractId: string | null;

  @Column("varchar", { name: "RefDoc", nullable: true, length: 50 })
  refDoc: string | null;

  @Column("decimal", {
    name: "BaseTotal",
    nullable: true,
    precision: 18,
    scale: 0
  })
  baseTotal: number | null;

  @Column("varchar", { name: "PayAccID", length: 50, default: () => "(331)" })
  payAccId: string;

  @Column("varchar", { name: "VATAccID", length: 50, default: () => "(13311)" })
  vatAccId: string;

  @Column("varchar", { name: "ImportAccID", nullable: true, length: 50 })
  importAccId: string | null;

  @Column("varchar", {
    name: "ImportAccID2",
    nullable: true,
    length: 50,
    default: () => "(33312)"
  })
  importAccId2: string | null;

  @Column("bit", { name: "isImport", default: () => "(0)" })
  isImport: boolean;

  @Column("bit", { name: "isLock", default: () => "(0)" })
  isLock: boolean;

  @Column("varchar", { name: "InvoiceNo", nullable: true, length: 50 })
  invoiceNo: string | null;

  @Column("nvarchar", { name: "SearchField", nullable: true, length: 2000 })
  searchField: string | null;

  @Column("varchar", { name: "UserCreate", nullable: true, length: 30 })
  userCreate: string | null;

  @Column("varchar", { name: "UserUpdate", nullable: true, length: 30 })
  userUpdate: string | null;

  @Column("datetime", { name: "DateUpdate", nullable: true })
  dateUpdate: Date | null;

  @Column("datetime", { name: "DateCreate", nullable: true })
  dateCreate: Date | null;

  @Column("int", { name: "Status", default: () => "(1)" })
  status: number;

  @Column("varchar", { name: "LinkID", nullable: true, length: 50 })
  linkId: string | null;

  @Column("decimal", {
    name: "AmountTotal",
    nullable: true,
    precision: 28,
    scale: 0
  })
  amountTotal: number | null;

  @Column("decimal", {
    name: "VATAmountTotal",
    nullable: true,
    precision: 28,
    scale: 0
  })
  vatAmountTotal: number | null;

  @Column("decimal", {
    name: "ImportAmountTotal",
    nullable: true,
    precision: 28,
    scale: 0
  })
  importAmountTotal: number | null;

  @Column("varchar", { name: "ImpExpID", nullable: true, length: 10 })
  impExpId: string | null;

  @Column("varchar", { name: "DocBatch", nullable: true, length: 50 })
  docBatch: string | null;

  @Column("nvarchar", { name: "Attach", nullable: true, length: 100 })
  attach: string | null;

  @Column("datetime", { name: "AttachDate", nullable: true })
  attachDate: Date | null;

  @Column("varchar", { name: "ImportAccID3", nullable: true, length: 50 })
  importAccId3: string | null;

  @Column("varchar", { name: "BranchID", length: 50 })
  branchId: string;

  @Column("varchar", { name: "ImportAccID4", nullable: true, length: 50 })
  importAccId4: string | null;

  @OneToMany(
    () => ApPurchaseAllocateTbl,
    apPurchaseAllocateTbl => apPurchaseAllocateTbl.document
  )
  apPurchaseAllocateTbls: ApPurchaseAllocateTbl[];

  @OneToMany(
    () => ApPurchaseDetailTbl,
    apPurchaseDetailTbl => apPurchaseDetailTbl.document
  )
  apPurchaseDetailTbls: ApPurchaseDetailTbl[];

  @ManyToOne(
    () => CfObjectTbl,
    cfObjectTbl => cfObjectTbl.apPurchaseTbls,
    { onUpdate: "CASCADE" }
  )
  @JoinColumn([{ name: "ObjectID", referencedColumnName: "objectId" }])
  object: CfObjectTbl;

  @OneToMany(
    () => ApVatInvoiceTbl,
    apVatInvoiceTbl => apVatInvoiceTbl.purchase
  )
  apVatInvoiceTbls: ApVatInvoiceTbl[];
}
