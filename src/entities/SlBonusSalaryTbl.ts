import { Column, Entity, Index, JoinColumn, ManyToOne } from "typeorm";
import { SlSalaryGroupTbl } from "./SlSalaryGroupTbl";
import { KdTargetTypeTbl } from "./KdTargetTypeTbl";

@Index("PK_SL_BonusSalaryTbl", ["autoId"], { unique: true })
@Entity("SL_BonusSalaryTbl", { schema: "dbo" })
export class SlBonusSalaryTbl {
  @Column("varchar", {
    primary: true,
    name: "AutoID",
    length: 50,
    default: () => "newid()"
  })
  autoId: string;

  @Column("decimal", { name: "PercentTarget", precision: 3, scale: 0 })
  percentTarget: number;

  @Column("int", { name: "Quantity" })
  quantity: number;

  @Column("decimal", { name: "BonusAmount", precision: 18, scale: 0 })
  bonusAmount: number;

  @Column("datetime", { name: "StartDate" })
  startDate: Date;

  @Column("datetime", { name: "EndDate", nullable: true })
  endDate: Date | null;

  @Column("nvarchar", { name: "Notes", nullable: true, length: 200 })
  notes: string | null;

  @ManyToOne(
    () => SlSalaryGroupTbl,
    slSalaryGroupTbl => slSalaryGroupTbl.slBonusSalaryTbls,
    { onDelete: "CASCADE", onUpdate: "CASCADE" }
  )
  @JoinColumn([
    { name: "SalaryGroupID", referencedColumnName: "salaryGroupId" }
  ])
  salaryGroup: SlSalaryGroupTbl;

  @ManyToOne(
    () => KdTargetTypeTbl,
    kdTargetTypeTbl => kdTargetTypeTbl.slBonusSalaryTbls,
    { onUpdate: "CASCADE" }
  )
  @JoinColumn([{ name: "TargetTypeID", referencedColumnName: "targetTypeId" }])
  targetType: KdTargetTypeTbl;
}
