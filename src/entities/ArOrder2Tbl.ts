import {
  Column,
  Entity,
  Index,
  JoinColumn,
  ManyToOne,
  OneToMany
} from "typeorm";
import { ArOrder2DetailTbl } from "./ArOrder2DetailTbl";
import { CfObjectTbl } from "./CfObjectTbl";

@Index("PK_AR_Order2Tbl", ["documentId"], { unique: true })
@Entity("AR_Order2Tbl", { schema: "dbo" })
export class ArOrder2Tbl {
  @Column("varchar", { primary: true, name: "DocumentID", length: 30 })
  documentId: string;

  @Column("datetime", { name: "DocumentDate" })
  documentDate: Date;

  @Column("nvarchar", { name: "Memo", nullable: true, length: 200 })
  memo: string | null;

  @Column("nvarchar", { name: "Notes", nullable: true })
  notes: string | null;

  @Column("datetime", { name: "DeliverDate", nullable: true })
  deliverDate: Date | null;

  @Column("decimal", {
    name: "BaseTotal",
    nullable: true,
    precision: 28,
    scale: 0
  })
  baseTotal: number | null;

  @Column("nvarchar", { name: "SearchField", nullable: true, length: 2000 })
  searchField: string | null;

  @Column("varchar", { name: "LinkID", nullable: true, length: 50 })
  linkId: string | null;

  @Column("nvarchar", { name: "TrangThai", nullable: true, length: 50 })
  trangThai: string | null;

  @Column("int", { name: "MaTrangThai", nullable: true, default: () => "(0)" })
  maTrangThai: number | null;

  @Column("varchar", { name: "EmployeeID", nullable: true, length: 50 })
  employeeId: string | null;

  @Column("varchar", { name: "AgencyID", nullable: true, length: 50 })
  agencyId: string | null;

  @Column("bit", { name: "isVAT", nullable: true })
  isVat: boolean | null;

  @Column("varchar", { name: "TaxCode", nullable: true, length: 50 })
  taxCode: string | null;

  @Column("bit", { name: "isLock", default: () => "(0)" })
  isLock: boolean;

  @Column("varchar", { name: "UserCreate", nullable: true, length: 30 })
  userCreate: string | null;

  @Column("varchar", { name: "UserUpdate", nullable: true, length: 30 })
  userUpdate: string | null;

  @Column("datetime", { name: "DateUpdate", nullable: true })
  dateUpdate: Date | null;

  @Column("datetime", { name: "DateCreate", nullable: true })
  dateCreate: Date | null;

  @Column("bit", { name: "isOrder", default: () => "(0)" })
  isOrder: boolean;

  @Column("varchar", { name: "Tel", nullable: true, length: 150 })
  tel: string | null;

  @OneToMany(
    () => ArOrder2DetailTbl,
    arOrder2DetailTbl => arOrder2DetailTbl.document
  )
  arOrder2DetailTbls: ArOrder2DetailTbl[];

  @ManyToOne(
    () => CfObjectTbl,
    cfObjectTbl => cfObjectTbl.arOrder2Tbls,
    { onUpdate: "CASCADE" }
  )
  @JoinColumn([{ name: "ObjectID", referencedColumnName: "objectId" }])
  object: CfObjectTbl;
}
