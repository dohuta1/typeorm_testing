import { Column, Entity, Index, JoinColumn, ManyToOne } from "typeorm";
import { WaUserLog } from "./WaUserLog";

@Index("IX_WA_UserLogDetail_LogDate", ["logDate"], {})
@Index("IX_WA_UserLogDetail_LogID", ["logId"], {})
@Index("PK_WA_UserLogDetail", ["userAutoId"], { unique: true })
@Entity("WA_UserLogDetail", { schema: "dbo" })
export class WaUserLogDetail {
  @Column("varchar", { primary: true, name: "UserAutoID", length: 50 })
  userAutoId: string;

  @Column("varchar", { name: "LogID", length: 50 })
  logId: string;

  @Column("nvarchar", { name: "FormID", nullable: true, length: 150 })
  formId: string | null;

  @Column("nvarchar", { name: "Para", length: 50 })
  para: string;

  @Column("datetime", { name: "LogDate", nullable: true })
  logDate: Date | null;

  @Column("nvarchar", { name: "Note", nullable: true, length: 500 })
  note: string | null;

  @Column("varchar", { name: "FormKey", nullable: true, length: 250 })
  formKey: string | null;

  @Column("nvarchar", { name: "PrimaryValue", nullable: true, length: 250 })
  primaryValue: string | null;

  @Column("nvarchar", { name: "DataValue", nullable: true })
  dataValue: string | null;

  @ManyToOne(
    () => WaUserLog,
    waUserLog => waUserLog.waUserLogDetails,
    { onDelete: "CASCADE", onUpdate: "CASCADE" }
  )
  @JoinColumn([{ name: "LogID", referencedColumnName: "userAutoId" }])
  log: WaUserLog;
}
