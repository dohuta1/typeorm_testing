import { Column, Entity, Index, JoinColumn, ManyToOne } from "typeorm";
import { GjPlSetupTbl } from "./GjPlSetupTbl";

@Index("PK_GJ_ProfitLostSetupDetailTbl", ["userAutoId"], { unique: true })
@Entity("GJ_PLSetupDetailTbl", { schema: "dbo" })
export class GjPlSetupDetailTbl {
  @Column("varchar", { primary: true, name: "UserAutoID", length: 50 })
  userAutoId: string;

  @Column("varchar", { name: "DebitAccID", nullable: true, length: 50 })
  debitAccId: string | null;

  @Column("varchar", { name: "CreditAccID", nullable: true, length: 50 })
  creditAccId: string | null;

  @Column("varchar", { name: "SumID", nullable: true, length: 50 })
  sumId: string | null;

  @Column("bit", { name: "ReverseSign", nullable: true, default: () => "0" })
  reverseSign: boolean | null;

  @ManyToOne(
    () => GjPlSetupTbl,
    gjPlSetupTbl => gjPlSetupTbl.gjPlSetupDetailTbls,
    { onDelete: "CASCADE", onUpdate: "CASCADE" }
  )
  @JoinColumn([{ name: "SetupID", referencedColumnName: "setupId" }])
  setup: GjPlSetupTbl;
}
