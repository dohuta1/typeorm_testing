import { Column, Entity, Index, JoinColumn, ManyToOne } from "typeorm";
import { CfObjectTbl } from "./CfObjectTbl";

@Index("PK_WA_PlanObjectTbl", ["documentId"], { unique: true })
@Entity("WA_PlanObjectTbl", { schema: "dbo" })
export class WaPlanObjectTbl {
  @Column("varchar", { primary: true, name: "DocumentID", length: 30 })
  documentId: string;

  @Column("datetime", { name: "DocumentDate" })
  documentDate: Date;

  @Column("varchar", { name: "BranchID", length: 50 })
  branchId: string;

  @Column("varchar", { name: "ManagerID", length: 50 })
  managerId: string;

  @Column("varchar", { name: "EmployeeID", length: 50 })
  employeeId: string;

  @Column("nvarchar", { name: "Memo", nullable: true, length: 200 })
  memo: string | null;

  @Column("nvarchar", { name: "Notes", nullable: true })
  notes: string | null;

  @Column("varchar", { name: "UserCreate", nullable: true, length: 30 })
  userCreate: string | null;

  @Column("varchar", { name: "UserUpdate", nullable: true, length: 30 })
  userUpdate: string | null;

  @Column("datetime", { name: "DateUpdate", nullable: true })
  dateUpdate: Date | null;

  @Column("datetime", { name: "DateCreate", nullable: true })
  dateCreate: Date | null;

  @ManyToOne(
    () => CfObjectTbl,
    cfObjectTbl => cfObjectTbl.waPlanObjectTbls,
    { onUpdate: "CASCADE" }
  )
  @JoinColumn([{ name: "ObjectID", referencedColumnName: "objectId" }])
  object: CfObjectTbl;
}
