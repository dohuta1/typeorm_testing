import { Column, Entity, Index, OneToMany } from "typeorm";
import { CcTblCostPriceDetail } from "./CcTblCostPriceDetail";

@Index("PK_CC_TblCostPrice", ["costPriceId"], { unique: true })
@Entity("CC_TblCostPrice", { schema: "dbo" })
export class CcTblCostPrice {
  @Column("numeric", {
    primary: true,
    name: "CostPriceID",
    precision: 18,
    scale: 0
  })
  costPriceId: number;

  @Column("varchar", { name: "PeriodID", length: 7 })
  periodId: string;

  @Column("varchar", { name: "CostCenterID", nullable: true, length: 10 })
  costCenterId: string | null;

  @Column("varchar", { name: "ItemID", length: 50 })
  itemId: string;

  @Column("nvarchar", { name: "Memo", nullable: true, length: 255 })
  memo: string | null;

  @Column("varchar", { name: "Unit", nullable: true, length: 50 })
  unit: string | null;

  @Column("float", { name: "Quantity", nullable: true, precision: 53 })
  quantity: number | null;

  @Column("float", { name: "QuantitySE", nullable: true, precision: 53 })
  quantitySe: number | null;

  @Column("decimal", {
    name: "UnitCost",
    nullable: true,
    precision: 28,
    scale: 4
  })
  unitCost: number | null;

  @Column("decimal", {
    name: "CostAmount",
    nullable: true,
    precision: 28,
    scale: 4
  })
  costAmount: number | null;

  @Column("varchar", { name: "AccountID", nullable: true, length: 25 })
  accountId: string | null;

  @Column("float", { name: "Factor", nullable: true, precision: 53 })
  factor: number | null;

  @Column("float", { name: "Tmp", nullable: true, precision: 53 })
  tmp: number | null;

  @OneToMany(
    () => CcTblCostPriceDetail,
    ccTblCostPriceDetail => ccTblCostPriceDetail.costPrice
  )
  ccTblCostPriceDetails: CcTblCostPriceDetail[];
}
