import { Column, Entity, Index, OneToMany } from "typeorm";
import { PlKeHoachKinhDoanhDetailTbl } from "./PlKeHoachKinhDoanhDetailTbl";

@Index("PK_PL_KeHoachKinhDoanhTbl", ["maKeHoach"], { unique: true })
@Entity("PL_KeHoachKinhDoanhTbl", { schema: "dbo" })
export class PlKeHoachKinhDoanhTbl {
  @Column("varchar", { primary: true, name: "MaKeHoach", length: 50 })
  maKeHoach: string;

  @Column("nvarchar", { name: "DienGiai", nullable: true, length: 250 })
  dienGiai: string | null;

  @Column("int", { name: "Thang", nullable: true })
  thang: number | null;

  @Column("int", { name: "Quy", nullable: true })
  quy: number | null;

  @Column("int", { name: "Nam" })
  nam: number;

  @Column("varchar", { name: "DepartmentID", nullable: true, length: 50 })
  departmentId: string | null;

  @Column("bit", { name: "DieuChinh", default: () => "0" })
  dieuChinh: boolean;

  @Column("datetime", { name: "NgayApDung", nullable: true })
  ngayApDung: Date | null;

  @Column("decimal", {
    name: "BaseTotal",
    nullable: true,
    precision: 28,
    scale: 0
  })
  baseTotal: number | null;

  @Column("varchar", { name: "UserCreate", nullable: true, length: 30 })
  userCreate: string | null;

  @Column("varchar", { name: "UserUpdate", nullable: true, length: 30 })
  userUpdate: string | null;

  @Column("datetime", { name: "DateUpdate", nullable: true })
  dateUpdate: Date | null;

  @Column("datetime", { name: "DateCreate", nullable: true })
  dateCreate: Date | null;

  @OneToMany(
    () => PlKeHoachKinhDoanhDetailTbl,
    plKeHoachKinhDoanhDetailTbl => plKeHoachKinhDoanhDetailTbl.maKeHoach
  )
  plKeHoachKinhDoanhDetailTbls: PlKeHoachKinhDoanhDetailTbl[];
}
