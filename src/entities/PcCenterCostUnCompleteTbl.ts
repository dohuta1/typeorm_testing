import { Column, Entity, Index, JoinColumn, ManyToOne } from "typeorm";
import { PcCenterCostTbl } from "./PcCenterCostTbl";

@Index("PK_PC_CenterCostUnCompleteTbl", ["userAutoId"], { unique: true })
@Entity("PC_CenterCostUnCompleteTbl", { schema: "dbo" })
export class PcCenterCostUnCompleteTbl {
  @Column("varchar", { primary: true, name: "UserAutoID", length: 50 })
  userAutoId: string;

  @Column("varchar", { name: "CostCenterID", nullable: true, length: 50 })
  costCenterId: string | null;

  @Column("decimal", {
    name: "PeriodAmount",
    nullable: true,
    precision: 28,
    scale: 0,
    default: () => "0"
  })
  periodAmount: number | null;

  @ManyToOne(
    () => PcCenterCostTbl,
    pcCenterCostTbl => pcCenterCostTbl.pcCenterCostUnCompleteTbls,
    { onDelete: "CASCADE", onUpdate: "CASCADE" }
  )
  @JoinColumn([{ name: "PriceID", referencedColumnName: "priceId" }])
  price: PcCenterCostTbl;
}
