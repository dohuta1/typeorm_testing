import { Column, Entity, Index, JoinColumn, ManyToOne } from "typeorm";
import { ArOrderTbl } from "./ArOrderTbl";
import { CfItemTbl } from "./CfItemTbl";

@Index("IX_AR_OrderDetailTbl_DocumentID", ["documentId"], {})
@Index("PK_AR_OrderDetailTbl", ["userAutoId"], { unique: true })
@Entity("AR_OrderDetailTbl", { schema: "dbo" })
export class ArOrderDetailTbl {
  @Column("varchar", {
    primary: true,
    name: "UserAutoID",
    length: 40,
    default: () => "newid()"
  })
  userAutoId: string;

  @Column("varchar", { name: "DocumentID", length: 30 })
  documentId: string;

  @Column("decimal", {
    name: "Quantity",
    nullable: true,
    precision: 28,
    scale: 2
  })
  quantity: number | null;

  @Column("decimal", {
    name: "UnitPrice",
    nullable: true,
    precision: 28,
    scale: 4
  })
  unitPrice: number | null;

  @Column("decimal", {
    name: "SourceAmount",
    nullable: true,
    precision: 18,
    scale: 2
  })
  sourceAmount: number | null;

  @Column("decimal", {
    name: "Amount",
    nullable: true,
    precision: 28,
    scale: 0
  })
  amount: number | null;

  @Column("decimal", {
    name: "SoLuongTang",
    nullable: true,
    precision: 18,
    scale: 2
  })
  soLuongTang: number | null;

  @Column("decimal", {
    name: "DiscountPercent",
    nullable: true,
    precision: 18,
    scale: 2
  })
  discountPercent: number | null;

  @Column("decimal", {
    name: "DiscountAmount",
    nullable: true,
    precision: 18,
    scale: 0
  })
  discountAmount: number | null;

  @Column("decimal", {
    name: "TotalAmount",
    nullable: true,
    precision: 18,
    scale: 0
  })
  totalAmount: number | null;

  @Column("bit", { name: "isKM", default: () => "(0)" })
  isKm: boolean;

  @Column("decimal", {
    name: "VATQuantity",
    nullable: true,
    precision: 18,
    scale: 2
  })
  vatQuantity: number | null;

  @Column("nvarchar", { name: "DienGiai", nullable: true, length: 100 })
  dienGiai: string | null;

  @Column("nvarchar", { name: "Notes", nullable: true, length: 100 })
  notes: string | null;

  @Column("decimal", {
    name: "Quantity2",
    nullable: true,
    precision: 28,
    scale: 4
  })
  quantity2: number | null;

  @Column("nvarchar", { name: "Property", nullable: true, length: 50 })
  property: string | null;

  @Column("nvarchar", { name: "Property2", nullable: true, length: 50 })
  property2: string | null;

  @Column("varchar", { name: "ParentID", nullable: true, length: 50 })
  parentId: string | null;

  @Column("nvarchar", { name: "Notes2", nullable: true, length: 200 })
  notes2: string | null;

  @Column("varchar", { name: "CostID", nullable: true, length: 50 })
  costId: string | null;

  @ManyToOne(
    () => ArOrderTbl,
    arOrderTbl => arOrderTbl.arOrderDetailTbls,
    { onDelete: "CASCADE", onUpdate: "CASCADE" }
  )
  @JoinColumn([{ name: "DocumentID", referencedColumnName: "documentId" }])
  document: ArOrderTbl;

  @ManyToOne(
    () => CfItemTbl,
    cfItemTbl => cfItemTbl.arOrderDetailTbls,
    { onUpdate: "CASCADE" }
  )
  @JoinColumn([{ name: "ItemID", referencedColumnName: "itemId" }])
  item: CfItemTbl;
}
