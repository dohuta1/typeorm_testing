import { Column, Entity, Index, JoinColumn, ManyToOne } from "typeorm";
import { ArBanHoTbl } from "./ArBanHoTbl";
import { CfCostCenterTbl } from "./CfCostCenterTbl";
import { CfItemTbl } from "./CfItemTbl";
import { CfStoreHouseTbl } from "./CfStoreHouseTbl";
import { CfCostTbl } from "./CfCostTbl";

@Index("PK_BanHoDetailTbl", ["userAutoId"], { unique: true })
@Entity("AR_BanHoDetailTbl", { schema: "dbo" })
export class ArBanHoDetailTbl {
  @Column("varchar", { primary: true, name: "UserAutoID", length: 40 })
  userAutoId: string;

  @Column("varchar", { name: "Lot", nullable: true, length: 50 })
  lot: string | null;

  @Column("datetime", { name: "ExpireDate", nullable: true })
  expireDate: Date | null;

  @Column("decimal", {
    name: "Quantity",
    precision: 28,
    scale: 4,
    default: () => "(1)"
  })
  quantity: number;

  @Column("decimal", {
    name: "UnitPrice",
    nullable: true,
    precision: 28,
    scale: 4
  })
  unitPrice: number | null;

  @Column("decimal", {
    name: "Amount",
    nullable: true,
    precision: 28,
    scale: 0
  })
  amount: number | null;

  @Column("decimal", {
    name: "DiscountPercent",
    nullable: true,
    precision: 18,
    scale: 0
  })
  discountPercent: number | null;

  @Column("decimal", {
    name: "DiscountAmount",
    nullable: true,
    precision: 28,
    scale: 0
  })
  discountAmount: number | null;

  @Column("decimal", {
    name: "DiscountAmount2",
    nullable: true,
    precision: 18,
    scale: 0
  })
  discountAmount2: number | null;

  @Column("decimal", {
    name: "TotalAmount",
    nullable: true,
    precision: 28,
    scale: 0
  })
  totalAmount: number | null;

  @Column("varchar", { name: "InvAccID", nullable: true, length: 50 })
  invAccId: string | null;

  @Column("varchar", {
    name: "AccountID2",
    nullable: true,
    length: 50,
    default: () => "(13881)"
  })
  accountId2: string | null;

  @Column("float", {
    name: "UnitCost",
    nullable: true,
    precision: 53,
    default: () => "(0)"
  })
  unitCost: number | null;

  @Column("decimal", {
    name: "AmountCost",
    nullable: true,
    precision: 28,
    scale: 0,
    default: () => "(0)"
  })
  amountCost: number | null;

  @Column("decimal", {
    name: "Quantity2",
    nullable: true,
    precision: 28,
    scale: 4
  })
  quantity2: number | null;

  @Column("varchar", { name: "ObjectID", nullable: true, length: 50 })
  objectId: string | null;

  @Column("varchar", { name: "ParentID", nullable: true, length: 50 })
  parentId: string | null;

  @Column("nvarchar", { name: "DienGiai", nullable: true, length: 250 })
  dienGiai: string | null;

  @Column("nvarchar", { name: "Notes", nullable: true, length: 100 })
  notes: string | null;

  @Column("nvarchar", { name: "Property", nullable: true, length: 50 })
  property: string | null;

  @Column("nvarchar", { name: "Property2", nullable: true, length: 50 })
  property2: string | null;

  @ManyToOne(
    () => ArBanHoTbl,
    arBanHoTbl => arBanHoTbl.arBanHoDetailTbls,
    { onDelete: "CASCADE", onUpdate: "CASCADE" }
  )
  @JoinColumn([{ name: "DocumentID", referencedColumnName: "documentId" }])
  document: ArBanHoTbl;

  @ManyToOne(
    () => CfCostCenterTbl,
    cfCostCenterTbl => cfCostCenterTbl.arBanHoDetailTbls,
    { onUpdate: "CASCADE" }
  )
  @JoinColumn([{ name: "CostCenterID", referencedColumnName: "costCenterId" }])
  costCenter: CfCostCenterTbl;

  @ManyToOne(
    () => CfItemTbl,
    cfItemTbl => cfItemTbl.arBanHoDetailTbls,
    { onUpdate: "CASCADE" }
  )
  @JoinColumn([{ name: "ItemID", referencedColumnName: "itemId" }])
  item: CfItemTbl;

  @ManyToOne(
    () => CfStoreHouseTbl,
    cfStoreHouseTbl => cfStoreHouseTbl.arBanHoDetailTbls,
    { onUpdate: "CASCADE" }
  )
  @JoinColumn([{ name: "StoreHouseID", referencedColumnName: "storeHouseId" }])
  storeHouse: CfStoreHouseTbl;

  @ManyToOne(
    () => CfCostTbl,
    cfCostTbl => cfCostTbl.arBanHoDetailTbls,
    { onUpdate: "CASCADE" }
  )
  @JoinColumn([{ name: "CostID", referencedColumnName: "costId" }])
  cost: CfCostTbl;
}
