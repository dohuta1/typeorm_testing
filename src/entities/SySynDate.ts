import { Column, Entity, PrimaryColumn } from "typeorm";

@Entity("SY_SynDate", { schema: "dbo" })
export class SySynDate {
  @PrimaryColumn("datetime", { name: "SynDate" })
  synDate: Date;

  @Column("datetime", { name: "SynDate2", nullable: true })
  synDate2: Date | null;

  @Column("nvarchar", { name: "Notes", nullable: true, length: 150 })
  notes: string | null;

  @Column("int", { name: "isData", default: () => "(1)" })
  isData: number;
}
