import { Column, Entity, Index, JoinColumn, ManyToOne } from "typeorm";
import { HrPersonTbl } from "./HrPersonTbl";

@Index("PK_HR_PersonNghiPhepTbl", ["userAutoId"], { unique: true })
@Entity("HR_PersonNghiPhepTbl", { schema: "dbo" })
export class HrPersonNghiPhepTbl {
  @Column("varchar", { primary: true, name: "UserAutoID", length: 50 })
  userAutoId: string;

  @Column("int", { name: "Nam" })
  nam: number;

  @Column("decimal", {
    name: "SoNgay",
    nullable: true,
    precision: 28,
    scale: 1
  })
  soNgay: number | null;

  @Column("nvarchar", { name: "GhiChu", nullable: true, length: 250 })
  ghiChu: string | null;

  @ManyToOne(
    () => HrPersonTbl,
    hrPersonTbl => hrPersonTbl.hrPersonNghiPhepTbls,
    { onDelete: "CASCADE", onUpdate: "CASCADE" }
  )
  @JoinColumn([{ name: "PersonID", referencedColumnName: "personId" }])
  person: HrPersonTbl;
}
