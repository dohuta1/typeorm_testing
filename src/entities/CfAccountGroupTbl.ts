import { Column, Entity, Index } from "typeorm";

@Index("PK_CF_AccountGroupTbl", ["accountGroupId"], { unique: true })
@Entity("CF_AccountGroupTbl", { schema: "dbo" })
export class CfAccountGroupTbl {
  @Column("nvarchar", { primary: true, name: "AccountGroupID", length: 1 })
  accountGroupId: string;

  @Column("nvarchar", { name: "AccountGroupName", length: 70 })
  accountGroupName: string;
}
