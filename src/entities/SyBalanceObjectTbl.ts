import { Column, Entity, Index } from "typeorm";

@Index("IX_SY_BalanceObjectTbl", ["periodId"], {})
@Index("PK_SY_BalanceObjectTbl", ["userAutoId"], { unique: true })
@Entity("SY_BalanceObjectTbl", { schema: "dbo" })
export class SyBalanceObjectTbl {
  @Column("varchar", { primary: true, name: "UserAutoID", length: 40 })
  userAutoId: string;

  @Column("varchar", { name: "AccountID", length: 50 })
  accountId: string;

  @Column("varchar", { name: "ObjectID", length: 50 })
  objectId: string;

  @Column("decimal", {
    name: "SourceAmount",
    precision: 28,
    scale: 2,
    default: () => "(0)"
  })
  sourceAmount: number;

  @Column("varchar", { name: "CurrencyID", length: 3, default: () => "'VND'" })
  currencyId: string;

  @Column("float", {
    name: "RateExchange",
    precision: 53,
    default: () => "(1)"
  })
  rateExchange: number;

  @Column("decimal", { name: "Amount", precision: 28, scale: 0 })
  amount: number;

  @Column("bit", { name: "IsDebit", default: () => "(0)" })
  isDebit: boolean;

  @Column("varchar", { name: "ItemID", nullable: true, length: 50 })
  itemId: string | null;

  @Column("decimal", {
    name: "Quantity",
    nullable: true,
    precision: 28,
    scale: 2
  })
  quantity: number | null;

  @Column("varchar", { name: "SourceID", length: 3, default: () => "'BL'" })
  sourceId: string;

  @Column("varchar", { name: "DocumentID", nullable: true, length: 30 })
  documentId: string | null;

  @Column("datetime", { name: "DocumentDate", nullable: true })
  documentDate: Date | null;

  @Column("varchar", { name: "InvoiceNo", nullable: true, length: 100 })
  invoiceNo: string | null;

  @Column("varchar", { name: "PaymentTermID", nullable: true, length: 50 })
  paymentTermId: string | null;

  @Column("nvarchar", { name: "Memo", nullable: true, length: 250 })
  memo: string | null;

  @Column("varchar", { name: "CostCenterID", nullable: true, length: 50 })
  costCenterId: string | null;

  @Column("varchar", {
    name: "PeriodID",
    length: 10,
    default: () => "(201005)"
  })
  periodId: string;

  @Column("bit", { name: "IsBalance", default: () => "(0)" })
  isBalance: boolean;

  @Column("varchar", { name: "RefDoc", nullable: true, length: 50 })
  refDoc: string | null;

  @Column("nvarchar", { name: "ContractID", nullable: true, length: 50 })
  contractId: string | null;

  @Column("varchar", { name: "EmployeeID", nullable: true, length: 50 })
  employeeId: string | null;

  @Column("varchar", { name: "ManagerID", nullable: true, length: 50 })
  managerId: string | null;

  @Column("varchar", { name: "BranchID", length: 50 })
  branchId: string;

  @Column("decimal", {
    name: "SourceDebit",
    nullable: true,
    precision: 18,
    scale: 4
  })
  sourceDebit: number | null;

  @Column("decimal", {
    name: "SourceCredit",
    nullable: true,
    precision: 18,
    scale: 4
  })
  sourceCredit: number | null;

  @Column("decimal", {
    name: "AmountDebit",
    nullable: true,
    precision: 18,
    scale: 0
  })
  amountDebit: number | null;

  @Column("decimal", {
    name: "AmountCredit",
    nullable: true,
    precision: 18,
    scale: 0
  })
  amountCredit: number | null;
}
