import { Column, Entity, Index } from "typeorm";

@Index("PK_IS_InputTbl", ["inputId"], { unique: true })
@Entity("IS_InputTbl", { schema: "dbo" })
export class IsInputTbl {
  @Column("nvarchar", { primary: true, name: "InputID", length: 50 })
  inputId: string;

  @Column("nvarchar", { name: "TrangThai", nullable: true, length: 50 })
  trangThai: string | null;

  @Column("datetime", { name: "NgayNhap", nullable: true })
  ngayNhap: Date | null;

  @Column("varchar", { name: "NguoiDuyet", nullable: true, length: 50 })
  nguoiDuyet: string | null;

  @Column("int", { name: "MaTrangThai", default: () => "(0)" })
  maTrangThai: number;
}
