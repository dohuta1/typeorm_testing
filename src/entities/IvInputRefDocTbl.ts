import { Column, Entity, Index, JoinColumn, ManyToOne } from "typeorm";
import { IvInputTbl } from "./IvInputTbl";

@Index("PK_IV_InputRefDocTbl", ["userAutoId"], { unique: true })
@Entity("IV_InputRefDocTbl", { schema: "dbo" })
export class IvInputRefDocTbl {
  @Column("varchar", { primary: true, name: "UserAutoID", length: 40 })
  userAutoId: string;

  @Column("varchar", { name: "RefDoc", length: 50 })
  refDoc: string;

  @Column("decimal", {
    name: "Amount",
    nullable: true,
    precision: 28,
    scale: 2
  })
  amount: number | null;

  @Column("nvarchar", { name: "Memo", nullable: true, length: 150 })
  memo: string | null;

  @ManyToOne(
    () => IvInputTbl,
    ivInputTbl => ivInputTbl.ivInputRefDocTbls,
    { onDelete: "CASCADE", onUpdate: "CASCADE" }
  )
  @JoinColumn([{ name: "DocumentID", referencedColumnName: "documentId" }])
  document: IvInputTbl;
}
