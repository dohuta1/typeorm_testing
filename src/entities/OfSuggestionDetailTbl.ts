import { Column, Entity, Index, JoinColumn, ManyToOne } from "typeorm";
import { OfSuggestionTbl } from "./OfSuggestionTbl";

@Index("PK_OF_SuggestionDetailTbl", ["userAutoId"], { unique: true })
@Entity("OF_SuggestionDetailTbl", { schema: "dbo" })
export class OfSuggestionDetailTbl {
  @Column("varchar", { primary: true, name: "UserAutoID", length: 40 })
  userAutoId: string;

  @Column("nvarchar", { name: "NoiDung", nullable: true, length: 250 })
  noiDung: string | null;

  @Column("nvarchar", { name: "DVT", nullable: true, length: 50 })
  dvt: string | null;

  @Column("decimal", {
    name: "SoLuong",
    nullable: true,
    precision: 18,
    scale: 2
  })
  soLuong: number | null;

  @Column("nvarchar", { name: "DienGiai", nullable: true, length: 250 })
  dienGiai: string | null;

  @ManyToOne(
    () => OfSuggestionTbl,
    ofSuggestionTbl => ofSuggestionTbl.ofSuggestionDetailTbls,
    { onDelete: "CASCADE", onUpdate: "CASCADE" }
  )
  @JoinColumn([{ name: "DocumentID", referencedColumnName: "documentId" }])
  document: OfSuggestionTbl;
}
