import { Column, Entity, Index } from "typeorm";

@Index("PK_SY_DocumentCashTbl", ["documentType"], { unique: true })
@Entity("SY_DocumentCashTbl", { schema: "dbo" })
export class SyDocumentCashTbl {
  @Column("varchar", { primary: true, name: "DocumentType", length: 50 })
  documentType: string;

  @Column("nvarchar", { name: "DocumentTypeName", nullable: true, length: 100 })
  documentTypeName: string | null;

  @Column("varchar", { name: "Prefix", nullable: true, length: 10 })
  prefix: string | null;

  @Column("varchar", { name: "CashAccountID", nullable: true, length: 50 })
  cashAccountId: string | null;

  @Column("bit", { name: "isPayment", nullable: true })
  isPayment: boolean | null;

  @Column("bit", { name: "isReceipt", nullable: true })
  isReceipt: boolean | null;

  @Column("bit", { name: "isBank", nullable: true })
  isBank: boolean | null;

  @Column("varchar", { name: "DocumentMask", nullable: true, length: 50 })
  documentMask: string | null;
}
