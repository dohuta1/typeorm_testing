import { Column, Entity, Index, JoinColumn, ManyToOne } from "typeorm";
import { PcCostGroupSetupTbl } from "./PcCostGroupSetupTbl";

@Index("PK_PC_CostGroupSetupDetailTbl", ["userAutoId"], { unique: true })
@Entity("PC_CostGroupSetupDetailTbl", { schema: "dbo" })
export class PcCostGroupSetupDetailTbl {
  @Column("varchar", { primary: true, name: "UserAutoID", length: 50 })
  userAutoId: string;

  @Column("varchar", { name: "CostCenterID", nullable: true, length: 50 })
  costCenterId: string | null;

  @Column("varchar", { name: "CostID", nullable: true, length: 50 })
  costId: string | null;

  @Column("decimal", {
    name: "Factor",
    nullable: true,
    precision: 18,
    scale: 4
  })
  factor: number | null;

  @ManyToOne(
    () => PcCostGroupSetupTbl,
    pcCostGroupSetupTbl => pcCostGroupSetupTbl.pcCostGroupSetupDetailTbls,
    { onDelete: "CASCADE", onUpdate: "CASCADE" }
  )
  @JoinColumn([{ name: "SetupID", referencedColumnName: "setupId" }])
  setup: PcCostGroupSetupTbl;
}
