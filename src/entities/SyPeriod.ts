import { Column, Entity, Index, JoinColumn, ManyToOne } from "typeorm";
import { SyYear } from "./SyYear";

@Index("IX_SY_Period", ["periodName"], { unique: true })
@Index("PK_SysFiscalPeriod", ["periodId"], { unique: true })
@Entity("SY_Period", { schema: "dbo" })
export class SyPeriod {
  @Column("varchar", { primary: true, name: "PeriodID", length: 6 })
  periodId: string;

  @Column("nvarchar", { name: "PeriodName", unique: true, length: 100 })
  periodName: string;

  @Column("nvarchar", { name: "PeriodName2", nullable: true, length: 100 })
  periodName2: string | null;

  @Column("datetime", { name: "FromDate" })
  fromDate: Date;

  @Column("datetime", { name: "ToDate" })
  toDate: Date;

  @Column("tinyint", { name: "PeriodNo", nullable: true })
  periodNo: number | null;

  @Column("bit", { name: "isLock", nullable: true, default: () => "0" })
  isLock: boolean | null;

  @Column("bit", { name: "isUse", nullable: true, default: () => "0" })
  isUse: boolean | null;

  @ManyToOne(
    () => SyYear,
    syYear => syYear.syPeriods,
    { onDelete: "CASCADE", onUpdate: "CASCADE" }
  )
  @JoinColumn([{ name: "YearID", referencedColumnName: "yearId" }])
  year: SyYear;
}
