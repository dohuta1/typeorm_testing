import { Column, Entity, Index } from "typeorm";

@Index("PK_CF_ChartAccVH", ["accountId"], { unique: true })
@Entity("CF_ChartAccVH", { schema: "dbo" })
export class CfChartAccVh {
  @Column("varchar", { primary: true, name: "AccountID", length: 30 })
  accountId: string;

  @Column("nvarchar", { name: "AccountName", length: 150 })
  accountName: string;

  @Column("varchar", { name: "AccountName2", nullable: true, length: 150 })
  accountName2: string | null;

  @Column("varchar", { name: "CurrencyID", nullable: true, length: 3 })
  currencyId: string | null;

  @Column("varchar", { name: "AccountKindID", length: 2 })
  accountKindId: string;

  @Column("nvarchar", { name: "AccountKindName", nullable: true, length: 100 })
  accountKindName: string | null;

  @Column("varchar", { name: "AccountTypeID", nullable: true, length: 20 })
  accountTypeId: string | null;

  @Column("nvarchar", { name: "AccountTypeName", nullable: true, length: 70 })
  accountTypeName: string | null;

  @Column("bit", { name: "isDetail" })
  isDetail: boolean;

  @Column("bit", { name: "isObject" })
  isObject: boolean;
}
