import { Column, Entity, Index } from "typeorm";

@Index("PK_SY_Setup", ["codeId"], { unique: true })
@Entity("SY_Setup", { schema: "dbo" })
export class SySetup {
  @Column("varchar", { primary: true, name: "CodeID", length: 50 })
  codeId: string;

  @Column("nvarchar", { name: "CodeName", nullable: true, length: 100 })
  codeName: string | null;

  @Column("int", { name: "ValueType", nullable: true })
  valueType: number | null;

  @Column("nvarchar", { name: "CodeValue", nullable: true, length: 1000 })
  codeValue: string | null;

  @Column("varchar", { name: "SourceTable", nullable: true, length: 50 })
  sourceTable: string | null;

  @Column("varchar", { name: "FieldName", nullable: true, length: 50 })
  fieldName: string | null;

  @Column("varchar", { name: "GroupID", nullable: true, length: 2 })
  groupId: string | null;

  @Column("nvarchar", { name: "CodeValue2", nullable: true, length: 1000 })
  codeValue2: string | null;

  @Column("int", { name: "IndexID", nullable: true })
  indexId: number | null;

  @Column("varchar", { name: "Ghichu", nullable: true, length: 200 })
  ghichu: string | null;
}
