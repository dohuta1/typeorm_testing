import { Column, Entity, Index, JoinColumn, ManyToOne } from "typeorm";
import { PlKeHoachSanXuatTbl } from "./PlKeHoachSanXuatTbl";

@Index("PK_PL_KeHoachSanXuatDetailTbl", ["userAutoId"], { unique: true })
@Entity("PL_KeHoachSanXuatDetailTbl", { schema: "dbo" })
export class PlKeHoachSanXuatDetailTbl {
  @Column("varchar", { primary: true, name: "UserAutoID", length: 50 })
  userAutoId: string;

  @Column("varchar", { name: "ItemID", nullable: true, length: 50 })
  itemId: string | null;

  @Column("decimal", {
    name: "KyTruocSL",
    nullable: true,
    precision: 28,
    scale: 2
  })
  kyTruocSl: number | null;

  @Column("decimal", {
    name: "KyTruocKL",
    nullable: true,
    precision: 28,
    scale: 2
  })
  kyTruocKl: number | null;

  @Column("decimal", {
    name: "DoanhThuKytruoc",
    nullable: true,
    precision: 28,
    scale: 0
  })
  doanhThuKytruoc: number | null;

  @Column("decimal", {
    name: "DoanhThukytruocHV",
    nullable: true,
    precision: 28,
    scale: 0
  })
  doanhThukytruocHv: number | null;

  @Column("decimal", {
    name: "KyNaySL",
    nullable: true,
    precision: 28,
    scale: 2
  })
  kyNaySl: number | null;

  @Column("decimal", {
    name: "KyNaySLTT",
    nullable: true,
    precision: 28,
    scale: 2
  })
  kyNaySltt: number | null;

  @Column("decimal", {
    name: "KyNayKL",
    nullable: true,
    precision: 28,
    scale: 2
  })
  kyNayKl: number | null;

  @Column("decimal", {
    name: "KyNayKLTT",
    nullable: true,
    precision: 28,
    scale: 2
  })
  kyNayKltt: number | null;

  @Column("decimal", {
    name: "DoanhThu",
    nullable: true,
    precision: 28,
    scale: 0
  })
  doanhThu: number | null;

  @Column("decimal", {
    name: "DoanhThuTT",
    nullable: true,
    precision: 28,
    scale: 2
  })
  doanhThuTt: number | null;

  @Column("decimal", {
    name: "PTKhachHang",
    nullable: true,
    precision: 18,
    scale: 0
  })
  ptKhachHang: number | null;

  @Column("nvarchar", { name: "GhiChu", nullable: true, length: 150 })
  ghiChu: string | null;

  @Column("varchar", { name: "CostID", nullable: true, length: 50 })
  costId: string | null;

  @Column("varchar", { name: "CostCenterID", nullable: true, length: 50 })
  costCenterId: string | null;

  @Column("varchar", { name: "AccountID", nullable: true, length: 50 })
  accountId: string | null;

  @ManyToOne(
    () => PlKeHoachSanXuatTbl,
    plKeHoachSanXuatTbl => plKeHoachSanXuatTbl.plKeHoachSanXuatDetailTbls,
    { onDelete: "CASCADE", onUpdate: "CASCADE" }
  )
  @JoinColumn([{ name: "MaKeHoach", referencedColumnName: "maKeHoach" }])
  maKeHoach: PlKeHoachSanXuatTbl;
}
