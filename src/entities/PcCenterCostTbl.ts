import { Column, Entity, Index, OneToMany } from "typeorm";
import { PcCenterCostCompleteTbl } from "./PcCenterCostCompleteTbl";
import { PcCenterCostExTbl } from "./PcCenterCostExTbl";
import { PcCenterCostMaterialTbl } from "./PcCenterCostMaterialTbl";
import { PcCenterCostUnCompleteTbl } from "./PcCenterCostUnCompleteTbl";

@Index("PK_PC_CenterCostTbl", ["priceId"], { unique: true })
@Entity("PC_CenterCostTbl", { schema: "dbo" })
export class PcCenterCostTbl {
  @Column("varchar", { primary: true, name: "PriceID", length: 50 })
  priceId: string;

  @Column("varchar", { name: "PeriodID", length: 50 })
  periodId: string;

  @Column("nvarchar", { name: "Memo", nullable: true, length: 50 })
  memo: string | null;

  @Column("bit", { name: "isLock", default: () => "0" })
  isLock: boolean;

  @Column("decimal", {
    name: "TotalCost",
    nullable: true,
    precision: 28,
    scale: 0
  })
  totalCost: number | null;

  @Column("varchar", { name: "UserCreate", nullable: true, length: 30 })
  userCreate: string | null;

  @Column("varchar", { name: "UserUpdate", nullable: true, length: 30 })
  userUpdate: string | null;

  @Column("datetime", { name: "DateUpdate", nullable: true })
  dateUpdate: Date | null;

  @Column("datetime", { name: "DateCreate", nullable: true })
  dateCreate: Date | null;

  @Column("varchar", {
    name: "UnCompleteAccID",
    nullable: true,
    length: 50,
    default: () => "'1541'"
  })
  unCompleteAccId: string | null;

  @OneToMany(
    () => PcCenterCostCompleteTbl,
    pcCenterCostCompleteTbl => pcCenterCostCompleteTbl.price
  )
  pcCenterCostCompleteTbls: PcCenterCostCompleteTbl[];

  @OneToMany(
    () => PcCenterCostExTbl,
    pcCenterCostExTbl => pcCenterCostExTbl.price
  )
  pcCenterCostExTbls: PcCenterCostExTbl[];

  @OneToMany(
    () => PcCenterCostMaterialTbl,
    pcCenterCostMaterialTbl => pcCenterCostMaterialTbl.price
  )
  pcCenterCostMaterialTbls: PcCenterCostMaterialTbl[];

  @OneToMany(
    () => PcCenterCostUnCompleteTbl,
    pcCenterCostUnCompleteTbl => pcCenterCostUnCompleteTbl.price
  )
  pcCenterCostUnCompleteTbls: PcCenterCostUnCompleteTbl[];
}
