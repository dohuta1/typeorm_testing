import { Column, Entity, Index, JoinColumn, ManyToOne } from "typeorm";
import { ArReturnTbl } from "./ArReturnTbl";
import { CfCostCenterTbl } from "./CfCostCenterTbl";
import { CfItemTbl } from "./CfItemTbl";
import { CfStoreHouseTbl } from "./CfStoreHouseTbl";
import { CfCostTbl } from "./CfCostTbl";

@Index("IX_AR_ReturnDetailTbl_DocumentID", ["documentId"], {})
@Index("PK_AR_ReturnDetailTbl", ["userAutoId"], { unique: true })
@Entity("AR_ReturnDetailTbl", { schema: "dbo" })
export class ArReturnDetailTbl {
  @Column("varchar", { primary: true, name: "UserAutoID", length: 40 })
  userAutoId: string;

  @Column("varchar", { name: "DocumentID", length: 30 })
  documentId: string;

  @Column("varchar", { name: "Lot", nullable: true, length: 50 })
  lot: string | null;

  @Column("datetime", { name: "ExpireDate", nullable: true })
  expireDate: Date | null;

  @Column("nvarchar", { name: "Notes", nullable: true, length: 100 })
  notes: string | null;

  @Column("decimal", {
    name: "Quantity",
    nullable: true,
    precision: 28,
    scale: 2
  })
  quantity: number | null;

  @Column("decimal", {
    name: "UnitPrice",
    nullable: true,
    precision: 28,
    scale: 2
  })
  unitPrice: number | null;

  @Column("decimal", {
    name: "SourceAmount",
    nullable: true,
    precision: 28,
    scale: 2,
    default: () => "(0)"
  })
  sourceAmount: number | null;

  @Column("decimal", {
    name: "Amount",
    nullable: true,
    precision: 28,
    scale: 0,
    default: () => "(0)"
  })
  amount: number | null;

  @Column("decimal", {
    name: "DiscountPercent",
    nullable: true,
    precision: 18,
    scale: 2
  })
  discountPercent: number | null;

  @Column("decimal", {
    name: "DiscountAmount",
    nullable: true,
    precision: 28,
    scale: 0,
    default: () => "(0)"
  })
  discountAmount: number | null;

  @Column("decimal", {
    name: "DiscountAmount2",
    nullable: true,
    precision: 28,
    scale: 0,
    default: () => "(0)"
  })
  discountAmount2: number | null;

  @Column("varchar", { name: "VATID", nullable: true, length: 50 })
  vatid: string | null;

  @Column("decimal", {
    name: "VATPercent",
    nullable: true,
    precision: 18,
    scale: 2
  })
  vatPercent: number | null;

  @Column("decimal", {
    name: "VATAmount",
    nullable: true,
    precision: 28,
    scale: 0
  })
  vatAmount: number | null;

  @Column("decimal", {
    name: "TotalAmount",
    nullable: true,
    precision: 28,
    scale: 0
  })
  totalAmount: number | null;

  @Column("decimal", {
    name: "ChietKhau1",
    nullable: true,
    precision: 18,
    scale: 2
  })
  chietKhau1: number | null;

  @Column("decimal", {
    name: "TienChietKhau1",
    nullable: true,
    precision: 18,
    scale: 2
  })
  tienChietKhau1: number | null;

  @Column("decimal", {
    name: "ChietKhau2",
    nullable: true,
    precision: 18,
    scale: 2
  })
  chietKhau2: number | null;

  @Column("decimal", {
    name: "TienChietKhau2",
    nullable: true,
    precision: 18,
    scale: 2
  })
  tienChietKhau2: number | null;

  @Column("decimal", {
    name: "ChietKhau3",
    nullable: true,
    precision: 18,
    scale: 2
  })
  chietKhau3: number | null;

  @Column("decimal", {
    name: "TienChietKhau3",
    nullable: true,
    precision: 18,
    scale: 2
  })
  tienChietKhau3: number | null;

  @Column("decimal", {
    name: "ChietKhau4",
    nullable: true,
    precision: 18,
    scale: 2
  })
  chietKhau4: number | null;

  @Column("decimal", {
    name: "TienChietKhau4",
    nullable: true,
    precision: 18,
    scale: 2
  })
  tienChietKhau4: number | null;

  @Column("varchar", { name: "InvAccID", nullable: true, length: 50 })
  invAccId: string | null;

  @Column("varchar", {
    name: "DiscountAcctID",
    nullable: true,
    length: 50,
    default: () => "(5211)"
  })
  discountAcctId: string | null;

  @Column("varchar", { name: "CostOfSaleAccID", nullable: true, length: 50 })
  costOfSaleAccId: string | null;

  @Column("varchar", { name: "RefDoc", nullable: true, length: 50 })
  refDoc: string | null;

  @Column("float", { name: "UnitCost", nullable: true, precision: 53 })
  unitCost: number | null;

  @Column("decimal", {
    name: "AmountCost",
    nullable: true,
    precision: 28,
    scale: 0
  })
  amountCost: number | null;

  @Column("decimal", {
    name: "Quantity2",
    nullable: true,
    precision: 28,
    scale: 4
  })
  quantity2: number | null;

  @Column("nvarchar", { name: "Property", nullable: true, length: 50 })
  property: string | null;

  @Column("nvarchar", { name: "Property2", nullable: true, length: 50 })
  property2: string | null;

  @Column("varchar", { name: "ParentID", nullable: true, length: 50 })
  parentId: string | null;

  @Column("varchar", {
    name: "ReturnAccID",
    length: 50,
    default: () => "(531)"
  })
  returnAccId: string;

  @Column("float", { name: "UnitFactor", nullable: true, precision: 53 })
  unitFactor: number | null;

  @ManyToOne(
    () => ArReturnTbl,
    arReturnTbl => arReturnTbl.arReturnDetailTbls,
    { onDelete: "CASCADE", onUpdate: "CASCADE" }
  )
  @JoinColumn([{ name: "DocumentID", referencedColumnName: "documentId" }])
  document: ArReturnTbl;

  @ManyToOne(
    () => CfCostCenterTbl,
    cfCostCenterTbl => cfCostCenterTbl.arReturnDetailTbls,
    { onUpdate: "CASCADE" }
  )
  @JoinColumn([{ name: "CostCenterID", referencedColumnName: "costCenterId" }])
  costCenter: CfCostCenterTbl;

  @ManyToOne(
    () => CfItemTbl,
    cfItemTbl => cfItemTbl.arReturnDetailTbls,
    { onUpdate: "CASCADE" }
  )
  @JoinColumn([{ name: "ItemID", referencedColumnName: "itemId" }])
  item: CfItemTbl;

  @ManyToOne(
    () => CfStoreHouseTbl,
    cfStoreHouseTbl => cfStoreHouseTbl.arReturnDetailTbls,
    { onUpdate: "CASCADE" }
  )
  @JoinColumn([{ name: "StoreHouseID", referencedColumnName: "storeHouseId" }])
  storeHouse: CfStoreHouseTbl;

  @ManyToOne(
    () => CfCostTbl,
    cfCostTbl => cfCostTbl.arReturnDetailTbls,
    { onUpdate: "CASCADE" }
  )
  @JoinColumn([{ name: "CostID", referencedColumnName: "costId" }])
  cost: CfCostTbl;
}
