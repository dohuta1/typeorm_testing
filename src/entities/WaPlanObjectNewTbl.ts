import { Column, Entity, Index } from "typeorm";

@Index("PK_WA_PlanObjectNewTbl", ["documentId"], { unique: true })
@Entity("WA_PlanObjectNewTbl", { schema: "dbo" })
export class WaPlanObjectNewTbl {
  @Column("varchar", { primary: true, name: "DocumentID", length: 30 })
  documentId: string;

  @Column("datetime", { name: "DocumentDate" })
  documentDate: Date;

  @Column("varchar", { name: "BranchID", length: 50 })
  branchId: string;

  @Column("varchar", { name: "ManagerID", length: 50 })
  managerId: string;

  @Column("varchar", { name: "EmployeeID", length: 50 })
  employeeId: string;

  @Column("nvarchar", { name: "ObjectName", length: 250 })
  objectName: string;

  @Column("nvarchar", { name: "Address", nullable: true, length: 250 })
  address: string | null;

  @Column("nvarchar", { name: "Phone", nullable: true, length: 50 })
  phone: string | null;

  @Column("nvarchar", { name: "Memo", nullable: true, length: 200 })
  memo: string | null;

  @Column("nvarchar", { name: "Notes", nullable: true })
  notes: string | null;

  @Column("varchar", { name: "UserCreate", nullable: true, length: 30 })
  userCreate: string | null;

  @Column("varchar", { name: "UserUpdate", nullable: true, length: 30 })
  userUpdate: string | null;

  @Column("datetime", { name: "DateUpdate", nullable: true })
  dateUpdate: Date | null;

  @Column("datetime", { name: "DateCreate", nullable: true })
  dateCreate: Date | null;
}
