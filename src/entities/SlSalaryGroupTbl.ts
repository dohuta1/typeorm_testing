import { Column, Entity, Index, OneToMany } from "typeorm";
import { SlBasicSalaryTbl } from "./SlBasicSalaryTbl";
import { SlBonusSalaryTbl } from "./SlBonusSalaryTbl";
import { SlItemGroupTargetTbl } from "./SlItemGroupTargetTbl";
import { SlPunishSalaryTbl } from "./SlPunishSalaryTbl";
import { SlQuaterBonusSalaryTbl } from "./SlQuaterBonusSalaryTbl";
import { SlSalaryGroupEmployeeTbl } from "./SlSalaryGroupEmployeeTbl";
import { SlSalaryRateTbl } from "./SlSalaryRateTbl";

@Index("PK_SL_SalaryGroupTbl_1", ["salaryGroupId"], { unique: true })
@Entity("SL_SalaryGroupTbl", { schema: "dbo" })
export class SlSalaryGroupTbl {
  @Column("varchar", { primary: true, name: "SalaryGroupID", length: 10 })
  salaryGroupId: string;

  @Column("nvarchar", { name: "SalaryGroupName", nullable: true, length: 250 })
  salaryGroupName: string | null;

  @Column("nvarchar", { name: "Memo", nullable: true, length: 200 })
  memo: string | null;

  @Column("nvarchar", { name: "Notes", nullable: true, length: 200 })
  notes: string | null;

  @Column("bit", { name: "isDisable", default: () => "(0)" })
  isDisable: boolean;

  @Column("varchar", { name: "UserCreate", nullable: true, length: 30 })
  userCreate: string | null;

  @Column("varchar", { name: "UserUpdate", nullable: true, length: 30 })
  userUpdate: string | null;

  @Column("datetime", { name: "DateUpdate", nullable: true })
  dateUpdate: Date | null;

  @Column("datetime", { name: "DateCreate", nullable: true })
  dateCreate: Date | null;

  @OneToMany(
    () => SlBasicSalaryTbl,
    slBasicSalaryTbl => slBasicSalaryTbl.salaryGroup
  )
  slBasicSalaryTbls: SlBasicSalaryTbl[];

  @OneToMany(
    () => SlBonusSalaryTbl,
    slBonusSalaryTbl => slBonusSalaryTbl.salaryGroup
  )
  slBonusSalaryTbls: SlBonusSalaryTbl[];

  @OneToMany(
    () => SlItemGroupTargetTbl,
    slItemGroupTargetTbl => slItemGroupTargetTbl.salaryGroup
  )
  slItemGroupTargetTbls: SlItemGroupTargetTbl[];

  @OneToMany(
    () => SlPunishSalaryTbl,
    slPunishSalaryTbl => slPunishSalaryTbl.salaryGroup
  )
  slPunishSalaryTbls: SlPunishSalaryTbl[];

  @OneToMany(
    () => SlQuaterBonusSalaryTbl,
    slQuaterBonusSalaryTbl => slQuaterBonusSalaryTbl.salaryGroup
  )
  slQuaterBonusSalaryTbls: SlQuaterBonusSalaryTbl[];

  @OneToMany(
    () => SlSalaryGroupEmployeeTbl,
    slSalaryGroupEmployeeTbl => slSalaryGroupEmployeeTbl.salaryGroup
  )
  slSalaryGroupEmployeeTbls: SlSalaryGroupEmployeeTbl[];

  @OneToMany(
    () => SlSalaryRateTbl,
    slSalaryRateTbl => slSalaryRateTbl.salaryGroup
  )
  slSalaryRateTbls: SlSalaryRateTbl[];
}
