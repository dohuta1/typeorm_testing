import { Column, Entity, Index } from "typeorm";

@Index("PK_HR_PersonStatusTbl", ["personStatus"], { unique: true })
@Entity("HR_PersonStatusTbl", { schema: "dbo" })
export class HrPersonStatusTbl {
  @Column("int", { primary: true, name: "PersonStatus" })
  personStatus: number;

  @Column("nvarchar", { name: "PersonStatusName", length: 50 })
  personStatusName: string;

  @Column("int", { name: "BackColor", nullable: true })
  backColor: number | null;

  @Column("int", { name: "ForeColor", nullable: true })
  foreColor: number | null;
}
