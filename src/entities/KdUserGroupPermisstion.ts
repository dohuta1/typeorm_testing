import { Column, Entity, Index } from "typeorm";

@Index("PK_KD_UserGroupPermisstion", ["id"], { unique: true })
@Entity("KD_UserGroupPermisstion", { schema: "dbo" })
export class KdUserGroupPermisstion {
  @Column("varchar", {
    primary: true,
    name: "ID",
    length: 50,
    default: () => "newid()"
  })
  id: string;

  @Column("varchar", { name: "UserGroupID", length: 20 })
  userGroupId: string;

  @Column("varchar", { name: "MenuID", length: 50 })
  menuId: string;

  @Column("bit", { name: "IsRun" })
  isRun: boolean;

  @Column("bit", { name: "IsAdd" })
  isAdd: boolean;

  @Column("bit", { name: "IsUpdate" })
  isUpdate: boolean;

  @Column("bit", { name: "IsDelete" })
  isDelete: boolean;

  @Column("bit", { name: "isManager", nullable: true })
  isManager: boolean | null;

  @Column("bit", { name: "isAdmin" })
  isAdmin: boolean;

  @Column("bit", { name: "isAutoLock", nullable: true, default: () => "(0)" })
  isAutoLock: boolean | null;

  @Column("bit", { name: "isHideAmount", nullable: true })
  isHideAmount: boolean | null;
}
