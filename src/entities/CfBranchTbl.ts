import { Column, Entity, Index } from "typeorm";

@Index("PK_CF_BranchTbl", ["branchId"], { unique: true })
@Entity("CF_BranchTbl", { schema: "dbo" })
export class CfBranchTbl {
  @Column("varchar", { primary: true, name: "BranchID", length: 50 })
  branchId: string;

  @Column("nvarchar", { name: "BranchName", length: 100 })
  branchName: string;

  @Column("nvarchar", { name: "BranchAddress", nullable: true, length: 150 })
  branchAddress: string | null;

  @Column("nvarchar", { name: "BranchPhone", nullable: true, length: 150 })
  branchPhone: string | null;

  @Column("bit", { name: "isDefault", default: () => "(0)" })
  isDefault: boolean;

  @Column("varchar", { name: "UserCreate", nullable: true, length: 30 })
  userCreate: string | null;

  @Column("varchar", { name: "UserUpdate", nullable: true, length: 30 })
  userUpdate: string | null;

  @Column("datetime", { name: "DateUpdate", nullable: true })
  dateUpdate: Date | null;

  @Column("datetime", { name: "DateCreate", nullable: true })
  dateCreate: Date | null;

  @Column("nvarchar", {
    name: "ThongTinChuyenKhoan",
    nullable: true,
    length: 500
  })
  thongTinChuyenKhoan: string | null;

  @Column("nvarchar", {
    name: "ThongTinChuyenKhoan2",
    nullable: true,
    length: 500
  })
  thongTinChuyenKhoan2: string | null;

  @Column("nvarchar", { name: "ChamSocKhachHang", nullable: true, length: 500 })
  chamSocKhachHang: string | null;

  @Column("nvarchar", {
    name: "ChamSocKhachHang2",
    nullable: true,
    length: 500
  })
  chamSocKhachHang2: string | null;

  @Column("varchar", { name: "SoDienThoaiCSKH", nullable: true, length: 50 })
  soDienThoaiCskh: string | null;
}
