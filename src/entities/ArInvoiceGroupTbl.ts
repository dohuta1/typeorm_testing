import { Column, Entity, Index, OneToMany } from "typeorm";
import { ArInvoiceGroupObjectTbl } from "./ArInvoiceGroupObjectTbl";

@Index("PK_AR_InvoiceObjectGroupTbl", ["groupId"], { unique: true })
@Entity("AR_InvoiceGroupTbl", { schema: "dbo" })
export class ArInvoiceGroupTbl {
  @Column("int", { primary: true, name: "GroupID" })
  groupId: number;

  @Column("nvarchar", { name: "GroupName", length: 50 })
  groupName: string;

  @Column("nvarchar", { name: "Note", nullable: true, length: 50 })
  note: string | null;

  @OneToMany(
    () => ArInvoiceGroupObjectTbl,
    arInvoiceGroupObjectTbl => arInvoiceGroupObjectTbl.group
  )
  arInvoiceGroupObjectTbls: ArInvoiceGroupObjectTbl[];
}
