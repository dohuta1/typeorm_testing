import { Column, Entity, Index } from "typeorm";

@Index("PK_CF_ZoneTbl", ["zoneId"], { unique: true })
@Entity("CF_ZoneTbl", { schema: "dbo" })
export class CfZoneTbl {
  @Column("nvarchar", { primary: true, name: "ZoneID", length: 50 })
  zoneId: string;

  @Column("nvarchar", { name: "ZoneName", nullable: true, length: 250 })
  zoneName: string | null;

  @Column("nvarchar", { name: "ZoneGroup", nullable: true, length: 250 })
  zoneGroup: string | null;

  @Column("nvarchar", { name: "Notes", nullable: true, length: 50 })
  notes: string | null;

  @Column("decimal", {
    name: "HanMucNoNhanVien",
    nullable: true,
    precision: 18,
    scale: 0
  })
  hanMucNoNhanVien: number | null;
}
