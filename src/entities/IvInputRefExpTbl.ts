import { Column, Entity, Index, JoinColumn, ManyToOne } from "typeorm";
import { IvInputTbl } from "./IvInputTbl";

@Index("PK_IV_InputRefExpTbl", ["userAutoId"], { unique: true })
@Entity("IV_InputRefExpTbl", { schema: "dbo" })
export class IvInputRefExpTbl {
  @Column("varchar", { primary: true, name: "UserAutoID", length: 40 })
  userAutoId: string;

  @Column("varchar", { name: "RefDoc", nullable: true, length: 50 })
  refDoc: string | null;

  @Column("nvarchar", { name: "Memo", nullable: true, length: 150 })
  memo: string | null;

  @Column("decimal", {
    name: "Amount",
    nullable: true,
    precision: 28,
    scale: 2
  })
  amount: number | null;

  @Column("varchar", { name: "PacketGroup", nullable: true, length: 50 })
  packetGroup: string | null;

  @Column("varchar", { name: "AllocateKind", length: 5, default: () => "'ST'" })
  allocateKind: string;

  @Column("varchar", { name: "CostAccID", nullable: true, length: 50 })
  costAccId: string | null;

  @ManyToOne(
    () => IvInputTbl,
    ivInputTbl => ivInputTbl.ivInputRefExpTbls,
    { onDelete: "CASCADE", onUpdate: "CASCADE" }
  )
  @JoinColumn([{ name: "DocumentID", referencedColumnName: "documentId" }])
  document: IvInputTbl;
}
