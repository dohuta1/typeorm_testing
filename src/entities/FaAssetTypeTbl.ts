import { Column, Entity, Index } from "typeorm";

@Index("PK_FA_AssetTypeTbl", ["assetTypeId"], { unique: true })
@Entity("FA_AssetTypeTbl", { schema: "dbo" })
export class FaAssetTypeTbl {
  @Column("varchar", { primary: true, name: "AssetTypeID", length: 50 })
  assetTypeId: string;

  @Column("nvarchar", { name: "AssetTypeName", length: 150 })
  assetTypeName: string;

  @Column("nvarchar", { name: "AssetTypeName2", nullable: true, length: 150 })
  assetTypeName2: string | null;

  @Column("decimal", {
    name: "YearUse",
    precision: 18,
    scale: 2,
    default: () => "0"
  })
  yearUse: number;

  @Column("bit", { name: "isDefault", nullable: true })
  isDefault: boolean | null;
}
