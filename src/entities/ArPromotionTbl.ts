import {
  Column,
  Entity,
  Index,
  JoinColumn,
  ManyToOne,
  OneToMany
} from "typeorm";
import { ArPromotionDenyTbl } from "./ArPromotionDenyTbl";
import { ArPromotionDetailTbl } from "./ArPromotionDetailTbl";
import { ArPromotionExceptTbl } from "./ArPromotionExceptTbl";
import { ArPromotionGiftTbl } from "./ArPromotionGiftTbl";
import { ArPromotionObjectTbl } from "./ArPromotionObjectTbl";
import { ArDiemTbl } from "./ArDiemTbl";

@Index("PK_AR_PromotionTbl", ["documentId"], { unique: true })
@Entity("AR_PromotionTbl", { schema: "dbo" })
export class ArPromotionTbl {
  @Column("varchar", { primary: true, name: "DocumentID", length: 30 })
  documentId: string;

  @Column("datetime", { name: "FromDate" })
  fromDate: Date;

  @Column("datetime", { name: "ToDate" })
  toDate: Date;

  @Column("nvarchar", { name: "TenChuongTrinh", nullable: true, length: 500 })
  tenChuongTrinh: string | null;

  @Column("nvarchar", { name: "NoiDung", nullable: true, length: 1000 })
  noiDung: string | null;

  @Column("bit", { name: "ByOrder", default: () => "(0)" })
  byOrder: boolean;

  @Column("bit", { name: "ByMonth", default: () => "(1)" })
  byMonth: boolean;

  @Column("bit", { name: "ByQuarter", default: () => "(1)" })
  byQuarter: boolean;

  @Column("bit", { name: "ByYear", default: () => "(1)" })
  byYear: boolean;

  @Column("bit", { name: "ByDiem", default: () => "(1)" })
  byDiem: boolean;

  @Column("bit", { name: "ByAmount", default: () => "(0)" })
  byAmount: boolean;

  @Column("bit", { name: "ApplyAll", default: () => "(1)" })
  applyAll: boolean;

  @Column("nvarchar", { name: "Notes", nullable: true, length: 200 })
  notes: string | null;

  @Column("bit", { name: "isDisable", default: () => "(0)" })
  isDisable: boolean;

  @Column("varchar", { name: "UserCreate", nullable: true, length: 30 })
  userCreate: string | null;

  @Column("varchar", { name: "UserUpdate", nullable: true, length: 30 })
  userUpdate: string | null;

  @Column("datetime", { name: "DateUpdate", nullable: true })
  dateUpdate: Date | null;

  @Column("datetime", { name: "DateCreate", nullable: true })
  dateCreate: Date | null;

  @OneToMany(
    () => ArPromotionDenyTbl,
    arPromotionDenyTbl => arPromotionDenyTbl.document
  )
  arPromotionDenyTbls: ArPromotionDenyTbl[];

  @OneToMany(
    () => ArPromotionDetailTbl,
    arPromotionDetailTbl => arPromotionDetailTbl.document
  )
  arPromotionDetailTbls: ArPromotionDetailTbl[];

  @OneToMany(
    () => ArPromotionExceptTbl,
    arPromotionExceptTbl => arPromotionExceptTbl.document
  )
  arPromotionExceptTbls: ArPromotionExceptTbl[];

  @OneToMany(
    () => ArPromotionGiftTbl,
    arPromotionGiftTbl => arPromotionGiftTbl.document
  )
  arPromotionGiftTbls: ArPromotionGiftTbl[];

  @OneToMany(
    () => ArPromotionObjectTbl,
    arPromotionObjectTbl => arPromotionObjectTbl.document
  )
  arPromotionObjectTbls: ArPromotionObjectTbl[];

  @ManyToOne(
    () => ArDiemTbl,
    arDiemTbl => arDiemTbl.arPromotionTbls,
    { onUpdate: "CASCADE" }
  )
  @JoinColumn([{ name: "DiemTichLuy", referencedColumnName: "documentId" }])
  diemTichLuy: ArDiemTbl;
}
