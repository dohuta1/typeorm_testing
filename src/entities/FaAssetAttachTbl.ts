import { Column, Entity, Index, JoinColumn, ManyToOne } from "typeorm";
import { FaAssetTbl } from "./FaAssetTbl";

@Index("PK_FA_AssetAttachTbl", ["userAutoId"], { unique: true })
@Entity("FA_AssetAttachTbl", { schema: "dbo" })
export class FaAssetAttachTbl {
  @Column("varchar", { primary: true, name: "UserAutoID", length: 50 })
  userAutoId: string;

  @Column("nvarchar", { name: "FileName", nullable: true, length: 250 })
  fileName: string | null;

  @Column("int", { name: "FileType", nullable: true, default: () => "0" })
  fileType: number | null;

  @Column("nvarchar", { name: "STT", nullable: true, length: 50 })
  stt: string | null;

  @Column("image", { name: "Content", nullable: true })
  content: Buffer | null;

  @Column("image", { name: "Content2", nullable: true })
  content2: Buffer | null;

  @ManyToOne(
    () => FaAssetTbl,
    faAssetTbl => faAssetTbl.faAssetAttachTbls,
    { onDelete: "CASCADE", onUpdate: "CASCADE" }
  )
  @JoinColumn([{ name: "AssetID", referencedColumnName: "assetId" }])
  asset: FaAssetTbl;
}
