import { Column, Entity, Index, JoinColumn, ManyToOne } from "typeorm";
import { ArPromotionTbl } from "./ArPromotionTbl";
import { CfItemTbl } from "./CfItemTbl";

@Index("PK_AR_PromotionDetailTbl", ["userAutoId"], { unique: true })
@Entity("AR_PromotionDetailTbl", { schema: "dbo" })
export class ArPromotionDetailTbl {
  @Column("varchar", { primary: true, name: "UserAutoID", length: 40 })
  userAutoId: string;

  @Column("decimal", {
    name: "FromQuantity",
    nullable: true,
    precision: 28,
    scale: 2,
    default: () => "(1)"
  })
  fromQuantity: number | null;

  @Column("decimal", {
    name: "ToQuantity",
    nullable: true,
    precision: 28,
    scale: 2,
    default: () => "(0)"
  })
  toQuantity: number | null;

  @Column("decimal", {
    name: "DiemTichLuy",
    nullable: true,
    precision: 28,
    scale: 2
  })
  diemTichLuy: number | null;

  @Column("decimal", {
    name: "ChietKhau",
    nullable: true,
    precision: 28,
    scale: 2
  })
  chietKhau: number | null;

  @Column("varchar", { name: "PromotionItemID", nullable: true, length: 50 })
  promotionItemId: string | null;

  @Column("decimal", {
    name: "PromotionQuantity",
    nullable: true,
    precision: 28,
    scale: 2
  })
  promotionQuantity: number | null;

  @Column("varchar", { name: "PromotionItemID2", nullable: true, length: 50 })
  promotionItemId2: string | null;

  @Column("decimal", {
    name: "PromotionQuantity2",
    nullable: true,
    precision: 28,
    scale: 2
  })
  promotionQuantity2: number | null;

  @Column("nvarchar", { name: "Notes", nullable: true, length: 100 })
  notes: string | null;

  @ManyToOne(
    () => ArPromotionTbl,
    arPromotionTbl => arPromotionTbl.arPromotionDetailTbls,
    { onDelete: "CASCADE", onUpdate: "CASCADE" }
  )
  @JoinColumn([{ name: "DocumentID", referencedColumnName: "documentId" }])
  document: ArPromotionTbl;

  @ManyToOne(
    () => CfItemTbl,
    cfItemTbl => cfItemTbl.arPromotionDetailTbls,
    { onUpdate: "CASCADE" }
  )
  @JoinColumn([{ name: "ItemID", referencedColumnName: "itemId" }])
  item: CfItemTbl;
}
