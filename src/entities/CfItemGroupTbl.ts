import { Column, Entity, Index } from "typeorm";

@Index("PK_CF_ItemGroupTbl", ["itemGroupId"], { unique: true })
@Entity("CF_ItemGroupTbl", { schema: "dbo" })
export class CfItemGroupTbl {
  @Column("varchar", { primary: true, name: "ItemGroupID", length: 50 })
  itemGroupId: string;

  @Column("nvarchar", { name: "ItemGroupName", length: 100 })
  itemGroupName: string;

  @Column("nvarchar", { name: "Notes", nullable: true, length: 100 })
  notes: string | null;

  @Column("varchar", { name: "ParentItemGroupID", nullable: true, length: 50 })
  parentItemGroupId: string | null;

  @Column("bit", { name: "isDefault", default: () => "0" })
  isDefault: boolean;

  @Column("varchar", { name: "UserCreate", nullable: true, length: 30 })
  userCreate: string | null;

  @Column("varchar", { name: "UserUpdate", nullable: true, length: 30 })
  userUpdate: string | null;

  @Column("datetime", { name: "DateUpdate", nullable: true })
  dateUpdate: Date | null;

  @Column("datetime", { name: "DateCreate", nullable: true })
  dateCreate: Date | null;
}
