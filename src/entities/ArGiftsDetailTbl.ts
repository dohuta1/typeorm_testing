import { Column, Entity, Index, JoinColumn, ManyToOne } from "typeorm";
import { ArGiftsTbl } from "./ArGiftsTbl";

@Index("PK_AR_GiftsDetailTbl", ["userAutoId"], { unique: true })
@Entity("AR_GiftsDetailTbl", { schema: "dbo" })
export class ArGiftsDetailTbl {
  @Column("varchar", { primary: true, name: "UserAutoID", length: 40 })
  userAutoId: string;

  @Column("varchar", { name: "ItemID", length: 50 })
  itemId: string;

  @Column("decimal", {
    name: "InvoiceQuantity",
    nullable: true,
    precision: 28,
    scale: 2,
    default: () => "(1)"
  })
  invoiceQuantity: number | null;

  @Column("decimal", {
    name: "InvoiceAmount",
    nullable: true,
    precision: 28,
    scale: 2,
    default: () => "(0)"
  })
  invoiceAmount: number | null;

  @Column("nvarchar", { name: "Notes", nullable: true, length: 100 })
  notes: string | null;

  @Column("varchar", { name: "GiftsItemID", length: 50 })
  giftsItemId: string;

  @Column("decimal", {
    name: "GiftsQuantity",
    nullable: true,
    precision: 28,
    scale: 2,
    default: () => "(0)"
  })
  giftsQuantity: number | null;

  @Column("decimal", {
    name: "DiscountPercentTT",
    nullable: true,
    precision: 18,
    scale: 0
  })
  discountPercentTt: number | null;

  @ManyToOne(
    () => ArGiftsTbl,
    arGiftsTbl => arGiftsTbl.arGiftsDetailTbls,
    { onDelete: "CASCADE", onUpdate: "CASCADE" }
  )
  @JoinColumn([{ name: "DocumentID", referencedColumnName: "documentId" }])
  document: ArGiftsTbl;
}
