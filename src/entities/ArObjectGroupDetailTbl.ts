import { Column, Entity, Index, JoinColumn, ManyToOne } from "typeorm";
import { ArObjectGroupTbl } from "./ArObjectGroupTbl";
import { CfObjectTbl } from "./CfObjectTbl";

@Index("PK_AR_ObjectGroupDetailTbl", ["userAutoId"], { unique: true })
@Entity("AR_ObjectGroupDetailTbl", { schema: "dbo" })
export class ArObjectGroupDetailTbl {
  @Column("varchar", { primary: true, name: "UserAutoID", length: 50 })
  userAutoId: string;

  @ManyToOne(
    () => ArObjectGroupTbl,
    arObjectGroupTbl => arObjectGroupTbl.arObjectGroupDetailTbls,
    { onDelete: "CASCADE", onUpdate: "CASCADE" }
  )
  @JoinColumn([
    { name: "ObjectGroupCode", referencedColumnName: "objectGroupCode" }
  ])
  objectGroupCode: ArObjectGroupTbl;

  @ManyToOne(
    () => CfObjectTbl,
    cfObjectTbl => cfObjectTbl.arObjectGroupDetailTbls,
    { onUpdate: "CASCADE" }
  )
  @JoinColumn([{ name: "ObjectID", referencedColumnName: "objectId" }])
  object: CfObjectTbl;
}
