import { Column, Entity, Index, JoinColumn, ManyToOne } from "typeorm";
import { CfObjectTbl } from "./CfObjectTbl";
import { CfItemTbl } from "./CfItemTbl";

@Index("PK_AR_ObjectAgencyStockTbl", ["userAutoId"], { unique: true })
@Entity("AR_ObjectAgencyStockTbl", { schema: "dbo" })
export class ArObjectAgencyStockTbl {
  @Column("varchar", { primary: true, name: "UserAutoID", length: 50 })
  userAutoId: string;

  @Column("decimal", {
    name: "MinQuantity",
    nullable: true,
    precision: 18,
    scale: 2,
    default: () => "(0)"
  })
  minQuantity: number | null;

  @Column("decimal", {
    name: "MaxQuantity",
    nullable: true,
    precision: 18,
    scale: 2,
    default: () => "(0)"
  })
  maxQuantity: number | null;

  @Column("nvarchar", { name: "Notes", nullable: true, length: 250 })
  notes: string | null;

  @ManyToOne(
    () => CfObjectTbl,
    cfObjectTbl => cfObjectTbl.arObjectAgencyStockTbls,
    { onDelete: "CASCADE", onUpdate: "CASCADE" }
  )
  @JoinColumn([{ name: "ObjectID", referencedColumnName: "objectId" }])
  object: CfObjectTbl;

  @ManyToOne(
    () => CfItemTbl,
    cfItemTbl => cfItemTbl.arObjectAgencyStockTbls,
    { onDelete: "CASCADE", onUpdate: "CASCADE" }
  )
  @JoinColumn([{ name: "ItemID", referencedColumnName: "itemId" }])
  item: CfItemTbl;
}
