import { Column, Entity, Index } from "typeorm";

@Index("PK_SY_FormControl", ["userAutoId"], { unique: true })
@Entity("SY_FrmCtrTbl", { schema: "dbo" })
export class SyFrmCtrTbl {
  @Column("varchar", { primary: true, name: "UserAutoID", length: 50 })
  userAutoId: string;

  @Column("varchar", { name: "FormID", nullable: true, length: 250 })
  formId: string | null;

  @Column("nvarchar", { name: "ParentCtlName", nullable: true, length: 500 })
  parentCtlName: string | null;

  @Column("varchar", { name: "CtlName", length: 50 })
  ctlName: string;

  @Column("nvarchar", { name: "CaptionVN", length: 150 })
  captionVn: string;

  @Column("nvarchar", { name: "CaptionEN", length: 150 })
  captionEn: string;

  @Column("int", { name: "CtlTop", default: () => "0" })
  ctlTop: number;

  @Column("int", { name: "CtlLeft", default: () => "0" })
  ctlLeft: number;

  @Column("int", { name: "CaptionWidth", default: () => "0" })
  captionWidth: number;

  @Column("int", { name: "Width", default: () => "0" })
  width: number;

  @Column("int", { name: "Height", default: () => "0" })
  height: number;

  @Column("int", { name: "OrderBy", default: () => "5" })
  orderBy: number;

  @Column("bit", { name: "IsDisable", default: () => "0" })
  isDisable: boolean;

  @Column("nvarchar", { name: "CaptionCH", nullable: true, length: 200 })
  captionCh: string | null;
}
