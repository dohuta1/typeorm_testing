import { Column, Entity, Index, JoinColumn, ManyToOne } from "typeorm";
import { HrAllowanceTbl } from "./HrAllowanceTbl";

@Index("PK_HR_AllowanceDetailTbl", ["userAutoId"], { unique: true })
@Entity("HR_AllowanceDetailTbl", { schema: "dbo" })
export class HrAllowanceDetailTbl {
  @Column("varchar", { primary: true, name: "UserAutoID", length: 40 })
  userAutoId: string;

  @Column("varchar", { name: "PersonID", length: 50 })
  personId: string;

  @Column("nvarchar", {
    name: "NoiDung",
    length: 250,
    default: () => "N'Phụ cấp khác'"
  })
  noiDung: string;

  @Column("decimal", {
    name: "SoTien",
    nullable: true,
    precision: 28,
    scale: 0
  })
  soTien: number | null;

  @Column("nvarchar", { name: "GhiChu", nullable: true, length: 250 })
  ghiChu: string | null;

  @ManyToOne(
    () => HrAllowanceTbl,
    hrAllowanceTbl => hrAllowanceTbl.hrAllowanceDetailTbls,
    { onDelete: "CASCADE", onUpdate: "CASCADE" }
  )
  @JoinColumn([{ name: "DocumentID", referencedColumnName: "documentId" }])
  document: HrAllowanceTbl;
}
