import { Column, Entity, Index, OneToMany } from "typeorm";
import { CsPaymentPlanTbl } from "./CsPaymentPlanTbl";

@Index("PK_CS_PaymentPlanStatusTbl", ["statusId"], { unique: true })
@Entity("CS_PaymentPlanStatusTbl", { schema: "dbo" })
export class CsPaymentPlanStatusTbl {
  @Column("int", { primary: true, name: "StatusID" })
  statusId: number;

  @Column("nvarchar", { name: "StatusName", length: 50 })
  statusName: string;

  @Column("int", { name: "BackColor", nullable: true })
  backColor: number | null;

  @Column("int", { name: "ForeColor", nullable: true })
  foreColor: number | null;

  @OneToMany(
    () => CsPaymentPlanTbl,
    csPaymentPlanTbl => csPaymentPlanTbl.status
  )
  csPaymentPlanTbls: CsPaymentPlanTbl[];
}
