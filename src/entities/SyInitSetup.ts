import { Column, Entity, PrimaryColumn } from "typeorm";

@Entity("SY_InitSetup", { schema: "dbo" })
export class SyInitSetup {
  @PrimaryColumn("varchar", { name: "PeriodID", length: 10 })
  periodId: string;

  @Column("varchar", { name: "BalancePeriodID", length: 10 })
  balancePeriodId: string;

  @Column("datetime", { name: "StartDate" })
  startDate: Date;

  @Column("varchar", { name: "CostMethod", length: 50 })
  costMethod: string;
}
