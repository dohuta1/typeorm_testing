import { Column, Entity, Index, JoinColumn, ManyToOne } from "typeorm";
import { PcProductCostTbl } from "./PcProductCostTbl";

@Index("PK_PC_ProdustCostMaterialTbl", ["userAutoId"], { unique: true })
@Entity("PC_ProdustCostMaterialTbl", { schema: "dbo" })
export class PcProdustCostMaterialTbl {
  @Column("varchar", { primary: true, name: "UserAutoID", length: 50 })
  userAutoId: string;

  @Column("varchar", { name: "AccountID", length: 50 })
  accountId: string;

  @Column("varchar", { name: "CostCenterID", nullable: true, length: 50 })
  costCenterId: string | null;

  @Column("varchar", { name: "CostID", nullable: true, length: 50 })
  costId: string | null;

  @Column("varchar", { name: "MaterialID", length: 50 })
  materialId: string;

  @Column("decimal", {
    name: "Quantity",
    nullable: true,
    precision: 28,
    scale: 4
  })
  quantity: number | null;

  @Column("decimal", {
    name: "Amount",
    nullable: true,
    precision: 28,
    scale: 0,
    default: () => "0"
  })
  amount: number | null;

  @Column("decimal", {
    name: "AmountAllocate",
    nullable: true,
    precision: 28,
    scale: 0,
    default: () => "0"
  })
  amountAllocate: number | null;

  @Column("bit", { name: "isWaste", nullable: true, default: () => "0" })
  isWaste: boolean | null;

  @Column("bit", { name: "isBalance", nullable: true, default: () => "0" })
  isBalance: boolean | null;

  @Column("int", { name: "ALType", default: () => "1" })
  alType: number;

  @Column("nvarchar", { name: "Notes", nullable: true, length: 50 })
  notes: string | null;

  @Column("decimal", {
    name: "CostAmount",
    nullable: true,
    precision: 28,
    scale: 2
  })
  costAmount: number | null;

  @ManyToOne(
    () => PcProductCostTbl,
    pcProductCostTbl => pcProductCostTbl.pcProdustCostMaterialTbls,
    { onDelete: "CASCADE", onUpdate: "CASCADE" }
  )
  @JoinColumn([{ name: "PriceID", referencedColumnName: "priceId" }])
  price: PcProductCostTbl;
}
