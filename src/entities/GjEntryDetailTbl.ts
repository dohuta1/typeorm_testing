import { Column, Entity, Index, JoinColumn, ManyToOne } from "typeorm";
import { CfCostCenterTbl } from "./CfCostCenterTbl";
import { GjEntryTbl } from "./GjEntryTbl";
import { CfCostTbl } from "./CfCostTbl";

@Index("IX_GJ_EntryDetailTbl_DocumentID", ["documentId"], {})
@Index("PK_GJ_EntryDetailTbl", ["userAutoId"], { unique: true })
@Entity("GJ_EntryDetailTbl", { schema: "dbo" })
export class GjEntryDetailTbl {
  @Column("varchar", { primary: true, name: "UserAutoID", length: 40 })
  userAutoId: string;

  @Column("varchar", { name: "DocumentID", length: 30 })
  documentId: string;

  @Column("varchar", { name: "DebitAccID", length: 50 })
  debitAccId: string;

  @Column("varchar", { name: "CreditAccID", length: 50 })
  creditAccId: string;

  @Column("nvarchar", { name: "Memo", nullable: true, length: 250 })
  memo: string | null;

  @Column("decimal", {
    name: "SourceAmount",
    precision: 28,
    scale: 2,
    default: () => "(0)"
  })
  sourceAmount: number;

  @Column("decimal", {
    name: "Amount",
    precision: 28,
    scale: 0,
    default: () => "(0)"
  })
  amount: number;

  @Column("decimal", {
    name: "TienChietKhau1",
    nullable: true,
    precision: 18,
    scale: 2
  })
  tienChietKhau1: number | null;

  @Column("decimal", {
    name: "TienChietKhau2",
    nullable: true,
    precision: 18,
    scale: 2
  })
  tienChietKhau2: number | null;

  @Column("decimal", {
    name: "TienChietKhau3",
    nullable: true,
    precision: 18,
    scale: 2
  })
  tienChietKhau3: number | null;

  @Column("decimal", {
    name: "TienChietKhau4",
    nullable: true,
    precision: 18,
    scale: 2
  })
  tienChietKhau4: number | null;

  @Column("varchar", { name: "ObjectID", nullable: true, length: 50 })
  objectId: string | null;

  @Column("varchar", { name: "ItemID", nullable: true, length: 50 })
  itemId: string | null;

  @Column("nvarchar", { name: "ContactCode", nullable: true, length: 50 })
  contactCode: string | null;

  @Column("varchar", { name: "RefDoc", nullable: true, length: 50 })
  refDoc: string | null;

  @Column("varchar", { name: "ObjectID2", nullable: true, length: 50 })
  objectId2: string | null;

  @Column("varchar", { name: "RefDoc2", nullable: true, length: 50 })
  refDoc2: string | null;

  @Column("varchar", { name: "LinkItem", nullable: true, length: 50 })
  linkItem: string | null;

  @Column("varchar", { name: "ContractID", nullable: true, length: 50 })
  contractId: string | null;

  @Column("varchar", { name: "ParentID", nullable: true, length: 50 })
  parentId: string | null;

  @Column("decimal", {
    name: "Quantity",
    nullable: true,
    precision: 18,
    scale: 2,
    default: () => "(0)"
  })
  quantity: number | null;

  @Column("decimal", {
    name: "UnitPrice",
    nullable: true,
    precision: 18,
    scale: 2
  })
  unitPrice: number | null;

  @Column("varchar", { name: "EmployeeID", nullable: true, length: 50 })
  employeeId: string | null;

  @ManyToOne(
    () => CfCostCenterTbl,
    cfCostCenterTbl => cfCostCenterTbl.gjEntryDetailTbls,
    { onUpdate: "CASCADE" }
  )
  @JoinColumn([{ name: "CostCenterID", referencedColumnName: "costCenterId" }])
  costCenter: CfCostCenterTbl;

  @ManyToOne(
    () => GjEntryTbl,
    gjEntryTbl => gjEntryTbl.gjEntryDetailTbls,
    { onDelete: "CASCADE", onUpdate: "CASCADE" }
  )
  @JoinColumn([{ name: "DocumentID", referencedColumnName: "documentId" }])
  document: GjEntryTbl;

  @ManyToOne(
    () => CfCostTbl,
    cfCostTbl => cfCostTbl.gjEntryDetailTbls,
    { onUpdate: "CASCADE" }
  )
  @JoinColumn([{ name: "CostID", referencedColumnName: "costId" }])
  cost: CfCostTbl;
}
