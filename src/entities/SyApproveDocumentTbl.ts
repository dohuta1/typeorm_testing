import { Column, Entity, Index } from "typeorm";

@Index("PK_SY_ApproveDocumentTbl", ["userAutoId"], { unique: true })
@Entity("SY_ApproveDocumentTbl", { schema: "dbo" })
export class SyApproveDocumentTbl {
  @Column("varchar", { primary: true, name: "UserAutoID", length: 50 })
  userAutoId: string;

  @Column("varchar", { name: "SourceID", length: 50 })
  sourceId: string;

  @Column("varchar", { name: "DocumentID", length: 50 })
  documentId: string;

  @Column("datetime", { name: "DocumentDate", default: () => "getdate()" })
  documentDate: Date;

  @Column("varchar", { name: "BranchID", nullable: true, length: 50 })
  branchId: string | null;

  @Column("varchar", { name: "UserLevel1", nullable: true, length: 50 })
  userLevel1: string | null;

  @Column("varchar", { name: "UserLevel2", nullable: true, length: 50 })
  userLevel2: string | null;

  @Column("varchar", { name: "UserLevel3", nullable: true, length: 50 })
  userLevel3: string | null;

  @Column("varchar", { name: "UserLevel4", nullable: true, length: 50 })
  userLevel4: string | null;

  @Column("varchar", { name: "UserLevel5", nullable: true, length: 50 })
  userLevel5: string | null;

  @Column("nvarchar", { name: "UserCaption1", nullable: true, length: 50 })
  userCaption1: string | null;

  @Column("nvarchar", { name: "UserCaption2", nullable: true, length: 50 })
  userCaption2: string | null;

  @Column("nvarchar", { name: "UserCaption3", nullable: true, length: 50 })
  userCaption3: string | null;

  @Column("nvarchar", { name: "UserCaption4", nullable: true, length: 50 })
  userCaption4: string | null;

  @Column("nvarchar", { name: "UserCaption5", nullable: true, length: 50 })
  userCaption5: string | null;

  @Column("nvarchar", { name: "UserRole1", nullable: true, length: 50 })
  userRole1: string | null;

  @Column("nvarchar", { name: "UserRole2", nullable: true, length: 50 })
  userRole2: string | null;

  @Column("nvarchar", { name: "UserRole3", nullable: true, length: 50 })
  userRole3: string | null;

  @Column("nvarchar", { name: "UserRole4", nullable: true, length: 50 })
  userRole4: string | null;

  @Column("nvarchar", { name: "UserRole5", nullable: true, length: 50 })
  userRole5: string | null;

  @Column("int", { name: "isApprove1", nullable: true })
  isApprove1: number | null;

  @Column("int", { name: "isApprove2", nullable: true })
  isApprove2: number | null;

  @Column("int", { name: "isApprove3", nullable: true })
  isApprove3: number | null;

  @Column("int", { name: "isApprove4", nullable: true })
  isApprove4: number | null;

  @Column("int", { name: "isApprove5", nullable: true })
  isApprove5: number | null;

  @Column("datetime", { name: "ApproveDate1", nullable: true })
  approveDate1: Date | null;

  @Column("datetime", { name: "ApproveDate2", nullable: true })
  approveDate2: Date | null;

  @Column("datetime", { name: "ApproveDate3", nullable: true })
  approveDate3: Date | null;

  @Column("datetime", { name: "ApproveDate4", nullable: true })
  approveDate4: Date | null;

  @Column("datetime", { name: "ApproveDate5", nullable: true })
  approveDate5: Date | null;

  @Column("nvarchar", { name: "ApproveNote1", nullable: true, length: 150 })
  approveNote1: string | null;

  @Column("nvarchar", { name: "ApproveNote2", nullable: true, length: 150 })
  approveNote2: string | null;

  @Column("nvarchar", { name: "ApproveNote3", nullable: true, length: 150 })
  approveNote3: string | null;

  @Column("nvarchar", { name: "ApproveNote4", nullable: true, length: 150 })
  approveNote4: string | null;

  @Column("nvarchar", { name: "ApproveNote5", nullable: true, length: 150 })
  approveNote5: string | null;

  @Column("varchar", { name: "ToApproveID", nullable: true, length: 50 })
  toApproveId: string | null;

  @Column("varchar", { name: "ToApproveID2", nullable: true, length: 50 })
  toApproveId2: string | null;

  @Column("varchar", { name: "ToUser1", nullable: true, length: 50 })
  toUser1: string | null;

  @Column("varchar", { name: "ToUser2", nullable: true, length: 50 })
  toUser2: string | null;

  @Column("bit", { name: "isFinish", default: () => "0" })
  isFinish: boolean;

  @Column("bit", { name: "isFinishTo", default: () => "0" })
  isFinishTo: boolean;

  @Column("varchar", { name: "Notifier1", nullable: true, length: 50 })
  notifier1: string | null;

  @Column("varchar", { name: "Notifier2", nullable: true, length: 50 })
  notifier2: string | null;

  @Column("bit", { name: "isNotifier1", default: () => "0" })
  isNotifier1: boolean;

  @Column("bit", { name: "isNotifier2", default: () => "0" })
  isNotifier2: boolean;
}
