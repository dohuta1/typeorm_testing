import { Column, Entity, Index } from "typeorm";

@Index("PK_SY_FormExpression", ["userAutoId"], { unique: true })
@Entity("SY_FrmExpTbl", { schema: "dbo" })
export class SyFrmExpTbl {
  @Column("varchar", { primary: true, name: "UserAutoID", length: 40 })
  userAutoId: string;

  @Column("varchar", { name: "FormID", nullable: true, length: 250 })
  formId: string | null;

  @Column("varchar", { name: "DataName", length: 50 })
  dataName: string;

  @Column("int", { name: "Oderby", default: () => "6" })
  oderby: number;

  @Column("varchar", { name: "ColumnID", length: 50 })
  columnId: string;

  @Column("nvarchar", { name: "Expression", nullable: true, length: 500 })
  expression: string | null;

  @Column("varchar", { name: "RefColumn", nullable: true, length: 500 })
  refColumn: string | null;

  @Column("varchar", { name: "ParaArr", nullable: true, length: 100 })
  paraArr: string | null;

  @Column("bit", { name: "IsDisable", default: () => "0" })
  isDisable: boolean;

  @Column("int", { name: "LevelLoop", nullable: true })
  levelLoop: number | null;
}
