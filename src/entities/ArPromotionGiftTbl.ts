import { Column, Entity, Index, JoinColumn, ManyToOne } from "typeorm";
import { ArPromotionTbl } from "./ArPromotionTbl";

@Index("PK_AR_PromotionGiftTbl", ["userAutoId"], { unique: true })
@Entity("AR_PromotionGiftTbl", { schema: "dbo" })
export class ArPromotionGiftTbl {
  @Column("varchar", {
    primary: true,
    name: "UserAutoID",
    length: 50,
    default: () => "newid()"
  })
  userAutoId: string;

  @Column("decimal", { name: "TuDiem", precision: 18, scale: 0 })
  tuDiem: number;

  @Column("decimal", { name: "DenDiem", precision: 18, scale: 0 })
  denDiem: number;

  @Column("nvarchar", { name: "QuaTang", length: 150 })
  quaTang: string;

  @Column("nvarchar", { name: "Notes", nullable: true, length: 250 })
  notes: string | null;

  @ManyToOne(
    () => ArPromotionTbl,
    arPromotionTbl => arPromotionTbl.arPromotionGiftTbls,
    { onDelete: "CASCADE", onUpdate: "CASCADE" }
  )
  @JoinColumn([{ name: "DocumentID", referencedColumnName: "documentId" }])
  document: ArPromotionTbl;
}
