import { Column, Entity, Index, JoinColumn, ManyToOne } from "typeorm";
import { FaLiquidateTbl } from "./FaLiquidateTbl";

@Index("PK_FA_LiquidateDetailTbl", ["userAutoId"], { unique: true })
@Entity("FA_LiquidateDetailTbl", { schema: "dbo" })
export class FaLiquidateDetailTbl {
  @Column("varchar", { primary: true, name: "UserAutoID", length: 40 })
  userAutoId: string;

  @Column("varchar", { name: "AssetID", nullable: true, length: 30 })
  assetId: string | null;

  @Column("decimal", {
    name: "ResidualAmount",
    nullable: true,
    precision: 28,
    scale: 0
  })
  residualAmount: number | null;

  @Column("decimal", {
    name: "LiquidateAmount",
    nullable: true,
    precision: 28,
    scale: 0
  })
  liquidateAmount: number | null;

  @Column("varchar", { name: "IncomeAccID", nullable: true, length: 50 })
  incomeAccId: string | null;

  @Column("nvarchar", { name: "Notes", nullable: true, length: 150 })
  notes: string | null;

  @Column("decimal", {
    name: "ChiPhiTL",
    nullable: true,
    precision: 28,
    scale: 0
  })
  chiPhiTl: number | null;

  @Column("varchar", { name: "ExpenseAccID", nullable: true, length: 50 })
  expenseAccId: string | null;

  @ManyToOne(
    () => FaLiquidateTbl,
    faLiquidateTbl => faLiquidateTbl.faLiquidateDetailTbls,
    { onDelete: "CASCADE", onUpdate: "CASCADE" }
  )
  @JoinColumn([{ name: "DocumentID", referencedColumnName: "documentId" }])
  document: FaLiquidateTbl;
}
