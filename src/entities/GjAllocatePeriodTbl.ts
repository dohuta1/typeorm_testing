import { Column, Entity, Index, JoinColumn, ManyToOne } from "typeorm";
import { GjAllocateSetupTbl } from "./GjAllocateSetupTbl";

@Index("PK_GJ_AllocatePeriodTbl", ["userAutoId"], { unique: true })
@Entity("GJ_AllocatePeriodTbl", { schema: "dbo" })
export class GjAllocatePeriodTbl {
  @Column("varchar", { primary: true, name: "UserAutoID", length: 50 })
  userAutoId: string;

  @Column("varchar", { name: "PeriodID", length: 50 })
  periodId: string;

  @Column("nvarchar", { name: "AllocateName", length: 100 })
  allocateName: string;

  @Column("varchar", { name: "DocumentID", length: 50 })
  documentId: string;

  @Column("datetime", { name: "DocumentDate" })
  documentDate: Date;

  @Column("decimal", { name: "Amount", precision: 28, scale: 0 })
  amount: number;

  @Column("decimal", {
    name: "OpenAllocate",
    nullable: true,
    precision: 28,
    scale: 0
  })
  openAllocate: number | null;

  @Column("decimal", {
    name: "PeriodAmount",
    nullable: true,
    precision: 28,
    scale: 0
  })
  periodAmount: number | null;

  @Column("decimal", {
    name: "EndAllocate",
    nullable: true,
    precision: 28,
    scale: 0
  })
  endAllocate: number | null;

  @Column("decimal", {
    name: "RemainAmount",
    nullable: true,
    precision: 28,
    scale: 0
  })
  remainAmount: number | null;

  @Column("varchar", { name: "ExpenseAccID", length: 50 })
  expenseAccId: string;

  @Column("varchar", { name: "CostCenterID", nullable: true, length: 50 })
  costCenterId: string | null;

  @Column("varchar", { name: "CostID", nullable: true, length: 50 })
  costId: string | null;

  @ManyToOne(
    () => GjAllocateSetupTbl,
    gjAllocateSetupTbl => gjAllocateSetupTbl.gjAllocatePeriodTbls,
    { onDelete: "CASCADE", onUpdate: "CASCADE" }
  )
  @JoinColumn([{ name: "AllocateID", referencedColumnName: "allocateId" }])
  allocate: GjAllocateSetupTbl;
}
