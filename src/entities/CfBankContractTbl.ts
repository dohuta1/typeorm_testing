import { Column, Entity, Index, OneToMany } from "typeorm";
import { CsPaymentDetailTbl } from "./CsPaymentDetailTbl";
import { CsReceiptDetailTbl } from "./CsReceiptDetailTbl";

@Index("PK_CF_BankContractTbl", ["bankContractId"], { unique: true })
@Entity("CF_BankContractTbl", { schema: "dbo" })
export class CfBankContractTbl {
  @Column("varchar", { primary: true, name: "BankContractID", length: 50 })
  bankContractId: string;

  @Column("nvarchar", { name: "BankContractName", length: 50 })
  bankContractName: string;

  @Column("datetime", { name: "NgayVay" })
  ngayVay: Date;

  @Column("varchar", { name: "AccountID", length: 50, default: () => "311" })
  accountId: string;

  @Column("decimal", { name: "SourceAmount", precision: 28, scale: 4 })
  sourceAmount: number;

  @Column("varchar", { name: "CurrencyID", length: 3, default: () => "'VND'" })
  currencyId: string;

  @Column("decimal", {
    name: "RateExchange",
    precision: 18,
    scale: 4,
    default: () => "1"
  })
  rateExchange: number;

  @Column("decimal", { name: "Amount", precision: 28, scale: 0 })
  amount: number;

  @Column("decimal", {
    name: "LaiSuat",
    nullable: true,
    precision: 18,
    scale: 4
  })
  laiSuat: number | null;

  @Column("datetime", { name: "NgayDaoHan", nullable: true })
  ngayDaoHan: Date | null;

  @Column("nvarchar", { name: "Notes", nullable: true, length: 150 })
  notes: string | null;

  @Column("varchar", { name: "ObjectID", nullable: true, length: 50 })
  objectId: string | null;

  @OneToMany(
    () => CsPaymentDetailTbl,
    csPaymentDetailTbl => csPaymentDetailTbl.bankContract
  )
  csPaymentDetailTbls: CsPaymentDetailTbl[];

  @OneToMany(
    () => CsReceiptDetailTbl,
    csReceiptDetailTbl => csReceiptDetailTbl.bankContract
  )
  csReceiptDetailTbls: CsReceiptDetailTbl[];
}
