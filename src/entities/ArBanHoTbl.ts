import {
  Column,
  Entity,
  Index,
  JoinColumn,
  ManyToOne,
  OneToMany
} from "typeorm";
import { ArBanHoCouponTbl } from "./ArBanHoCouponTbl";
import { ArBanHoDetailTbl } from "./ArBanHoDetailTbl";
import { CfObjectTbl } from "./CfObjectTbl";

@Index("PK_BanHoTbl", ["documentId"], { unique: true })
@Entity("AR_BanHoTbl", { schema: "dbo" })
export class ArBanHoTbl {
  @Column("varchar", { primary: true, name: "DocumentID", length: 30 })
  documentId: string;

  @Column("datetime", { name: "DocumentDate" })
  documentDate: Date;

  @Column("varchar", { name: "EmployeeID", nullable: true, length: 50 })
  employeeId: string | null;

  @Column("nvarchar", { name: "Memo", nullable: true, length: 200 })
  memo: string | null;

  @Column("nvarchar", { name: "Notes", nullable: true, length: 200 })
  notes: string | null;

  @Column("nvarchar", { name: "Receiver", nullable: true, length: 100 })
  receiver: string | null;

  @Column("varchar", { name: "CurrencyID", length: 3, default: () => "'VND'" })
  currencyId: string;

  @Column("float", {
    name: "RateExchange",
    precision: 53,
    default: () => "(1)"
  })
  rateExchange: number;

  @Column("varchar", { name: "PaymentTypeID", nullable: true, length: 50 })
  paymentTypeId: string | null;

  @Column("varchar", { name: "PaymentTermID", nullable: true, length: 50 })
  paymentTermId: string | null;

  @Column("decimal", {
    name: "BaseTotal",
    nullable: true,
    precision: 18,
    scale: 0
  })
  baseTotal: number | null;

  @Column("bit", { name: "isLock", default: () => "(0)" })
  isLock: boolean;

  @Column("nvarchar", { name: "SearchField", nullable: true, length: 2000 })
  searchField: string | null;

  @Column("varchar", { name: "UserCreate", nullable: true, length: 30 })
  userCreate: string | null;

  @Column("varchar", { name: "UserUpdate", nullable: true, length: 30 })
  userUpdate: string | null;

  @Column("datetime", { name: "DateUpdate", nullable: true })
  dateUpdate: Date | null;

  @Column("datetime", { name: "DateCreate", nullable: true })
  dateCreate: Date | null;

  @Column("int", { name: "Status", default: () => "(1)" })
  status: number;

  @Column("varchar", { name: "LinkID", nullable: true, length: 50 })
  linkId: string | null;

  @Column("decimal", {
    name: "AmountTotal",
    nullable: true,
    precision: 28,
    scale: 0
  })
  amountTotal: number | null;

  @Column("varchar", { name: "DocBatch", nullable: true, length: 50 })
  docBatch: string | null;

  @Column("decimal", {
    name: "CouponAmount",
    nullable: true,
    precision: 18,
    scale: 0
  })
  couponAmount: number | null;

  @Column("decimal", {
    name: "DiemTichLuy",
    nullable: true,
    precision: 18,
    scale: 1
  })
  diemTichLuy: number | null;

  @Column("int", { name: "LanIn", nullable: true })
  lanIn: number | null;

  @Column("varchar", { name: "BranchID", length: 50 })
  branchId: string;

  @OneToMany(
    () => ArBanHoCouponTbl,
    arBanHoCouponTbl => arBanHoCouponTbl.document
  )
  arBanHoCouponTbls: ArBanHoCouponTbl[];

  @OneToMany(
    () => ArBanHoDetailTbl,
    arBanHoDetailTbl => arBanHoDetailTbl.document
  )
  arBanHoDetailTbls: ArBanHoDetailTbl[];

  @ManyToOne(
    () => CfObjectTbl,
    cfObjectTbl => cfObjectTbl.arBanHoTbls,
    { onUpdate: "CASCADE" }
  )
  @JoinColumn([{ name: "ObjectID", referencedColumnName: "objectId" }])
  object: CfObjectTbl;
}
