import {
  Column,
  Entity,
  Index,
  JoinColumn,
  ManyToOne,
  OneToMany
} from "typeorm";
import { GjEntryCouponTbl } from "./GjEntryCouponTbl";
import { GjEntryDetailTbl } from "./GjEntryDetailTbl";
import { CfObjectTbl } from "./CfObjectTbl";
import { GjVatInvoiceTbl } from "./GjVatInvoiceTbl";

@Index("PK_GJ_EntryTbl", ["documentId"], { unique: true })
@Entity("GJ_EntryTbl", { schema: "dbo" })
export class GjEntryTbl {
  @Column("varchar", { primary: true, name: "DocumentID", length: 30 })
  documentId: string;

  @Column("datetime", { name: "DocumentDate" })
  documentDate: Date;

  @Column("nvarchar", { name: "Memo", nullable: true, length: 200 })
  memo: string | null;

  @Column("nvarchar", { name: "Notes", nullable: true, length: 200 })
  notes: string | null;

  @Column("varchar", { name: "CurrencyID", length: 3, default: () => "'VND'" })
  currencyId: string;

  @Column("float", { name: "RateExchange", precision: 53, default: () => "1" })
  rateExchange: number;

  @Column("decimal", {
    name: "BaseTotal",
    nullable: true,
    precision: 18,
    scale: 0
  })
  baseTotal: number | null;

  @Column("bit", { name: "isLock", default: () => "0" })
  isLock: boolean;

  @Column("varchar", { name: "EntryType", length: 5, default: () => "'GJ'" })
  entryType: string;

  @Column("nvarchar", { name: "SearchField", nullable: true, length: 2000 })
  searchField: string | null;

  @Column("varchar", { name: "InvoiceNo", nullable: true, length: 50 })
  invoiceNo: string | null;

  @Column("varchar", { name: "UserCreate", nullable: true, length: 30 })
  userCreate: string | null;

  @Column("varchar", { name: "UserUpdate", nullable: true, length: 30 })
  userUpdate: string | null;

  @Column("datetime", { name: "DateUpdate", nullable: true })
  dateUpdate: Date | null;

  @Column("datetime", { name: "DateCreate", nullable: true })
  dateCreate: Date | null;

  @Column("int", { name: "Status", default: () => "1" })
  status: number;

  @Column("varchar", { name: "LinkID", nullable: true, length: 50 })
  linkId: string | null;

  @Column("varchar", { name: "DocBatch", nullable: true, length: 50 })
  docBatch: string | null;

  @Column("nvarchar", { name: "Attach", nullable: true, length: 100 })
  attach: string | null;

  @Column("datetime", { name: "AttachDate", nullable: true })
  attachDate: Date | null;

  @Column("varchar", { name: "BranchID", length: 50 })
  branchId: string;

  @Column("varchar", { name: "EmployeeID", nullable: true, length: 50 })
  employeeId: string | null;

  @OneToMany(
    () => GjEntryCouponTbl,
    gjEntryCouponTbl => gjEntryCouponTbl.document
  )
  gjEntryCouponTbls: GjEntryCouponTbl[];

  @OneToMany(
    () => GjEntryDetailTbl,
    gjEntryDetailTbl => gjEntryDetailTbl.document
  )
  gjEntryDetailTbls: GjEntryDetailTbl[];

  @ManyToOne(
    () => CfObjectTbl,
    cfObjectTbl => cfObjectTbl.gjEntryTbls,
    { onUpdate: "CASCADE" }
  )
  @JoinColumn([{ name: "ObjectID", referencedColumnName: "objectId" }])
  object: CfObjectTbl;

  @OneToMany(
    () => GjVatInvoiceTbl,
    gjVatInvoiceTbl => gjVatInvoiceTbl.entry
  )
  gjVatInvoiceTbls: GjVatInvoiceTbl[];
}
