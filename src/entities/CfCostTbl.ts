import { Column, Entity, Index, OneToMany } from "typeorm";
import { ApPurchaseDetailTbl } from "./ApPurchaseDetailTbl";
import { ApReturnDetailTbl } from "./ApReturnDetailTbl";
import { ApServiceDetailTbl } from "./ApServiceDetailTbl";
import { ArBanHoDetailTbl } from "./ArBanHoDetailTbl";
import { ArInvoiceDetailTbl } from "./ArInvoiceDetailTbl";
import { ArReturnDetailTbl } from "./ArReturnDetailTbl";
import { ArServiceDetailTbl } from "./ArServiceDetailTbl";
import { CsPaymentDetailTbl } from "./CsPaymentDetailTbl";
import { CsReceiptDetailTbl } from "./CsReceiptDetailTbl";
import { GjEntryDetailTbl } from "./GjEntryDetailTbl";
import { IvInOutDetailTbl } from "./IvInOutDetailTbl";
import { IvInputDetailTbl } from "./IvInputDetailTbl";
import { IvOutputDetailTbl } from "./IvOutputDetailTbl";
import { PcAccSetupTbl } from "./PcAccSetupTbl";

@Index("PK_CF_CostTbl", ["costId"], { unique: true })
@Entity("CF_CostTbl", { schema: "dbo" })
export class CfCostTbl {
  @Column("varchar", { primary: true, name: "CostID", length: 50 })
  costId: string;

  @Column("nvarchar", { name: "CostName", length: 250 })
  costName: string;

  @Column("nvarchar", { name: "CostGroup", nullable: true, length: 100 })
  costGroup: string | null;

  @Column("nvarchar", { name: "Notes", nullable: true, length: 100 })
  notes: string | null;

  @Column("varchar", { name: "UserCreate", nullable: true, length: 30 })
  userCreate: string | null;

  @Column("varchar", { name: "UserUpdate", nullable: true, length: 30 })
  userUpdate: string | null;

  @Column("datetime", { name: "DateUpdate", nullable: true })
  dateUpdate: Date | null;

  @Column("datetime", { name: "DateCreate", nullable: true })
  dateCreate: Date | null;

  @Column("varchar", { name: "ParentCostID", nullable: true, length: 50 })
  parentCostId: string | null;

  @Column("bit", { name: "isSelect", nullable: true, default: () => "(1)" })
  isSelect: boolean | null;

  @Column("decimal", { name: "HeSo", nullable: true, precision: 28, scale: 4 })
  heSo: number | null;

  @OneToMany(
    () => ApPurchaseDetailTbl,
    apPurchaseDetailTbl => apPurchaseDetailTbl.cost
  )
  apPurchaseDetailTbls: ApPurchaseDetailTbl[];

  @OneToMany(
    () => ApReturnDetailTbl,
    apReturnDetailTbl => apReturnDetailTbl.cost
  )
  apReturnDetailTbls: ApReturnDetailTbl[];

  @OneToMany(
    () => ApServiceDetailTbl,
    apServiceDetailTbl => apServiceDetailTbl.cost
  )
  apServiceDetailTbls: ApServiceDetailTbl[];

  @OneToMany(
    () => ArBanHoDetailTbl,
    arBanHoDetailTbl => arBanHoDetailTbl.cost
  )
  arBanHoDetailTbls: ArBanHoDetailTbl[];

  @OneToMany(
    () => ArInvoiceDetailTbl,
    arInvoiceDetailTbl => arInvoiceDetailTbl.cost
  )
  arInvoiceDetailTbls: ArInvoiceDetailTbl[];

  @OneToMany(
    () => ArReturnDetailTbl,
    arReturnDetailTbl => arReturnDetailTbl.cost
  )
  arReturnDetailTbls: ArReturnDetailTbl[];

  @OneToMany(
    () => ArServiceDetailTbl,
    arServiceDetailTbl => arServiceDetailTbl.cost
  )
  arServiceDetailTbls: ArServiceDetailTbl[];

  @OneToMany(
    () => CsPaymentDetailTbl,
    csPaymentDetailTbl => csPaymentDetailTbl.cost
  )
  csPaymentDetailTbls: CsPaymentDetailTbl[];

  @OneToMany(
    () => CsReceiptDetailTbl,
    csReceiptDetailTbl => csReceiptDetailTbl.cost
  )
  csReceiptDetailTbls: CsReceiptDetailTbl[];

  @OneToMany(
    () => GjEntryDetailTbl,
    gjEntryDetailTbl => gjEntryDetailTbl.cost
  )
  gjEntryDetailTbls: GjEntryDetailTbl[];

  @OneToMany(
    () => IvInOutDetailTbl,
    ivInOutDetailTbl => ivInOutDetailTbl.cost
  )
  ivInOutDetailTbls: IvInOutDetailTbl[];

  @OneToMany(
    () => IvInputDetailTbl,
    ivInputDetailTbl => ivInputDetailTbl.cost
  )
  ivInputDetailTbls: IvInputDetailTbl[];

  @OneToMany(
    () => IvOutputDetailTbl,
    ivOutputDetailTbl => ivOutputDetailTbl.cost
  )
  ivOutputDetailTbls: IvOutputDetailTbl[];

  @OneToMany(
    () => PcAccSetupTbl,
    pcAccSetupTbl => pcAccSetupTbl.cost
  )
  pcAccSetupTbls: PcAccSetupTbl[];
}
