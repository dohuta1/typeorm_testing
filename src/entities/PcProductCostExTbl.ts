import { Column, Entity, Index, JoinColumn, ManyToOne } from "typeorm";
import { PcProductCostTbl } from "./PcProductCostTbl";

@Index("PK_PC_ProductCostExTbl", ["userAutoId"], { unique: true })
@Entity("PC_ProductCostExTbl", { schema: "dbo" })
export class PcProductCostExTbl {
  @Column("varchar", { primary: true, name: "UserAutoID", length: 50 })
  userAutoId: string;

  @Column("varchar", { name: "CostCenterID", nullable: true, length: 50 })
  costCenterId: string | null;

  @Column("varchar", { name: "CostID", nullable: true, length: 50 })
  costId: string | null;

  @Column("varchar", { name: "AccountID", length: 50 })
  accountId: string;

  @Column("decimal", {
    name: "Amount",
    precision: 28,
    scale: 0,
    default: () => "0"
  })
  amount: number;

  @Column("decimal", {
    name: "AmountAllocate",
    nullable: true,
    precision: 28,
    scale: 0,
    default: () => "0"
  })
  amountAllocate: number | null;

  @Column("int", { name: "ALType", default: () => "1" })
  alType: number;

  @Column("nvarchar", { name: "Notes", nullable: true, length: 50 })
  notes: string | null;

  @ManyToOne(
    () => PcProductCostTbl,
    pcProductCostTbl => pcProductCostTbl.pcProductCostExTbls,
    { onDelete: "CASCADE", onUpdate: "CASCADE" }
  )
  @JoinColumn([{ name: "PriceID", referencedColumnName: "priceId" }])
  price: PcProductCostTbl;
}
