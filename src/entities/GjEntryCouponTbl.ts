import { Column, Entity, Index, JoinColumn, ManyToOne } from "typeorm";
import { GjEntryTbl } from "./GjEntryTbl";

@Index("PK_GJ_EntryCouponTbl", ["userAutoId"], { unique: true })
@Entity("GJ_EntryCouponTbl", { schema: "dbo" })
export class GjEntryCouponTbl {
  @Column("varchar", { primary: true, name: "UserAutoID", length: 40 })
  userAutoId: string;

  @Column("varchar", { name: "CouponSeri", nullable: true, length: 100 })
  couponSeri: string | null;

  @Column("decimal", {
    name: "TriGia",
    nullable: true,
    precision: 18,
    scale: 0
  })
  triGia: number | null;

  @Column("nvarchar", { name: "Notes", nullable: true, length: 50 })
  notes: string | null;

  @ManyToOne(
    () => GjEntryTbl,
    gjEntryTbl => gjEntryTbl.gjEntryCouponTbls,
    { onDelete: "CASCADE", onUpdate: "CASCADE" }
  )
  @JoinColumn([{ name: "DocumentID", referencedColumnName: "documentId" }])
  document: GjEntryTbl;
}
