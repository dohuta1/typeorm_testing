import { Column, Entity, Index } from "typeorm";

@Index("PK_OF_PaymentOrderTbl", ["documentId"], { unique: true })
@Entity("OF_PaymentOrderTbl", { schema: "dbo" })
export class OfPaymentOrderTbl {
  @Column("varchar", { primary: true, name: "DocumentID", length: 50 })
  documentId: string;

  @Column("datetime", { name: "DocumentDate" })
  documentDate: Date;

  @Column("varchar", { name: "NguoiXinChi", length: 50 })
  nguoiXinChi: string;

  @Column("decimal", {
    name: "SoTien",
    precision: 18,
    scale: 1,
    default: () => "0"
  })
  soTien: number;

  @Column("datetime", { name: "NgayChi" })
  ngayChi: Date;

  @Column("nvarchar", { name: "LyDo", nullable: true, length: 250 })
  lyDo: string | null;

  @Column("nvarchar", { name: "GhiChu", nullable: true, length: 250 })
  ghiChu: string | null;

  @Column("nvarchar", { name: "YKien", nullable: true, length: 250 })
  yKien: string | null;

  @Column("bit", { name: "isLock", default: () => "0" })
  isLock: boolean;

  @Column("nvarchar", { name: "SoTienBangChu", nullable: true, length: 250 })
  soTienBangChu: string | null;

  @Column("datetime", { name: "DateCreate", nullable: true })
  dateCreate: Date | null;

  @Column("datetime", { name: "DateUpdate", nullable: true })
  dateUpdate: Date | null;

  @Column("varchar", { name: "UserCreate", nullable: true, length: 20 })
  userCreate: string | null;

  @Column("varchar", { name: "UserUpdate", nullable: true, length: 20 })
  userUpdate: string | null;
}
