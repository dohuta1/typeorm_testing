import { Column, Entity, Index, JoinColumn, ManyToOne } from "typeorm";
import { CsPaymentPlanTbl } from "./CsPaymentPlanTbl";
import { CfItemTbl } from "./CfItemTbl";

@Index("PK_CS_PaymentPlanDetailTbl", ["userAutoId"], { unique: true })
@Entity("CS_PaymentPlanDetailTbl", { schema: "dbo" })
export class CsPaymentPlanDetailTbl {
  @Column("varchar", { primary: true, name: "UserAutoID", length: 40 })
  userAutoId: string;

  @Column("varchar", { name: "AccountID", length: 50 })
  accountId: string;

  @Column("decimal", {
    name: "Quantity",
    nullable: true,
    precision: 18,
    scale: 2
  })
  quantity: number | null;

  @Column("decimal", {
    name: "ChietKhau1",
    nullable: true,
    precision: 18,
    scale: 2
  })
  chietKhau1: number | null;

  @Column("decimal", {
    name: "TienChietKhau1",
    nullable: true,
    precision: 18,
    scale: 2
  })
  tienChietKhau1: number | null;

  @Column("decimal", {
    name: "ChietKhau2",
    nullable: true,
    precision: 18,
    scale: 2
  })
  chietKhau2: number | null;

  @Column("decimal", {
    name: "TienChietKhau2",
    nullable: true,
    precision: 18,
    scale: 2
  })
  tienChietKhau2: number | null;

  @Column("decimal", {
    name: "ChietKhau3",
    nullable: true,
    precision: 18,
    scale: 2
  })
  chietKhau3: number | null;

  @Column("decimal", {
    name: "TienChietKhau3",
    nullable: true,
    precision: 18,
    scale: 2
  })
  tienChietKhau3: number | null;

  @Column("decimal", {
    name: "ChietKhau4",
    nullable: true,
    precision: 18,
    scale: 2
  })
  chietKhau4: number | null;

  @Column("decimal", {
    name: "TienChietKhau4",
    nullable: true,
    precision: 18,
    scale: 2
  })
  tienChietKhau4: number | null;

  @Column("decimal", {
    name: "TotalAmount",
    nullable: true,
    precision: 28,
    scale: 0
  })
  totalAmount: number | null;

  @Column("varchar", { name: "RefDoc", nullable: true, length: 50 })
  refDoc: string | null;

  @Column("varchar", { name: "ContactCode", nullable: true, length: 50 })
  contactCode: string | null;

  @Column("nvarchar", { name: "Memo", nullable: true, length: 150 })
  memo: string | null;

  @Column("varchar", { name: "CostID", nullable: true, length: 50 })
  costId: string | null;

  @Column("varchar", { name: "CostCenterID", nullable: true, length: 50 })
  costCenterId: string | null;

  @Column("decimal", {
    name: "Amount",
    nullable: true,
    precision: 28,
    scale: 0
  })
  amount: number | null;

  @Column("varchar", { name: "ObjectID", nullable: true, length: 50 })
  objectId: string | null;

  @Column("varchar", { name: "ContractID", nullable: true, length: 50 })
  contractId: string | null;

  @ManyToOne(
    () => CsPaymentPlanTbl,
    csPaymentPlanTbl => csPaymentPlanTbl.csPaymentPlanDetailTbls,
    { onDelete: "CASCADE", onUpdate: "CASCADE" }
  )
  @JoinColumn([{ name: "DocumentID", referencedColumnName: "documentId" }])
  document: CsPaymentPlanTbl;

  @ManyToOne(
    () => CfItemTbl,
    cfItemTbl => cfItemTbl.csPaymentPlanDetailTbls,
    { onUpdate: "CASCADE" }
  )
  @JoinColumn([{ name: "ItemID", referencedColumnName: "itemId" }])
  item: CfItemTbl;
}
