import { Column, Entity, Index, OneToMany } from "typeorm";
import { FaAssetTbl } from "./FaAssetTbl";

@Index("PK_FA_CapitalTbl", ["capitalId"], { unique: true })
@Entity("FA_CapitalTbl", { schema: "dbo" })
export class FaCapitalTbl {
  @Column("varchar", { primary: true, name: "CapitalID", length: 50 })
  capitalId: string;

  @Column("nvarchar", { name: "CapitalName", nullable: true, length: 40 })
  capitalName: string | null;

  @Column("nvarchar", { name: "CapitalName2", nullable: true, length: 40 })
  capitalName2: string | null;

  @Column("bit", { name: "isDefault", nullable: true, default: () => "(1)" })
  isDefault: boolean | null;

  @OneToMany(
    () => FaAssetTbl,
    faAssetTbl => faAssetTbl.capital
  )
  faAssetTbls: FaAssetTbl[];
}
