import { Column, Entity, Index, OneToMany } from "typeorm";
import { GjDocBatchSetupDetailTbl } from "./GjDocBatchSetupDetailTbl";

@Index("PK_GJ_DocBatchSetupTbl", ["docBatchId"], { unique: true })
@Entity("GJ_DocBatchSetupTbl", { schema: "dbo" })
export class GjDocBatchSetupTbl {
  @Column("varchar", { primary: true, name: "DocBatchID", length: 50 })
  docBatchId: string;

  @Column("nvarchar", { name: "Memo", nullable: true, length: 150 })
  memo: string | null;

  @Column("int", { name: "STT", default: () => "99" })
  stt: number;

  @Column("varchar", { name: "UserCreate", nullable: true, length: 30 })
  userCreate: string | null;

  @Column("varchar", { name: "UserUpdate", nullable: true, length: 30 })
  userUpdate: string | null;

  @Column("datetime", { name: "DateUpdate", nullable: true })
  dateUpdate: Date | null;

  @Column("datetime", { name: "DateCreate", nullable: true })
  dateCreate: Date | null;

  @OneToMany(
    () => GjDocBatchSetupDetailTbl,
    gjDocBatchSetupDetailTbl => gjDocBatchSetupDetailTbl.docBatch
  )
  gjDocBatchSetupDetailTbls: GjDocBatchSetupDetailTbl[];
}
