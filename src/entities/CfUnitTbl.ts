import { Column, Entity, Index } from "typeorm";

@Index("PK_CF_UnitTbl", ["unit"], { unique: true })
@Entity("CF_UnitTbl", { schema: "dbo" })
export class CfUnitTbl {
  @Column("nvarchar", { primary: true, name: "Unit", length: 50 })
  unit: string;

  @Column("nvarchar", { name: "Notes", nullable: true, length: 50 })
  notes: string | null;

  @Column("bit", { name: "isDefault", nullable: true, default: () => "0" })
  isDefault: boolean | null;
}
