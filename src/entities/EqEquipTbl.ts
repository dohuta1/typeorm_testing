import {
  Column,
  Entity,
  Index,
  JoinColumn,
  ManyToOne,
  OneToMany
} from "typeorm";
import { EqEquipAttachTbl } from "./EqEquipAttachTbl";
import { EqEquipDepreTbl } from "./EqEquipDepreTbl";
import { EqEquipLogTbl } from "./EqEquipLogTbl";
import { EqEquipPeriodSetupTbl } from "./EqEquipPeriodSetupTbl";
import { EqEquipGroupTbl } from "./EqEquipGroupTbl";
import { EqLiquidateDetailTbl } from "./EqLiquidateDetailTbl";
import { EqMoveDetailTbl } from "./EqMoveDetailTbl";
import { EqRepairDetailTbl } from "./EqRepairDetailTbl";

@Index("PK_EQ_EquipTbl", ["equipId"], { unique: true })
@Entity("EQ_EquipTbl", { schema: "dbo" })
export class EqEquipTbl {
  @Column("varchar", { primary: true, name: "EquipID", length: 50 })
  equipId: string;

  @Column("nvarchar", { name: "EquipName", length: 200 })
  equipName: string;

  @Column("decimal", {
    name: "Quantity",
    nullable: true,
    precision: 18,
    scale: 2
  })
  quantity: number | null;

  @Column("decimal", {
    name: "Amount",
    nullable: true,
    precision: 28,
    scale: 0,
    default: () => "(0)"
  })
  amount: number | null;

  @Column("datetime", { name: "AllocateDate", nullable: true })
  allocateDate: Date | null;

  @Column("int", { name: "MonthUse", nullable: true, default: () => "(3)" })
  monthUse: number | null;

  @Column("varchar", {
    name: "InvAccID",
    nullable: true,
    length: 50,
    default: () => "(153)"
  })
  invAccId: string | null;

  @Column("varchar", {
    name: "AccountID",
    nullable: true,
    length: 50,
    default: () => "(1421)"
  })
  accountId: string | null;

  @Column("varchar", {
    name: "ExpenseAccID",
    nullable: true,
    length: 50,
    default: () => "(6427)"
  })
  expenseAccId: string | null;

  @Column("varchar", { name: "RefItemID", nullable: true, length: 50 })
  refItemId: string | null;

  @Column("decimal", {
    name: "OpenAllocate",
    nullable: true,
    precision: 28,
    scale: 0,
    default: () => "(0)"
  })
  openAllocate: number | null;

  @Column("varchar", { name: "FixMonthUse", nullable: true, length: 50 })
  fixMonthUse: string | null;

  @Column("varchar", { name: "CostCenterID", nullable: true, length: 50 })
  costCenterId: string | null;

  @Column("varchar", { name: "CostID", nullable: true, length: 50 })
  costId: string | null;

  @Column("varchar", { name: "ObjectID", nullable: true, length: 50 })
  objectId: string | null;

  @Column("datetime", { name: "StopDate", nullable: true })
  stopDate: Date | null;

  @Column("nvarchar", { name: "Notes", nullable: true, length: 100 })
  notes: string | null;

  @Column("datetime", {
    name: "DateCreate",
    nullable: true,
    default: () => "getdate()"
  })
  dateCreate: Date | null;

  @Column("datetime", {
    name: "DateUpdate",
    nullable: true,
    default: () => "getdate()"
  })
  dateUpdate: Date | null;

  @Column("varchar", { name: "UserCreate", nullable: true, length: 20 })
  userCreate: string | null;

  @Column("varchar", { name: "UserUpdate", nullable: true, length: 20 })
  userUpdate: string | null;

  @Column("varchar", { name: "DepartmentID", nullable: true, length: 50 })
  departmentId: string | null;

  @Column("varchar", { name: "RefDoc", nullable: true, length: 50 })
  refDoc: string | null;

  @Column("varchar", { name: "RefSourceID", nullable: true, length: 3 })
  refSourceId: string | null;

  @Column("nvarchar", { name: "Unit", nullable: true, length: 50 })
  unit: string | null;

  @Column("varchar", { name: "LinkID", nullable: true, length: 50 })
  linkId: string | null;

  @Column("datetime", { name: "LiquidationDate", nullable: true })
  liquidationDate: Date | null;

  @Column("varchar", { name: "BranchID", length: 50 })
  branchId: string;

  @Column("datetime", { name: "NgayChungTu", nullable: true })
  ngayChungTu: Date | null;

  @OneToMany(
    () => EqEquipAttachTbl,
    eqEquipAttachTbl => eqEquipAttachTbl.equip
  )
  eqEquipAttachTbls: EqEquipAttachTbl[];

  @OneToMany(
    () => EqEquipDepreTbl,
    eqEquipDepreTbl => eqEquipDepreTbl.equip
  )
  eqEquipDepreTbls: EqEquipDepreTbl[];

  @OneToMany(
    () => EqEquipLogTbl,
    eqEquipLogTbl => eqEquipLogTbl.equip
  )
  eqEquipLogTbls: EqEquipLogTbl[];

  @OneToMany(
    () => EqEquipPeriodSetupTbl,
    eqEquipPeriodSetupTbl => eqEquipPeriodSetupTbl.equip
  )
  eqEquipPeriodSetupTbls: EqEquipPeriodSetupTbl[];

  @ManyToOne(
    () => EqEquipGroupTbl,
    eqEquipGroupTbl => eqEquipGroupTbl.eqEquipTbls,
    { onUpdate: "CASCADE" }
  )
  @JoinColumn([{ name: "EquipGroupID", referencedColumnName: "equipGroupId" }])
  equipGroup: EqEquipGroupTbl;

  @OneToMany(
    () => EqLiquidateDetailTbl,
    eqLiquidateDetailTbl => eqLiquidateDetailTbl.equip
  )
  eqLiquidateDetailTbls: EqLiquidateDetailTbl[];

  @OneToMany(
    () => EqMoveDetailTbl,
    eqMoveDetailTbl => eqMoveDetailTbl.equip
  )
  eqMoveDetailTbls: EqMoveDetailTbl[];

  @OneToMany(
    () => EqRepairDetailTbl,
    eqRepairDetailTbl => eqRepairDetailTbl.equip
  )
  eqRepairDetailTbls: EqRepairDetailTbl[];
}
