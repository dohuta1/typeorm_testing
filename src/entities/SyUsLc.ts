import { Column, Entity, Index } from "typeorm";

@Index("PK_SY_UsLc", ["uHardId"], { unique: true })
@Entity("SY_UsLc", { schema: "dbo" })
export class SyUsLc {
  @Column("varchar", { primary: true, name: "UHardID", length: 100 })
  uHardId: string;

  @Column("varchar", { name: "USeri", nullable: true, length: 100 })
  uSeri: string | null;

  @Column("varchar", { name: "Version", nullable: true, length: 50 })
  version: string | null;

  @Column("datetime", { name: "LastDate", nullable: true })
  lastDate: Date | null;
}
