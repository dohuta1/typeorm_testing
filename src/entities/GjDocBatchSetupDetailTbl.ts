import { Column, Entity, Index, JoinColumn, ManyToOne } from "typeorm";
import { GjDocBatchSetupTbl } from "./GjDocBatchSetupTbl";

@Index("PK_GJ_DocBatchSetupDetailTbl", ["userAutoId"], { unique: true })
@Entity("GJ_DocBatchSetupDetailTbl", { schema: "dbo" })
export class GjDocBatchSetupDetailTbl {
  @Column("varchar", { primary: true, name: "UserAutoID", length: 50 })
  userAutoId: string;

  @Column("varchar", { name: "DebitAccID", length: 50 })
  debitAccId: string;

  @Column("varchar", { name: "CreditAccID", length: 50 })
  creditAccId: string;

  @Column("nvarchar", { name: "Notes", nullable: true, length: 150 })
  notes: string | null;

  @ManyToOne(
    () => GjDocBatchSetupTbl,
    gjDocBatchSetupTbl => gjDocBatchSetupTbl.gjDocBatchSetupDetailTbls,
    { onDelete: "CASCADE", onUpdate: "CASCADE" }
  )
  @JoinColumn([{ name: "DocBatchID", referencedColumnName: "docBatchId" }])
  docBatch: GjDocBatchSetupTbl;
}
