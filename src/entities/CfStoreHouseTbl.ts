import { Column, Entity, Index, OneToMany } from "typeorm";
import { ApPurchaseDetailTbl } from "./ApPurchaseDetailTbl";
import { ApReturnDetailTbl } from "./ApReturnDetailTbl";
import { ArBanHoDetailTbl } from "./ArBanHoDetailTbl";
import { ArInvoiceDetailTbl } from "./ArInvoiceDetailTbl";
import { ArReturnDetailTbl } from "./ArReturnDetailTbl";
import { ArTrustInvoiceDetailTbl } from "./ArTrustInvoiceDetailTbl";
import { CsPaymentDetailTbl } from "./CsPaymentDetailTbl";
import { IvAdjustDetailTbl } from "./IvAdjustDetailTbl";
import { IvInOutDetailTbl } from "./IvInOutDetailTbl";
import { IvInputDetailTbl } from "./IvInputDetailTbl";
import { IvOutputDetailTbl } from "./IvOutputDetailTbl";
import { SyBalanceFifo2Tbl } from "./SyBalanceFifo2Tbl";
import { SyBalanceFifoTbl } from "./SyBalanceFifoTbl";
import { SyBalanceItemTbl } from "./SyBalanceItemTbl";

@Index("PK_CF_StoreHouseTbl", ["storeHouseId"], { unique: true })
@Entity("CF_StoreHouseTbl", { schema: "dbo" })
export class CfStoreHouseTbl {
  @Column("varchar", { primary: true, name: "StoreHouseID", length: 50 })
  storeHouseId: string;

  @Column("nvarchar", { name: "StoreHouseName", length: 100 })
  storeHouseName: string;

  @Column("nvarchar", { name: "StoreHouseGroup", nullable: true, length: 100 })
  storeHouseGroup: string | null;

  @Column("bit", { name: "isDefault", default: () => "0" })
  isDefault: boolean;

  @Column("bit", { name: "isNL", default: () => "(1)" })
  isNl: boolean;

  @Column("bit", { name: "isBTP", default: () => "(1)" })
  isBtp: boolean;

  @Column("bit", { name: "isTP", default: () => "(1)" })
  isTp: boolean;

  @Column("bit", { name: "isHH", default: () => "(1)" })
  isHh: boolean;

  @Column("bit", { name: "isVT", default: () => "(1)" })
  isVt: boolean;

  @OneToMany(
    () => ApPurchaseDetailTbl,
    apPurchaseDetailTbl => apPurchaseDetailTbl.storeHouse
  )
  apPurchaseDetailTbls: ApPurchaseDetailTbl[];

  @OneToMany(
    () => ApReturnDetailTbl,
    apReturnDetailTbl => apReturnDetailTbl.storeHouse
  )
  apReturnDetailTbls: ApReturnDetailTbl[];

  @OneToMany(
    () => ArBanHoDetailTbl,
    arBanHoDetailTbl => arBanHoDetailTbl.storeHouse
  )
  arBanHoDetailTbls: ArBanHoDetailTbl[];

  @OneToMany(
    () => ArInvoiceDetailTbl,
    arInvoiceDetailTbl => arInvoiceDetailTbl.storeHouse
  )
  arInvoiceDetailTbls: ArInvoiceDetailTbl[];

  @OneToMany(
    () => ArReturnDetailTbl,
    arReturnDetailTbl => arReturnDetailTbl.storeHouse
  )
  arReturnDetailTbls: ArReturnDetailTbl[];

  @OneToMany(
    () => ArTrustInvoiceDetailTbl,
    arTrustInvoiceDetailTbl => arTrustInvoiceDetailTbl.storeHouse
  )
  arTrustInvoiceDetailTbls: ArTrustInvoiceDetailTbl[];

  @OneToMany(
    () => CsPaymentDetailTbl,
    csPaymentDetailTbl => csPaymentDetailTbl.storeHouse
  )
  csPaymentDetailTbls: CsPaymentDetailTbl[];

  @OneToMany(
    () => IvAdjustDetailTbl,
    ivAdjustDetailTbl => ivAdjustDetailTbl.storeHouse
  )
  ivAdjustDetailTbls: IvAdjustDetailTbl[];

  @OneToMany(
    () => IvInOutDetailTbl,
    ivInOutDetailTbl => ivInOutDetailTbl.storeHouse
  )
  ivInOutDetailTbls: IvInOutDetailTbl[];

  @OneToMany(
    () => IvInputDetailTbl,
    ivInputDetailTbl => ivInputDetailTbl.storeHouse
  )
  ivInputDetailTbls: IvInputDetailTbl[];

  @OneToMany(
    () => IvOutputDetailTbl,
    ivOutputDetailTbl => ivOutputDetailTbl.storeHouse
  )
  ivOutputDetailTbls: IvOutputDetailTbl[];

  @OneToMany(
    () => SyBalanceFifo2Tbl,
    syBalanceFifo2Tbl => syBalanceFifo2Tbl.storehouse
  )
  syBalanceFifo2Tbls: SyBalanceFifo2Tbl[];

  @OneToMany(
    () => SyBalanceFifoTbl,
    syBalanceFifoTbl => syBalanceFifoTbl.storehouse
  )
  syBalanceFifoTbls: SyBalanceFifoTbl[];

  @OneToMany(
    () => SyBalanceItemTbl,
    syBalanceItemTbl => syBalanceItemTbl.storehouse
  )
  syBalanceItemTbls: SyBalanceItemTbl[];
}
