import { Column, Entity, Index } from "typeorm";

@Index("PK_SY_ApproveSetupTbl", ["approveId"], { unique: true })
@Entity("SY_ApproveSetupTbl", { schema: "dbo" })
export class SyApproveSetupTbl {
  @Column("varchar", { primary: true, name: "ApproveID", length: 50 })
  approveId: string;

  @Column("nvarchar", { name: "SourceGroup", length: 150 })
  sourceGroup: string;

  @Column("nvarchar", { name: "SourceName", length: 150 })
  sourceName: string;

  @Column("varchar", { name: "SourceID", length: 50 })
  sourceId: string;

  @Column("nvarchar", { name: "GhiChu", nullable: true, length: 150 })
  ghiChu: string | null;

  @Column("varchar", { name: "Notifier1", nullable: true, length: 50 })
  notifier1: string | null;

  @Column("varchar", { name: "Notifier2", nullable: true, length: 50 })
  notifier2: string | null;

  @Column("varchar", { name: "UserLevel1", nullable: true, length: 50 })
  userLevel1: string | null;

  @Column("varchar", { name: "UserLevel2", nullable: true, length: 50 })
  userLevel2: string | null;

  @Column("varchar", { name: "UserLevel3", nullable: true, length: 50 })
  userLevel3: string | null;

  @Column("varchar", { name: "UserLevel4", nullable: true, length: 50 })
  userLevel4: string | null;

  @Column("varchar", { name: "UserLevel5", nullable: true, length: 50 })
  userLevel5: string | null;

  @Column("nvarchar", { name: "UserCaption1", nullable: true, length: 50 })
  userCaption1: string | null;

  @Column("nvarchar", { name: "UserCaption2", nullable: true, length: 50 })
  userCaption2: string | null;

  @Column("nvarchar", { name: "UserCaption3", nullable: true, length: 50 })
  userCaption3: string | null;

  @Column("nvarchar", { name: "UserCaption4", nullable: true, length: 50 })
  userCaption4: string | null;

  @Column("nvarchar", { name: "UserCaption5", nullable: true, length: 50 })
  userCaption5: string | null;

  @Column("nvarchar", { name: "UserRole1", nullable: true, length: 50 })
  userRole1: string | null;

  @Column("nvarchar", { name: "UserRole2", nullable: true, length: 50 })
  userRole2: string | null;

  @Column("nvarchar", { name: "UserRole3", nullable: true, length: 50 })
  userRole3: string | null;

  @Column("nvarchar", { name: "UserRole4", nullable: true, length: 50 })
  userRole4: string | null;

  @Column("nvarchar", { name: "UserRole5", nullable: true, length: 50 })
  userRole5: string | null;

  @Column("bit", { name: "isLevelEnd4", nullable: true, default: () => "1" })
  isLevelEnd4: boolean | null;

  @Column("bit", { name: "isLevelEnd5", nullable: true, default: () => "1" })
  isLevelEnd5: boolean | null;

  @Column("varchar", { name: "ToApproveID", nullable: true, length: 50 })
  toApproveId: string | null;

  @Column("varchar", { name: "ToApproveID2", nullable: true, length: 50 })
  toApproveId2: string | null;

  @Column("varchar", { name: "ToUser1", nullable: true, length: 50 })
  toUser1: string | null;

  @Column("varchar", { name: "ToUser2", nullable: true, length: 50 })
  toUser2: string | null;

  @Column("varchar", { name: "TableName", nullable: true, length: 50 })
  tableName: string | null;

  @Column("varchar", { name: "ToTableName", nullable: true, length: 50 })
  toTableName: string | null;

  @Column("nvarchar", { name: "FilterEx", nullable: true, length: 100 })
  filterEx: string | null;

  @Column("varchar", { name: "FormID", nullable: true, length: 50 })
  formId: string | null;

  @Column("int", { name: "STT", nullable: true, default: () => "0" })
  stt: number | null;

  @Column("int", { name: "ApproveDay", nullable: true })
  approveDay: number | null;
}
