import { Column, Entity, Index } from "typeorm";

@Index("PK_SY_String", ["stringId"], { unique: true })
@Entity("SY_StTbl", { schema: "dbo" })
export class SyStTbl {
  @Column("varchar", { primary: true, name: "StringID", length: 250 })
  stringId: string;

  @Column("nvarchar", { name: "StringParam", nullable: true, length: 500 })
  stringParam: string | null;

  @Column("nvarchar", { name: "StringVN", nullable: true, length: 500 })
  stringVn: string | null;

  @Column("nvarchar", { name: "StringEN", nullable: true, length: 500 })
  stringEn: string | null;

  @Column("nvarchar", { name: "StringCH", nullable: true, length: 500 })
  stringCh: string | null;
}
