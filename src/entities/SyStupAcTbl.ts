import { Column, Entity, Index } from "typeorm";

@Index("PK_SY_SetupAccount", ["systemAccountType"], { unique: true })
@Entity("SY_StupAcTbl", { schema: "dbo" })
export class SyStupAcTbl {
  @Column("varchar", { primary: true, name: "SystemAccountType", length: 50 })
  systemAccountType: string;

  @Column("nvarchar", { name: "Description", length: 100 })
  description: string;

  @Column("varchar", { name: "AccountID", nullable: true, length: 20 })
  accountId: string | null;

  @Column("bit", { name: "IsIncludeDetail", default: () => "(0)" })
  isIncludeDetail: boolean;

  @Column("bit", { name: "IsDebit", default: () => "(1)" })
  isDebit: boolean;
}
