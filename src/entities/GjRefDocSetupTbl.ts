import { Column, Entity, Index } from "typeorm";

@Index("PK_GJ_RefDocSetupTbl", ["documentId"], { unique: true })
@Entity("GJ_RefDocSetupTbl", { schema: "dbo" })
export class GjRefDocSetupTbl {
  @Column("varchar", { primary: true, name: "DocumentID", length: 30 })
  documentId: string;

  @Column("datetime", { name: "DocumentDate" })
  documentDate: Date;

  @Column("varchar", { name: "ObjectID", length: 50 })
  objectId: string;

  @Column("nvarchar", { name: "Memo", nullable: true, length: 200 })
  memo: string | null;

  @Column("varchar", { name: "AccountID", length: 50 })
  accountId: string;

  @Column("varchar", { name: "RefDebit", length: 50 })
  refDebit: string;

  @Column("varchar", { name: "RefCredit", length: 50 })
  refCredit: string;

  @Column("decimal", {
    name: "SourceAmount",
    nullable: true,
    precision: 28,
    scale: 2
  })
  sourceAmount: number | null;

  @Column("decimal", {
    name: "Amount",
    nullable: true,
    precision: 28,
    scale: 0
  })
  amount: number | null;

  @Column("varchar", { name: "UserCreate", nullable: true, length: 30 })
  userCreate: string | null;

  @Column("varchar", { name: "UserUpdate", nullable: true, length: 30 })
  userUpdate: string | null;

  @Column("datetime", { name: "DateUpdate", nullable: true })
  dateUpdate: Date | null;

  @Column("datetime", { name: "DateCreate", nullable: true })
  dateCreate: Date | null;

  @Column("varchar", { name: "ItemID", nullable: true, length: 50 })
  itemId: string | null;

  @Column("decimal", {
    name: "Quantity",
    nullable: true,
    precision: 18,
    scale: 2
  })
  quantity: number | null;
}
