import { Column, Entity, Index } from "typeorm";

@Index("PK_SY_DshBrdCnt", ["menuId"], { unique: true })
@Entity("SY_DshBrdCnt", { schema: "dbo" })
export class SyDshBrdCnt {
  @Column("varchar", { primary: true, name: "MenuID", length: 50 })
  menuId: string;

  @Column("image", { name: "Contain", nullable: true })
  contain: Buffer | null;

  @Column("varchar", { name: "Version", nullable: true, length: 50 })
  version: string | null;
}
