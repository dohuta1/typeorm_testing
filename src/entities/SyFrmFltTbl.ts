import { Column, Entity, Index } from "typeorm";

@Index("PK_SY_FrmFltTbl", ["userAutoId"], { unique: true })
@Entity("SY_FrmFltTbl", { schema: "dbo" })
export class SyFrmFltTbl {
  @Column("varchar", { primary: true, name: "UserAutoID", length: 40 })
  userAutoId: string;

  @Column("varchar", { name: "FormID", nullable: true, length: 250 })
  formId: string | null;

  @Column("varchar", { name: "KeyID", length: 50, default: () => "(0)" })
  keyId: string;

  @Column("varchar", { name: "ColumnID", length: 50 })
  columnId: string;

  @Column("nvarchar", { name: "Caption", nullable: true, length: 150 })
  caption: string | null;

  @Column("int", { name: "ControlWidth", nullable: true })
  controlWidth: number | null;

  @Column("int", { name: "Type", nullable: true })
  type: number | null;

  @Column("nvarchar", { name: "Source", nullable: true })
  source: string | null;

  @Column("int", { name: "SourceType", nullable: true })
  sourceType: number | null;

  @Column("varchar", { name: "ValueColumn", nullable: true, length: 50 })
  valueColumn: string | null;

  @Column("varchar", { name: "DisplayColumn", nullable: true, length: 50 })
  displayColumn: string | null;

  @Column("varchar", { name: "ColumnArr", nullable: true, length: 200 })
  columnArr: string | null;

  @Column("varchar", { name: "WidthArr", nullable: true, length: 200 })
  widthArr: string | null;

  @Column("bit", { name: "IsSetDefaultValue", default: () => "(1)" })
  isSetDefaultValue: boolean;

  @Column("bit", { name: "RememberLastValue", default: () => "(0)" })
  rememberLastValue: boolean;

  @Column("bit", { name: "isLockWhenEditData", default: () => "(0)" })
  isLockWhenEditData: boolean;

  @Column("bit", { name: "UseLikeOperator", default: () => "(0)" })
  useLikeOperator: boolean;

  @Column("varchar", { name: "LinkColumn", nullable: true, length: 200 })
  linkColumn: string | null;

  @Column("varchar", { name: "ParaArr", nullable: true, length: 200 })
  paraArr: string | null;

  @Column("bit", { name: "IsDisable", nullable: true, default: () => "(0)" })
  isDisable: boolean | null;

  @Column("bit", { name: "IsReload", default: () => "(0)" })
  isReload: boolean;

  @Column("nvarchar", { name: "WhereString", nullable: true, length: 250 })
  whereString: string | null;

  @Column("bit", { name: "IsPrimaryKeyCombine", nullable: true })
  isPrimaryKeyCombine: boolean | null;

  @Column("nvarchar", { name: "PrimaryKeyFormat", nullable: true, length: 250 })
  primaryKeyFormat: string | null;

  @Column("int", { name: "PrimaryKeyLen", default: () => "(4)" })
  primaryKeyLen: number;

  @Column("int", { name: "Operator", nullable: true })
  operator: number | null;

  @Column("nvarchar", { name: "DefaultValue", nullable: true, length: 100 })
  defaultValue: string | null;

  @Column("nvarchar", { name: "DefaultValueSQL", nullable: true, length: 250 })
  defaultValueSql: string | null;

  @Column("nvarchar", { name: "GroupColumnArr", nullable: true, length: 250 })
  groupColumnArr: string | null;

  @Column("int", { name: "EditType", nullable: true })
  editType: number | null;

  @Column("varchar", { name: "DisplayMember2", nullable: true, length: 50 })
  displayMember2: string | null;
}
