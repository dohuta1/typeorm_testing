import { Column, Entity, Index, JoinColumn, ManyToOne } from "typeorm";
import { FaAssetTbl } from "./FaAssetTbl";

@Index("PK_FA_AssetDepreTbl", ["userAutoId"], { unique: true })
@Entity("FA_AssetDepreTbl", { schema: "dbo" })
export class FaAssetDepreTbl {
  @Column("varchar", { primary: true, name: "UserAutoID", length: 50 })
  userAutoId: string;

  @Column("varchar", { name: "PeriodID", length: 50 })
  periodId: string;

  @Column("nvarchar", { name: "AssetName", nullable: true, length: 200 })
  assetName: string | null;

  @Column("varchar", { name: "DocumentID", length: 50 })
  documentId: string;

  @Column("datetime", { name: "DocumentDate" })
  documentDate: Date;

  @Column("decimal", { name: "Amount", precision: 28, scale: 0 })
  amount: number;

  @Column("decimal", {
    name: "OpenDepre",
    nullable: true,
    precision: 28,
    scale: 0
  })
  openDepre: number | null;

  @Column("decimal", {
    name: "DepreAmount",
    nullable: true,
    precision: 28,
    scale: 0
  })
  depreAmount: number | null;

  @Column("decimal", {
    name: "EndDepre",
    nullable: true,
    precision: 28,
    scale: 0
  })
  endDepre: number | null;

  @Column("decimal", {
    name: "RemainAmount",
    nullable: true,
    precision: 28,
    scale: 0
  })
  remainAmount: number | null;

  @Column("varchar", { name: "DepreAccID", length: 30 })
  depreAccId: string;

  @Column("varchar", { name: "ExpenseAccID", length: 50 })
  expenseAccId: string;

  @ManyToOne(
    () => FaAssetTbl,
    faAssetTbl => faAssetTbl.faAssetDepreTbls,
    { onDelete: "CASCADE", onUpdate: "CASCADE" }
  )
  @JoinColumn([{ name: "AssetID", referencedColumnName: "assetId" }])
  asset: FaAssetTbl;
}
