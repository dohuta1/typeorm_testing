import { Column, Entity, Index, OneToMany } from "typeorm";
import { CfItemTbl } from "./CfItemTbl";

@Index("PK_CF_CategoryTbl", ["categoryId"], { unique: true })
@Entity("CF_CategoryTbl", { schema: "dbo" })
export class CfCategoryTbl {
  @Column("varchar", { primary: true, name: "CategoryID", length: 50 })
  categoryId: string;

  @Column("nvarchar", { name: "CategoryName", length: 100 })
  categoryName: string;

  @Column("bit", { name: "isDefault", default: () => "0" })
  isDefault: boolean;

  @Column("varchar", { name: "InvAccID", nullable: true, length: 50 })
  invAccId: string | null;

  @Column("bit", { name: "isWaste", default: () => "0" })
  isWaste: boolean;

  @Column("varchar", { name: "UserCreate", nullable: true, length: 30 })
  userCreate: string | null;

  @Column("varchar", { name: "UserUpdate", nullable: true, length: 30 })
  userUpdate: string | null;

  @Column("datetime", { name: "DateUpdate", nullable: true })
  dateUpdate: Date | null;

  @Column("datetime", { name: "DateCreate", nullable: true })
  dateCreate: Date | null;

  @Column("bit", { name: "isNL", default: () => "(1)" })
  isNl: boolean;

  @Column("bit", { name: "isBTP", default: () => "(1)" })
  isBtp: boolean;

  @Column("bit", { name: "isTP", default: () => "(1)" })
  isTp: boolean;

  @Column("bit", { name: "isHH", default: () => "(1)" })
  isHh: boolean;

  @Column("bit", { name: "isVT", default: () => "(1)" })
  isVt: boolean;

  @OneToMany(
    () => CfItemTbl,
    cfItemTbl => cfItemTbl.category
  )
  cfItemTbls: CfItemTbl[];
}
