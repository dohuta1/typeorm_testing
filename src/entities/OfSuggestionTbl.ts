import { Column, Entity, Index, OneToMany } from "typeorm";
import { OfSuggestionDetailTbl } from "./OfSuggestionDetailTbl";

@Index("PK_OF_SuggestionTbl", ["documentId"], { unique: true })
@Entity("OF_SuggestionTbl", { schema: "dbo" })
export class OfSuggestionTbl {
  @Column("varchar", { primary: true, name: "DocumentID", length: 50 })
  documentId: string;

  @Column("datetime", { name: "DocumentDate" })
  documentDate: Date;

  @Column("varchar", { name: "NguoiDeXuat", length: 50 })
  nguoiDeXuat: string;

  @Column("nvarchar", { name: "BoPhan", nullable: true, length: 50 })
  boPhan: string | null;

  @Column("int", { name: "LoaiDeXuat", default: () => "0" })
  loaiDeXuat: number;

  @Column("nvarchar", { name: "LyDo", nullable: true, length: 250 })
  lyDo: string | null;

  @Column("nvarchar", { name: "GhiChu", nullable: true, length: 250 })
  ghiChu: string | null;

  @Column("nvarchar", { name: "YKien", nullable: true, length: 250 })
  yKien: string | null;

  @Column("bit", { name: "isLock", default: () => "0" })
  isLock: boolean;

  @Column("datetime", { name: "DateCreate", nullable: true })
  dateCreate: Date | null;

  @Column("datetime", { name: "DateUpdate", nullable: true })
  dateUpdate: Date | null;

  @Column("varchar", { name: "UserCreate", nullable: true, length: 20 })
  userCreate: string | null;

  @Column("varchar", { name: "UserUpdate", nullable: true, length: 20 })
  userUpdate: string | null;

  @OneToMany(
    () => OfSuggestionDetailTbl,
    ofSuggestionDetailTbl => ofSuggestionDetailTbl.document
  )
  ofSuggestionDetailTbls: OfSuggestionDetailTbl[];
}
