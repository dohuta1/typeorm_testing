import { Column, Entity, Index } from "typeorm";

@Index("PK_SY_ReportPara", ["paraName"], { unique: true })
@Entity("SY_RpTbl", { schema: "dbo" })
export class SyRpTbl {
  @Column("varchar", { primary: true, name: "ParaName", length: 50 })
  paraName: string;

  @Column("nvarchar", { name: "VN", nullable: true, length: 50 })
  vn: string | null;

  @Column("int", { name: "Type" })
  type: number;

  @Column("int", { name: "W", nullable: true, default: () => "(250)" })
  w: number | null;

  @Column("int", { name: "H", nullable: true, default: () => "(0)" })
  h: number | null;

  @Column("nvarchar", { name: "RequireCondition", nullable: true, length: 100 })
  requireCondition: string | null;

  @Column("int", { name: "ActionEnum", default: () => "(0)" })
  actionEnum: number;

  @Column("nvarchar", { name: "ValueList", nullable: true })
  valueList: string | null;

  @Column("varchar", { name: "ValueMember", nullable: true, length: 50 })
  valueMember: string | null;

  @Column("varchar", { name: "DisplayMember", nullable: true, length: 50 })
  displayMember: string | null;

  @Column("varchar", { name: "DisplayMember2", nullable: true, length: 50 })
  displayMember2: string | null;

  @Column("varchar", { name: "ColArr", nullable: true, length: 100 })
  colArr: string | null;

  @Column("varchar", { name: "WidthArr", nullable: true, length: 50 })
  widthArr: string | null;

  @Column("varchar", { name: "LinkColumn", nullable: true, length: 100 })
  linkColumn: string | null;

  @Column("varchar", { name: "GroupColumnArr", nullable: true, length: 100 })
  groupColumnArr: string | null;

  @Column("int", { name: "ArrangeEnum", nullable: true, default: () => "(0)" })
  arrangeEnum: number | null;

  @Column("int", { name: "WUnit", nullable: true, default: () => "(0)" })
  wUnit: number | null;

  @Column("int", { name: "L", nullable: true, default: () => "(0)" })
  l: number | null;

  @Column("int", { name: "T", nullable: true, default: () => "(0)" })
  t: number | null;

  @Column("bit", {
    name: "ShowComboButton",
    nullable: true,
    default: () => "(0)"
  })
  showComboButton: boolean | null;

  @Column("bit", {
    name: "isMultiSelect",
    nullable: true,
    default: () => "(0)"
  })
  isMultiSelect: boolean | null;

  @Column("bit", { name: "isManualSearch", nullable: true })
  isManualSearch: boolean | null;

  @Column("bit", { name: "IsReload", nullable: true, default: () => "(1)" })
  isReload: boolean | null;
}
