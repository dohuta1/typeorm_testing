import { Column, Entity, Index } from "typeorm";

@Index("PK_CF_PaymentTermTbl", ["paymentTermId"], { unique: true })
@Entity("CF_PaymentTermTbl", { schema: "dbo" })
export class CfPaymentTermTbl {
  @Column("varchar", { primary: true, name: "PaymentTermID", length: 50 })
  paymentTermId: string;

  @Column("nvarchar", { name: "PaymentTermName", nullable: true, length: 70 })
  paymentTermName: string | null;

  @Column("nvarchar", { name: "PaymentTermName2", nullable: true, length: 70 })
  paymentTermName2: string | null;

  @Column("smallint", { name: "BalanceDueDays", nullable: true })
  balanceDueDays: number | null;

  @Column("bit", { name: "isDefault", nullable: true })
  isDefault: boolean | null;

  @Column("varchar", { name: "UserCreate", nullable: true, length: 30 })
  userCreate: string | null;

  @Column("varchar", { name: "UserUpdate", nullable: true, length: 30 })
  userUpdate: string | null;

  @Column("datetime", { name: "DateUpdate", nullable: true })
  dateUpdate: Date | null;

  @Column("datetime", { name: "DateCreate", nullable: true })
  dateCreate: Date | null;
}
