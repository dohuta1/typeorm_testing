import { Column, Entity, Index } from "typeorm";

@Index("PK_SY_RpFTbl", ["userAutoId"], { unique: true })
@Entity("SY_RpFTbl", { schema: "dbo" })
export class SyRpFTbl {
  @Column("varchar", { primary: true, name: "UserAutoID", length: 50 })
  userAutoId: string;

  @Column("nvarchar", { name: "FileName", nullable: true, length: 250 })
  fileName: string | null;

  @Column("datetime", { name: "FileDate", nullable: true })
  fileDate: Date | null;

  @Column("nvarchar", { name: "Title", nullable: true, length: 150 })
  title: string | null;

  @Column("nvarchar", { name: "Notes", nullable: true, length: 150 })
  notes: string | null;

  @Column("image", { name: "FileContent", nullable: true })
  fileContent: Buffer | null;
}
