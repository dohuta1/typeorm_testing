import { Column, Entity, Index, JoinColumn, ManyToOne } from "typeorm";
import { SyUserGroup } from "./SyUserGroup";

@Index("PK_SY_UserGroupDetail", ["userAutoId"], { unique: true })
@Entity("SY_UserGroupDetail", { schema: "dbo" })
export class SyUserGroupDetail {
  @Column("varchar", { primary: true, name: "UserAutoID", length: 50 })
  userAutoId: string;

  @Column("varchar", { name: "KeyID", length: 50 })
  keyId: string;

  @Column("nvarchar", { name: "KeyName", length: 100 })
  keyName: string;

  @Column("bit", { name: "isDisable", default: () => "0" })
  isDisable: boolean;

  @Column("decimal", { name: "Value", nullable: true, precision: 18, scale: 0 })
  value: number | null;

  @Column("nvarchar", { name: "Notes", nullable: true, length: 50 })
  notes: string | null;

  @ManyToOne(
    () => SyUserGroup,
    syUserGroup => syUserGroup.syUserGroupDetails,
    { onDelete: "CASCADE", onUpdate: "CASCADE" }
  )
  @JoinColumn([{ name: "UserGroupID", referencedColumnName: "userGroupId" }])
  userGroup: SyUserGroup;
}
