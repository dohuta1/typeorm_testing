import { Column, Entity, Index, JoinColumn, ManyToOne } from "typeorm";
import { FaAssetTbl } from "./FaAssetTbl";
import { FaAdjustTbl } from "./FaAdjustTbl";

@Index("PK_FA_AdjustDetailTbl", ["userAutoId"], { unique: true })
@Entity("FA_AdjustDetailTbl", { schema: "dbo" })
export class FaAdjustDetailTbl {
  @Column("varchar", { primary: true, name: "UserAutoID", length: 40 })
  userAutoId: string;

  @Column("decimal", {
    name: "SourceAmount",
    nullable: true,
    precision: 28,
    scale: 2
  })
  sourceAmount: number | null;

  @Column("decimal", {
    name: "Amount",
    nullable: true,
    precision: 28,
    scale: 0
  })
  amount: number | null;

  @Column("nvarchar", { name: "Notes", nullable: true, length: 150 })
  notes: string | null;

  @Column("varchar", { name: "ParentID", nullable: true, length: 50 })
  parentId: string | null;

  @Column("numeric", {
    name: "ItemBin",
    nullable: true,
    precision: 18,
    scale: 0
  })
  itemBin: number | null;

  @Column("varchar", { name: "PosID", nullable: true, length: 50 })
  posId: string | null;

  @ManyToOne(
    () => FaAssetTbl,
    faAssetTbl => faAssetTbl.faAdjustDetailTbls,
    { onUpdate: "CASCADE" }
  )
  @JoinColumn([{ name: "AssetID", referencedColumnName: "assetId" }])
  asset: FaAssetTbl;

  @ManyToOne(
    () => FaAdjustTbl,
    faAdjustTbl => faAdjustTbl.faAdjustDetailTbls,
    { onDelete: "CASCADE", onUpdate: "CASCADE" }
  )
  @JoinColumn([{ name: "DocumentID", referencedColumnName: "documentId" }])
  document: FaAdjustTbl;
}
