import { Column, Entity, Index, OneToMany } from "typeorm";
import { ArGiftsDetailTbl } from "./ArGiftsDetailTbl";
import { ArGiftsObjectTbl } from "./ArGiftsObjectTbl";

@Index("PK_AR_GiftsTbl", ["documentId"], { unique: true })
@Entity("AR_GiftsTbl", { schema: "dbo" })
export class ArGiftsTbl {
  @Column("varchar", { primary: true, name: "DocumentID", length: 30 })
  documentId: string;

  @Column("datetime", { name: "FromDate" })
  fromDate: Date;

  @Column("datetime", { name: "ToDate" })
  toDate: Date;

  @Column("nvarchar", { name: "Memo", nullable: true, length: 200 })
  memo: string | null;

  @Column("bit", { name: "isLock", default: () => "(0)" })
  isLock: boolean;

  @Column("varchar", { name: "UserCreate", nullable: true, length: 30 })
  userCreate: string | null;

  @Column("varchar", { name: "UserUpdate", nullable: true, length: 30 })
  userUpdate: string | null;

  @Column("datetime", { name: "DateUpdate", nullable: true })
  dateUpdate: Date | null;

  @Column("datetime", { name: "DateCreate", nullable: true })
  dateCreate: Date | null;

  @Column("bit", { name: "isDisable", default: () => "(0)" })
  isDisable: boolean;

  @OneToMany(
    () => ArGiftsDetailTbl,
    arGiftsDetailTbl => arGiftsDetailTbl.document
  )
  arGiftsDetailTbls: ArGiftsDetailTbl[];

  @OneToMany(
    () => ArGiftsObjectTbl,
    arGiftsObjectTbl => arGiftsObjectTbl.document
  )
  arGiftsObjectTbls: ArGiftsObjectTbl[];
}
