import { Column, Entity, Index, JoinColumn, ManyToOne } from "typeorm";
import { CfCostTbl } from "./CfCostTbl";
import { CfCostCenterTbl } from "./CfCostCenterTbl";
import { CfItemTbl } from "./CfItemTbl";
import { CfStoreHouseTbl } from "./CfStoreHouseTbl";
import { IvInputTbl } from "./IvInputTbl";

@Index("IX_IV_InputDetailTbl_DocumentID", ["documentId"], {})
@Index("PK_IV_InputDetailTbl", ["userAutoId"], { unique: true })
@Entity("IV_InputDetailTbl", { schema: "dbo" })
export class IvInputDetailTbl {
  @Column("varchar", { primary: true, name: "UserAutoID", length: 40 })
  userAutoId: string;

  @Column("varchar", { name: "DocumentID", length: 30 })
  documentId: string;

  @Column("varchar", { name: "Lot", nullable: true, length: 50 })
  lot: string | null;

  @Column("datetime", { name: "ExpireDate", nullable: true })
  expireDate: Date | null;

  @Column("decimal", {
    name: "Quantity",
    nullable: true,
    precision: 28,
    scale: 4
  })
  quantity: number | null;

  @Column("decimal", {
    name: "UnitPrice",
    nullable: true,
    precision: 28,
    scale: 2
  })
  unitPrice: number | null;

  @Column("decimal", {
    name: "Amount",
    nullable: true,
    precision: 28,
    scale: 0
  })
  amount: number | null;

  @Column("varchar", { name: "InvAccID", length: 50 })
  invAccId: string;

  @Column("varchar", { name: "AccountID2", length: 50 })
  accountId2: string;

  @Column("nvarchar", { name: "Notes", nullable: true, length: 100 })
  notes: string | null;

  @Column("decimal", {
    name: "Quantity2",
    nullable: true,
    precision: 28,
    scale: 4
  })
  quantity2: number | null;

  @Column("nvarchar", { name: "Property", nullable: true, length: 50 })
  property: string | null;

  @Column("nvarchar", { name: "Property2", nullable: true, length: 50 })
  property2: string | null;

  @Column("varchar", { name: "ParentID", nullable: true, length: 50 })
  parentId: string | null;

  @Column("float", { name: "UnitFactor", nullable: true, precision: 53 })
  unitFactor: number | null;

  @Column("varchar", { name: "PacketGroup", nullable: true, length: 50 })
  packetGroup: string | null;

  @Column("varchar", { name: "EquipID", nullable: true, length: 50 })
  equipId: string | null;

  @Column("varchar", { name: "RefDoc", nullable: true, length: 50 })
  refDoc: string | null;

  @ManyToOne(
    () => CfCostTbl,
    cfCostTbl => cfCostTbl.ivInputDetailTbls,
    { onUpdate: "CASCADE" }
  )
  @JoinColumn([{ name: "CostID", referencedColumnName: "costId" }])
  cost: CfCostTbl;

  @ManyToOne(
    () => CfCostCenterTbl,
    cfCostCenterTbl => cfCostCenterTbl.ivInputDetailTbls,
    { onUpdate: "CASCADE" }
  )
  @JoinColumn([{ name: "CostCenterID", referencedColumnName: "costCenterId" }])
  costCenter: CfCostCenterTbl;

  @ManyToOne(
    () => CfItemTbl,
    cfItemTbl => cfItemTbl.ivInputDetailTbls,
    { onUpdate: "CASCADE" }
  )
  @JoinColumn([{ name: "ItemID", referencedColumnName: "itemId" }])
  item: CfItemTbl;

  @ManyToOne(
    () => CfStoreHouseTbl,
    cfStoreHouseTbl => cfStoreHouseTbl.ivInputDetailTbls,
    { onUpdate: "CASCADE" }
  )
  @JoinColumn([{ name: "StoreHouseID", referencedColumnName: "storeHouseId" }])
  storeHouse: CfStoreHouseTbl;

  @ManyToOne(
    () => IvInputTbl,
    ivInputTbl => ivInputTbl.ivInputDetailTbls,
    { onDelete: "CASCADE", onUpdate: "CASCADE" }
  )
  @JoinColumn([{ name: "DocumentID", referencedColumnName: "documentId" }])
  document: IvInputTbl;
}
