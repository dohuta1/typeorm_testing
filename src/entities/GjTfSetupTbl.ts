import { Column, Entity, Index } from "typeorm";

@Index("PK_GJ_TransferSetupTbl", ["transferId"], { unique: true })
@Entity("GJ_TFSetupTbl", { schema: "dbo" })
export class GjTfSetupTbl {
  @Column("varchar", { primary: true, name: "TransferID", length: 10 })
  transferId: string;

  @Column("nvarchar", { name: "TransferName", length: 100 })
  transferName: string;

  @Column("varchar", { name: "FromAccountID", nullable: true, length: 20 })
  fromAccountId: string | null;

  @Column("varchar", { name: "ToAccountID", nullable: true, length: 20 })
  toAccountId: string | null;

  @Column("varchar", { name: "TransferKind", length: 1 })
  transferKind: string;

  @Column("int", { name: "STT", nullable: true })
  stt: number | null;

  @Column("varchar", { name: "AccountID2", nullable: true, length: 50 })
  accountId2: string | null;

  @Column("bit", { name: "isCost", default: () => "(0)" })
  isCost: boolean;

  @Column("bit", { name: "isCostCenter", default: () => "(0)" })
  isCostCenter: boolean;

  @Column("bit", { name: "isDisable", default: () => "(0)" })
  isDisable: boolean;

  @Column("varchar", { name: "ToObjectID", nullable: true, length: 50 })
  toObjectId: string | null;
}
