import { Column, Entity, Index } from "typeorm";

@Index("PK_HR_TimeSheetTbl", ["userAutoId"], { unique: true })
@Entity("HR_TimeSheetTbl", { schema: "dbo" })
export class HrTimeSheetTbl {
  @Column("varchar", { primary: true, name: "UserAutoID", length: 50 })
  userAutoId: string;

  @Column("varchar", { name: "PersonID", length: 50 })
  personId: string;

  @Column("datetime", { name: "DocumentDate" })
  documentDate: Date;

  @Column("decimal", {
    name: "SoNgayThang",
    nullable: true,
    precision: 18,
    scale: 1
  })
  soNgayThang: number | null;

  @Column("decimal", {
    name: "SoNgayDiLam",
    nullable: true,
    precision: 18,
    scale: 1
  })
  soNgayDiLam: number | null;

  @Column("decimal", {
    name: "TangCa",
    nullable: true,
    precision: 18,
    scale: 1
  })
  tangCa: number | null;

  @Column("decimal", {
    name: "SoNgayCongTac",
    nullable: true,
    precision: 18,
    scale: 1
  })
  soNgayCongTac: number | null;

  @Column("decimal", {
    name: "CongPhep",
    nullable: true,
    precision: 18,
    scale: 1
  })
  congPhep: number | null;

  @Column("decimal", {
    name: "NghiPhep",
    nullable: true,
    precision: 18,
    scale: 1
  })
  nghiPhep: number | null;

  @Column("decimal", {
    name: "NghiKhongPhep",
    nullable: true,
    precision: 18,
    scale: 1
  })
  nghiKhongPhep: number | null;

  @Column("nvarchar", { name: "GhiChu", nullable: true, length: 50 })
  ghiChu: string | null;
}
