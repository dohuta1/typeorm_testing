import { Column, Entity, Index } from "typeorm";

@Index("PK_CF_ImpExpTbl", ["impExpId"], { unique: true })
@Entity("CF_ImpExpTbl", { schema: "dbo" })
export class CfImpExpTbl {
  @Column("varchar", { primary: true, name: "ImpExpID", length: 50 })
  impExpId: string;

  @Column("nvarchar", { name: "ImExName", length: 150 })
  imExName: string;

  @Column("bit", { name: "isDefault", nullable: true, default: () => "0" })
  isDefault: boolean | null;

  @Column("int", { name: "Type" })
  type: number;

  @Column("bit", { name: "CalPrice", default: () => "0" })
  calPrice: boolean;

  @Column("bit", { name: "CalCost", default: () => "0" })
  calCost: boolean;

  @Column("bit", { name: "isPacket", default: () => "0" })
  isPacket: boolean;

  @Column("varchar", { name: "AccountID2", nullable: true, length: 50 })
  accountId2: string | null;
}
