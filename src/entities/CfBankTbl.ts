import { Column, Entity, Index } from "typeorm";

@Index("PK_CF_BankTbl_1", ["accountNo"], { unique: true })
@Entity("CF_BankTbl", { schema: "dbo" })
export class CfBankTbl {
  @Column("varchar", { primary: true, name: "AccountNo", length: 50 })
  accountNo: string;

  @Column("nvarchar", { name: "BankName", length: 100 })
  bankName: string;

  @Column("nvarchar", { name: "BankName2", nullable: true, length: 100 })
  bankName2: string | null;

  @Column("nvarchar", { name: "Address", nullable: true, length: 150 })
  address: string | null;

  @Column("varchar", { name: "ObjectID", nullable: true, length: 50 })
  objectId: string | null;

  @Column("nvarchar", { name: "Notes", nullable: true, length: 50 })
  notes: string | null;

  @Column("varchar", { name: "UserCreate", length: 30 })
  userCreate: string;

  @Column("varchar", { name: "UserUpdate", nullable: true, length: 30 })
  userUpdate: string | null;

  @Column("datetime", { name: "DateUpdate", nullable: true })
  dateUpdate: Date | null;

  @Column("datetime", { name: "DateCreate", nullable: true })
  dateCreate: Date | null;
}
