import { Column, Entity, Index } from "typeorm";

@Index("PK_SY_FormList", ["formId"], { unique: true })
@Entity("SY_FrmLstTbl", { schema: "dbo" })
export class SyFrmLstTbl {
  @Column("varchar", { primary: true, name: "FormID", length: 100 })
  formId: string;

  @Column("varchar", {
    name: "FormType",
    nullable: true,
    length: 50,
    default: () => "'LISTEDIT2'"
  })
  formType: string | null;

  @Column("nvarchar", {
    name: "CaptionVN",
    length: 100,
    default: () => "N'danh mục'"
  })
  captionVn: string;

  @Column("nvarchar", { name: "CaptionEN", nullable: true, length: 100 })
  captionEn: string | null;

  @Column("varchar", { name: "TableName", nullable: true, length: 100 })
  tableName: string | null;

  @Column("varchar", { name: "PrimaryKey", nullable: true, length: 100 })
  primaryKey: string | null;

  @Column("varchar", { name: "HideColumnArr", nullable: true })
  hideColumnArr: string | null;

  @Column("varchar", { name: "SummaryColumnArr", nullable: true, length: 200 })
  summaryColumnArr: string | null;

  @Column("varchar", { name: "AddNewColumnArr", nullable: true })
  addNewColumnArr: string | null;

  @Column("nvarchar", { name: "FilterEx", nullable: true, length: 50 })
  filterEx: string | null;

  @Column("varchar", { name: "LockColumnArr", nullable: true, length: 100 })
  lockColumnArr: string | null;

  @Column("varchar", { name: "TableDetail", nullable: true, length: 50 })
  tableDetail: string | null;

  @Column("varchar", {
    name: "TableDetailLeftJoinField",
    nullable: true,
    length: 100
  })
  tableDetailLeftJoinField: string | null;

  @Column("varchar", {
    name: "TableDetailSelectFrom",
    nullable: true,
    length: 500
  })
  tableDetailSelectFrom: string | null;

  @Column("varchar", { name: "TableDetail2", nullable: true, length: 50 })
  tableDetail2: string | null;

  @Column("varchar", {
    name: "TableDetail2LeftJoinField",
    nullable: true,
    length: 100
  })
  tableDetail2LeftJoinField: string | null;

  @Column("varchar", {
    name: "TableDetail2SelectFrom",
    nullable: true,
    length: 500
  })
  tableDetail2SelectFrom: string | null;

  @Column("nvarchar", { name: "Filter", nullable: true, length: 100 })
  filter: string | null;

  @Column("nvarchar", { name: "PrimaryKeyFormat", nullable: true, length: 250 })
  primaryKeyFormat: string | null;

  @Column("int", { name: "PrimaryKeyLen", nullable: true })
  primaryKeyLen: number | null;

  @Column("varchar", { name: "DefaultColumnArr", nullable: true, length: 250 })
  defaultColumnArr: string | null;

  @Column("nvarchar", { name: "CaptionCH", nullable: true, length: 200 })
  captionCh: string | null;

  @Column("varchar", { name: "EditorColumnArr", nullable: true })
  editorColumnArr: string | null;
}
