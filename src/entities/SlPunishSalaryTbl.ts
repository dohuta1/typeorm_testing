import { Column, Entity, Index, JoinColumn, ManyToOne } from "typeorm";
import { KdTargetTypeTbl } from "./KdTargetTypeTbl";
import { SlSalaryGroupTbl } from "./SlSalaryGroupTbl";

@Index("PK_SL_PunishSalaryTbl", ["autoId"], { unique: true })
@Entity("SL_PunishSalaryTbl", { schema: "dbo" })
export class SlPunishSalaryTbl {
  @Column("varchar", {
    primary: true,
    name: "AutoID",
    length: 50,
    default: () => "newid()"
  })
  autoId: string;

  @Column("decimal", { name: "PercentTarget", precision: 3, scale: 0 })
  percentTarget: number;

  @Column("decimal", { name: "PunishAmount", precision: 18, scale: 0 })
  punishAmount: number;

  @Column("datetime", { name: "StartDate" })
  startDate: Date;

  @Column("datetime", { name: "EndDate", nullable: true })
  endDate: Date | null;

  @Column("nvarchar", { name: "Notes", nullable: true, length: 200 })
  notes: string | null;

  @ManyToOne(
    () => KdTargetTypeTbl,
    kdTargetTypeTbl => kdTargetTypeTbl.slPunishSalaryTbls,
    { onUpdate: "CASCADE" }
  )
  @JoinColumn([{ name: "TargetTypeID", referencedColumnName: "targetTypeId" }])
  targetType: KdTargetTypeTbl;

  @ManyToOne(
    () => SlSalaryGroupTbl,
    slSalaryGroupTbl => slSalaryGroupTbl.slPunishSalaryTbls,
    { onDelete: "CASCADE", onUpdate: "CASCADE" }
  )
  @JoinColumn([
    { name: "SalaryGroupID", referencedColumnName: "salaryGroupId" }
  ])
  salaryGroup: SlSalaryGroupTbl;
}
