import { Column, Entity, Index, JoinColumn, ManyToOne } from "typeorm";
import { IvInputTbl } from "./IvInputTbl";

@Index("PK_IV_InputCouponTbl", ["userAutoId"], { unique: true })
@Entity("IV_InputCouponTbl", { schema: "dbo" })
export class IvInputCouponTbl {
  @Column("varchar", { primary: true, name: "UserAutoID", length: 40 })
  userAutoId: string;

  @Column("varchar", { name: "CouponSeri", nullable: true, length: 100 })
  couponSeri: string | null;

  @Column("decimal", {
    name: "TriGia",
    nullable: true,
    precision: 18,
    scale: 0
  })
  triGia: number | null;

  @Column("nvarchar", { name: "Notes", nullable: true, length: 50 })
  notes: string | null;

  @ManyToOne(
    () => IvInputTbl,
    ivInputTbl => ivInputTbl.ivInputCouponTbls,
    { onDelete: "CASCADE", onUpdate: "CASCADE" }
  )
  @JoinColumn([{ name: "DocumentID", referencedColumnName: "documentId" }])
  document: IvInputTbl;
}
