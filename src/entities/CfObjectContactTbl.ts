import { Column, Entity, Index, JoinColumn, ManyToOne } from "typeorm";
import { CfObjectTbl } from "./CfObjectTbl";

@Index("PK_CF_ObjectContactTbl", ["userAutoId"], { unique: true })
@Entity("CF_ObjectContactTbl", { schema: "dbo" })
export class CfObjectContactTbl {
  @Column("varchar", { primary: true, name: "UserAutoID", length: 50 })
  userAutoId: string;

  @Column("nvarchar", { name: "ContactName", nullable: true, length: 50 })
  contactName: string | null;

  @Column("nvarchar", { name: "ContactPhone", nullable: true, length: 50 })
  contactPhone: string | null;

  @Column("nvarchar", { name: "Notes", nullable: true, length: 50 })
  notes: string | null;

  @Column("varchar", { name: "ContactCode", nullable: true, length: 50 })
  contactCode: string | null;

  @Column("nvarchar", { name: "KhoaLamViec", nullable: true, length: 250 })
  khoaLamViec: string | null;

  @Column("nvarchar", { name: "SoTaiKhoan", nullable: true, length: 250 })
  soTaiKhoan: string | null;

  @Column("nvarchar", { name: "ChuTaiKhoan", nullable: true, length: 250 })
  chuTaiKhoan: string | null;

  @Column("nvarchar", { name: "NganHang", nullable: true, length: 250 })
  nganHang: string | null;

  @Column("nvarchar", { name: "ChiNhanh", nullable: true, length: 250 })
  chiNhanh: string | null;

  @Column("nvarchar", { name: "NoiDungChamSoc", nullable: true, length: 250 })
  noiDungChamSoc: string | null;

  @Column("varchar", { name: "LoaiChamSoc", nullable: true, length: 50 })
  loaiChamSoc: string | null;

  @Column("datetime", { name: "NgayChamSoc", nullable: true })
  ngayChamSoc: Date | null;

  @Column("datetime", { name: "NgayChamSoc2", nullable: true })
  ngayChamSoc2: Date | null;

  @Column("datetime", { name: "NgayChamSoc3", nullable: true })
  ngayChamSoc3: Date | null;

  @ManyToOne(
    () => CfObjectTbl,
    cfObjectTbl => cfObjectTbl.cfObjectContactTbls,
    { onDelete: "CASCADE", onUpdate: "CASCADE" }
  )
  @JoinColumn([{ name: "ObjectID", referencedColumnName: "objectId" }])
  object: CfObjectTbl;
}
