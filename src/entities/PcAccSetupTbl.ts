import { Column, Entity, Index, JoinColumn, ManyToOne } from "typeorm";
import { CfCostCenterTbl } from "./CfCostCenterTbl";
import { CfCostTbl } from "./CfCostTbl";

@Index("PK_PC_AccountSetupTbl", ["userAutoId"], { unique: true })
@Entity("PC_AccSetupTbl", { schema: "dbo" })
export class PcAccSetupTbl {
  @Column("varchar", { primary: true, name: "UserAutoID", length: 40 })
  userAutoId: string;

  @Column("varchar", { name: "AccountID", nullable: true, length: 50 })
  accountId: string | null;

  @Column("varchar", { name: "CategoryID", nullable: true, length: 50 })
  categoryId: string | null;

  @Column("varchar", { name: "ItemID", nullable: true, length: 50 })
  itemId: string | null;

  @Column("int", { name: "ALType", default: () => "1" })
  alType: number;

  @ManyToOne(
    () => CfCostCenterTbl,
    cfCostCenterTbl => cfCostCenterTbl.pcAccSetupTbls,
    { onUpdate: "CASCADE" }
  )
  @JoinColumn([{ name: "CostCenterID", referencedColumnName: "costCenterId" }])
  costCenter: CfCostCenterTbl;

  @ManyToOne(
    () => CfCostTbl,
    cfCostTbl => cfCostTbl.pcAccSetupTbls,
    { onUpdate: "CASCADE" }
  )
  @JoinColumn([{ name: "CostID", referencedColumnName: "costId" }])
  cost: CfCostTbl;
}
