import {
  Column,
  Entity,
  Index,
  OneToMany,
  PrimaryGeneratedColumn
} from "typeorm";
import { CfLocationDetailTbl } from "./CfLocationDetailTbl";
import { CfObjectTbl } from "./CfObjectTbl";

@Index("PK_CF_LocationTbl", ["locationId"], { unique: true })
@Entity("CF_LocationTbl", { schema: "dbo" })
export class CfLocationTbl {
  @Column("nvarchar", { primary: true, name: "LocationID", length: 50 })
  locationId: string;

  @Column("nvarchar", { name: "LocationName", length: 100 })
  locationName: string;

  @Column("nvarchar", { name: "LocationGroup", nullable: true, length: 100 })
  locationGroup: string | null;

  @PrimaryGeneratedColumn({ type: "int", name: "STT" })
  stt: number;

  @Column("varchar", { name: "UserCreate", nullable: true, length: 30 })
  userCreate: string | null;

  @Column("varchar", { name: "UserUpdate", nullable: true, length: 30 })
  userUpdate: string | null;

  @Column("datetime", { name: "DateUpdate", nullable: true })
  dateUpdate: Date | null;

  @Column("datetime", { name: "DateCreate", nullable: true })
  dateCreate: Date | null;

  @OneToMany(
    () => CfLocationDetailTbl,
    cfLocationDetailTbl => cfLocationDetailTbl.location
  )
  cfLocationDetailTbls: CfLocationDetailTbl[];

  @OneToMany(
    () => CfObjectTbl,
    cfObjectTbl => cfObjectTbl.location
  )
  cfObjectTbls: CfObjectTbl[];
}
