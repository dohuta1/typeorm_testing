import { Column, Entity, Index, JoinColumn, ManyToOne } from "typeorm";
import { CsPaymentPlanTbl } from "./CsPaymentPlanTbl";

@Index("PK_CS_PaymentPlanAttachTbl", ["userAutoId"], { unique: true })
@Entity("CS_PaymentPlanAttachTbl", { schema: "dbo" })
export class CsPaymentPlanAttachTbl {
  @Column("varchar", { primary: true, name: "UserAutoID", length: 50 })
  userAutoId: string;

  @Column("nvarchar", { name: "FileName", nullable: true, length: 250 })
  fileName: string | null;

  @Column("int", { name: "FileType", nullable: true, default: () => "(0)" })
  fileType: number | null;

  @Column("nvarchar", { name: "STT", nullable: true, length: 50 })
  stt: string | null;

  @Column("image", { name: "Content", nullable: true })
  content: Buffer | null;

  @Column("image", { name: "Content2", nullable: true })
  content2: Buffer | null;

  @ManyToOne(
    () => CsPaymentPlanTbl,
    csPaymentPlanTbl => csPaymentPlanTbl.csPaymentPlanAttachTbls,
    { onDelete: "CASCADE", onUpdate: "CASCADE" }
  )
  @JoinColumn([{ name: "DocumentID", referencedColumnName: "documentId" }])
  document: CsPaymentPlanTbl;
}
