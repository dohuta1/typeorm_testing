import { Column, Entity, Index, JoinColumn, ManyToOne } from "typeorm";
import { ArItemGroupTbl } from "./ArItemGroupTbl";
import { SlSalaryGroupTbl } from "./SlSalaryGroupTbl";

@Index("PK_SL_ItemGroupTargetTbl", ["autoId"], { unique: true })
@Entity("SL_ItemGroupTargetTbl", { schema: "dbo" })
export class SlItemGroupTargetTbl {
  @Column("varchar", {
    primary: true,
    name: "AutoID",
    length: 50,
    default: () => "newid()"
  })
  autoId: string;

  @Column("decimal", {
    name: "Target",
    precision: 18,
    scale: 0,
    default: () => "(0)"
  })
  target: number;

  @Column("decimal", { name: "Factor", precision: 5, scale: 2 })
  factor: number;

  @Column("datetime", { name: "StartDate" })
  startDate: Date;

  @Column("datetime", { name: "EndDate", nullable: true })
  endDate: Date | null;

  @Column("nvarchar", { name: "Notes", nullable: true, length: 200 })
  notes: string | null;

  @ManyToOne(
    () => ArItemGroupTbl,
    arItemGroupTbl => arItemGroupTbl.slItemGroupTargetTbls,
    { onUpdate: "CASCADE" }
  )
  @JoinColumn([
    { name: "ItemGroupCode", referencedColumnName: "itemGroupCode" }
  ])
  itemGroupCode: ArItemGroupTbl;

  @ManyToOne(
    () => SlSalaryGroupTbl,
    slSalaryGroupTbl => slSalaryGroupTbl.slItemGroupTargetTbls,
    { onDelete: "CASCADE", onUpdate: "CASCADE" }
  )
  @JoinColumn([
    { name: "SalaryGroupID", referencedColumnName: "salaryGroupId" }
  ])
  salaryGroup: SlSalaryGroupTbl;
}
