import { Column, Entity, Index } from "typeorm";

@Index("PK_PC_ProductCostBeginTbl", ["userAutoId"], { unique: true })
@Entity("PC_ProductCostBeginTbl", { schema: "dbo" })
export class PcProductCostBeginTbl {
  @Column("varchar", { primary: true, name: "UserAutoID", length: 50 })
  userAutoId: string;

  @Column("varchar", { name: "AccountID", length: 50 })
  accountId: string;

  @Column("varchar", { name: "CostCenterID", nullable: true, length: 50 })
  costCenterId: string | null;

  @Column("varchar", { name: "CostID", nullable: true, length: 50 })
  costId: string | null;

  @Column("varchar", { name: "ItemID", length: 50 })
  itemId: string;

  @Column("decimal", {
    name: "Quantity",
    nullable: true,
    precision: 28,
    scale: 4
  })
  quantity: number | null;

  @Column("float", { name: "UnitPrice", nullable: true, precision: 53 })
  unitPrice: number | null;

  @Column("decimal", {
    name: "Amount",
    precision: 28,
    scale: 0,
    default: () => "0"
  })
  amount: number;

  @Column("int", { name: "ALType", default: () => "1" })
  alType: number;

  @Column("nvarchar", { name: "Notes", nullable: true, length: 50 })
  notes: string | null;
}
