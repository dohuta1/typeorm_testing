import {
  Column,
  Entity,
  Index,
  JoinColumn,
  ManyToOne,
  OneToMany
} from "typeorm";
import { PcProductCostBalTbl } from "./PcProductCostBalTbl";
import { PcProductCostDetailTbl } from "./PcProductCostDetailTbl";
import { PcProductCostExTbl } from "./PcProductCostExTbl";
import { PcProductCostItemTbl } from "./PcProductCostItemTbl";
import { PcBoMTbl } from "./PcBoMTbl";
import { PcProdustCostMaterialTbl } from "./PcProdustCostMaterialTbl";

@Index("PK_PC_ProductCostTbl", ["priceId"], { unique: true })
@Entity("PC_ProductCostTbl", { schema: "dbo" })
export class PcProductCostTbl {
  @Column("varchar", { primary: true, name: "PriceID", length: 50 })
  priceId: string;

  @Column("varchar", { name: "PeriodID", length: 50 })
  periodId: string;

  @Column("varchar", { name: "CostCenterID", nullable: true, length: 50 })
  costCenterId: string | null;

  @Column("nvarchar", { name: "Memo", nullable: true, length: 50 })
  memo: string | null;

  @Column("int", { name: "CostStep", default: () => "3" })
  costStep: number;

  @Column("bit", { name: "FullMaterial", default: () => "0" })
  fullMaterial: boolean;

  @Column("bit", { name: "isLock", default: () => "0" })
  isLock: boolean;

  @Column("varchar", { name: "UserCreate", nullable: true, length: 30 })
  userCreate: string | null;

  @Column("varchar", { name: "UserUpdate", nullable: true, length: 30 })
  userUpdate: string | null;

  @Column("datetime", { name: "DateUpdate", nullable: true })
  dateUpdate: Date | null;

  @Column("datetime", { name: "DateCreate", nullable: true })
  dateCreate: Date | null;

  @Column("bit", { name: "isCost", default: () => "1" })
  isCost: boolean;

  @Column("bit", { name: "isCostCenter", default: () => "1" })
  isCostCenter: boolean;

  @OneToMany(
    () => PcProductCostBalTbl,
    pcProductCostBalTbl => pcProductCostBalTbl.price
  )
  pcProductCostBalTbls: PcProductCostBalTbl[];

  @OneToMany(
    () => PcProductCostDetailTbl,
    pcProductCostDetailTbl => pcProductCostDetailTbl.price
  )
  pcProductCostDetailTbls: PcProductCostDetailTbl[];

  @OneToMany(
    () => PcProductCostExTbl,
    pcProductCostExTbl => pcProductCostExTbl.price
  )
  pcProductCostExTbls: PcProductCostExTbl[];

  @OneToMany(
    () => PcProductCostItemTbl,
    pcProductCostItemTbl => pcProductCostItemTbl.price
  )
  pcProductCostItemTbls: PcProductCostItemTbl[];

  @ManyToOne(
    () => PcBoMTbl,
    pcBoMTbl => pcBoMTbl.pcProductCostTbls,
    { onUpdate: "CASCADE" }
  )
  @JoinColumn([{ name: "BomID", referencedColumnName: "bomid" }])
  bom: PcBoMTbl;

  @OneToMany(
    () => PcProdustCostMaterialTbl,
    pcProdustCostMaterialTbl => pcProdustCostMaterialTbl.price
  )
  pcProdustCostMaterialTbls: PcProdustCostMaterialTbl[];
}
