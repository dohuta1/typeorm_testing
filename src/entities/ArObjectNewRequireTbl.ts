import { Column, Entity, Index } from "typeorm";

@Index("PK_AR_ObjectNewRequireTbl", ["objectId"], { unique: true })
@Entity("AR_ObjectNewRequireTbl", { schema: "dbo" })
export class ArObjectNewRequireTbl {
  @Column("varchar", { primary: true, name: "ObjectID", length: 50 })
  objectId: string;

  @Column("nvarchar", { name: "ObjectName", length: 150 })
  objectName: string;

  @Column("nvarchar", { name: "Address", nullable: true, length: 150 })
  address: string | null;

  @Column("varchar", { name: "TaxCode", nullable: true, length: 20 })
  taxCode: string | null;

  @Column("varchar", { name: "Phone", nullable: true, length: 100 })
  phone: string | null;

  @Column("varchar", { name: "ObjectGroupID", length: 50 })
  objectGroupId: string;

  @Column("nvarchar", { name: "LocationID", length: 50 })
  locationId: string;

  @Column("nvarchar", { name: "QuanHuyen", length: 50 })
  quanHuyen: string;

  @Column("nvarchar", { name: "ZoneID", nullable: true, length: 50 })
  zoneId: string | null;

  @Column("nvarchar", { name: "DonViNhanHang", nullable: true, length: 100 })
  donViNhanHang: string | null;

  @Column("nvarchar", { name: "DiaChiNhanHang", nullable: true, length: 100 })
  diaChiNhanHang: string | null;

  @Column("nvarchar", { name: "NguoiNhanHang", nullable: true, length: 100 })
  nguoiNhanHang: string | null;

  @Column("nvarchar", {
    name: "DienThoaiNguoiNhanHang",
    nullable: true,
    length: 100
  })
  dienThoaiNguoiNhanHang: string | null;

  @Column("nvarchar", { name: "NguoiDoiChieuCN", nullable: true, length: 100 })
  nguoiDoiChieuCn: string | null;

  @Column("nvarchar", {
    name: "DienThoaiNguoiDoiChieuCN",
    nullable: true,
    length: 100
  })
  dienThoaiNguoiDoiChieuCn: string | null;

  @Column("nvarchar", {
    name: "ThoiGianDoiChieuCN",
    nullable: true,
    length: 100
  })
  thoiGianDoiChieuCn: string | null;

  @Column("nvarchar", { name: "PhuTrachCN", nullable: true, length: 100 })
  phuTrachCn: string | null;

  @Column("nvarchar", { name: "NhaXeGoiHang", nullable: true, length: 100 })
  nhaXeGoiHang: string | null;

  @Column("nvarchar", {
    name: "DienThoaiNhaXeGoiHang",
    nullable: true,
    length: 100
  })
  dienThoaiNhaXeGoiHang: string | null;

  @Column("nvarchar", { name: "BenXe", nullable: true, length: 100 })
  benXe: string | null;

  @Column("nvarchar", { name: "GioXuatBen", nullable: true, length: 100 })
  gioXuatBen: string | null;

  @Column("nvarchar", { name: "GhiChuNhaXe", nullable: true, length: 100 })
  ghiChuNhaXe: string | null;

  @Column("varchar", { name: "Fax", nullable: true, length: 100 })
  fax: string | null;

  @Column("nvarchar", { name: "Notes", nullable: true, length: 250 })
  notes: string | null;

  @Column("varchar", { name: "Email", nullable: true, length: 100 })
  email: string | null;

  @Column("nvarchar", { name: "Representer", nullable: true, length: 100 })
  representer: string | null;

  @Column("nvarchar", { name: "RepPosition", nullable: true, length: 50 })
  repPosition: string | null;

  @Column("datetime", { name: "Birthday", nullable: true })
  birthday: Date | null;

  @Column("varchar", { name: "UserCreate", nullable: true, length: 30 })
  userCreate: string | null;

  @Column("varchar", { name: "UserUpdate", nullable: true, length: 30 })
  userUpdate: string | null;

  @Column("datetime", { name: "DateUpdate", nullable: true })
  dateUpdate: Date | null;

  @Column("datetime", { name: "DateCreate", nullable: true })
  dateCreate: Date | null;

  @Column("int", { name: "StatusID", nullable: true, default: () => "(0)" })
  statusId: number | null;

  @Column("varchar", { name: "PaymentTypeID", nullable: true, length: 50 })
  paymentTypeId: string | null;

  @Column("varchar", { name: "PaymentTermID", nullable: true, length: 50 })
  paymentTermId: string | null;

  @Column("varchar", { name: "EmployeeID", nullable: true, length: 50 })
  employeeId: string | null;
}
