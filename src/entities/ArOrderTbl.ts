import {
  Column,
  Entity,
  Index,
  JoinColumn,
  ManyToOne,
  OneToMany
} from "typeorm";
import { ArOrderDetailTbl } from "./ArOrderDetailTbl";
import { ArOrderLogTbl } from "./ArOrderLogTbl";
import { ArOrderStatusTbl } from "./ArOrderStatusTbl";
import { CfObjectTbl } from "./CfObjectTbl";

@Index("PK_AR_OrderTbl", ["documentId"], { unique: true })
@Entity("AR_OrderTbl", { schema: "dbo" })
export class ArOrderTbl {
  @Column("varchar", { primary: true, name: "DocumentID", length: 30 })
  documentId: string;

  @Column("datetime", { name: "DocumentDate" })
  documentDate: Date;

  @Column("nvarchar", { name: "Memo", nullable: true, length: 200 })
  memo: string | null;

  @Column("datetime", { name: "DeliverDate", nullable: true })
  deliverDate: Date | null;

  @Column("decimal", {
    name: "BaseTotal",
    nullable: true,
    precision: 28,
    scale: 0
  })
  baseTotal: number | null;

  @Column("varchar", { name: "EmployeeID", length: 50 })
  employeeId: string;

  @Column("varchar", { name: "ManagerID", length: 50 })
  managerId: string;

  @Column("varchar", { name: "Phone", nullable: true, length: 150 })
  phone: string | null;

  @Column("varchar", { name: "BranchID", length: 50 })
  branchId: string;

  @Column("varchar", { name: "TaxCode", nullable: true, length: 50 })
  taxCode: string | null;

  @Column("nvarchar", { name: "Notes", nullable: true })
  notes: string | null;

  @Column("decimal", {
    name: "DepositAmount",
    nullable: true,
    precision: 28,
    scale: 0
  })
  depositAmount: number | null;

  @Column("bit", { name: "isLock", default: () => "(0)" })
  isLock: boolean;

  @Column("varchar", { name: "AgencyID", nullable: true, length: 50 })
  agencyId: string | null;

  @Column("bit", { name: "isVAT", nullable: true })
  isVat: boolean | null;

  @Column("bit", { name: "isOrder", default: () => "(0)" })
  isOrder: boolean;

  @Column("varchar", { name: "CurrencyID", length: 3, default: () => "'VND'" })
  currencyId: string;

  @Column("float", {
    name: "RateExchange",
    precision: 53,
    default: () => "(1)"
  })
  rateExchange: number;

  @Column("varchar", { name: "LinkID", nullable: true, length: 50 })
  linkId: string | null;

  @Column("varchar", { name: "ContractID", nullable: true, length: 50 })
  contractId: string | null;

  @Column("varchar", { name: "UserCreate", nullable: true, length: 30 })
  userCreate: string | null;

  @Column("varchar", { name: "UserUpdate", nullable: true, length: 30 })
  userUpdate: string | null;

  @Column("datetime", { name: "DateUpdate", nullable: true })
  dateUpdate: Date | null;

  @Column("datetime", { name: "DateCreate", nullable: true })
  dateCreate: Date | null;

  @Column("nvarchar", { name: "GhiChuDonHang", nullable: true, length: 250 })
  ghiChuDonHang: string | null;

  @OneToMany(
    () => ArOrderDetailTbl,
    arOrderDetailTbl => arOrderDetailTbl.document
  )
  arOrderDetailTbls: ArOrderDetailTbl[];

  @OneToMany(
    () => ArOrderLogTbl,
    arOrderLogTbl => arOrderLogTbl.document
  )
  arOrderLogTbls: ArOrderLogTbl[];

  @ManyToOne(
    () => ArOrderStatusTbl,
    arOrderStatusTbl => arOrderStatusTbl.arOrderTbls,
    { onUpdate: "CASCADE" }
  )
  @JoinColumn([{ name: "StatusID", referencedColumnName: "statusId" }])
  status: ArOrderStatusTbl;

  @ManyToOne(
    () => CfObjectTbl,
    cfObjectTbl => cfObjectTbl.arOrderTbls,
    { onUpdate: "CASCADE" }
  )
  @JoinColumn([{ name: "ObjectID", referencedColumnName: "objectId" }])
  object: CfObjectTbl;
}
