import { Column, Entity, Index, JoinColumn, ManyToOne } from "typeorm";
import { CfStoreHouseTbl } from "./CfStoreHouseTbl";
import { CsPaymentTbl } from "./CsPaymentTbl";
import { CfBankContractTbl } from "./CfBankContractTbl";
import { CfCostCenterTbl } from "./CfCostCenterTbl";
import { CfItemTbl } from "./CfItemTbl";
import { CfCostTbl } from "./CfCostTbl";

@Index("IX_CS_PaymentDetailTbl_DocumentID", ["documentId"], {})
@Index("PK_CS_PaymentDetailTbl", ["userAutoId"], { unique: true })
@Entity("CS_PaymentDetailTbl", { schema: "dbo" })
export class CsPaymentDetailTbl {
  @Column("varchar", { primary: true, name: "UserAutoID", length: 40 })
  userAutoId: string;

  @Column("varchar", { name: "DocumentID", length: 30 })
  documentId: string;

  @Column("varchar", { name: "AccountID", length: 50 })
  accountId: string;

  @Column("nvarchar", { name: "Memo", nullable: true, length: 200 })
  memo: string | null;

  @Column("decimal", {
    name: "SourceAmount",
    precision: 28,
    scale: 2,
    default: () => "(0)"
  })
  sourceAmount: number;

  @Column("decimal", {
    name: "Amount",
    precision: 28,
    scale: 0,
    default: () => "(0)"
  })
  amount: number;

  @Column("varchar", { name: "VATID", nullable: true, length: 50 })
  vatid: string | null;

  @Column("decimal", {
    name: "VATPercent",
    nullable: true,
    precision: 18,
    scale: 2
  })
  vatPercent: number | null;

  @Column("decimal", {
    name: "VATAmount",
    nullable: true,
    precision: 28,
    scale: 0
  })
  vatAmount: number | null;

  @Column("decimal", {
    name: "TienChietKhau1",
    nullable: true,
    precision: 18,
    scale: 2
  })
  tienChietKhau1: number | null;

  @Column("decimal", {
    name: "TienChietKhau2",
    nullable: true,
    precision: 18,
    scale: 2
  })
  tienChietKhau2: number | null;

  @Column("decimal", {
    name: "TienChietKhau3",
    nullable: true,
    precision: 18,
    scale: 2
  })
  tienChietKhau3: number | null;

  @Column("decimal", {
    name: "TienChietKhau4",
    nullable: true,
    precision: 18,
    scale: 2
  })
  tienChietKhau4: number | null;

  @Column("decimal", {
    name: "TotalAmount",
    nullable: true,
    precision: 28,
    scale: 0
  })
  totalAmount: number | null;

  @Column("varchar", { name: "RefDoc", nullable: true, length: 50 })
  refDoc: string | null;

  @Column("decimal", {
    name: "Quantity",
    nullable: true,
    precision: 18,
    scale: 2
  })
  quantity: number | null;

  @Column("varchar", { name: "ContactCode", nullable: true, length: 50 })
  contactCode: string | null;

  @Column("varchar", { name: "ObjectID", nullable: true, length: 50 })
  objectId: string | null;

  @Column("varchar", { name: "DiffAccID", nullable: true, length: 50 })
  diffAccId: string | null;

  @Column("varchar", { name: "ParentID", nullable: true, length: 50 })
  parentId: string | null;

  @Column("varchar", { name: "ContractID", nullable: true, length: 50 })
  contractId: string | null;

  @Column("varchar", { name: "ObjectID2", nullable: true, length: 50 })
  objectId2: string | null;

  @Column("varchar", { name: "RefDoc2", nullable: true, length: 50 })
  refDoc2: string | null;

  @Column("varchar", { name: "CSType", nullable: true, length: 50 })
  csType: string | null;

  @Column("decimal", {
    name: "UnitPrice",
    nullable: true,
    precision: 18,
    scale: 2
  })
  unitPrice: number | null;

  @Column("decimal", {
    name: "Quantity2",
    nullable: true,
    precision: 18,
    scale: 2
  })
  quantity2: number | null;

  @Column("decimal", {
    name: "ChietKhau1",
    nullable: true,
    precision: 18,
    scale: 2
  })
  chietKhau1: number | null;

  @Column("decimal", {
    name: "ChietKhau2",
    nullable: true,
    precision: 18,
    scale: 2
  })
  chietKhau2: number | null;

  @Column("decimal", {
    name: "ChietKhau3",
    nullable: true,
    precision: 18,
    scale: 2
  })
  chietKhau3: number | null;

  @Column("decimal", {
    name: "ChietKhau4",
    nullable: true,
    precision: 18,
    scale: 2
  })
  chietKhau4: number | null;

  @ManyToOne(
    () => CfStoreHouseTbl,
    cfStoreHouseTbl => cfStoreHouseTbl.csPaymentDetailTbls,
    { onUpdate: "CASCADE" }
  )
  @JoinColumn([{ name: "StoreHouseID", referencedColumnName: "storeHouseId" }])
  storeHouse: CfStoreHouseTbl;

  @ManyToOne(
    () => CsPaymentTbl,
    csPaymentTbl => csPaymentTbl.csPaymentDetailTbls,
    { onDelete: "CASCADE", onUpdate: "CASCADE" }
  )
  @JoinColumn([{ name: "DocumentID", referencedColumnName: "documentId" }])
  document: CsPaymentTbl;

  @ManyToOne(
    () => CfBankContractTbl,
    cfBankContractTbl => cfBankContractTbl.csPaymentDetailTbls,
    { onUpdate: "CASCADE" }
  )
  @JoinColumn([
    { name: "BankContractID", referencedColumnName: "bankContractId" }
  ])
  bankContract: CfBankContractTbl;

  @ManyToOne(
    () => CfCostCenterTbl,
    cfCostCenterTbl => cfCostCenterTbl.csPaymentDetailTbls,
    { onUpdate: "CASCADE" }
  )
  @JoinColumn([{ name: "CostCenterID", referencedColumnName: "costCenterId" }])
  costCenter: CfCostCenterTbl;

  @ManyToOne(
    () => CfItemTbl,
    cfItemTbl => cfItemTbl.csPaymentDetailTbls,
    { onUpdate: "CASCADE" }
  )
  @JoinColumn([{ name: "ItemID", referencedColumnName: "itemId" }])
  item: CfItemTbl;

  @ManyToOne(
    () => CfCostTbl,
    cfCostTbl => cfCostTbl.csPaymentDetailTbls,
    { onUpdate: "CASCADE" }
  )
  @JoinColumn([{ name: "CostID", referencedColumnName: "costId" }])
  cost: CfCostTbl;
}
