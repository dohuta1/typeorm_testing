import { Column, Entity, Index } from "typeorm";

@Index("PK_CustomerType", ["customerTypeId"], { unique: true })
@Entity("CF_CustomerTypeTbl", { schema: "dbo" })
export class CfCustomerTypeTbl {
  @Column("int", { primary: true, name: "CustomerTypeID" })
  customerTypeId: number;

  @Column("nvarchar", { name: "CustomerTypeName", length: 100 })
  customerTypeName: string;
}
