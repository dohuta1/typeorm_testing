import { Column, Entity, Index, JoinColumn, ManyToOne } from "typeorm";
import { ArInvoiceGroupTbl } from "./ArInvoiceGroupTbl";

@Index("PK_AR_InvoiceGroupObjectTbl", ["userAutoId"], { unique: true })
@Entity("AR_InvoiceGroupObjectTbl", { schema: "dbo" })
export class ArInvoiceGroupObjectTbl {
  @Column("varchar", { primary: true, name: "UserAutoID", length: 50 })
  userAutoId: string;

  @Column("varchar", { name: "ObjectID", nullable: true, length: 50 })
  objectId: string | null;

  @ManyToOne(
    () => ArInvoiceGroupTbl,
    arInvoiceGroupTbl => arInvoiceGroupTbl.arInvoiceGroupObjectTbls,
    { onDelete: "CASCADE", onUpdate: "CASCADE" }
  )
  @JoinColumn([{ name: "GroupID", referencedColumnName: "groupId" }])
  group: ArInvoiceGroupTbl;
}
