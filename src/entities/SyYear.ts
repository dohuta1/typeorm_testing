import { Column, Entity, Index, OneToMany } from "typeorm";
import { SyPeriod } from "./SyPeriod";
import { SyQuarter } from "./SyQuarter";

@Index("PK_SysFiscalYear", ["yearId"], { unique: true })
@Entity("SY_Year", { schema: "dbo" })
export class SyYear {
  @Column("smallint", { primary: true, name: "YearID" })
  yearId: number;

  @Column("nvarchar", { name: "YearName", length: 50 })
  yearName: string;

  @Column("nvarchar", { name: "YearName2", nullable: true, length: 50 })
  yearName2: string | null;

  @OneToMany(
    () => SyPeriod,
    syPeriod => syPeriod.year
  )
  syPeriods: SyPeriod[];

  @OneToMany(
    () => SyQuarter,
    syQuarter => syQuarter.year
  )
  syQuarters: SyQuarter[];
}
