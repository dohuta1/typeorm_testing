import { Column, Entity, Index } from "typeorm";

@Index("PK_CF_VATCodeTbl", ["vatid"], { unique: true })
@Entity("CF_VATTbl", { schema: "dbo" })
export class CfVatTbl {
  @Column("varchar", { primary: true, name: "VATID", length: 4 })
  vatid: string;

  @Column("nvarchar", { name: "VATName", nullable: true, length: 50 })
  vatName: string | null;

  @Column("decimal", {
    name: "VATPercent",
    nullable: true,
    precision: 18,
    scale: 0
  })
  vatPercent: number | null;

  @Column("bit", { name: "isDirect", nullable: true })
  isDirect: boolean | null;

  @Column("bit", { name: "isDeduce", nullable: true })
  isDeduce: boolean | null;

  @Column("nvarchar", { name: "VATGroup", nullable: true, length: 150 })
  vatGroup: string | null;

  @Column("varchar", { name: "UserCreate", nullable: true, length: 30 })
  userCreate: string | null;

  @Column("varchar", { name: "UserUpdate", nullable: true, length: 30 })
  userUpdate: string | null;

  @Column("datetime", { name: "DateUpdate", nullable: true })
  dateUpdate: Date | null;

  @Column("datetime", { name: "DateCreate", nullable: true })
  dateCreate: Date | null;

  @Column("bit", { name: "IsDefault", default: () => "(0)" })
  isDefault: boolean;
}
