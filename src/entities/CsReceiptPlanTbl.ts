import {
  Column,
  Entity,
  Index,
  JoinColumn,
  ManyToOne,
  OneToMany
} from "typeorm";
import { CsReceiptPlanDetailTbl } from "./CsReceiptPlanDetailTbl";
import { CsReceiptPlanStatusTbl } from "./CsReceiptPlanStatusTbl";
import { CfObjectTbl } from "./CfObjectTbl";

@Index("PK_CS_ReceiptPlanTbl", ["documentId"], { unique: true })
@Entity("CS_ReceiptPlanTbl", { schema: "dbo" })
export class CsReceiptPlanTbl {
  @Column("varchar", { primary: true, name: "DocumentID", length: 30 })
  documentId: string;

  @Column("datetime", { name: "DocumentDate" })
  documentDate: Date;

  @Column("nvarchar", { name: "Memo", nullable: true, length: 200 })
  memo: string | null;

  @Column("nvarchar", { name: "Notes", nullable: true, length: 200 })
  notes: string | null;

  @Column("decimal", {
    name: "BaseTotal",
    nullable: true,
    precision: 18,
    scale: 0
  })
  baseTotal: number | null;

  @Column("varchar", { name: "CashAccountID", length: 50 })
  cashAccountId: string;

  @Column("bit", { name: "isBank", default: () => "(0)" })
  isBank: boolean;

  @Column("bit", { name: "isLock", default: () => "(0)" })
  isLock: boolean;

  @Column("nvarchar", { name: "SearchField", nullable: true, length: 2000 })
  searchField: string | null;

  @Column("nvarchar", { name: "Receiver", nullable: true, length: 150 })
  receiver: string | null;

  @Column("varchar", { name: "UserCreate", nullable: true, length: 30 })
  userCreate: string | null;

  @Column("varchar", { name: "UserUpdate", nullable: true, length: 30 })
  userUpdate: string | null;

  @Column("datetime", { name: "DateUpdate", nullable: true })
  dateUpdate: Date | null;

  @Column("datetime", { name: "DateCreate", nullable: true })
  dateCreate: Date | null;

  @Column("varchar", { name: "BranchID", length: 50 })
  branchId: string;

  @Column("varchar", { name: "EmployeeID", nullable: true, length: 50 })
  employeeId: string | null;

  @Column("nvarchar", { name: "ManagerID", nullable: true, length: 50 })
  managerId: string | null;

  @Column("nvarchar", { name: "LocationID", nullable: true, length: 50 })
  locationId: string | null;

  @OneToMany(
    () => CsReceiptPlanDetailTbl,
    csReceiptPlanDetailTbl => csReceiptPlanDetailTbl.document
  )
  csReceiptPlanDetailTbls: CsReceiptPlanDetailTbl[];

  @ManyToOne(
    () => CsReceiptPlanStatusTbl,
    csReceiptPlanStatusTbl => csReceiptPlanStatusTbl.csReceiptPlanTbls,
    { onUpdate: "CASCADE" }
  )
  @JoinColumn([{ name: "StatusID", referencedColumnName: "statusId" }])
  status: CsReceiptPlanStatusTbl;

  @ManyToOne(
    () => CfObjectTbl,
    cfObjectTbl => cfObjectTbl.csReceiptPlanTbls,
    { onUpdate: "CASCADE" }
  )
  @JoinColumn([{ name: "ObjectID", referencedColumnName: "objectId" }])
  object: CfObjectTbl;
}
