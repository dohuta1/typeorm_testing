import { Column, Entity, Index } from "typeorm";

@Index("PK_OF_ScheduleTbl", ["userAutoId"], { unique: true })
@Entity("OF_ScheduleTbl", { schema: "dbo" })
export class OfScheduleTbl {
  @Column("varchar", { primary: true, name: "UserAutoID", length: 50 })
  userAutoId: string;

  @Column("nvarchar", { name: "Subject", length: 300 })
  subject: string;

  @Column("datetime", { name: "StartDate" })
  startDate: Date;

  @Column("datetime", { name: "EndDate", nullable: true })
  endDate: Date | null;

  @Column("nvarchar", { name: "Description", nullable: true, length: 1000 })
  description: string | null;

  @Column("nvarchar", { name: "Status", nullable: true, length: 50 })
  status: string | null;

  @Column("nvarchar", { name: "Priority", nullable: true, length: 50 })
  priority: string | null;

  @Column("nvarchar", { name: "ScheduleType", nullable: true, length: 50 })
  scheduleType: string | null;

  @Column("bit", { name: "AllDayEvent", nullable: true, default: () => "0" })
  allDayEvent: boolean | null;

  @Column("bit", { name: "Completed", nullable: true, default: () => "0" })
  completed: boolean | null;

  @Column("int", { name: "Reminder", nullable: true, default: () => "0" })
  reminder: number | null;

  @Column("bit", { name: "Private", nullable: true, default: () => "0" })
  private: boolean | null;

  @Column("varchar", { name: "Owner", nullable: true, length: 50 })
  owner: string | null;

  @Column("varchar", { name: "ShareUser", nullable: true, length: 1000 })
  shareUser: string | null;

  @Column("varchar", { name: "ShareGroup", nullable: true, length: 1000 })
  shareGroup: string | null;

  @Column("varchar", { name: "LinkSourceID", nullable: true, length: 50 })
  linkSourceId: string | null;

  @Column("varchar", { name: "LinkSourceDoc", nullable: true, length: 50 })
  linkSourceDoc: string | null;

  @Column("nvarchar", {
    name: "AppointmentLayout",
    nullable: true,
    length: 500
  })
  appointmentLayout: string | null;

  @Column("nvarchar", { name: "Notes", nullable: true, length: 1000 })
  notes: string | null;

  @Column("datetime", { name: "DateCreate", nullable: true })
  dateCreate: Date | null;
}
