import { Column, Entity, Index, JoinColumn, ManyToOne } from "typeorm";
import { CfItemTbl } from "./CfItemTbl";

@Index("PK_CF_LotTbl", ["lot"], { unique: true })
@Entity("CF_LotTbl", { schema: "dbo" })
export class CfLotTbl {
  @Column("varchar", {
    primary: true,
    name: "Lot",
    length: 50,
    default: () => "'SY_GetLot'"
  })
  lot: string;

  @Column("nvarchar", { name: "LotName", length: 150 })
  lotName: string;

  @Column("datetime", {
    name: "LotDate",
    nullable: true,
    default: () => "getdate()"
  })
  lotDate: Date | null;

  @Column("datetime", { name: "ExpireDate", nullable: true })
  expireDate: Date | null;

  @Column("nvarchar", { name: "Notes", nullable: true, length: 100 })
  notes: string | null;

  @Column("varchar", { name: "UserCreate", nullable: true, length: 30 })
  userCreate: string | null;

  @Column("varchar", { name: "UserUpdate", nullable: true, length: 30 })
  userUpdate: string | null;

  @Column("datetime", { name: "DateUpdate", nullable: true })
  dateUpdate: Date | null;

  @Column("datetime", { name: "DateCreate", nullable: true })
  dateCreate: Date | null;

  @ManyToOne(
    () => CfItemTbl,
    cfItemTbl => cfItemTbl.cfLotTbls,
    { onUpdate: "CASCADE" }
  )
  @JoinColumn([{ name: "ItemID", referencedColumnName: "itemId" }])
  item: CfItemTbl;
}
