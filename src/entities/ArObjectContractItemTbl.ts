import { Column, Entity, Index, JoinColumn, ManyToOne } from "typeorm";
import { ArObjectContractTbl } from "./ArObjectContractTbl";

@Index("PK_AR_ObjectContractItemTbl", ["userAutoId"], { unique: true })
@Entity("AR_ObjectContractItemTbl", { schema: "dbo" })
export class ArObjectContractItemTbl {
  @Column("varchar", { primary: true, name: "UserAutoID", length: 50 })
  userAutoId: string;

  @Column("varchar", { name: "ItemID", nullable: true, length: 50 })
  itemId: string | null;

  @Column("nvarchar", { name: "GhiChu", nullable: true, length: 250 })
  ghiChu: string | null;

  @ManyToOne(
    () => ArObjectContractTbl,
    arObjectContractTbl => arObjectContractTbl.arObjectContractItemTbls,
    { onDelete: "CASCADE", onUpdate: "CASCADE" }
  )
  @JoinColumn([{ name: "MaHopDong", referencedColumnName: "maHopDong" }])
  maHopDong: ArObjectContractTbl;
}
