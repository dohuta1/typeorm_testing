import {
  Column,
  Entity,
  Index,
  JoinColumn,
  ManyToOne,
  OneToMany
} from "typeorm";
import { ArReturnCouponTbl } from "./ArReturnCouponTbl";
import { ArReturnDetailTbl } from "./ArReturnDetailTbl";
import { CfObjectTbl } from "./CfObjectTbl";

@Index("PK_AR_ReturnTbl", ["documentId"], { unique: true })
@Entity("AR_ReturnTbl", { schema: "dbo" })
export class ArReturnTbl {
  @Column("varchar", { primary: true, name: "DocumentID", length: 30 })
  documentId: string;

  @Column("datetime", { name: "DocumentDate" })
  documentDate: Date;

  @Column("nvarchar", { name: "Memo", nullable: true, length: 200 })
  memo: string | null;

  @Column("nvarchar", { name: "Notes", nullable: true, length: 200 })
  notes: string | null;

  @Column("varchar", { name: "CurrencyID", length: 3, default: () => "'VND'" })
  currencyId: string;

  @Column("float", {
    name: "RateExchange",
    precision: 53,
    default: () => "(1)"
  })
  rateExchange: number;

  @Column("varchar", { name: "EmployeeID", nullable: true, length: 50 })
  employeeId: string | null;

  @Column("varchar", { name: "ManagerID", nullable: true, length: 50 })
  managerId: string | null;

  @Column("decimal", {
    name: "BaseTotal",
    nullable: true,
    precision: 18,
    scale: 0
  })
  baseTotal: number | null;

  @Column("varchar", { name: "RevAccID", length: 50, default: () => "(131)" })
  revAccId: string;

  @Column("varchar", { name: "VATAccID", length: 50, default: () => "(3331)" })
  vatAccId: string;

  @Column("bit", { name: "isLock" })
  isLock: boolean;

  @Column("nvarchar", { name: "SearchField", nullable: true, length: 2000 })
  searchField: string | null;

  @Column("varchar", { name: "InvoiceNo", nullable: true, length: 50 })
  invoiceNo: string | null;

  @Column("varchar", { name: "UserCreate", nullable: true, length: 30 })
  userCreate: string | null;

  @Column("varchar", { name: "UserUpdate", nullable: true, length: 30 })
  userUpdate: string | null;

  @Column("datetime", { name: "DateUpdate", nullable: true })
  dateUpdate: Date | null;

  @Column("datetime", { name: "DateCreate", nullable: true })
  dateCreate: Date | null;

  @Column("int", { name: "Status", default: () => "(1)" })
  status: number;

  @Column("varchar", { name: "LinkID", nullable: true, length: 50 })
  linkId: string | null;

  @Column("varchar", { name: "ContractID", nullable: true, length: 50 })
  contractId: string | null;

  @Column("decimal", {
    name: "AmountTotal",
    nullable: true,
    precision: 28,
    scale: 0
  })
  amountTotal: number | null;

  @Column("decimal", {
    name: "VATAmountTotal",
    nullable: true,
    precision: 28,
    scale: 0
  })
  vatAmountTotal: number | null;

  @Column("varchar", { name: "ImpExpID", nullable: true, length: 10 })
  impExpId: string | null;

  @Column("bit", { name: "CalPrice", default: () => "(1)" })
  calPrice: boolean;

  @Column("varchar", { name: "DocBatch", nullable: true, length: 50 })
  docBatch: string | null;

  @Column("decimal", {
    name: "DiemTichLuy",
    nullable: true,
    precision: 18,
    scale: 1
  })
  diemTichLuy: number | null;

  @Column("nvarchar", { name: "Attach", nullable: true, length: 100 })
  attach: string | null;

  @Column("datetime", { name: "AttachDate", nullable: true })
  attachDate: Date | null;

  @Column("varchar", {
    name: "ChietKhau1AccID",
    length: 50,
    default: () => "(3388)"
  })
  chietKhau1AccId: string;

  @Column("varchar", {
    name: "ChietKhau2AccID",
    length: 50,
    default: () => "(1311)"
  })
  chietKhau2AccId: string;

  @Column("varchar", {
    name: "ChietKhau3AccID",
    length: 50,
    default: () => "(3388)"
  })
  chietKhau3AccId: string;

  @Column("varchar", {
    name: "ChietKhau4AccID",
    length: 50,
    default: () => "(3388)"
  })
  chietKhau4AccId: string;

  @Column("varchar", { name: "BranchID", length: 50 })
  branchId: string;

  @OneToMany(
    () => ArReturnCouponTbl,
    arReturnCouponTbl => arReturnCouponTbl.document
  )
  arReturnCouponTbls: ArReturnCouponTbl[];

  @OneToMany(
    () => ArReturnDetailTbl,
    arReturnDetailTbl => arReturnDetailTbl.document
  )
  arReturnDetailTbls: ArReturnDetailTbl[];

  @ManyToOne(
    () => CfObjectTbl,
    cfObjectTbl => cfObjectTbl.arReturnTbls,
    { onUpdate: "CASCADE" }
  )
  @JoinColumn([{ name: "ObjectID", referencedColumnName: "objectId" }])
  object: CfObjectTbl;
}
