import { Column, Entity, Index } from "typeorm";

@Index("PK_SY_ResUx", ["fid"], { unique: true })
@Entity("SY_ResUx", { schema: "dbo" })
export class SyResUx {
  @Column("nvarchar", { primary: true, name: "FID", length: 500 })
  fid: string;

  @Column("nvarchar", { name: "FN", nullable: true, length: 200 })
  fn: string | null;

  @Column("datetime", { name: "FD" })
  fd: Date;

  @Column("varchar", { name: "FV", length: 50 })
  fv: string;

  @Column("image", { name: "FC", nullable: true })
  fc: Buffer | null;
}
