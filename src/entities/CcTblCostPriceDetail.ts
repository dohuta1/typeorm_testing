import { Column, Entity, Index, JoinColumn, ManyToOne } from "typeorm";
import { CcTblCostPrice } from "./CcTblCostPrice";

@Index("PK_CC_TblCostPriceDetail", ["id"], { unique: true })
@Entity("CC_TblCostPriceDetail", { schema: "dbo" })
export class CcTblCostPriceDetail {
  @Column("numeric", { primary: true, name: "ID", precision: 18, scale: 0 })
  id: number;

  @Column("varchar", { name: "CostCenterID", nullable: true, length: 10 })
  costCenterId: string | null;

  @Column("varchar", { name: "CostID", nullable: true, length: 10 })
  costId: string | null;

  @Column("int", { name: "Type", nullable: true })
  type: number | null;

  @Column("varchar", { name: "ItemID", nullable: true, length: 50 })
  itemId: string | null;

  @Column("nvarchar", { name: "Memo", nullable: true, length: 255 })
  memo: string | null;

  @Column("nvarchar", { name: "Notes", nullable: true, length: 50 })
  notes: string | null;

  @Column("varchar", { name: "Unit", nullable: true, length: 50 })
  unit: string | null;

  @Column("decimal", {
    name: "Quantity",
    nullable: true,
    precision: 28,
    scale: 4
  })
  quantity: number | null;

  @Column("decimal", {
    name: "Amount",
    nullable: true,
    precision: 28,
    scale: 2
  })
  amount: number | null;

  @Column("varchar", { name: "AccountID", nullable: true, length: 25 })
  accountId: string | null;

  @Column("bit", { name: "isReturn", nullable: true })
  isReturn: boolean | null;

  @ManyToOne(
    () => CcTblCostPrice,
    ccTblCostPrice => ccTblCostPrice.ccTblCostPriceDetails,
    { onDelete: "CASCADE", onUpdate: "CASCADE" }
  )
  @JoinColumn([{ name: "CostPriceID", referencedColumnName: "costPriceId" }])
  costPrice: CcTblCostPrice;
}
