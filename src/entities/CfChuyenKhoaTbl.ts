import { Column, Entity, Index } from "typeorm";

@Index("PK_CF_ChuyenKhoaTbl", ["chuyenKhoa"], { unique: true })
@Entity("CF_ChuyenKhoaTbl", { schema: "dbo" })
export class CfChuyenKhoaTbl {
  @Column("nvarchar", { primary: true, name: "ChuyenKhoa", length: 250 })
  chuyenKhoa: string;

  @Column("nvarchar", { name: "GhiChu", nullable: true, length: 250 })
  ghiChu: string | null;
}
