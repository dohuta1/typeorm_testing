import { Column, Entity, Index, JoinColumn, ManyToOne } from "typeorm";
import { SyYear } from "./SyYear";

@Index("IX_SY_Quarter", ["quarterName"], { unique: true })
@Index("PK_SysFicalQuarter", ["quarterId"], { unique: true })
@Entity("SY_Quarter", { schema: "dbo" })
export class SyQuarter {
  @Column("varchar", { primary: true, name: "QuarterID", length: 10 })
  quarterId: string;

  @Column("nvarchar", { name: "QuarterName", unique: true, length: 50 })
  quarterName: string;

  @Column("nvarchar", { name: "QuarterName2", nullable: true, length: 50 })
  quarterName2: string | null;

  @Column("datetime", { name: "FromDate", nullable: true })
  fromDate: Date | null;

  @Column("datetime", { name: "ToDate", nullable: true })
  toDate: Date | null;

  @Column("tinyint", { name: "QuarterNo", nullable: true })
  quarterNo: number | null;

  @ManyToOne(
    () => SyYear,
    syYear => syYear.syQuarters,
    { onDelete: "CASCADE", onUpdate: "CASCADE" }
  )
  @JoinColumn([{ name: "YearID", referencedColumnName: "yearId" }])
  year: SyYear;
}
