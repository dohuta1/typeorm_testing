import { Column, Entity, Index, JoinColumn, ManyToOne } from "typeorm";
import { ArInvoiceTbl } from "./ArInvoiceTbl";

@Index("PK_AR_InvoiceLogTbl", ["userAutoId"], { unique: true })
@Entity("AR_InvoiceLogTbl", { schema: "dbo" })
export class ArInvoiceLogTbl {
  @Column("varchar", { primary: true, name: "UserAutoID", length: 40 })
  userAutoId: string;

  @Column("int", { name: "StatusID", nullable: true })
  statusId: number | null;

  @Column("nvarchar", { name: "StatusName", nullable: true, length: 100 })
  statusName: string | null;

  @Column("datetime", { name: "StatusDate", nullable: true })
  statusDate: Date | null;

  @Column("nvarchar", { name: "UserName", nullable: true, length: 100 })
  userName: string | null;

  @ManyToOne(
    () => ArInvoiceTbl,
    arInvoiceTbl => arInvoiceTbl.arInvoiceLogTbls,
    { onDelete: "CASCADE", onUpdate: "CASCADE" }
  )
  @JoinColumn([{ name: "DocumentID", referencedColumnName: "documentId" }])
  document: ArInvoiceTbl;
}
