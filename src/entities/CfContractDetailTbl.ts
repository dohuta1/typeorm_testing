import { Column, Entity, Index, JoinColumn, ManyToOne } from "typeorm";
import { CfContractTbl } from "./CfContractTbl";

@Index("PK_CF_ContractDetailTbl", ["userAutoId"], { unique: true })
@Entity("CF_ContractDetailTbl", { schema: "dbo" })
export class CfContractDetailTbl {
  @Column("varchar", { primary: true, name: "UserAutoID", length: 50 })
  userAutoId: string;

  @Column("varchar", { name: "ItemID", nullable: true, length: 50 })
  itemId: string | null;

  @Column("decimal", {
    name: "Quantity",
    nullable: true,
    precision: 18,
    scale: 2
  })
  quantity: number | null;

  @Column("decimal", {
    name: "UnitPrice",
    nullable: true,
    precision: 18,
    scale: 2
  })
  unitPrice: number | null;

  @Column("decimal", {
    name: "Amount",
    nullable: true,
    precision: 18,
    scale: 0
  })
  amount: number | null;

  @Column("decimal", {
    name: "VATPercent",
    nullable: true,
    precision: 18,
    scale: 0
  })
  vatPercent: number | null;

  @Column("decimal", {
    name: "VATAmount",
    nullable: true,
    precision: 18,
    scale: 0
  })
  vatAmount: number | null;

  @Column("decimal", {
    name: "TotalAmount",
    nullable: true,
    precision: 18,
    scale: 0
  })
  totalAmount: number | null;

  @Column("decimal", {
    name: "UyThac",
    nullable: true,
    precision: 18,
    scale: 0
  })
  uyThac: number | null;

  @Column("decimal", {
    name: "ContractAmount",
    nullable: true,
    precision: 18,
    scale: 0
  })
  contractAmount: number | null;

  @Column("nvarchar", { name: "Notes", nullable: true, length: 50 })
  notes: string | null;

  @ManyToOne(
    () => CfContractTbl,
    cfContractTbl => cfContractTbl.cfContractDetailTbls,
    { onDelete: "CASCADE", onUpdate: "CASCADE" }
  )
  @JoinColumn([{ name: "ContractID", referencedColumnName: "contractId" }])
  contract: CfContractTbl;
}
