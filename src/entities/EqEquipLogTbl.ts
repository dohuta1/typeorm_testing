import { Column, Entity, Index, JoinColumn, ManyToOne } from "typeorm";
import { EqEquipTbl } from "./EqEquipTbl";

@Index("PK_EQ_EquipLogTbl", ["userAutoId"], { unique: true })
@Entity("EQ_EquipLogTbl", { schema: "dbo" })
export class EqEquipLogTbl {
  @Column("varchar", { primary: true, name: "UserAutoID", length: 50 })
  userAutoId: string;

  @Column("datetime", { name: "Date" })
  date: Date;

  @Column("datetime", { name: "ToDate", nullable: true })
  toDate: Date | null;

  @Column("nvarchar", { name: "Memo", nullable: true, length: 150 })
  memo: string | null;

  @Column("varchar", { name: "CostCenterID", nullable: true, length: 50 })
  costCenterId: string | null;

  @Column("varchar", { name: "CostID", nullable: true, length: 50 })
  costId: string | null;

  @Column("varchar", { name: "ObjectID", nullable: true, length: 50 })
  objectId: string | null;

  @Column("decimal", {
    name: "ExpenseAmount",
    nullable: true,
    precision: 18,
    scale: 0
  })
  expenseAmount: number | null;

  @Column("varchar", { name: "DepartmentID", nullable: true, length: 50 })
  departmentId: string | null;

  @ManyToOne(
    () => EqEquipTbl,
    eqEquipTbl => eqEquipTbl.eqEquipLogTbls,
    { onDelete: "CASCADE", onUpdate: "CASCADE" }
  )
  @JoinColumn([{ name: "EquipID", referencedColumnName: "equipId" }])
  equip: EqEquipTbl;
}
