import { Column, Entity, Index, JoinColumn, ManyToOne } from "typeorm";
import { CfItemTbl } from "./CfItemTbl";
import { ArPriceTbl } from "./ArPriceTbl";

@Index("PK_AR_PriceDetailTbl", ["userAutoId"], { unique: true })
@Entity("AR_PriceDetailTbl", { schema: "dbo" })
export class ArPriceDetailTbl {
  @Column("varchar", { primary: true, name: "UserAutoID", length: 40 })
  userAutoId: string;

  @Column("decimal", {
    name: "UnitPrice",
    precision: 28,
    scale: 2,
    default: () => "(0)"
  })
  unitPrice: number;

  @Column("nvarchar", {
    name: "ChuongTrinhTangHang",
    nullable: true,
    length: 100
  })
  chuongTrinhTangHang: string | null;

  @Column("decimal", {
    name: "PhanTramChietKhau1",
    nullable: true,
    precision: 28,
    scale: 2,
    default: () => "(0)"
  })
  phanTramChietKhau1: number | null;

  @Column("decimal", {
    name: "ChietKhau1",
    nullable: true,
    precision: 28,
    scale: 0,
    default: () => "(0)"
  })
  chietKhau1: number | null;

  @Column("decimal", {
    name: "PhanTramChietKhau2",
    nullable: true,
    precision: 28,
    scale: 2,
    default: () => "(0)"
  })
  phanTramChietKhau2: number | null;

  @Column("decimal", {
    name: "ChietKhau2",
    nullable: true,
    precision: 28,
    scale: 0,
    default: () => "(0)"
  })
  chietKhau2: number | null;

  @Column("decimal", {
    name: "PhanTramChietKhau3",
    nullable: true,
    precision: 28,
    scale: 2,
    default: () => "(0)"
  })
  phanTramChietKhau3: number | null;

  @Column("decimal", {
    name: "ChietKhau3",
    nullable: true,
    precision: 28,
    scale: 0,
    default: () => "(0)"
  })
  chietKhau3: number | null;

  @Column("decimal", {
    name: "PhanTramChietKhau4",
    nullable: true,
    precision: 28,
    scale: 2,
    default: () => "(0)"
  })
  phanTramChietKhau4: number | null;

  @Column("decimal", {
    name: "ChietKhau4",
    nullable: true,
    precision: 28,
    scale: 0,
    default: () => "(0)"
  })
  chietKhau4: number | null;

  @Column("nvarchar", { name: "Notes", nullable: true, length: 100 })
  notes: string | null;

  @Column("decimal", {
    name: "DiscountPercentTT",
    nullable: true,
    precision: 28,
    scale: 2,
    default: () => "(0)"
  })
  discountPercentTt: number | null;

  @Column("decimal", {
    name: "DiscountAmountTT",
    nullable: true,
    precision: 28,
    scale: 0,
    default: () => "(0)"
  })
  discountAmountTt: number | null;

  @Column("varchar", { name: "ParentID", nullable: true, length: 50 })
  parentId: string | null;

  @ManyToOne(
    () => CfItemTbl,
    cfItemTbl => cfItemTbl.arPriceDetailTbls,
    { onUpdate: "CASCADE" }
  )
  @JoinColumn([{ name: "ItemID", referencedColumnName: "itemId" }])
  item: CfItemTbl;

  @ManyToOne(
    () => ArPriceTbl,
    arPriceTbl => arPriceTbl.arPriceDetailTbls,
    { onDelete: "CASCADE", onUpdate: "CASCADE" }
  )
  @JoinColumn([{ name: "DocumentID", referencedColumnName: "documentId" }])
  document: ArPriceTbl;
}
