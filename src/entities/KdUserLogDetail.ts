import { Column, Entity, Index, JoinColumn, ManyToOne } from "typeorm";
import { KdUserLog } from "./KdUserLog";

@Index("PK_KD_UserLogDetail", ["userAutoId"], { unique: true })
@Entity("KD_UserLogDetail", { schema: "dbo" })
export class KdUserLogDetail {
  @Column("varchar", { primary: true, name: "UserAutoID", length: 50 })
  userAutoId: string;

  @Column("nvarchar", { name: "FormID", nullable: true, length: 150 })
  formId: string | null;

  @Column("nvarchar", { name: "Para", length: 50 })
  para: string;

  @Column("datetime", { name: "LogDate", nullable: true })
  logDate: Date | null;

  @Column("nvarchar", { name: "Note", nullable: true, length: 500 })
  note: string | null;

  @ManyToOne(
    () => KdUserLog,
    kdUserLog => kdUserLog.kdUserLogDetails,
    { onDelete: "CASCADE", onUpdate: "CASCADE" }
  )
  @JoinColumn([{ name: "LogID", referencedColumnName: "userAutoId" }])
  log: KdUserLog;
}
