import { Column, Entity, Index } from "typeorm";

@Index("PK_SY_FormDropdown", ["userAutoId"], { unique: true })
@Entity("SY_FrmDrdwTbl", { schema: "dbo" })
export class SyFrmDrdwTbl {
  @Column("varchar", { primary: true, name: "UserAutoID", length: 40 })
  userAutoId: string;

  @Column("varchar", { name: "FormID", nullable: true, length: 250 })
  formId: string | null;

  @Column("varchar", { name: "GridName", nullable: true, length: 100 })
  gridName: string | null;

  @Column("varchar", { name: "ColumnID", length: 50 })
  columnId: string;

  @Column("varchar", { name: "ValueColumn", nullable: true, length: 50 })
  valueColumn: string | null;

  @Column("varchar", { name: "DisplayColumn", nullable: true, length: 50 })
  displayColumn: string | null;

  @Column("varchar", { name: "ColumnArr", nullable: true, length: 500 })
  columnArr: string | null;

  @Column("varchar", { name: "WidthArr", nullable: true, length: 500 })
  widthArr: string | null;

  @Column("nvarchar", { name: "Source", nullable: true })
  source: string | null;

  @Column("varchar", { name: "LinkColumn", nullable: true, length: 500 })
  linkColumn: string | null;

  @Column("bit", { name: "DisableAddNew", nullable: true })
  disableAddNew: boolean | null;

  @Column("varchar", { name: "ParaArr", nullable: true, length: 200 })
  paraArr: string | null;

  @Column("varchar", { name: "ParaRequireArr", nullable: true, length: 100 })
  paraRequireArr: string | null;

  @Column("varchar", { name: "Type", nullable: true, length: 10 })
  type: string | null;

  @Column("bit", { name: "KeepValue", nullable: true, default: () => "0" })
  keepValue: boolean | null;

  @Column("varchar", { name: "SummaryFieldArr", nullable: true, length: 100 })
  summaryFieldArr: string | null;

  @Column("bit", { name: "IsMultiSelect", nullable: true })
  isMultiSelect: boolean | null;

  @Column("bit", { name: "IsNotInList", nullable: true })
  isNotInList: boolean | null;

  @Column("bit", { name: "IsDisable", nullable: true, default: () => "0" })
  isDisable: boolean | null;

  @Column("varchar", { name: "ColumnName_Filter", nullable: true, length: 50 })
  columnNameFilter: string | null;

  @Column("varchar", { name: "ColumnValue_Filter", nullable: true, length: 50 })
  columnValueFilter: string | null;

  @Column("varchar", { name: "OnlyValue_Filter", nullable: true, length: 50 })
  onlyValueFilter: string | null;

  @Column("bit", {
    name: "ManualSQLSearch",
    nullable: true,
    default: () => "0"
  })
  manualSqlSearch: boolean | null;

  @Column("varchar", { name: "ManualSQLOrderBy", nullable: true, length: 50 })
  manualSqlOrderBy: string | null;

  @Column("nvarchar", { name: "DefaultValue", nullable: true, length: 100 })
  defaultValue: string | null;

  @Column("bit", { name: "IsReload", default: () => "0" })
  isReload: boolean;

  @Column("varchar", { name: "EditableColumns", nullable: true, length: 250 })
  editableColumns: string | null;

  @Column("nvarchar", { name: "Caption", nullable: true, length: 150 })
  caption: string | null;

  @Column("bit", { name: "isLock", default: () => "0" })
  isLock: boolean;

  @Column("bit", { name: "isInvisible", default: () => "0" })
  isInvisible: boolean;

  @Column("bit", { name: "isWordWrap", default: () => "0" })
  isWordWrap: boolean;

  @Column("bit", { name: "isMultiValue", default: () => "0" })
  isMultiValue: boolean;

  @Column("nvarchar", { name: "GroupCaption", nullable: true, length: 250 })
  groupCaption: string | null;

  @Column("varchar", { name: "WordWrapArr", nullable: true, length: 250 })
  wordWrapArr: string | null;

  @Column("varchar", { name: "GroupColumnArr", nullable: true, length: 100 })
  groupColumnArr: string | null;

  @Column("varchar", { name: "DisplayMember2", nullable: true, length: 50 })
  displayMember2: string | null;

  @Column("varchar", { name: "TreeViewColumn", nullable: true, length: 50 })
  treeViewColumn: string | null;

  @Column("varchar", {
    name: "TreeViewColumnParent",
    nullable: true,
    length: 50
  })
  treeViewColumnParent: string | null;

  @Column("int", { name: "ReloadType", nullable: true })
  reloadType: number | null;
}
