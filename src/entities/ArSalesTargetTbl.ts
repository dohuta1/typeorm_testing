import { Column, Entity, Index, OneToMany } from "typeorm";
import { ArSalesTargetDetail2Tbl } from "./ArSalesTargetDetail2Tbl";
import { ArSalesTargetDetailTbl } from "./ArSalesTargetDetailTbl";

@Index("PK_AR_SalesTargetTbl", ["salesTargetId"], { unique: true })
@Entity("AR_SalesTargetTbl", { schema: "dbo" })
export class ArSalesTargetTbl {
  @Column("int", { primary: true, name: "SalesTargetID" })
  salesTargetId: number;

  @Column("int", { name: "SaleYear" })
  saleYear: number;

  @Column("int", { name: "SaleMonth" })
  saleMonth: number;

  @Column("int", { name: "SalesTargetType" })
  salesTargetType: number;

  @Column("nvarchar", { name: "LocationID", nullable: true, length: 50 })
  locationId: string | null;

  @Column("varchar", { name: "EmployeeID", nullable: true, length: 50 })
  employeeId: string | null;

  @Column("decimal", {
    name: "TongDoanhSo",
    nullable: true,
    precision: 18,
    scale: 0
  })
  tongDoanhSo: number | null;

  @Column("nvarchar", { name: "Notes", nullable: true, length: 150 })
  notes: string | null;

  @Column("varchar", { name: "UserCreate", nullable: true, length: 30 })
  userCreate: string | null;

  @Column("varchar", { name: "UserUpdate", nullable: true, length: 30 })
  userUpdate: string | null;

  @Column("datetime", { name: "DateUpdate", nullable: true })
  dateUpdate: Date | null;

  @Column("datetime", { name: "DateCreate", nullable: true })
  dateCreate: Date | null;

  @OneToMany(
    () => ArSalesTargetDetail2Tbl,
    arSalesTargetDetail2Tbl => arSalesTargetDetail2Tbl.salesTarget
  )
  arSalesTargetDetail2Tbls: ArSalesTargetDetail2Tbl[];

  @OneToMany(
    () => ArSalesTargetDetailTbl,
    arSalesTargetDetailTbl => arSalesTargetDetailTbl.salesTarget
  )
  arSalesTargetDetailTbls: ArSalesTargetDetailTbl[];
}
