import { Column, Entity, Index } from "typeorm";

@Index("PK_CF_AccKindTbl", ["accountKindId"], { unique: true })
@Entity("CF_AccountKindTbl", { schema: "dbo" })
export class CfAccountKindTbl {
  @Column("nvarchar", { primary: true, name: "AccountKindID", length: 2 })
  accountKindId: string;

  @Column("nvarchar", { name: "AccountKindName", nullable: true, length: 100 })
  accountKindName: string | null;

  @Column("nvarchar", { name: "AccountGroupID", nullable: true, length: 1 })
  accountGroupId: string | null;

  @Column("nvarchar", { name: "AccountKindName2", nullable: true, length: 100 })
  accountKindName2: string | null;

  @Column("varchar", { name: "UserCreate", nullable: true, length: 30 })
  userCreate: string | null;

  @Column("varchar", { name: "UserUpdate", nullable: true, length: 30 })
  userUpdate: string | null;

  @Column("datetime", { name: "DateUpdate", nullable: true })
  dateUpdate: Date | null;

  @Column("datetime", { name: "DateCreate", nullable: true })
  dateCreate: Date | null;
}
