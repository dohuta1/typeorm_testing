import { Column, Entity, Index, OneToMany } from "typeorm";
import { ArItemGroupDetailTbl } from "./ArItemGroupDetailTbl";
import { ArItemGroupEmployeeTbl } from "./ArItemGroupEmployeeTbl";
import { SlItemGroupTargetTbl } from "./SlItemGroupTargetTbl";

@Index("PK_AR_ItemGroupTbl", ["itemGroupCode"], { unique: true })
@Entity("AR_ItemGroupTbl", { schema: "dbo" })
export class ArItemGroupTbl {
  @Column("int", { primary: true, name: "ItemGroupCode" })
  itemGroupCode: number;

  @Column("nvarchar", { name: "ItemGroupName", length: 50 })
  itemGroupName: string;

  @Column("nvarchar", { name: "Memo", nullable: true, length: 150 })
  memo: string | null;

  @Column("bit", { name: "isDisable", default: () => "(0)" })
  isDisable: boolean;

  @Column("varchar", { name: "UserCreate", nullable: true, length: 30 })
  userCreate: string | null;

  @Column("varchar", { name: "UserUpdate", nullable: true, length: 30 })
  userUpdate: string | null;

  @Column("datetime", { name: "DateUpdate", nullable: true })
  dateUpdate: Date | null;

  @Column("datetime", { name: "DateCreate", nullable: true })
  dateCreate: Date | null;

  @OneToMany(
    () => ArItemGroupDetailTbl,
    arItemGroupDetailTbl => arItemGroupDetailTbl.itemGroupCode
  )
  arItemGroupDetailTbls: ArItemGroupDetailTbl[];

  @OneToMany(
    () => ArItemGroupEmployeeTbl,
    arItemGroupEmployeeTbl => arItemGroupEmployeeTbl.itemGroupCode
  )
  arItemGroupEmployeeTbls: ArItemGroupEmployeeTbl[];

  @OneToMany(
    () => SlItemGroupTargetTbl,
    slItemGroupTargetTbl => slItemGroupTargetTbl.itemGroupCode
  )
  slItemGroupTargetTbls: SlItemGroupTargetTbl[];
}
