import { Column, Entity, Index, JoinColumn, ManyToOne } from "typeorm";
import { CfItemTbl } from "./CfItemTbl";
import { ApPriceTbl } from "./ApPriceTbl";

@Index("PK_AP_PriceDetailTbl", ["userAutoId"], { unique: true })
@Entity("AP_PriceDetailTbl", { schema: "dbo" })
export class ApPriceDetailTbl {
  @Column("varchar", { primary: true, name: "UserAutoID", length: 40 })
  userAutoId: string;

  @Column("decimal", { name: "UnitPrice", precision: 28, scale: 2 })
  unitPrice: number;

  @Column("nvarchar", { name: "Notes", nullable: true, length: 150 })
  notes: string | null;

  @ManyToOne(
    () => CfItemTbl,
    cfItemTbl => cfItemTbl.apPriceDetailTbls,
    { onUpdate: "CASCADE" }
  )
  @JoinColumn([{ name: "ItemID", referencedColumnName: "itemId" }])
  item: CfItemTbl;

  @ManyToOne(
    () => ApPriceTbl,
    apPriceTbl => apPriceTbl.apPriceDetailTbls,
    { onDelete: "CASCADE", onUpdate: "CASCADE" }
  )
  @JoinColumn([{ name: "DocumentID", referencedColumnName: "documentId" }])
  document: ApPriceTbl;
}
