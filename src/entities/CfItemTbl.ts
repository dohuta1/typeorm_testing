import {
  Column,
  Entity,
  Index,
  JoinColumn,
  ManyToOne,
  OneToMany
} from "typeorm";
import { ApInputRequestDetailTbl } from "./ApInputRequestDetailTbl";
import { ApOrderDetailTbl } from "./ApOrderDetailTbl";
import { ApPriceDetailTbl } from "./ApPriceDetailTbl";
import { ApPurchaseAllocateTbl } from "./ApPurchaseAllocateTbl";
import { ApPurchaseDetailTbl } from "./ApPurchaseDetailTbl";
import { ApReturnDetailTbl } from "./ApReturnDetailTbl";
import { ApServiceDetailTbl } from "./ApServiceDetailTbl";
import { ArBanHoDetailTbl } from "./ArBanHoDetailTbl";
import { ArInvoiceDetailTbl } from "./ArInvoiceDetailTbl";
import { ArInvoiceOrderDetailTbl } from "./ArInvoiceOrderDetailTbl";
import { ArObjectAgencyStockTbl } from "./ArObjectAgencyStockTbl";
import { ArOrder2DetailTbl } from "./ArOrder2DetailTbl";
import { ArOrderCancelDetailTbl } from "./ArOrderCancelDetailTbl";
import { ArOrderDetailTbl } from "./ArOrderDetailTbl";
import { ArPriceDetailTbl } from "./ArPriceDetailTbl";
import { ArPromotionDetailTbl } from "./ArPromotionDetailTbl";
import { ArReturnDetailTbl } from "./ArReturnDetailTbl";
import { ArServiceDetailTbl } from "./ArServiceDetailTbl";
import { ArTrustInvoiceDetailTbl } from "./ArTrustInvoiceDetailTbl";
import { CfCategoryTbl } from "./CfCategoryTbl";
import { CfLotTbl } from "./CfLotTbl";
import { CsPaymentDetailTbl } from "./CsPaymentDetailTbl";
import { CsPaymentPlanDetailTbl } from "./CsPaymentPlanDetailTbl";
import { CsReceiptPlan2DetailTbl } from "./CsReceiptPlan2DetailTbl";
import { CsReceiptPlanDetailTbl } from "./CsReceiptPlanDetailTbl";
import { IvAdjustDetailTbl } from "./IvAdjustDetailTbl";
import { IvCheckStockDetailTbl } from "./IvCheckStockDetailTbl";
import { IvInOutDetailTbl } from "./IvInOutDetailTbl";
import { IvInputDetailTbl } from "./IvInputDetailTbl";
import { IvItemLevelTbl } from "./IvItemLevelTbl";
import { IvOutputDetailTbl } from "./IvOutputDetailTbl";
import { IvTranferDetailTbl } from "./IvTranferDetailTbl";
import { SyBalanceFifo2Tbl } from "./SyBalanceFifo2Tbl";
import { SyBalanceFifoTbl } from "./SyBalanceFifoTbl";
import { SyBalanceItemTbl } from "./SyBalanceItemTbl";

@Index("PK_CF_ItemTbl", ["itemId"], { unique: true })
@Entity("CF_ItemTbl", { schema: "dbo" })
export class CfItemTbl {
  @Column("varchar", { primary: true, name: "ItemID", length: 50 })
  itemId: string;

  @Column("nvarchar", { name: "ItemName", nullable: true, length: 150 })
  itemName: string | null;

  @Column("nvarchar", { name: "Unit", nullable: true, length: 50 })
  unit: string | null;

  @Column("decimal", {
    name: "NguyenGia",
    nullable: true,
    precision: 18,
    scale: 0
  })
  nguyenGia: number | null;

  @Column("nvarchar", { name: "HangSX", nullable: true, length: 100 })
  hangSx: string | null;

  @Column("nvarchar", { name: "QuiCachDongGoi", nullable: true, length: 100 })
  quiCachDongGoi: string | null;

  @Column("bit", { name: "isProperty", default: () => "(1)" })
  isProperty: boolean;

  @Column("bit", { name: "isLot", default: () => "(1)" })
  isLot: boolean;

  @Column("bit", { name: "IsService", nullable: true, default: () => "(0)" })
  isService: boolean | null;

  @Column("varchar", { name: "VATID", nullable: true, length: 5 })
  vatid: string | null;

  @Column("varchar", { name: "InvAccID", nullable: true, length: 30 })
  invAccId: string | null;

  @Column("varchar", { name: "CostOfSaleAccID", nullable: true, length: 30 })
  costOfSaleAccId: string | null;

  @Column("varchar", { name: "IncomeAccID", length: 30 })
  incomeAccId: string;

  @Column("varchar", { name: "DiscountAcctID", length: 50 })
  discountAcctId: string;

  @Column("varchar", { name: "ExpenseAccID", length: 30 })
  expenseAccId: string;

  @Column("float", { name: "UnitFactor", nullable: true, precision: 53 })
  unitFactor: number | null;

  @Column("nvarchar", { name: "Unit2", nullable: true, length: 50 })
  unit2: string | null;

  @Column("varchar", { name: "CostCenterID", nullable: true, length: 50 })
  costCenterId: string | null;

  @Column("varchar", { name: "CostID", nullable: true, length: 50 })
  costId: string | null;

  @Column("varchar", { name: "CostCenterEx", nullable: true, length: 50 })
  costCenterEx: string | null;

  @Column("varchar", { name: "CostEx", nullable: true, length: 50 })
  costEx: string | null;

  @Column("varchar", { name: "ItemGroupID", nullable: true, length: 50 })
  itemGroupId: string | null;

  @Column("varchar", { name: "BarCode", nullable: true, length: 20 })
  barCode: string | null;

  @Column("decimal", {
    name: "CostFactor",
    nullable: true,
    precision: 28,
    scale: 4,
    default: () => "(1)"
  })
  costFactor: number | null;

  @Column("bit", { name: "isDisable", default: () => "(0)" })
  isDisable: boolean;

  @Column("bit", { name: "isQuantity2", default: () => "(0)" })
  isQuantity2: boolean;

  @Column("bit", { name: "isProperty2", default: () => "(0)" })
  isProperty2: boolean;

  @Column("varchar", { name: "LotPrefix", nullable: true, length: 20 })
  lotPrefix: string | null;

  @Column("varchar", { name: "UserCreate", nullable: true, length: 30 })
  userCreate: string | null;

  @Column("varchar", { name: "UserUpdate", nullable: true, length: 30 })
  userUpdate: string | null;

  @Column("datetime", { name: "DateUpdate", nullable: true })
  dateUpdate: Date | null;

  @Column("datetime", { name: "DateCreate", nullable: true })
  dateCreate: Date | null;

  @OneToMany(
    () => ApInputRequestDetailTbl,
    apInputRequestDetailTbl => apInputRequestDetailTbl.item
  )
  apInputRequestDetailTbls: ApInputRequestDetailTbl[];

  @OneToMany(
    () => ApOrderDetailTbl,
    apOrderDetailTbl => apOrderDetailTbl.item
  )
  apOrderDetailTbls: ApOrderDetailTbl[];

  @OneToMany(
    () => ApPriceDetailTbl,
    apPriceDetailTbl => apPriceDetailTbl.item
  )
  apPriceDetailTbls: ApPriceDetailTbl[];

  @OneToMany(
    () => ApPurchaseAllocateTbl,
    apPurchaseAllocateTbl => apPurchaseAllocateTbl.item
  )
  apPurchaseAllocateTbls: ApPurchaseAllocateTbl[];

  @OneToMany(
    () => ApPurchaseDetailTbl,
    apPurchaseDetailTbl => apPurchaseDetailTbl.item
  )
  apPurchaseDetailTbls: ApPurchaseDetailTbl[];

  @OneToMany(
    () => ApReturnDetailTbl,
    apReturnDetailTbl => apReturnDetailTbl.item
  )
  apReturnDetailTbls: ApReturnDetailTbl[];

  @OneToMany(
    () => ApServiceDetailTbl,
    apServiceDetailTbl => apServiceDetailTbl.item
  )
  apServiceDetailTbls: ApServiceDetailTbl[];

  @OneToMany(
    () => ArBanHoDetailTbl,
    arBanHoDetailTbl => arBanHoDetailTbl.item
  )
  arBanHoDetailTbls: ArBanHoDetailTbl[];

  @OneToMany(
    () => ArInvoiceDetailTbl,
    arInvoiceDetailTbl => arInvoiceDetailTbl.item
  )
  arInvoiceDetailTbls: ArInvoiceDetailTbl[];

  @OneToMany(
    () => ArInvoiceOrderDetailTbl,
    arInvoiceOrderDetailTbl => arInvoiceOrderDetailTbl.item
  )
  arInvoiceOrderDetailTbls: ArInvoiceOrderDetailTbl[];

  @OneToMany(
    () => ArObjectAgencyStockTbl,
    arObjectAgencyStockTbl => arObjectAgencyStockTbl.item
  )
  arObjectAgencyStockTbls: ArObjectAgencyStockTbl[];

  @OneToMany(
    () => ArOrder2DetailTbl,
    arOrder2DetailTbl => arOrder2DetailTbl.item
  )
  arOrder2DetailTbls: ArOrder2DetailTbl[];

  @OneToMany(
    () => ArOrderCancelDetailTbl,
    arOrderCancelDetailTbl => arOrderCancelDetailTbl.item
  )
  arOrderCancelDetailTbls: ArOrderCancelDetailTbl[];

  @OneToMany(
    () => ArOrderDetailTbl,
    arOrderDetailTbl => arOrderDetailTbl.item
  )
  arOrderDetailTbls: ArOrderDetailTbl[];

  @OneToMany(
    () => ArPriceDetailTbl,
    arPriceDetailTbl => arPriceDetailTbl.item
  )
  arPriceDetailTbls: ArPriceDetailTbl[];

  @OneToMany(
    () => ArPromotionDetailTbl,
    arPromotionDetailTbl => arPromotionDetailTbl.item
  )
  arPromotionDetailTbls: ArPromotionDetailTbl[];

  @OneToMany(
    () => ArReturnDetailTbl,
    arReturnDetailTbl => arReturnDetailTbl.item
  )
  arReturnDetailTbls: ArReturnDetailTbl[];

  @OneToMany(
    () => ArServiceDetailTbl,
    arServiceDetailTbl => arServiceDetailTbl.item
  )
  arServiceDetailTbls: ArServiceDetailTbl[];

  @OneToMany(
    () => ArTrustInvoiceDetailTbl,
    arTrustInvoiceDetailTbl => arTrustInvoiceDetailTbl.item
  )
  arTrustInvoiceDetailTbls: ArTrustInvoiceDetailTbl[];

  @ManyToOne(
    () => CfCategoryTbl,
    cfCategoryTbl => cfCategoryTbl.cfItemTbls,
    { onUpdate: "CASCADE" }
  )
  @JoinColumn([{ name: "CategoryID", referencedColumnName: "categoryId" }])
  category: CfCategoryTbl;

  @OneToMany(
    () => CfLotTbl,
    cfLotTbl => cfLotTbl.item
  )
  cfLotTbls: CfLotTbl[];

  @OneToMany(
    () => CsPaymentDetailTbl,
    csPaymentDetailTbl => csPaymentDetailTbl.item
  )
  csPaymentDetailTbls: CsPaymentDetailTbl[];

  @OneToMany(
    () => CsPaymentPlanDetailTbl,
    csPaymentPlanDetailTbl => csPaymentPlanDetailTbl.item
  )
  csPaymentPlanDetailTbls: CsPaymentPlanDetailTbl[];

  @OneToMany(
    () => CsReceiptPlan2DetailTbl,
    csReceiptPlan2DetailTbl => csReceiptPlan2DetailTbl.item
  )
  csReceiptPlan2DetailTbls: CsReceiptPlan2DetailTbl[];

  @OneToMany(
    () => CsReceiptPlanDetailTbl,
    csReceiptPlanDetailTbl => csReceiptPlanDetailTbl.item
  )
  csReceiptPlanDetailTbls: CsReceiptPlanDetailTbl[];

  @OneToMany(
    () => IvAdjustDetailTbl,
    ivAdjustDetailTbl => ivAdjustDetailTbl.item
  )
  ivAdjustDetailTbls: IvAdjustDetailTbl[];

  @OneToMany(
    () => IvCheckStockDetailTbl,
    ivCheckStockDetailTbl => ivCheckStockDetailTbl.item
  )
  ivCheckStockDetailTbls: IvCheckStockDetailTbl[];

  @OneToMany(
    () => IvInOutDetailTbl,
    ivInOutDetailTbl => ivInOutDetailTbl.item
  )
  ivInOutDetailTbls: IvInOutDetailTbl[];

  @OneToMany(
    () => IvInputDetailTbl,
    ivInputDetailTbl => ivInputDetailTbl.item
  )
  ivInputDetailTbls: IvInputDetailTbl[];

  @OneToMany(
    () => IvItemLevelTbl,
    ivItemLevelTbl => ivItemLevelTbl.item
  )
  ivItemLevelTbls: IvItemLevelTbl[];

  @OneToMany(
    () => IvOutputDetailTbl,
    ivOutputDetailTbl => ivOutputDetailTbl.item
  )
  ivOutputDetailTbls: IvOutputDetailTbl[];

  @OneToMany(
    () => IvTranferDetailTbl,
    ivTranferDetailTbl => ivTranferDetailTbl.item
  )
  ivTranferDetailTbls: IvTranferDetailTbl[];

  @OneToMany(
    () => SyBalanceFifo2Tbl,
    syBalanceFifo2Tbl => syBalanceFifo2Tbl.item
  )
  syBalanceFifo2Tbls: SyBalanceFifo2Tbl[];

  @OneToMany(
    () => SyBalanceFifoTbl,
    syBalanceFifoTbl => syBalanceFifoTbl.item
  )
  syBalanceFifoTbls: SyBalanceFifoTbl[];

  @OneToMany(
    () => SyBalanceItemTbl,
    syBalanceItemTbl => syBalanceItemTbl.item
  )
  syBalanceItemTbls: SyBalanceItemTbl[];
}
