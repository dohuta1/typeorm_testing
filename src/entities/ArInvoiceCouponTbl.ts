import { Column, Entity, Index, JoinColumn, ManyToOne } from "typeorm";
import { ArInvoiceTbl } from "./ArInvoiceTbl";

@Index("PK_AR_InvoiceCouponTbl", ["userAutoId"], { unique: true })
@Entity("AR_InvoiceCouponTbl", { schema: "dbo" })
export class ArInvoiceCouponTbl {
  @Column("varchar", { primary: true, name: "UserAutoID", length: 40 })
  userAutoId: string;

  @Column("varchar", { name: "CouponSeri", nullable: true, length: 100 })
  couponSeri: string | null;

  @Column("decimal", {
    name: "TriGia",
    nullable: true,
    precision: 18,
    scale: 0
  })
  triGia: number | null;

  @Column("nvarchar", { name: "Notes", nullable: true, length: 50 })
  notes: string | null;

  @ManyToOne(
    () => ArInvoiceTbl,
    arInvoiceTbl => arInvoiceTbl.arInvoiceCouponTbls,
    { onDelete: "CASCADE", onUpdate: "CASCADE" }
  )
  @JoinColumn([{ name: "DocumentID", referencedColumnName: "documentId" }])
  document: ArInvoiceTbl;
}
