import { Column, Entity, Index } from "typeorm";

@Index("PK_SY_FrmGrdActTbl", ["userAutoId"], { unique: true })
@Entity("SY_FrmGrdActTbl", { schema: "dbo" })
export class SyFrmGrdActTbl {
  @Column("varchar", { primary: true, name: "UserAutoID", length: 40 })
  userAutoId: string;

  @Column("varchar", { name: "FormID", nullable: true, length: 250 })
  formId: string | null;

  @Column("varchar", { name: "GridName", length: 100 })
  gridName: string;

  @Column("varchar", { name: "ColumnID", length: 50 })
  columnId: string;

  @Column("varchar", { name: "Action", nullable: true, length: 50 })
  action: string | null;

  @Column("nvarchar", { name: "Source", nullable: true })
  source: string | null;

  @Column("varchar", { name: "Para", nullable: true, length: 100 })
  para: string | null;

  @Column("nvarchar", { name: "TargetValue", nullable: true, length: 100 })
  targetValue: string | null;

  @Column("nvarchar", { name: "TargetValue2", nullable: true, length: 100 })
  targetValue2: string | null;

  @Column("varchar", { name: "TargetColumn", nullable: true, length: 250 })
  targetColumn: string | null;

  @Column("varchar", { name: "TargetColumn2", nullable: true, length: 250 })
  targetColumn2: string | null;

  @Column("varchar", { name: "MsgID", nullable: true, length: 50 })
  msgId: string | null;

  @Column("bit", { name: "IsDisable", default: () => "0" })
  isDisable: boolean;

  @Column("varchar", { name: "ActType", nullable: true, length: 100 })
  actType: string | null;

  @Column("int", { name: "Oderby", nullable: true })
  oderby: number | null;
}
