import {
  Column,
  Entity,
  Index,
  JoinColumn,
  ManyToOne,
  OneToMany
} from "typeorm";
import { ArTrustInvoiceDetailTbl } from "./ArTrustInvoiceDetailTbl";
import { CfObjectTbl } from "./CfObjectTbl";

@Index("PK_AR_TrustInvoiceTbl", ["documentId"], { unique: true })
@Entity("AR_TrustInvoiceTbl", { schema: "dbo" })
export class ArTrustInvoiceTbl {
  @Column("varchar", { primary: true, name: "DocumentID", length: 30 })
  documentId: string;

  @Column("datetime", { name: "DocumentDate" })
  documentDate: Date;

  @Column("varchar", { name: "EmployeeID", nullable: true, length: 50 })
  employeeId: string | null;

  @Column("nvarchar", { name: "Memo", nullable: true, length: 200 })
  memo: string | null;

  @Column("nvarchar", { name: "Notes", nullable: true, length: 200 })
  notes: string | null;

  @Column("nvarchar", { name: "Receiver", nullable: true, length: 100 })
  receiver: string | null;

  @Column("varchar", { name: "CurrencyID", length: 3, default: () => "'VND'" })
  currencyId: string;

  @Column("float", {
    name: "RateExchange",
    precision: 53,
    default: () => "(1)"
  })
  rateExchange: number;

  @Column("varchar", { name: "PaymentTypeID", nullable: true, length: 50 })
  paymentTypeId: string | null;

  @Column("varchar", { name: "PaymentTermID", nullable: true, length: 50 })
  paymentTermId: string | null;

  @Column("datetime", { name: "DueDate", nullable: true })
  dueDate: Date | null;

  @Column("varchar", { name: "PONo", nullable: true, length: 50 })
  poNo: string | null;

  @Column("varchar", { name: "ContractID", nullable: true, length: 50 })
  contractId: string | null;

  @Column("decimal", {
    name: "BaseTotal",
    nullable: true,
    precision: 18,
    scale: 0
  })
  baseTotal: number | null;

  @Column("varchar", { name: "RevAccID", length: 50, default: () => "(331)" })
  revAccId: string;

  @Column("varchar", { name: "VATAccID", length: 50, default: () => "(33311)" })
  vatAccId: string;

  @Column("bit", { name: "isLock", default: () => "(0)" })
  isLock: boolean;

  @Column("varchar", { name: "InvoiceNo", nullable: true, length: 50 })
  invoiceNo: string | null;

  @Column("nvarchar", { name: "SearchField", nullable: true, length: 2000 })
  searchField: string | null;

  @Column("int", { name: "Status", default: () => "(1)" })
  status: number;

  @Column("varchar", { name: "LinkID", nullable: true, length: 50 })
  linkId: string | null;

  @Column("decimal", {
    name: "AmountTotal",
    nullable: true,
    precision: 28,
    scale: 0
  })
  amountTotal: number | null;

  @Column("decimal", {
    name: "VATAmountTotal",
    nullable: true,
    precision: 28,
    scale: 0
  })
  vatAmountTotal: number | null;

  @Column("varchar", { name: "CostCenterID", nullable: true, length: 50 })
  costCenterId: string | null;

  @Column("varchar", { name: "CostID", nullable: true, length: 50 })
  costId: string | null;

  @Column("varchar", { name: "DocBatch", nullable: true, length: 50 })
  docBatch: string | null;

  @Column("varchar", { name: "UserCreate", nullable: true, length: 30 })
  userCreate: string | null;

  @Column("varchar", { name: "UserUpdate", nullable: true, length: 30 })
  userUpdate: string | null;

  @Column("datetime", { name: "DateUpdate", nullable: true })
  dateUpdate: Date | null;

  @Column("datetime", { name: "DateCreate", nullable: true })
  dateCreate: Date | null;

  @Column("varchar", { name: "BranchID", length: 50 })
  branchId: string;

  @OneToMany(
    () => ArTrustInvoiceDetailTbl,
    arTrustInvoiceDetailTbl => arTrustInvoiceDetailTbl.document
  )
  arTrustInvoiceDetailTbls: ArTrustInvoiceDetailTbl[];

  @ManyToOne(
    () => CfObjectTbl,
    cfObjectTbl => cfObjectTbl.arTrustInvoiceTbls,
    { onUpdate: "CASCADE" }
  )
  @JoinColumn([{ name: "ObjectID", referencedColumnName: "objectId" }])
  object: CfObjectTbl;
}
