import { Column, Entity, Index } from "typeorm";

@Index("PK_CF_ChartAccQD48", ["accountId"], { unique: true })
@Entity("CF_ChartAccQD48", { schema: "dbo" })
export class CfChartAccQd48 {
  @Column("nvarchar", { primary: true, name: "AccountID", length: 30 })
  accountId: string;

  @Column("nvarchar", { name: "AccountName", length: 255 })
  accountName: string;

  @Column("nvarchar", { name: "AccountName2", length: 255 })
  accountName2: string;

  @Column("varchar", { name: "CurrencyID", length: 3 })
  currencyId: string;

  @Column("int", { name: "PP_tinh_tggs_no", nullable: true })
  ppTinhTggsNo: number | null;

  @Column("int", { name: "PP_tinh_tggs_co", nullable: true })
  ppTinhTggsCo: number | null;

  @Column("nvarchar", { name: "Parent", nullable: true, length: 30 })
  parent: string | null;

  @Column("nvarchar", { name: "AccKind", nullable: true, length: 30 })
  accKind: string | null;

  @Column("int", { name: "isDetail", nullable: true })
  isDetail: number | null;

  @Column("int", { name: "AccLevel", nullable: true })
  accLevel: number | null;

  @Column("int", { name: "TK_SoCai", nullable: true })
  tkSoCai: number | null;

  @Column("int", { name: "isObject", nullable: true })
  isObject: number | null;
}
