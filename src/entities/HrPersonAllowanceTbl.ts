import { Column, Entity, Index, JoinColumn, ManyToOne } from "typeorm";
import { HrPersonTbl } from "./HrPersonTbl";

@Index("PK_HR_PersonAllowanceTbl", ["userAutoId"], { unique: true })
@Entity("HR_PersonAllowanceTbl", { schema: "dbo" })
export class HrPersonAllowanceTbl {
  @Column("varchar", { primary: true, name: "UserAutoID", length: 50 })
  userAutoId: string;

  @Column("datetime", { name: "FromDate", nullable: true })
  fromDate: Date | null;

  @Column("datetime", { name: "ToDate", nullable: true })
  toDate: Date | null;

  @Column("nvarchar", { name: "NoiDungPhuCap", nullable: true, length: 250 })
  noiDungPhuCap: string | null;

  @Column("decimal", {
    name: "TienPhuCap",
    nullable: true,
    precision: 18,
    scale: 0
  })
  tienPhuCap: number | null;

  @Column("nvarchar", { name: "GhiChu", nullable: true, length: 250 })
  ghiChu: string | null;

  @ManyToOne(
    () => HrPersonTbl,
    hrPersonTbl => hrPersonTbl.hrPersonAllowanceTbls,
    { onDelete: "CASCADE", onUpdate: "CASCADE" }
  )
  @JoinColumn([{ name: "PersonID", referencedColumnName: "personId" }])
  person: HrPersonTbl;
}
