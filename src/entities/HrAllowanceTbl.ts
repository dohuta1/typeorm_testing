import { Column, Entity, Index, OneToMany } from "typeorm";
import { HrAllowanceDetailTbl } from "./HrAllowanceDetailTbl";

@Index("PK_HR_AllowanceTbl", ["documentId"], { unique: true })
@Entity("HR_AllowanceTbl", { schema: "dbo" })
export class HrAllowanceTbl {
  @Column("varchar", { primary: true, name: "DocumentID", length: 20 })
  documentId: string;

  @Column("datetime", { name: "DocumentDate" })
  documentDate: Date;

  @Column("nvarchar", { name: "Memo", nullable: true, length: 200 })
  memo: string | null;

  @Column("nvarchar", { name: "Notes", nullable: true, length: 200 })
  notes: string | null;

  @Column("bit", { name: "isLock", nullable: true })
  isLock: boolean | null;

  @Column("varchar", { name: "UserCreate", nullable: true, length: 30 })
  userCreate: string | null;

  @Column("varchar", { name: "UserUpdate", nullable: true, length: 30 })
  userUpdate: string | null;

  @Column("datetime", { name: "DateUpdate", nullable: true })
  dateUpdate: Date | null;

  @Column("datetime", { name: "DateCreate", nullable: true })
  dateCreate: Date | null;

  @OneToMany(
    () => HrAllowanceDetailTbl,
    hrAllowanceDetailTbl => hrAllowanceDetailTbl.document
  )
  hrAllowanceDetailTbls: HrAllowanceDetailTbl[];
}
