import { Column, Entity, Index } from "typeorm";

@Index("PK__AR_ObjectNewStatusTbl", ["statusId"], { unique: true })
@Entity("AR_ObjectNewStatusTbl", { schema: "dbo" })
export class ArObjectNewStatusTbl {
  @Column("int", { primary: true, name: "StatusID" })
  statusId: number;

  @Column("nvarchar", { name: "StatusName", length: 50 })
  statusName: string;

  @Column("int", { name: "BackColor", nullable: true })
  backColor: number | null;

  @Column("int", { name: "ForeColor", nullable: true })
  foreColor: number | null;
}
