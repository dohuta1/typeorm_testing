import { Column, Entity, Index, OneToMany } from "typeorm";
import { WaUserLogDetail } from "./WaUserLogDetail";

@Index("PK_WA_UserLog", ["userAutoId"], { unique: true })
@Entity("WA_UserLog", { schema: "dbo" })
export class WaUserLog {
  @Column("varchar", { primary: true, name: "UserAutoID", length: 50 })
  userAutoId: string;

  @Column("numeric", { name: "SPID", nullable: true, precision: 18, scale: 0 })
  spid: number | null;

  @Column("varchar", { name: "ComputerName", nullable: true, length: 100 })
  computerName: string | null;

  @Column("varchar", { name: "UserName", length: 50 })
  userName: string;

  @Column("datetime", { name: "LoginDate", nullable: true })
  loginDate: Date | null;

  @Column("datetime", { name: "LogoutDate", nullable: true })
  logoutDate: Date | null;

  @Column("varchar", { name: "AppVer", nullable: true, length: 50 })
  appVer: string | null;

  @Column("datetime", { name: "tmpDate", nullable: true })
  tmpDate: Date | null;

  @OneToMany(
    () => WaUserLogDetail,
    waUserLogDetail => waUserLogDetail.log
  )
  waUserLogDetails: WaUserLogDetail[];
}
