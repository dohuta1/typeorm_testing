import { Column, Entity, Index, JoinColumn, ManyToOne } from "typeorm";
import { ArObjectContractTbl } from "./ArObjectContractTbl";

@Index("PK_AR_ObjectContractDetailTbl", ["userAutoId"], { unique: true })
@Entity("AR_ObjectContractDetailTbl", { schema: "dbo" })
export class ArObjectContractDetailTbl {
  @Column("varchar", { primary: true, name: "UserAutoID", length: 50 })
  userAutoId: string;

  @Column("nvarchar", { name: "SoHopDong", nullable: true, length: 50 })
  soHopDong: string | null;

  @Column("datetime", { name: "NgayThamGia", nullable: true })
  ngayThamGia: Date | null;

  @Column("datetime", { name: "NgayHetHan", nullable: true })
  ngayHetHan: Date | null;

  @Column("decimal", {
    name: "MucThamGia",
    nullable: true,
    precision: 18,
    scale: 0
  })
  mucThamGia: number | null;

  @Column("varchar", { name: "ObjectID", nullable: true, length: 50 })
  objectId: string | null;

  @Column("nvarchar", { name: "TenChuNhaThuoc", nullable: true, length: 250 })
  tenChuNhaThuoc: string | null;

  @Column("decimal", {
    name: "PhanQuaDangKy",
    nullable: true,
    precision: 18,
    scale: 0
  })
  phanQuaDangKy: number | null;

  @Column("datetime", { name: "NgayDeXuat", nullable: true })
  ngayDeXuat: Date | null;

  @Column("datetime", { name: "NgayNhanHopDong", nullable: true })
  ngayNhanHopDong: Date | null;

  @Column("datetime", { name: "NgayGoiTraHopDong", nullable: true })
  ngayGoiTraHopDong: Date | null;

  @Column("datetime", { name: "DeXuatQuaSMS", nullable: true })
  deXuatQuaSms: Date | null;

  @Column("nvarchar", { name: "GhiChu", nullable: true, length: 250 })
  ghiChu: string | null;

  @ManyToOne(
    () => ArObjectContractTbl,
    arObjectContractTbl => arObjectContractTbl.arObjectContractDetailTbls,
    { onDelete: "CASCADE", onUpdate: "CASCADE" }
  )
  @JoinColumn([{ name: "MaHopDong", referencedColumnName: "maHopDong" }])
  maHopDong: ArObjectContractTbl;
}
