import { Column, Entity, Index, JoinColumn, ManyToOne } from "typeorm";
import { SlSalaryGroupTbl } from "./SlSalaryGroupTbl";

@Index("PK_SL_BasicSalaryTbl", ["autoId"], { unique: true })
@Entity("SL_BasicSalaryTbl", { schema: "dbo" })
export class SlBasicSalaryTbl {
  @Column("varchar", {
    primary: true,
    name: "AutoID",
    length: 50,
    default: () => "newid()"
  })
  autoId: string;

  @Column("decimal", { name: "BasicSalary", precision: 18, scale: 0 })
  basicSalary: number;

  @Column("decimal", {
    name: "Target",
    precision: 18,
    scale: 0,
    default: () => "(0)"
  })
  target: number;

  @Column("datetime", { name: "StartDate" })
  startDate: Date;

  @Column("datetime", { name: "EndDate", nullable: true })
  endDate: Date | null;

  @Column("nvarchar", { name: "Notes", nullable: true, length: 200 })
  notes: string | null;

  @ManyToOne(
    () => SlSalaryGroupTbl,
    slSalaryGroupTbl => slSalaryGroupTbl.slBasicSalaryTbls,
    { onDelete: "CASCADE", onUpdate: "CASCADE" }
  )
  @JoinColumn([
    { name: "SalaryGroupID", referencedColumnName: "salaryGroupId" }
  ])
  salaryGroup: SlSalaryGroupTbl;
}
