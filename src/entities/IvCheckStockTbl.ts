import { Column, Entity, Index, OneToMany } from "typeorm";
import { IvCheckStockDetailTbl } from "./IvCheckStockDetailTbl";

@Index("PK_CheckStockTbl", ["documentId"], { unique: true })
@Entity("IV_CheckStockTbl", { schema: "dbo" })
export class IvCheckStockTbl {
  @Column("varchar", { primary: true, name: "DocumentID", length: 20 })
  documentId: string;

  @Column("datetime", { name: "DocumentDate" })
  documentDate: Date;

  @Column("varchar", { name: "EmployeeID", nullable: true, length: 50 })
  employeeId: string | null;

  @Column("nvarchar", { name: "Memo", nullable: true, length: 200 })
  memo: string | null;

  @Column("nvarchar", { name: "Notes", nullable: true, length: 200 })
  notes: string | null;

  @Column("varchar", { name: "StoreHouseID", nullable: true, length: 50 })
  storeHouseId: string | null;

  @Column("bit", { name: "isLock", nullable: true, default: () => "0" })
  isLock: boolean | null;

  @Column("varchar", { name: "UserCreate", nullable: true, length: 30 })
  userCreate: string | null;

  @Column("varchar", { name: "UserUpdate", nullable: true, length: 30 })
  userUpdate: string | null;

  @Column("datetime", { name: "DateUpdate", nullable: true })
  dateUpdate: Date | null;

  @Column("datetime", { name: "DateCreate", nullable: true })
  dateCreate: Date | null;

  @Column("varchar", { name: "LinkID", nullable: true, length: 50 })
  linkId: string | null;

  @Column("varchar", { name: "CategoryID", nullable: true, length: 50 })
  categoryId: string | null;

  @Column("varchar", { name: "ItemID", nullable: true, length: 50 })
  itemId: string | null;

  @OneToMany(
    () => IvCheckStockDetailTbl,
    ivCheckStockDetailTbl => ivCheckStockDetailTbl.document
  )
  ivCheckStockDetailTbls: IvCheckStockDetailTbl[];
}
