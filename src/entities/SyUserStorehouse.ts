import { Column, Entity, Index, JoinColumn, ManyToOne } from "typeorm";
import { SyUser } from "./SyUser";

@Index("PK_SY_UserStorehouse", ["userAutoId"], { unique: true })
@Entity("SY_UserStorehouse", { schema: "dbo" })
export class SyUserStorehouse {
  @Column("varchar", { primary: true, name: "UserAutoID", length: 50 })
  userAutoId: string;

  @Column("varchar", { name: "StoreHouseID", nullable: true, length: 50 })
  storeHouseId: string | null;

  @Column("bit", { name: "isDefault", nullable: true })
  isDefault: boolean | null;

  @Column("nvarchar", { name: "Notes", nullable: true, length: 50 })
  notes: string | null;

  @ManyToOne(
    () => SyUser,
    syUser => syUser.syUserStorehouses,
    { onDelete: "CASCADE", onUpdate: "CASCADE" }
  )
  @JoinColumn([{ name: "UserName", referencedColumnName: "userName" }])
  userName: SyUser;
}
