import { Column, Entity, Index, JoinColumn, ManyToOne } from "typeorm";
import { EqEquipTbl } from "./EqEquipTbl";
import { EqRepairTbl } from "./EqRepairTbl";

@Index("PK_EQ_RepairDetailTbl", ["userAutoId"], { unique: true })
@Entity("EQ_RepairDetailTbl", { schema: "dbo" })
export class EqRepairDetailTbl {
  @Column("varchar", { primary: true, name: "UserAutoID", length: 40 })
  userAutoId: string;

  @Column("nvarchar", { name: "NoiDung", nullable: true, length: 150 })
  noiDung: string | null;

  @Column("decimal", {
    name: "Quantity",
    nullable: true,
    precision: 18,
    scale: 2,
    default: () => "(1)"
  })
  quantity: number | null;

  @Column("decimal", {
    name: "Amount",
    nullable: true,
    precision: 18,
    scale: 2
  })
  amount: number | null;

  @Column("nvarchar", { name: "Notes", nullable: true, length: 150 })
  notes: string | null;

  @ManyToOne(
    () => EqEquipTbl,
    eqEquipTbl => eqEquipTbl.eqRepairDetailTbls,
    { onUpdate: "CASCADE" }
  )
  @JoinColumn([{ name: "EquipID", referencedColumnName: "equipId" }])
  equip: EqEquipTbl;

  @ManyToOne(
    () => EqRepairTbl,
    eqRepairTbl => eqRepairTbl.eqRepairDetailTbls,
    { onDelete: "CASCADE", onUpdate: "CASCADE" }
  )
  @JoinColumn([{ name: "DocumentID", referencedColumnName: "documentId" }])
  document: EqRepairTbl;
}
