import { Column, Entity, Index, JoinColumn, ManyToOne } from "typeorm";
import { FaAssetTbl } from "./FaAssetTbl";

@Index("PK_FA_AssetLogTbl", ["userAutoId"], { unique: true })
@Entity("FA_AssetLogTbl", { schema: "dbo" })
export class FaAssetLogTbl {
  @Column("varchar", { primary: true, name: "UserAutoID", length: 50 })
  userAutoId: string;

  @Column("datetime", { name: "Date" })
  date: Date;

  @Column("datetime", { name: "ToDate", nullable: true })
  toDate: Date | null;

  @Column("nvarchar", { name: "Memo", nullable: true, length: 150 })
  memo: string | null;

  @Column("varchar", { name: "CostCenterID", nullable: true, length: 50 })
  costCenterId: string | null;

  @Column("varchar", { name: "CostID", nullable: true, length: 50 })
  costId: string | null;

  @Column("varchar", { name: "ObjectID", nullable: true, length: 50 })
  objectId: string | null;

  @Column("decimal", {
    name: "ExpenseAmount",
    nullable: true,
    precision: 18,
    scale: 0
  })
  expenseAmount: number | null;

  @Column("varchar", { name: "DepartmentID", nullable: true, length: 50 })
  departmentId: string | null;

  @ManyToOne(
    () => FaAssetTbl,
    faAssetTbl => faAssetTbl.faAssetLogTbls,
    { onDelete: "CASCADE", onUpdate: "CASCADE" }
  )
  @JoinColumn([{ name: "AssetID", referencedColumnName: "assetId" }])
  asset: FaAssetTbl;
}
