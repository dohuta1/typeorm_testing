import { Column, Entity, Index } from "typeorm";

@Index("PK_WA_Dashboard", ["userAutoId"], { unique: true })
@Entity("WA_Dashboard", { schema: "dbo" })
export class WaDashboard {
  @Column("varchar", { primary: true, name: "UserAutoID", length: 50 })
  userAutoId: string;

  @Column("int", { name: "STT" })
  stt: number;

  @Column("nvarchar", { name: "StoreName", length: 250 })
  storeName: string;

  @Column("varchar", { name: "UserGroupID", nullable: true, length: 50 })
  userGroupId: string | null;

  @Column("bit", { name: "isDesktop", default: () => "(1)" })
  isDesktop: boolean;

  @Column("bit", { name: "isMobile", default: () => "(1)" })
  isMobile: boolean;
}
