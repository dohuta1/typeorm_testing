import { Column, Entity, Index, JoinColumn, ManyToOne } from "typeorm";
import { CfItemTbl } from "./CfItemTbl";
import { IvAdjustTbl } from "./IvAdjustTbl";
import { CfStoreHouseTbl } from "./CfStoreHouseTbl";

@Index("IX_IV_AdjustDetailTbl_DocumentID", ["documentId"], {})
@Index("PK_IV_AdjustDetailTbl", ["userAutoId"], { unique: true })
@Entity("IV_AdjustDetailTbl", { schema: "dbo" })
export class IvAdjustDetailTbl {
  @Column("varchar", { primary: true, name: "UserAutoID", length: 40 })
  userAutoId: string;

  @Column("varchar", { name: "DocumentID", length: 30 })
  documentId: string;

  @Column("varchar", { name: "Type", length: 5, default: () => "'X'" })
  type: string;

  @Column("varchar", { name: "Lot", nullable: true, length: 50 })
  lot: string | null;

  @Column("decimal", { name: "Quantity", precision: 28, scale: 4 })
  quantity: number;

  @Column("decimal", {
    name: "UnitPrice",
    nullable: true,
    precision: 28,
    scale: 2
  })
  unitPrice: number | null;

  @Column("decimal", {
    name: "Amount",
    nullable: true,
    precision: 28,
    scale: 0
  })
  amount: number | null;

  @Column("varchar", { name: "InvAccID", length: 50 })
  invAccId: string;

  @Column("varchar", { name: "AdjustGroup", length: 10, default: () => "1" })
  adjustGroup: string;

  @Column("nvarchar", { name: "Notes", nullable: true, length: 100 })
  notes: string | null;

  @Column("decimal", {
    name: "Quantity2",
    nullable: true,
    precision: 28,
    scale: 4
  })
  quantity2: number | null;

  @Column("nvarchar", { name: "Property", nullable: true, length: 50 })
  property: string | null;

  @Column("nvarchar", { name: "Property2", nullable: true, length: 50 })
  property2: string | null;

  @Column("bit", { name: "IsFixPrice", default: () => "0" })
  isFixPrice: boolean;

  @Column("datetime", { name: "ExpireDate", nullable: true })
  expireDate: Date | null;

  @ManyToOne(
    () => CfItemTbl,
    cfItemTbl => cfItemTbl.ivAdjustDetailTbls,
    { onUpdate: "CASCADE" }
  )
  @JoinColumn([{ name: "ItemID", referencedColumnName: "itemId" }])
  item: CfItemTbl;

  @ManyToOne(
    () => IvAdjustTbl,
    ivAdjustTbl => ivAdjustTbl.ivAdjustDetailTbls,
    { onDelete: "CASCADE", onUpdate: "CASCADE" }
  )
  @JoinColumn([{ name: "DocumentID", referencedColumnName: "documentId" }])
  document: IvAdjustTbl;

  @ManyToOne(
    () => CfStoreHouseTbl,
    cfStoreHouseTbl => cfStoreHouseTbl.ivAdjustDetailTbls,
    { onUpdate: "CASCADE" }
  )
  @JoinColumn([{ name: "StoreHouseID", referencedColumnName: "storeHouseId" }])
  storeHouse: CfStoreHouseTbl;
}
