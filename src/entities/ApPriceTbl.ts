import {
  Column,
  Entity,
  Index,
  JoinColumn,
  ManyToOne,
  OneToMany
} from "typeorm";
import { ApPriceDetailTbl } from "./ApPriceDetailTbl";
import { CfObjectTbl } from "./CfObjectTbl";

@Index("PK_AP_PriceTbl", ["documentId"], { unique: true })
@Entity("AP_PriceTbl", { schema: "dbo" })
export class ApPriceTbl {
  @Column("varchar", { primary: true, name: "DocumentID", length: 30 })
  documentId: string;

  @Column("datetime", { name: "DocumentDate" })
  documentDate: Date;

  @Column("nvarchar", { name: "Memo", nullable: true, length: 200 })
  memo: string | null;

  @Column("nvarchar", { name: "Notes", nullable: true, length: 200 })
  notes: string | null;

  @Column("bit", { name: "isLock", default: () => "(0)" })
  isLock: boolean;

  @Column("nvarchar", { name: "SearchField", nullable: true, length: 2000 })
  searchField: string | null;

  @Column("varchar", { name: "UserCreate", nullable: true, length: 30 })
  userCreate: string | null;

  @Column("varchar", { name: "UserUpdate", nullable: true, length: 30 })
  userUpdate: string | null;

  @Column("datetime", { name: "DateUpdate", nullable: true })
  dateUpdate: Date | null;

  @Column("datetime", { name: "DateCreate", nullable: true })
  dateCreate: Date | null;

  @OneToMany(
    () => ApPriceDetailTbl,
    apPriceDetailTbl => apPriceDetailTbl.document
  )
  apPriceDetailTbls: ApPriceDetailTbl[];

  @ManyToOne(
    () => CfObjectTbl,
    cfObjectTbl => cfObjectTbl.apPriceTbls,
    { onUpdate: "CASCADE" }
  )
  @JoinColumn([{ name: "ObjectID", referencedColumnName: "objectId" }])
  object: CfObjectTbl;
}
