import { Column, Entity, Index, JoinColumn, ManyToOne } from "typeorm";
import { PcCenterCostTbl } from "./PcCenterCostTbl";

@Index("PK_PC_CenterCostExTbl", ["userAutoId"], { unique: true })
@Entity("PC_CenterCostExTbl", { schema: "dbo" })
export class PcCenterCostExTbl {
  @Column("varchar", { primary: true, name: "UserAutoID", length: 50 })
  userAutoId: string;

  @Column("varchar", { name: "CostCenterID", nullable: true, length: 50 })
  costCenterId: string | null;

  @Column("varchar", { name: "CostID", nullable: true, length: 50 })
  costId: string | null;

  @Column("varchar", { name: "AccountID", length: 50 })
  accountId: string;

  @Column("decimal", {
    name: "Amount",
    precision: 28,
    scale: 0,
    default: () => "0"
  })
  amount: number;

  @Column("decimal", {
    name: "AmountAllocate",
    nullable: true,
    precision: 28,
    scale: 0,
    default: () => "0"
  })
  amountAllocate: number | null;

  @Column("nvarchar", { name: "Notes", nullable: true, length: 50 })
  notes: string | null;

  @ManyToOne(
    () => PcCenterCostTbl,
    pcCenterCostTbl => pcCenterCostTbl.pcCenterCostExTbls,
    { onDelete: "CASCADE", onUpdate: "CASCADE" }
  )
  @JoinColumn([{ name: "PriceID", referencedColumnName: "priceId" }])
  price: PcCenterCostTbl;
}
