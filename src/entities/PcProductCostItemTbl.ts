import { Column, Entity, Index, JoinColumn, ManyToOne } from "typeorm";
import { PcProductCostTbl } from "./PcProductCostTbl";

@Index("PK_PC_ProductCostItemTbl", ["userAutoId"], { unique: true })
@Entity("PC_ProductCostItemTbl", { schema: "dbo" })
export class PcProductCostItemTbl {
  @Column("varchar", { primary: true, name: "UserAutoID", length: 50 })
  userAutoId: string;

  @Column("varchar", { name: "AccountID", length: 50 })
  accountId: string;

  @Column("varchar", { name: "CostCenterID", nullable: true, length: 50 })
  costCenterId: string | null;

  @Column("varchar", { name: "CostID", nullable: true, length: 50 })
  costId: string | null;

  @Column("varchar", { name: "ItemID", length: 50 })
  itemId: string;

  @Column("decimal", {
    name: "Quantity",
    nullable: true,
    precision: 28,
    scale: 4
  })
  quantity: number | null;

  @Column("float", { name: "UnitPrice", nullable: true, precision: 53 })
  unitPrice: number | null;

  @Column("decimal", {
    name: "Amount",
    nullable: true,
    precision: 28,
    scale: 0,
    default: () => "0"
  })
  amount: number | null;

  @Column("decimal", {
    name: "Quantity2",
    nullable: true,
    precision: 28,
    scale: 4
  })
  quantity2: number | null;

  @Column("varchar", {
    name: "AccountID2",
    length: 50,
    default: () => "'1541'"
  })
  accountId2: string;

  @ManyToOne(
    () => PcProductCostTbl,
    pcProductCostTbl => pcProductCostTbl.pcProductCostItemTbls,
    { onDelete: "CASCADE", onUpdate: "CASCADE" }
  )
  @JoinColumn([{ name: "PriceID", referencedColumnName: "priceId" }])
  price: PcProductCostTbl;
}
