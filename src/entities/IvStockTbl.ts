import { Column, Entity, Index } from "typeorm";

@Index("PK_Stock", ["itemId"], { unique: true })
@Entity("IV_StockTbl", { schema: "dbo" })
export class IvStockTbl {
  @Column("varchar", { primary: true, name: "ItemID", length: 50 })
  itemId: string;

  @Column("nvarchar", { name: "Unit", length: 50 })
  unit: string;

  @Column("int", { name: "QuantityinStock" })
  quantityinStock: number;

  @Column("datetime", { name: "DateUpdate", nullable: true })
  dateUpdate: Date | null;
}
