import {
  Column,
  Entity,
  Index,
  JoinColumn,
  ManyToOne,
  OneToMany
} from "typeorm";
import { ApInputRequestTbl } from "./ApInputRequestTbl";
import { ApOrderTbl } from "./ApOrderTbl";
import { ApPriceTbl } from "./ApPriceTbl";
import { ApPurchaseTbl } from "./ApPurchaseTbl";
import { ApReturnTbl } from "./ApReturnTbl";
import { ApServiceTbl } from "./ApServiceTbl";
import { ArBanHoTbl } from "./ArBanHoTbl";
import { ArInvoiceOrderTbl } from "./ArInvoiceOrderTbl";
import { ArInvoiceTbl } from "./ArInvoiceTbl";
import { ArItemGroupEmployeeTbl } from "./ArItemGroupEmployeeTbl";
import { ArObjectAgencyStockTbl } from "./ArObjectAgencyStockTbl";
import { ArObjectGroupDetailTbl } from "./ArObjectGroupDetailTbl";
import { ArObjectGroupEmployeeTbl } from "./ArObjectGroupEmployeeTbl";
import { ArOrder2Tbl } from "./ArOrder2Tbl";
import { ArOrderCancelTbl } from "./ArOrderCancelTbl";
import { ArOrderTbl } from "./ArOrderTbl";
import { ArPriceObjectTbl } from "./ArPriceObjectTbl";
import { ArPromotionObjectTbl } from "./ArPromotionObjectTbl";
import { ArReturnTbl } from "./ArReturnTbl";
import { ArServiceTbl } from "./ArServiceTbl";
import { ArTrustInvoiceTbl } from "./ArTrustInvoiceTbl";
import { CfObjectContactTbl } from "./CfObjectContactTbl";
import { CfLocationTbl } from "./CfLocationTbl";
import { CfObjectGroupTbl } from "./CfObjectGroupTbl";
import { CsPaymentPlanTbl } from "./CsPaymentPlanTbl";
import { CsPaymentTbl } from "./CsPaymentTbl";
import { CsReceiptPlan2Tbl } from "./CsReceiptPlan2Tbl";
import { CsReceiptPlanTbl } from "./CsReceiptPlanTbl";
import { GjEntryTbl } from "./GjEntryTbl";
import { IvAdjustTbl } from "./IvAdjustTbl";
import { IvInOutTbl } from "./IvInOutTbl";
import { IvInputTbl } from "./IvInputTbl";
import { IvOutputTbl } from "./IvOutputTbl";
import { IvTranferTbl } from "./IvTranferTbl";
import { SlSalaryGroupEmployeeTbl } from "./SlSalaryGroupEmployeeTbl";
import { WaPlanObjectTbl } from "./WaPlanObjectTbl";

@Index("PK_CF_ObjectList", ["objectId"], { unique: true })
@Entity("CF_ObjectTbl", { schema: "dbo" })
export class CfObjectTbl {
  @Column("varchar", { primary: true, name: "ObjectID", length: 50 })
  objectId: string;

  @Column("nvarchar", { name: "ObjectName", length: 150 })
  objectName: string;

  @Column("nvarchar", { name: "Address", nullable: true, length: 250 })
  address: string | null;

  @Column("varchar", { name: "TaxCode", nullable: true, length: 20 })
  taxCode: string | null;

  @Column("varchar", { name: "Phone", nullable: true, length: 100 })
  phone: string | null;

  @Column("nvarchar", { name: "QuanHuyen", nullable: true, length: 50 })
  quanHuyen: string | null;

  @Column("nvarchar", { name: "ChuyenKhoa", nullable: true, length: 250 })
  chuyenKhoa: string | null;

  @Column("nvarchar", { name: "ZoneID", nullable: true, length: 50 })
  zoneId: string | null;

  @Column("nvarchar", { name: "NguoiMuaHang", nullable: true, length: 100 })
  nguoiMuaHang: string | null;

  @Column("nvarchar", { name: "ObjectNameHD", nullable: true, length: 150 })
  objectNameHd: string | null;

  @Column("nvarchar", { name: "AddressHD", nullable: true, length: 150 })
  addressHd: string | null;

  @Column("nvarchar", { name: "DonViNhanHang", nullable: true, length: 100 })
  donViNhanHang: string | null;

  @Column("nvarchar", { name: "DiaChiNhanHang", nullable: true, length: 100 })
  diaChiNhanHang: string | null;

  @Column("nvarchar", { name: "NguoiNhanHang", nullable: true, length: 100 })
  nguoiNhanHang: string | null;

  @Column("nvarchar", {
    name: "DienThoaiNguoiNhanHang",
    nullable: true,
    length: 100
  })
  dienThoaiNguoiNhanHang: string | null;

  @Column("nvarchar", { name: "NguoiDoiChieuCN", nullable: true, length: 100 })
  nguoiDoiChieuCn: string | null;

  @Column("nvarchar", {
    name: "DienThoaiNguoiDoiChieuCN",
    nullable: true,
    length: 100
  })
  dienThoaiNguoiDoiChieuCn: string | null;

  @Column("nvarchar", {
    name: "ThoiGianDoiChieuCN",
    nullable: true,
    length: 100
  })
  thoiGianDoiChieuCn: string | null;

  @Column("nvarchar", { name: "PhuTrachCN", nullable: true, length: 100 })
  phuTrachCn: string | null;

  @Column("nvarchar", { name: "NhaXeGoiHang", nullable: true, length: 100 })
  nhaXeGoiHang: string | null;

  @Column("nvarchar", {
    name: "DienThoaiNhaXeGoiHang",
    nullable: true,
    length: 100
  })
  dienThoaiNhaXeGoiHang: string | null;

  @Column("nvarchar", { name: "BenXe", nullable: true, length: 100 })
  benXe: string | null;

  @Column("nvarchar", { name: "GioXuatBen", nullable: true, length: 100 })
  gioXuatBen: string | null;

  @Column("nvarchar", { name: "GhiChuNhaXe", nullable: true, length: 100 })
  ghiChuNhaXe: string | null;

  @Column("bit", { name: "CongNoDonHang", default: () => "(0)" })
  congNoDonHang: boolean;

  @Column("bit", { name: "isEmployee", default: () => "(1)" })
  isEmployee: boolean;

  @Column("bit", { name: "isManager", default: () => "(0)" })
  isManager: boolean;

  @Column("bit", { name: "isVendor", default: () => "(1)" })
  isVendor: boolean;

  @Column("bit", { name: "isCustomer", default: () => "(1)" })
  isCustomer: boolean;

  @Column("bit", { name: "isAgency", default: () => "(0)" })
  isAgency: boolean;

  @Column("bit", { name: "isDefault", default: () => "(0)" })
  isDefault: boolean;

  @Column("bit", { name: "isDisable", default: () => "(0)" })
  isDisable: boolean;

  @Column("varchar", { name: "RevAccID", length: 50, default: () => "'1311'" })
  revAccId: string;

  @Column("varchar", { name: "PayAccID", length: 50, default: () => "'3311'" })
  payAccId: string;

  @Column("varchar", {
    name: "ChietKhau2AccID",
    nullable: true,
    length: 50,
    default: () => "(3388)"
  })
  chietKhau2AccId: string | null;

  @Column("varchar", {
    name: "ChietKhau4AccID",
    nullable: true,
    length: 50,
    default: () => "(3388)"
  })
  chietKhau4AccId: string | null;

  @Column("varchar", { name: "Fax", nullable: true, length: 100 })
  fax: string | null;

  @Column("nvarchar", { name: "Notes", nullable: true, length: 250 })
  notes: string | null;

  @Column("varchar", { name: "Email", nullable: true, length: 100 })
  email: string | null;

  @Column("nvarchar", { name: "Representer", nullable: true, length: 100 })
  representer: string | null;

  @Column("nvarchar", { name: "RepPosition", nullable: true, length: 50 })
  repPosition: string | null;

  @Column("datetime", { name: "Birthday", nullable: true })
  birthday: Date | null;

  @Column("decimal", {
    name: "CreditMax",
    nullable: true,
    precision: 18,
    scale: 0
  })
  creditMax: number | null;

  @Column("decimal", {
    name: "SoNgayGoiMa",
    nullable: true,
    precision: 18,
    scale: 0,
    default: () => "(45)"
  })
  soNgayGoiMa: number | null;

  @Column("varchar", { name: "PaymentTypeID", nullable: true, length: 50 })
  paymentTypeId: string | null;

  @Column("varchar", { name: "PaymentTermID", nullable: true, length: 50 })
  paymentTermId: string | null;

  @Column("varchar", { name: "AgencyID", nullable: true, length: 50 })
  agencyId: string | null;

  @Column("nvarchar", { name: "PhanLoai", nullable: true, length: 100 })
  phanLoai: string | null;

  @Column("bit", {
    name: "DSChietKhauTT",
    nullable: true,
    default: () => "(0)"
  })
  dsChietKhauTt: boolean | null;

  @Column("bit", { name: "KhachUuTien", nullable: true, default: () => "(0)" })
  khachUuTien: boolean | null;

  @Column("varchar", { name: "CeoID", nullable: true, length: 50 })
  ceoId: string | null;

  @Column("nvarchar", { name: "ThongTinThamKhao", nullable: true, length: 150 })
  thongTinThamKhao: string | null;

  @Column("nvarchar", { name: "HopDongThamGia", nullable: true, length: 100 })
  hopDongThamGia: string | null;

  @Column("decimal", {
    name: "CreditMin",
    nullable: true,
    precision: 18,
    scale: 0
  })
  creditMin: number | null;

  @Column("bit", { name: "isCostCenter", nullable: true, default: () => "(1)" })
  isCostCenter: boolean | null;

  @Column("bit", { name: "IsForeign", default: () => "(0)" })
  isForeign: boolean;

  @Column("varchar", { name: "UserCreate", nullable: true, length: 30 })
  userCreate: string | null;

  @Column("varchar", { name: "UserUpdate", nullable: true, length: 30 })
  userUpdate: string | null;

  @Column("datetime", { name: "DateUpdate", nullable: true })
  dateUpdate: Date | null;

  @Column("datetime", { name: "DateCreate", nullable: true })
  dateCreate: Date | null;

  @OneToMany(
    () => ApInputRequestTbl,
    apInputRequestTbl => apInputRequestTbl.object
  )
  apInputRequestTbls: ApInputRequestTbl[];

  @OneToMany(
    () => ApOrderTbl,
    apOrderTbl => apOrderTbl.object
  )
  apOrderTbls: ApOrderTbl[];

  @OneToMany(
    () => ApPriceTbl,
    apPriceTbl => apPriceTbl.object
  )
  apPriceTbls: ApPriceTbl[];

  @OneToMany(
    () => ApPurchaseTbl,
    apPurchaseTbl => apPurchaseTbl.object
  )
  apPurchaseTbls: ApPurchaseTbl[];

  @OneToMany(
    () => ApReturnTbl,
    apReturnTbl => apReturnTbl.object
  )
  apReturnTbls: ApReturnTbl[];

  @OneToMany(
    () => ApServiceTbl,
    apServiceTbl => apServiceTbl.object
  )
  apServiceTbls: ApServiceTbl[];

  @OneToMany(
    () => ArBanHoTbl,
    arBanHoTbl => arBanHoTbl.object
  )
  arBanHoTbls: ArBanHoTbl[];

  @OneToMany(
    () => ArInvoiceOrderTbl,
    arInvoiceOrderTbl => arInvoiceOrderTbl.object
  )
  arInvoiceOrderTbls: ArInvoiceOrderTbl[];

  @OneToMany(
    () => ArInvoiceTbl,
    arInvoiceTbl => arInvoiceTbl.object
  )
  arInvoiceTbls: ArInvoiceTbl[];

  @OneToMany(
    () => ArItemGroupEmployeeTbl,
    arItemGroupEmployeeTbl => arItemGroupEmployeeTbl.employee
  )
  arItemGroupEmployeeTbls: ArItemGroupEmployeeTbl[];

  @OneToMany(
    () => ArObjectAgencyStockTbl,
    arObjectAgencyStockTbl => arObjectAgencyStockTbl.object
  )
  arObjectAgencyStockTbls: ArObjectAgencyStockTbl[];

  @OneToMany(
    () => ArObjectGroupDetailTbl,
    arObjectGroupDetailTbl => arObjectGroupDetailTbl.object
  )
  arObjectGroupDetailTbls: ArObjectGroupDetailTbl[];

  @OneToMany(
    () => ArObjectGroupEmployeeTbl,
    arObjectGroupEmployeeTbl => arObjectGroupEmployeeTbl.employee
  )
  arObjectGroupEmployeeTbls: ArObjectGroupEmployeeTbl[];

  @OneToMany(
    () => ArOrder2Tbl,
    arOrder2Tbl => arOrder2Tbl.object
  )
  arOrder2Tbls: ArOrder2Tbl[];

  @OneToMany(
    () => ArOrderCancelTbl,
    arOrderCancelTbl => arOrderCancelTbl.object
  )
  arOrderCancelTbls: ArOrderCancelTbl[];

  @OneToMany(
    () => ArOrderTbl,
    arOrderTbl => arOrderTbl.object
  )
  arOrderTbls: ArOrderTbl[];

  @OneToMany(
    () => ArPriceObjectTbl,
    arPriceObjectTbl => arPriceObjectTbl.object
  )
  arPriceObjectTbls: ArPriceObjectTbl[];

  @OneToMany(
    () => ArPromotionObjectTbl,
    arPromotionObjectTbl => arPromotionObjectTbl.object
  )
  arPromotionObjectTbls: ArPromotionObjectTbl[];

  @OneToMany(
    () => ArReturnTbl,
    arReturnTbl => arReturnTbl.object
  )
  arReturnTbls: ArReturnTbl[];

  @OneToMany(
    () => ArServiceTbl,
    arServiceTbl => arServiceTbl.object
  )
  arServiceTbls: ArServiceTbl[];

  @OneToMany(
    () => ArTrustInvoiceTbl,
    arTrustInvoiceTbl => arTrustInvoiceTbl.object
  )
  arTrustInvoiceTbls: ArTrustInvoiceTbl[];

  @OneToMany(
    () => CfObjectContactTbl,
    cfObjectContactTbl => cfObjectContactTbl.object
  )
  cfObjectContactTbls: CfObjectContactTbl[];

  @ManyToOne(
    () => CfLocationTbl,
    cfLocationTbl => cfLocationTbl.cfObjectTbls,
    { onUpdate: "CASCADE" }
  )
  @JoinColumn([{ name: "LocationID", referencedColumnName: "locationId" }])
  location: CfLocationTbl;

  @ManyToOne(
    () => CfObjectGroupTbl,
    cfObjectGroupTbl => cfObjectGroupTbl.cfObjectTbls,
    { onUpdate: "CASCADE" }
  )
  @JoinColumn([
    { name: "ObjectGroupID", referencedColumnName: "objectGroupId" }
  ])
  objectGroup: CfObjectGroupTbl;

  @OneToMany(
    () => CsPaymentPlanTbl,
    csPaymentPlanTbl => csPaymentPlanTbl.object
  )
  csPaymentPlanTbls: CsPaymentPlanTbl[];

  @OneToMany(
    () => CsPaymentTbl,
    csPaymentTbl => csPaymentTbl.object
  )
  csPaymentTbls: CsPaymentTbl[];

  @OneToMany(
    () => CsReceiptPlan2Tbl,
    csReceiptPlan2Tbl => csReceiptPlan2Tbl.object
  )
  csReceiptPlan2Tbls: CsReceiptPlan2Tbl[];

  @OneToMany(
    () => CsReceiptPlanTbl,
    csReceiptPlanTbl => csReceiptPlanTbl.object
  )
  csReceiptPlanTbls: CsReceiptPlanTbl[];

  @OneToMany(
    () => GjEntryTbl,
    gjEntryTbl => gjEntryTbl.object
  )
  gjEntryTbls: GjEntryTbl[];

  @OneToMany(
    () => IvAdjustTbl,
    ivAdjustTbl => ivAdjustTbl.object
  )
  ivAdjustTbls: IvAdjustTbl[];

  @OneToMany(
    () => IvInOutTbl,
    ivInOutTbl => ivInOutTbl.object
  )
  ivInOutTbls: IvInOutTbl[];

  @OneToMany(
    () => IvInputTbl,
    ivInputTbl => ivInputTbl.object
  )
  ivInputTbls: IvInputTbl[];

  @OneToMany(
    () => IvOutputTbl,
    ivOutputTbl => ivOutputTbl.object
  )
  ivOutputTbls: IvOutputTbl[];

  @OneToMany(
    () => IvTranferTbl,
    ivTranferTbl => ivTranferTbl.object
  )
  ivTranferTbls: IvTranferTbl[];

  @OneToMany(
    () => SlSalaryGroupEmployeeTbl,
    slSalaryGroupEmployeeTbl => slSalaryGroupEmployeeTbl.object
  )
  slSalaryGroupEmployeeTbls: SlSalaryGroupEmployeeTbl[];

  @OneToMany(
    () => WaPlanObjectTbl,
    waPlanObjectTbl => waPlanObjectTbl.object
  )
  waPlanObjectTbls: WaPlanObjectTbl[];
}
