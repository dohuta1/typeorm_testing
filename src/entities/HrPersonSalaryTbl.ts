import { Column, Entity, Index, JoinColumn, ManyToOne } from "typeorm";
import { HrPersonTbl } from "./HrPersonTbl";

@Index("PK_HR_PersonSalaryTbl", ["userAutoId"], { unique: true })
@Entity("HR_PersonSalaryTbl", { schema: "dbo" })
export class HrPersonSalaryTbl {
  @Column("varchar", { primary: true, name: "UserAutoID", length: 50 })
  userAutoId: string;

  @Column("int", { name: "TrangThai", nullable: true })
  trangThai: number | null;

  @Column("datetime", { name: "FromDate", nullable: true })
  fromDate: Date | null;

  @Column("datetime", { name: "ToDate", nullable: true })
  toDate: Date | null;

  @Column("decimal", {
    name: "MucLuong",
    nullable: true,
    precision: 18,
    scale: 0
  })
  mucLuong: number | null;

  @Column("decimal", {
    name: "LuongBaoHiem",
    nullable: true,
    precision: 18,
    scale: 0
  })
  luongBaoHiem: number | null;

  @Column("decimal", {
    name: "PCCongTac",
    nullable: true,
    precision: 18,
    scale: 0
  })
  pcCongTac: number | null;

  @Column("decimal", {
    name: "PCTrachNhiem",
    nullable: true,
    precision: 18,
    scale: 0
  })
  pcTrachNhiem: number | null;

  @Column("nvarchar", { name: "GhiChu", nullable: true, length: 250 })
  ghiChu: string | null;

  @ManyToOne(
    () => HrPersonTbl,
    hrPersonTbl => hrPersonTbl.hrPersonSalaryTbls,
    { onDelete: "CASCADE", onUpdate: "CASCADE" }
  )
  @JoinColumn([{ name: "PersonID", referencedColumnName: "personId" }])
  person: HrPersonTbl;
}
