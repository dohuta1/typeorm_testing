import { Column, Entity, Index } from "typeorm";

@Index("PK_WA_Menu", ["menuId"], { unique: true })
@Entity("WA_Menu", { schema: "dbo" })
export class WaMenu {
  @Column("varchar", { primary: true, name: "MenuID", length: 50 })
  menuId: string;

  @Column("nvarchar", { name: "VN", length: 200 })
  vn: string;

  @Column("nvarchar", { name: "EN", nullable: true, length: 200 })
  en: string | null;

  @Column("nvarchar", { name: "CH", nullable: true, length: 200 })
  ch: string | null;

  @Column("varchar", { name: "Parent", nullable: true, length: 50 })
  parent: string | null;

  @Column("varchar", { name: "IconClass", nullable: true, length: 50 })
  iconClass: string | null;

  @Column("varchar", { name: "FormKey", nullable: true, length: 50 })
  formKey: string | null;

  @Column("varchar", { name: "FormName", nullable: true, length: 100 })
  formName: string | null;

  @Column("varchar", { name: "URLPara", nullable: true, length: 100 })
  urlPara: string | null;

  @Column("bit", { name: "isDisable", default: () => "(0)" })
  isDisable: boolean;

  @Column("bit", { name: "isNotCheckPermission", default: () => "(0)" })
  isNotCheckPermission: boolean;
}
