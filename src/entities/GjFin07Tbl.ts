import { Column, Entity, Index } from "typeorm";

@Index("PK_GJ_Fin07Tbl", ["codeId"], { unique: true })
@Entity("GJ_Fin07Tbl", { schema: "dbo" })
export class GjFin07Tbl {
  @Column("varchar", { primary: true, name: "CodeID", length: 50 })
  codeId: string;

  @Column("varchar", { name: "GroupID", length: 13 })
  groupId: string;

  @Column("nvarchar", { name: "CodeName", nullable: true, length: 200 })
  codeName: string | null;

  @Column("nvarchar", { name: "CodeName2", nullable: true, length: 200 })
  codeName2: string | null;

  @Column("varchar", { name: "AccountID", nullable: true, length: 50 })
  accountId: string | null;

  @Column("varchar", { name: "AccountID2", nullable: true, length: 50 })
  accountId2: string | null;

  @Column("bit", { name: "PrintTF", nullable: true })
  printTf: boolean | null;

  @Column("bit", { name: "isBold", nullable: true })
  isBold: boolean | null;

  @Column("varchar", { name: "Formular", nullable: true, length: 50 })
  formular: string | null;

  @Column("decimal", { name: "Type", nullable: true, precision: 28, scale: 4 })
  type: number | null;

  @Column("bit", { name: "isObject", nullable: true })
  isObject: boolean | null;

  @Column("varchar", { name: "AmtType", nullable: true, length: 50 })
  amtType: string | null;

  @Column("bit", { name: "Negative", nullable: true })
  negative: boolean | null;

  @Column("nvarchar", { name: "Caption", nullable: true, length: 200 })
  caption: string | null;

  @Column("nvarchar", { name: "Caption2", nullable: true, length: 200 })
  caption2: string | null;

  @Column("decimal", {
    name: "BeginAmount",
    nullable: true,
    precision: 28,
    scale: 0
  })
  beginAmount: number | null;

  @Column("decimal", {
    name: "EndAmount",
    nullable: true,
    precision: 28,
    scale: 0
  })
  endAmount: number | null;
}
