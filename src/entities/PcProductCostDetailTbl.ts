import { Column, Entity, Index, JoinColumn, ManyToOne } from "typeorm";
import { PcProductCostTbl } from "./PcProductCostTbl";

@Index("IX_PC_ProductCostDetailTbl", ["priceId"], {})
@Index("IX_PC_ProductCostDetailTbl_1", ["parentId"], {})
@Index("IX_PC_ProductCostDetailTbl_2", ["masterId"], {})
@Index("PK_PC_ProductCostDetailTbl", ["userAutoId"], { unique: true })
@Entity("PC_ProductCostDetailTbl", { schema: "dbo" })
export class PcProductCostDetailTbl {
  @Column("varchar", { primary: true, name: "UserAutoID", length: 50 })
  userAutoId: string;

  @Column("varchar", { name: "PriceID", length: 50 })
  priceId: string;

  @Column("varchar", { name: "AccountID", length: 50 })
  accountId: string;

  @Column("varchar", { name: "CostCenterID", nullable: true, length: 50 })
  costCenterId: string | null;

  @Column("varchar", { name: "CostID", nullable: true, length: 50 })
  costId: string | null;

  @Column("varchar", { name: "ItemID", nullable: true, length: 50 })
  itemId: string | null;

  @Column("decimal", {
    name: "Quantity",
    nullable: true,
    precision: 28,
    scale: 4
  })
  quantity: number | null;

  @Column("decimal", {
    name: "BomQuantity",
    nullable: true,
    precision: 28,
    scale: 4
  })
  bomQuantity: number | null;

  @Column("decimal", {
    name: "Amount",
    precision: 28,
    scale: 0,
    default: () => "0"
  })
  amount: number;

  @Column("bit", { name: "isWaste", nullable: true, default: () => "0" })
  isWaste: boolean | null;

  @Column("bit", { name: "isBalance", nullable: true, default: () => "0" })
  isBalance: boolean | null;

  @Column("bit", { name: "isBom", nullable: true, default: () => "0" })
  isBom: boolean | null;

  @Column("varchar", { name: "ParentID", length: 50 })
  parentId: string;

  @Column("varchar", { name: "MasterID", length: 50 })
  masterId: string;

  @Column("int", { name: "ALType", default: () => "1" })
  alType: number;

  @Column("nvarchar", { name: "Notes", nullable: true, length: 50 })
  notes: string | null;

  @ManyToOne(
    () => PcProductCostTbl,
    pcProductCostTbl => pcProductCostTbl.pcProductCostDetailTbls,
    { onDelete: "CASCADE", onUpdate: "CASCADE" }
  )
  @JoinColumn([{ name: "PriceID", referencedColumnName: "priceId" }])
  price: PcProductCostTbl;
}
