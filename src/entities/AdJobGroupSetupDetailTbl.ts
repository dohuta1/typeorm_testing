import { Column, Entity, Index, JoinColumn, ManyToOne } from "typeorm";
import { AdJobGroupSetupTbl } from "./AdJobGroupSetupTbl";

@Index("PK_AD_JobGroupSetupDetailTbl", ["userAutoId"], { unique: true })
@Entity("AD_JobGroupSetupDetailTbl", { schema: "dbo" })
export class AdJobGroupSetupDetailTbl {
  @Column("varchar", { primary: true, name: "UserAutoID", length: 50 })
  userAutoId: string;

  @Column("varchar", { name: "CostCenterID", nullable: true, length: 50 })
  costCenterId: string | null;

  @Column("varchar", { name: "CostID", nullable: true, length: 50 })
  costId: string | null;

  @Column("decimal", {
    name: "Factor",
    nullable: true,
    precision: 18,
    scale: 4
  })
  factor: number | null;

  @ManyToOne(
    () => AdJobGroupSetupTbl,
    adJobGroupSetupTbl => adJobGroupSetupTbl.adJobGroupSetupDetailTbls,
    { onDelete: "CASCADE", onUpdate: "CASCADE" }
  )
  @JoinColumn([{ name: "CodeID", referencedColumnName: "codeId" }])
  code: AdJobGroupSetupTbl;
}
