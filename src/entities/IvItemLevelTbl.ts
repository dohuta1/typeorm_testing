import { Column, Entity, Index, JoinColumn, ManyToOne } from "typeorm";
import { CfItemTbl } from "./CfItemTbl";

@Index("PK_IV_ItemLevelTbl", ["userAutoId"], { unique: true })
@Entity("IV_ItemLevelTbl", { schema: "dbo" })
export class IvItemLevelTbl {
  @Column("varchar", { primary: true, name: "UserAutoID", length: 50 })
  userAutoId: string;

  @Column("nvarchar", { name: "Property", nullable: true, length: 50 })
  property: string | null;

  @Column("nvarchar", { name: "Property2", nullable: true, length: 50 })
  property2: string | null;

  @Column("decimal", {
    name: "Quantity2Min",
    nullable: true,
    precision: 28,
    scale: 4,
    default: () => "0"
  })
  quantity2Min: number | null;

  @Column("decimal", {
    name: "QuantityMin",
    nullable: true,
    precision: 28,
    scale: 4,
    default: () => "0"
  })
  quantityMin: number | null;

  @Column("decimal", {
    name: "Quantity2Max",
    nullable: true,
    precision: 28,
    scale: 4,
    default: () => "0"
  })
  quantity2Max: number | null;

  @Column("decimal", {
    name: "QuantityMax",
    nullable: true,
    precision: 28,
    scale: 4,
    default: () => "0"
  })
  quantityMax: number | null;

  @Column("varchar", { name: "StoreHouseID", nullable: true, length: 50 })
  storeHouseId: string | null;

  @Column("nvarchar", { name: "Notes", nullable: true, length: 150 })
  notes: string | null;

  @ManyToOne(
    () => CfItemTbl,
    cfItemTbl => cfItemTbl.ivItemLevelTbls,
    { onUpdate: "CASCADE" }
  )
  @JoinColumn([{ name: "ItemID", referencedColumnName: "itemId" }])
  item: CfItemTbl;
}
