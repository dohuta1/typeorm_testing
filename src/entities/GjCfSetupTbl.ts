import { Column, Entity, Index, OneToMany } from "typeorm";
import { GjCfSetupDetailTbl } from "./GjCfSetupDetailTbl";

@Index("PK_GJ_CashFlowSetupTbl", ["setupId"], { unique: true })
@Entity("GJ_CFSetupTbl", { schema: "dbo" })
export class GjCfSetupTbl {
  @Column("varchar", { primary: true, name: "SetupID", length: 50 })
  setupId: string;

  @Column("varchar", { name: "GroupID", length: 50, default: () => "'QD15'" })
  groupId: string;

  @Column("varchar", { name: "CodeID", nullable: true, length: 50 })
  codeId: string | null;

  @Column("nvarchar", { name: "CodeName", length: 200 })
  codeName: string;

  @Column("nvarchar", { name: "CodeName2", nullable: true, length: 200 })
  codeName2: string | null;

  @Column("nvarchar", { name: "Statement", nullable: true, length: 50 })
  statement: string | null;

  @Column("int", { name: "Priority", default: () => "5" })
  priority: number;

  @Column("bit", { name: "IsBold", default: () => "0" })
  isBold: boolean;

  @Column("bit", { name: "IsLeft", default: () => "0" })
  isLeft: boolean;

  @Column("int", { name: "STT", nullable: true })
  stt: number | null;

  @Column("bit", { name: "isNotPrint", default: () => "0" })
  isNotPrint: boolean;

  @OneToMany(
    () => GjCfSetupDetailTbl,
    gjCfSetupDetailTbl => gjCfSetupDetailTbl.setup
  )
  gjCfSetupDetailTbls: GjCfSetupDetailTbl[];
}
