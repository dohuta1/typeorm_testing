import { Column, Entity, Index } from "typeorm";

@Index("PK_CF_ChartAccTbl", ["accountId"], { unique: true })
@Entity("CF_ChartAccTbl", { schema: "dbo" })
export class CfChartAccTbl {
  @Column("varchar", { primary: true, name: "AccountID", length: 30 })
  accountId: string;

  @Column("nvarchar", { name: "AccountName", length: 150 })
  accountName: string;

  @Column("nvarchar", { name: "AccountName2", nullable: true, length: 150 })
  accountName2: string | null;

  @Column("varchar", { name: "AccountKindID", nullable: true, length: 2 })
  accountKindId: string | null;

  @Column("varchar", { name: "AccountTypeID", nullable: true, length: 20 })
  accountTypeId: string | null;

  @Column("bit", { name: "isDetail", default: () => "1" })
  isDetail: boolean;

  @Column("bit", { name: "isObject", default: () => "0" })
  isObject: boolean;

  @Column("varchar", { name: "CurrencyID", nullable: true, length: 3 })
  currencyId: string | null;

  @Column("varchar", { name: "UserCreate", nullable: true, length: 30 })
  userCreate: string | null;

  @Column("varchar", { name: "UserUpdate", nullable: true, length: 30 })
  userUpdate: string | null;

  @Column("datetime", { name: "DateUpdate", nullable: true })
  dateUpdate: Date | null;

  @Column("datetime", { name: "DateCreate", nullable: true })
  dateCreate: Date | null;

  @Column("varchar", { name: "ParentAccountID", nullable: true, length: 50 })
  parentAccountId: string | null;

  @Column("bit", { name: "IsCostCenter", nullable: true, default: () => "0" })
  isCostCenter: boolean | null;

  @Column("bit", { name: "IsCost", nullable: true, default: () => "0" })
  isCost: boolean | null;

  @Column("bit", { name: "isItem", default: () => "0" })
  isItem: boolean;

  @Column("bit", { name: "isInvoice", default: () => "0" })
  isInvoice: boolean;
}
