import { Column, Entity, Index, OneToMany } from "typeorm";
import { PcCostGroupSetupDetailTbl } from "./PcCostGroupSetupDetailTbl";

@Index("PK_PC_CostGroupSetupTbl", ["setupId"], { unique: true })
@Entity("PC_CostGroupSetupTbl", { schema: "dbo" })
export class PcCostGroupSetupTbl {
  @Column("varchar", { primary: true, name: "SetupID", length: 50 })
  setupId: string;

  @Column("datetime", { name: "FromDate", nullable: true })
  fromDate: Date | null;

  @Column("datetime", { name: "ToDate", nullable: true })
  toDate: Date | null;

  @Column("varchar", { name: "CostGroupID", nullable: true, length: 50 })
  costGroupId: string | null;

  @Column("varchar", { name: "CostCenterGroupID", nullable: true, length: 50 })
  costCenterGroupId: string | null;

  @Column("int", { name: "ALType", default: () => "1" })
  alType: number;

  @Column("nvarchar", { name: "Memo", nullable: true, length: 150 })
  memo: string | null;

  @Column("bit", { name: "IsDisable", nullable: true, default: () => "0" })
  isDisable: boolean | null;

  @Column("varchar", { name: "UserCreate", nullable: true, length: 30 })
  userCreate: string | null;

  @Column("varchar", { name: "UserUpdate", nullable: true, length: 30 })
  userUpdate: string | null;

  @Column("datetime", { name: "DateUpdate", nullable: true })
  dateUpdate: Date | null;

  @Column("datetime", { name: "DateCreate", nullable: true })
  dateCreate: Date | null;

  @OneToMany(
    () => PcCostGroupSetupDetailTbl,
    pcCostGroupSetupDetailTbl => pcCostGroupSetupDetailTbl.setup
  )
  pcCostGroupSetupDetailTbls: PcCostGroupSetupDetailTbl[];
}
