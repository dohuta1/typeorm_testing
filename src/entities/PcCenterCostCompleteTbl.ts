import { Column, Entity, Index, JoinColumn, ManyToOne } from "typeorm";
import { PcCenterCostTbl } from "./PcCenterCostTbl";

@Index("PK_PC_CenterCostCompleteTbl", ["userAutoId"], { unique: true })
@Entity("PC_CenterCostCompleteTbl", { schema: "dbo" })
export class PcCenterCostCompleteTbl {
  @Column("varchar", { primary: true, name: "UserAutoID", length: 50 })
  userAutoId: string;

  @Column("varchar", { name: "CostCenterID", nullable: true, length: 50 })
  costCenterId: string | null;

  @Column("decimal", {
    name: "AmountCost",
    nullable: true,
    precision: 28,
    scale: 0,
    default: () => "0"
  })
  amountCost: number | null;

  @ManyToOne(
    () => PcCenterCostTbl,
    pcCenterCostTbl => pcCenterCostTbl.pcCenterCostCompleteTbls,
    { onDelete: "CASCADE", onUpdate: "CASCADE" }
  )
  @JoinColumn([{ name: "PriceID", referencedColumnName: "priceId" }])
  price: PcCenterCostTbl;
}
