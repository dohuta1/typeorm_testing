import { Column, Entity, Index, JoinColumn, ManyToOne } from "typeorm";
import { IvOutputTbl } from "./IvOutputTbl";

@Index("PK_IV_OutputCouponTbl", ["userAutoId"], { unique: true })
@Entity("IV_OutputCouponTbl", { schema: "dbo" })
export class IvOutputCouponTbl {
  @Column("varchar", { primary: true, name: "UserAutoID", length: 40 })
  userAutoId: string;

  @Column("varchar", { name: "CouponSeri", nullable: true, length: 100 })
  couponSeri: string | null;

  @Column("decimal", {
    name: "TriGia",
    nullable: true,
    precision: 18,
    scale: 0
  })
  triGia: number | null;

  @Column("nvarchar", { name: "Notes", nullable: true, length: 50 })
  notes: string | null;

  @Column("bit", { name: "XuatHuy", default: () => "(0)" })
  xuatHuy: boolean;

  @ManyToOne(
    () => IvOutputTbl,
    ivOutputTbl => ivOutputTbl.ivOutputCouponTbls,
    { onDelete: "CASCADE", onUpdate: "CASCADE" }
  )
  @JoinColumn([{ name: "DocumentID", referencedColumnName: "documentId" }])
  document: IvOutputTbl;
}
