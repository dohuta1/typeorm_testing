import { Column, Entity, Index, JoinColumn, ManyToOne } from "typeorm";
import { GjEntryTbl } from "./GjEntryTbl";

@Index("PK_GJ_VATInvoiceTbl", ["userAutoId"], { unique: true })
@Entity("GJ_VATInvoiceTbl", { schema: "dbo" })
export class GjVatInvoiceTbl {
  @Column("varchar", { primary: true, name: "UserAutoID", length: 40 })
  userAutoId: string;

  @Column("varchar", { name: "InvoiceNo", length: 20 })
  invoiceNo: string;

  @Column("varchar", { name: "InvoiceSerialNo", length: 20 })
  invoiceSerialNo: string;

  @Column("datetime", { name: "InvoiceDate" })
  invoiceDate: Date;

  @Column("nvarchar", { name: "ObjectName", nullable: true, length: 250 })
  objectName: string | null;

  @Column("nvarchar", { name: "Address", nullable: true, length: 100 })
  address: string | null;

  @Column("nvarchar", { name: "TaxCode", nullable: true, length: 50 })
  taxCode: string | null;

  @Column("nvarchar", { name: "ItemName", nullable: true, length: 150 })
  itemName: string | null;

  @Column("decimal", {
    name: "Amount",
    nullable: true,
    precision: 28,
    scale: 0
  })
  amount: number | null;

  @Column("varchar", { name: "VATID", nullable: true, length: 5 })
  vatid: string | null;

  @Column("decimal", {
    name: "VATPercent",
    nullable: true,
    precision: 18,
    scale: 0
  })
  vatPercent: number | null;

  @Column("decimal", {
    name: "VATAmount",
    nullable: true,
    precision: 28,
    scale: 0
  })
  vatAmount: number | null;

  @Column("bit", { name: "IsOutput", default: () => "0" })
  isOutput: boolean;

  @Column("bit", { name: "IsOff", default: () => "0" })
  isOff: boolean;

  @Column("datetime", {
    name: "DateCreate",
    nullable: true,
    default: () => "getdate()"
  })
  dateCreate: Date | null;

  @Column("datetime", {
    name: "DateUpdate",
    nullable: true,
    default: () => "getdate()"
  })
  dateUpdate: Date | null;

  @Column("varchar", { name: "UserCreate", nullable: true, length: 20 })
  userCreate: string | null;

  @Column("varchar", { name: "UserUpdate", nullable: true, length: 20 })
  userUpdate: string | null;

  @Column("nvarchar", { name: "KyHieuMauHoaDon", nullable: true, length: 50 })
  kyHieuMauHoaDon: string | null;

  @Column("varchar", { name: "InvoiceLink", nullable: true, length: 250 })
  invoiceLink: string | null;

  @ManyToOne(
    () => GjEntryTbl,
    gjEntryTbl => gjEntryTbl.gjVatInvoiceTbls,
    { onDelete: "CASCADE", onUpdate: "CASCADE" }
  )
  @JoinColumn([{ name: "EntryID", referencedColumnName: "documentId" }])
  entry: GjEntryTbl;
}
