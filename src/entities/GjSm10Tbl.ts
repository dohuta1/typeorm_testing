import { Column, Entity, Index } from "typeorm";

@Index("PK_GJ_SM10Tbl", ["codeId"], { unique: true })
@Entity("GJ_SM10Tbl", { schema: "dbo" })
export class GjSm10Tbl {
  @Column("varchar", { primary: true, name: "CodeID", length: 50 })
  codeId: string;

  @Column("varchar", { name: "GroupID", length: 13 })
  groupId: string;

  @Column("nvarchar", { name: "CodeName", nullable: true, length: 200 })
  codeName: string | null;

  @Column("nvarchar", { name: "CodeName2", nullable: true, length: 200 })
  codeName2: string | null;

  @Column("varchar", { name: "AccountID", nullable: true, length: 50 })
  accountId: string | null;

  @Column("varchar", { name: "AccountID2", nullable: true, length: 50 })
  accountId2: string | null;

  @Column("bit", { name: "PrintTF", nullable: true })
  printTf: boolean | null;

  @Column("bit", { name: "isBold", nullable: true })
  isBold: boolean | null;

  @Column("varchar", { name: "Formular", nullable: true, length: 50 })
  formular: string | null;

  @Column("decimal", { name: "Type", nullable: true, precision: 28, scale: 4 })
  type: number | null;

  @Column("bit", { name: "isObject", nullable: true })
  isObject: boolean | null;

  @Column("varchar", { name: "AmtType", nullable: true, length: 50 })
  amtType: string | null;

  @Column("bit", { name: "Negative", nullable: true })
  negative: boolean | null;

  @Column("bit", { name: "isAsset", nullable: true })
  isAsset: boolean | null;

  @Column("decimal", {
    name: "BeginAmount",
    nullable: true,
    precision: 28,
    scale: 0
  })
  beginAmount: number | null;

  @Column("decimal", {
    name: "EndAmount",
    nullable: true,
    precision: 28,
    scale: 0
  })
  endAmount: number | null;
}
