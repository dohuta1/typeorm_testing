import { Column, Entity, Index, OneToMany } from "typeorm";
import { PcBomItemTbl } from "./PcBomItemTbl";
import { PcBomMaterialTbl } from "./PcBomMaterialTbl";
import { PcProductCostTbl } from "./PcProductCostTbl";

@Index("PK_PC_BoMTbl", ["bomid"], { unique: true })
@Entity("PC_BoMTbl", { schema: "dbo" })
export class PcBoMTbl {
  @Column("varchar", { primary: true, name: "BOMID", length: 50 })
  bomid: string;

  @Column("nvarchar", { name: "BOMName", length: 150 })
  bomName: string;

  @Column("nvarchar", { name: "Notes", nullable: true, length: 150 })
  notes: string | null;

  @Column("bit", { name: "isDefault", default: () => "0" })
  isDefault: boolean;

  @Column("varchar", { name: "UserCreate", nullable: true, length: 30 })
  userCreate: string | null;

  @Column("varchar", { name: "UserUpdate", nullable: true, length: 30 })
  userUpdate: string | null;

  @Column("datetime", { name: "DateUpdate", nullable: true })
  dateUpdate: Date | null;

  @Column("datetime", { name: "DateCreate", nullable: true })
  dateCreate: Date | null;

  @Column("bit", { name: "isMaterial", default: () => "0" })
  isMaterial: boolean;

  @OneToMany(
    () => PcBomItemTbl,
    pcBomItemTbl => pcBomItemTbl.bom
  )
  pcBomItemTbls: PcBomItemTbl[];

  @OneToMany(
    () => PcBomMaterialTbl,
    pcBomMaterialTbl => pcBomMaterialTbl.bom
  )
  pcBomMaterialTbls: PcBomMaterialTbl[];

  @OneToMany(
    () => PcProductCostTbl,
    pcProductCostTbl => pcProductCostTbl.bom
  )
  pcProductCostTbls: PcProductCostTbl[];
}
