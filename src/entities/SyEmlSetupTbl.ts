import { Column, Entity, Index } from "typeorm";

@Index("PK_SY_EmlSetupTbl", ["keyId"], { unique: true })
@Entity("SY_EmlSetupTbl", { schema: "dbo" })
export class SyEmlSetupTbl {
  @Column("varchar", { primary: true, name: "KeyID", length: 50 })
  keyId: string;

  @Column("nvarchar", { name: "DisplayName", nullable: true, length: 50 })
  displayName: string | null;

  @Column("nvarchar", { name: "Email", length: 250 })
  email: string;

  @Column("nvarchar", { name: "Pass", nullable: true, length: 250 })
  pass: string | null;

  @Column("nvarchar", { name: "Subject", nullable: true, length: 500 })
  subject: string | null;

  @Column("varchar", { name: "Recipient", nullable: true, length: 2000 })
  recipient: string | null;

  @Column("nvarchar", { name: "EmailHeader", nullable: true, length: 3000 })
  emailHeader: string | null;

  @Column("nvarchar", { name: "EmailFooter", nullable: true, length: 3000 })
  emailFooter: string | null;

  @Column("nvarchar", { name: "IncomingServer", nullable: true, length: 250 })
  incomingServer: string | null;

  @Column("int", {
    name: "IncomingPort",
    nullable: true,
    default: () => "(110)"
  })
  incomingPort: number | null;

  @Column("int", {
    name: "IncomingSecurity",
    nullable: true,
    default: () => "(0)"
  })
  incomingSecurity: number | null;

  @Column("bit", {
    name: "IncomingAuthe",
    nullable: true,
    default: () => "(0)"
  })
  incomingAuthe: boolean | null;

  @Column("nvarchar", { name: "IncomingUser", nullable: true, length: 250 })
  incomingUser: string | null;

  @Column("nvarchar", { name: "IncomingPass", nullable: true, length: 250 })
  incomingPass: string | null;

  @Column("bit", {
    name: "ReceiveHeaderOnly",
    nullable: true,
    default: () => "(0)"
  })
  receiveHeaderOnly: boolean | null;

  @Column("int", {
    name: "CheckMailInterval",
    nullable: true,
    default: () => "(0)"
  })
  checkMailInterval: number | null;

  @Column("bit", {
    name: "CheckEmailAtStartup",
    nullable: true,
    default: () => "(0)"
  })
  checkEmailAtStartup: boolean | null;

  @Column("int", { name: "MaxEmailSize", nullable: true, default: () => "(0)" })
  maxEmailSize: number | null;

  @Column("int", {
    name: "MaxAttachSize",
    nullable: true,
    default: () => "(0)"
  })
  maxAttachSize: number | null;

  @Column("nvarchar", { name: "OutgoingServer", nullable: true, length: 250 })
  outgoingServer: string | null;

  @Column("int", {
    name: "OutgoingPort",
    nullable: true,
    default: () => "(25)"
  })
  outgoingPort: number | null;

  @Column("int", {
    name: "OutgoingSecurity",
    nullable: true,
    default: () => "(0)"
  })
  outgoingSecurity: number | null;

  @Column("bit", {
    name: "OutgoingAuthe",
    nullable: true,
    default: () => "(0)"
  })
  outgoingAuthe: boolean | null;

  @Column("nvarchar", { name: "OutgoingUser", nullable: true, length: 250 })
  outgoingUser: string | null;

  @Column("nvarchar", { name: "OutgoingPass", nullable: true, length: 250 })
  outgoingPass: string | null;

  @Column("int", { name: "isOutlook", nullable: true })
  isOutlook: number | null;
}
