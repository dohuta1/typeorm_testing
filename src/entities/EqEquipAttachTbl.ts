import { Column, Entity, Index, JoinColumn, ManyToOne } from "typeorm";
import { EqEquipTbl } from "./EqEquipTbl";

@Index("PK_EQ_EquipAttachTbl", ["userAutoId"], { unique: true })
@Entity("EQ_EquipAttachTbl", { schema: "dbo" })
export class EqEquipAttachTbl {
  @Column("varchar", { primary: true, name: "UserAutoID", length: 50 })
  userAutoId: string;

  @Column("nvarchar", { name: "FileName", nullable: true, length: 250 })
  fileName: string | null;

  @Column("int", { name: "FileType", nullable: true, default: () => "0" })
  fileType: number | null;

  @Column("nvarchar", { name: "STT", nullable: true, length: 50 })
  stt: string | null;

  @Column("image", { name: "Content", nullable: true })
  content: Buffer | null;

  @Column("image", { name: "Content2", nullable: true })
  content2: Buffer | null;

  @ManyToOne(
    () => EqEquipTbl,
    eqEquipTbl => eqEquipTbl.eqEquipAttachTbls,
    { onDelete: "CASCADE", onUpdate: "CASCADE" }
  )
  @JoinColumn([{ name: "EquipID", referencedColumnName: "equipId" }])
  equip: EqEquipTbl;
}
