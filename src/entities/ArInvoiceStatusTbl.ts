import { Column, Entity, Index, OneToMany } from "typeorm";
import { ArInvoiceTbl } from "./ArInvoiceTbl";

@Index("PK__AR_InvoiceStatusTbl", ["statusId"], { unique: true })
@Entity("AR_InvoiceStatusTbl", { schema: "dbo" })
export class ArInvoiceStatusTbl {
  @Column("int", { primary: true, name: "StatusID" })
  statusId: number;

  @Column("nvarchar", { name: "StatusName", length: 50 })
  statusName: string;

  @Column("int", { name: "BackColor", nullable: true })
  backColor: number | null;

  @Column("int", { name: "ForeColor", nullable: true })
  foreColor: number | null;

  @Column("int", { name: "OrderStatusID", nullable: true })
  orderStatusId: number | null;

  @OneToMany(
    () => ArInvoiceTbl,
    arInvoiceTbl => arInvoiceTbl.status2
  )
  arInvoiceTbls: ArInvoiceTbl[];
}
