import {
  Column,
  Entity,
  Index,
  JoinColumn,
  ManyToOne,
  OneToMany
} from "typeorm";
import { CsPaymentPlanAttachTbl } from "./CsPaymentPlanAttachTbl";
import { CsPaymentPlanDetailTbl } from "./CsPaymentPlanDetailTbl";
import { CfObjectTbl } from "./CfObjectTbl";
import { CsPaymentPlanStatusTbl } from "./CsPaymentPlanStatusTbl";

@Index("PK_CS_PaymentPlanTbl", ["documentId"], { unique: true })
@Entity("CS_PaymentPlanTbl", { schema: "dbo" })
export class CsPaymentPlanTbl {
  @Column("varchar", { primary: true, name: "DocumentID", length: 30 })
  documentId: string;

  @Column("datetime", { name: "DocumentDate" })
  documentDate: Date;

  @Column("nvarchar", { name: "Memo", nullable: true, length: 200 })
  memo: string | null;

  @Column("nvarchar", { name: "Notes", nullable: true, length: 200 })
  notes: string | null;

  @Column("decimal", {
    name: "BaseTotal",
    nullable: true,
    precision: 18,
    scale: 0
  })
  baseTotal: number | null;

  @Column("varchar", { name: "CashAccountID", length: 50 })
  cashAccountId: string;

  @Column("bit", { name: "isBank", default: () => "(0)" })
  isBank: boolean;

  @Column("bit", { name: "isLock", default: () => "(0)" })
  isLock: boolean;

  @Column("nvarchar", { name: "SearchField", nullable: true, length: 2000 })
  searchField: string | null;

  @Column("nvarchar", { name: "Receiver", nullable: true, length: 150 })
  receiver: string | null;

  @Column("varchar", { name: "UserCreate", nullable: true, length: 30 })
  userCreate: string | null;

  @Column("varchar", { name: "UserUpdate", nullable: true, length: 30 })
  userUpdate: string | null;

  @Column("datetime", { name: "DateUpdate", nullable: true })
  dateUpdate: Date | null;

  @Column("datetime", { name: "DateCreate", nullable: true })
  dateCreate: Date | null;

  @Column("varchar", { name: "BranchID", length: 50 })
  branchId: string;

  @Column("varchar", { name: "EmployeeID", nullable: true, length: 50 })
  employeeId: string | null;

  @Column("bit", { name: "isCK", default: () => "(1)" })
  isCk: boolean;

  @Column("nvarchar", { name: "LocationID", nullable: true, length: 50 })
  locationId: string | null;

  @OneToMany(
    () => CsPaymentPlanAttachTbl,
    csPaymentPlanAttachTbl => csPaymentPlanAttachTbl.document
  )
  csPaymentPlanAttachTbls: CsPaymentPlanAttachTbl[];

  @OneToMany(
    () => CsPaymentPlanDetailTbl,
    csPaymentPlanDetailTbl => csPaymentPlanDetailTbl.document
  )
  csPaymentPlanDetailTbls: CsPaymentPlanDetailTbl[];

  @ManyToOne(
    () => CfObjectTbl,
    cfObjectTbl => cfObjectTbl.csPaymentPlanTbls,
    { onUpdate: "CASCADE" }
  )
  @JoinColumn([{ name: "ObjectID", referencedColumnName: "objectId" }])
  object: CfObjectTbl;

  @ManyToOne(
    () => CsPaymentPlanStatusTbl,
    csPaymentPlanStatusTbl => csPaymentPlanStatusTbl.csPaymentPlanTbls,
    { onUpdate: "CASCADE" }
  )
  @JoinColumn([{ name: "StatusID", referencedColumnName: "statusId" }])
  status: CsPaymentPlanStatusTbl;
}
