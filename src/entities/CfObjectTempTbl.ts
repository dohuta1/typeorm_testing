import { Column, Entity, Index } from "typeorm";

@Index("PK_CF_ObjectTempTbl", ["objectTempId"], { unique: true })
@Entity("CF_ObjectTempTbl", { schema: "dbo" })
export class CfObjectTempTbl {
  @Column("varchar", {
    primary: true,
    name: "ObjectTempID",
    length: 50,
    default: () => "newid()"
  })
  objectTempId: string;

  @Column("nvarchar", { name: "ObjectName", length: 250 })
  objectName: string;

  @Column("nvarchar", { name: "Address", length: 150 })
  address: string;

  @Column("nvarchar", { name: "Phone", length: 50 })
  phone: string;

  @Column("varchar", { name: "TaxCode", nullable: true, length: 50 })
  taxCode: string | null;

  @Column("varchar", { name: "ObjectGroupID", nullable: true, length: 50 })
  objectGroupId: string | null;

  @Column("nvarchar", { name: "LocationID", nullable: true, length: 50 })
  locationId: string | null;

  @Column("int", { name: "ObjectGroupCode" })
  objectGroupCode: number;

  @Column("varchar", { name: "UserCreate", nullable: true, length: 30 })
  userCreate: string | null;

  @Column("varchar", { name: "UserUpdate", nullable: true, length: 30 })
  userUpdate: string | null;

  @Column("datetime", { name: "DateUpdate", nullable: true })
  dateUpdate: Date | null;

  @Column("datetime", { name: "DateCreate", nullable: true })
  dateCreate: Date | null;

  @Column("varchar", { name: "NewObjectID", nullable: true, length: 50 })
  newObjectId: string | null;
}
