import { Column, Entity, Index, OneToMany } from "typeorm";
import { ApPurchaseDetailTbl } from "./ApPurchaseDetailTbl";
import { ApReturnDetailTbl } from "./ApReturnDetailTbl";
import { ApServiceDetailTbl } from "./ApServiceDetailTbl";
import { ArBanHoDetailTbl } from "./ArBanHoDetailTbl";
import { ArInvoiceDetailTbl } from "./ArInvoiceDetailTbl";
import { ArReturnDetailTbl } from "./ArReturnDetailTbl";
import { ArServiceDetailTbl } from "./ArServiceDetailTbl";
import { CsPaymentDetailTbl } from "./CsPaymentDetailTbl";
import { CsReceiptDetailTbl } from "./CsReceiptDetailTbl";
import { GjEntryDetailTbl } from "./GjEntryDetailTbl";
import { IvInOutDetailTbl } from "./IvInOutDetailTbl";
import { IvInputDetailTbl } from "./IvInputDetailTbl";
import { IvOutputDetailTbl } from "./IvOutputDetailTbl";
import { PcAccSetupTbl } from "./PcAccSetupTbl";
import { SyBalanceAccountTbl } from "./SyBalanceAccountTbl";
import { SyBalanceItemTbl } from "./SyBalanceItemTbl";

@Index("PK_CF_CostCenterTbl", ["costCenterId"], { unique: true })
@Entity("CF_CostCenterTbl", { schema: "dbo" })
export class CfCostCenterTbl {
  @Column("varchar", { primary: true, name: "CostCenterID", length: 50 })
  costCenterId: string;

  @Column("nvarchar", { name: "CostCenterName", nullable: true, length: 250 })
  costCenterName: string | null;

  @Column("nvarchar", { name: "DiaChi", nullable: true, length: 250 })
  diaChi: string | null;

  @Column("nvarchar", { name: "DienThoai", nullable: true, length: 250 })
  dienThoai: string | null;

  @Column("varchar", { name: "ObjectID", nullable: true, length: 50 })
  objectId: string | null;

  @Column("decimal", { name: "Value", nullable: true, precision: 28, scale: 2 })
  value: number | null;

  @Column("char", { name: "Notes", nullable: true, length: 10 })
  notes: string | null;

  @Column("datetime", { name: "StartDate", nullable: true })
  startDate: Date | null;

  @Column("datetime", { name: "EndDate", nullable: true })
  endDate: Date | null;

  @Column("varchar", { name: "UserCreate", nullable: true, length: 30 })
  userCreate: string | null;

  @Column("varchar", { name: "UserUpdate", nullable: true, length: 30 })
  userUpdate: string | null;

  @Column("datetime", { name: "DateUpdate", nullable: true })
  dateUpdate: Date | null;

  @Column("datetime", { name: "DateCreate", nullable: true })
  dateCreate: Date | null;

  @Column("varchar", { name: "ParentCostCenterID", nullable: true, length: 50 })
  parentCostCenterId: string | null;

  @Column("bit", { name: "isSelect", nullable: true, default: () => "(1)" })
  isSelect: boolean | null;

  @Column("decimal", { name: "HeSo", nullable: true, precision: 28, scale: 4 })
  heSo: number | null;

  @OneToMany(
    () => ApPurchaseDetailTbl,
    apPurchaseDetailTbl => apPurchaseDetailTbl.costCenter
  )
  apPurchaseDetailTbls: ApPurchaseDetailTbl[];

  @OneToMany(
    () => ApReturnDetailTbl,
    apReturnDetailTbl => apReturnDetailTbl.costCenter
  )
  apReturnDetailTbls: ApReturnDetailTbl[];

  @OneToMany(
    () => ApServiceDetailTbl,
    apServiceDetailTbl => apServiceDetailTbl.costCenter
  )
  apServiceDetailTbls: ApServiceDetailTbl[];

  @OneToMany(
    () => ArBanHoDetailTbl,
    arBanHoDetailTbl => arBanHoDetailTbl.costCenter
  )
  arBanHoDetailTbls: ArBanHoDetailTbl[];

  @OneToMany(
    () => ArInvoiceDetailTbl,
    arInvoiceDetailTbl => arInvoiceDetailTbl.costCenter
  )
  arInvoiceDetailTbls: ArInvoiceDetailTbl[];

  @OneToMany(
    () => ArReturnDetailTbl,
    arReturnDetailTbl => arReturnDetailTbl.costCenter
  )
  arReturnDetailTbls: ArReturnDetailTbl[];

  @OneToMany(
    () => ArServiceDetailTbl,
    arServiceDetailTbl => arServiceDetailTbl.costCenter
  )
  arServiceDetailTbls: ArServiceDetailTbl[];

  @OneToMany(
    () => CsPaymentDetailTbl,
    csPaymentDetailTbl => csPaymentDetailTbl.costCenter
  )
  csPaymentDetailTbls: CsPaymentDetailTbl[];

  @OneToMany(
    () => CsReceiptDetailTbl,
    csReceiptDetailTbl => csReceiptDetailTbl.costCenter
  )
  csReceiptDetailTbls: CsReceiptDetailTbl[];

  @OneToMany(
    () => GjEntryDetailTbl,
    gjEntryDetailTbl => gjEntryDetailTbl.costCenter
  )
  gjEntryDetailTbls: GjEntryDetailTbl[];

  @OneToMany(
    () => IvInOutDetailTbl,
    ivInOutDetailTbl => ivInOutDetailTbl.costCenter
  )
  ivInOutDetailTbls: IvInOutDetailTbl[];

  @OneToMany(
    () => IvInputDetailTbl,
    ivInputDetailTbl => ivInputDetailTbl.costCenter
  )
  ivInputDetailTbls: IvInputDetailTbl[];

  @OneToMany(
    () => IvOutputDetailTbl,
    ivOutputDetailTbl => ivOutputDetailTbl.costCenter
  )
  ivOutputDetailTbls: IvOutputDetailTbl[];

  @OneToMany(
    () => PcAccSetupTbl,
    pcAccSetupTbl => pcAccSetupTbl.costCenter
  )
  pcAccSetupTbls: PcAccSetupTbl[];

  @OneToMany(
    () => SyBalanceAccountTbl,
    syBalanceAccountTbl => syBalanceAccountTbl.costCenter
  )
  syBalanceAccountTbls: SyBalanceAccountTbl[];

  @OneToMany(
    () => SyBalanceItemTbl,
    syBalanceItemTbl => syBalanceItemTbl.costCenter
  )
  syBalanceItemTbls: SyBalanceItemTbl[];
}
