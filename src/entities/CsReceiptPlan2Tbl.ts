import {
  Column,
  Entity,
  Index,
  JoinColumn,
  ManyToOne,
  OneToMany
} from "typeorm";
import { CsReceiptPlan2DetailTbl } from "./CsReceiptPlan2DetailTbl";
import { CsReceiptPlanStatusTbl } from "./CsReceiptPlanStatusTbl";
import { CfObjectTbl } from "./CfObjectTbl";

@Index("PK_CS_ReceiptPlan2Tbl", ["documentId"], { unique: true })
@Entity("CS_ReceiptPlan2Tbl", { schema: "dbo" })
export class CsReceiptPlan2Tbl {
  @Column("varchar", { primary: true, name: "DocumentID", length: 30 })
  documentId: string;

  @Column("datetime", { name: "DocumentDate" })
  documentDate: Date;

  @Column("nvarchar", { name: "Memo", nullable: true, length: 200 })
  memo: string | null;

  @Column("nvarchar", { name: "Notes", nullable: true, length: 200 })
  notes: string | null;

  @Column("decimal", {
    name: "BaseTotal",
    nullable: true,
    precision: 18,
    scale: 0
  })
  baseTotal: number | null;

  @Column("varchar", {
    name: "CashAccountID",
    length: 50,
    default: () => "(1111)"
  })
  cashAccountId: string;

  @Column("bit", { name: "isBank", default: () => "(0)" })
  isBank: boolean;

  @Column("bit", { name: "isCash", nullable: true, default: () => "(0)" })
  isCash: boolean | null;

  @Column("bit", { name: "isLock", default: () => "(0)" })
  isLock: boolean;

  @Column("nvarchar", { name: "SearchField", nullable: true, length: 2000 })
  searchField: string | null;

  @Column("nvarchar", { name: "Receiver", nullable: true, length: 150 })
  receiver: string | null;

  @Column("varchar", { name: "UserCreate", nullable: true, length: 30 })
  userCreate: string | null;

  @Column("varchar", { name: "UserUpdate", nullable: true, length: 30 })
  userUpdate: string | null;

  @Column("datetime", { name: "DateUpdate", nullable: true })
  dateUpdate: Date | null;

  @Column("datetime", { name: "DateCreate", nullable: true })
  dateCreate: Date | null;

  @Column("varchar", { name: "BranchID", length: 50 })
  branchId: string;

  @Column("varchar", { name: "EmployeeID", length: 50 })
  employeeId: string;

  @Column("varchar", { name: "ManagerID", length: 50 })
  managerId: string;

  @Column("nvarchar", { name: "LocationID", nullable: true, length: 50 })
  locationId: string | null;

  @OneToMany(
    () => CsReceiptPlan2DetailTbl,
    csReceiptPlan2DetailTbl => csReceiptPlan2DetailTbl.document
  )
  csReceiptPlan2DetailTbls: CsReceiptPlan2DetailTbl[];

  @ManyToOne(
    () => CsReceiptPlanStatusTbl,
    csReceiptPlanStatusTbl => csReceiptPlanStatusTbl.csReceiptPlan2Tbls,
    { onUpdate: "CASCADE" }
  )
  @JoinColumn([{ name: "StatusID", referencedColumnName: "statusId" }])
  status: CsReceiptPlanStatusTbl;

  @ManyToOne(
    () => CfObjectTbl,
    cfObjectTbl => cfObjectTbl.csReceiptPlan2Tbls,
    { onUpdate: "CASCADE" }
  )
  @JoinColumn([{ name: "ObjectID", referencedColumnName: "objectId" }])
  object: CfObjectTbl;
}
