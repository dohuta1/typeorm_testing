import { Column, Entity, Index, JoinColumn, ManyToOne } from "typeorm";
import { SlSalaryGroupTbl } from "./SlSalaryGroupTbl";
import { CfObjectTbl } from "./CfObjectTbl";

@Index("PK_SL_SalaryGroupEmployeeTbl", ["userAutoId"], { unique: true })
@Entity("SL_SalaryGroupEmployeeTbl", { schema: "dbo" })
export class SlSalaryGroupEmployeeTbl {
  @Column("varchar", {
    primary: true,
    name: "UserAutoID",
    length: 50,
    default: () => "newid()"
  })
  userAutoId: string;

  @Column("datetime", { name: "StartDate" })
  startDate: Date;

  @Column("datetime", { name: "EndDate", nullable: true })
  endDate: Date | null;

  @Column("varchar", { name: "Notes", nullable: true, length: 500 })
  notes: string | null;

  @ManyToOne(
    () => SlSalaryGroupTbl,
    slSalaryGroupTbl => slSalaryGroupTbl.slSalaryGroupEmployeeTbls,
    { onUpdate: "CASCADE" }
  )
  @JoinColumn([
    { name: "SalaryGroupID", referencedColumnName: "salaryGroupId" }
  ])
  salaryGroup: SlSalaryGroupTbl;

  @ManyToOne(
    () => CfObjectTbl,
    cfObjectTbl => cfObjectTbl.slSalaryGroupEmployeeTbls,
    { onUpdate: "CASCADE" }
  )
  @JoinColumn([{ name: "ObjectID", referencedColumnName: "objectId" }])
  object: CfObjectTbl;
}
