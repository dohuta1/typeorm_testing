import { Column, Entity, Index } from "typeorm";

@Index("PK_AD_ApproveSetupTbl", ["approveId"], { unique: true })
@Entity("AD_ApproveSetupTbl", { schema: "dbo" })
export class AdApproveSetupTbl {
  @Column("varchar", { primary: true, name: "ApproveID", length: 50 })
  approveId: string;

  @Column("nvarchar", { name: "SourceGroup", length: 150 })
  sourceGroup: string;

  @Column("nvarchar", { name: "SourceName", length: 150 })
  sourceName: string;

  @Column("varchar", { name: "SourceID", length: 50 })
  sourceId: string;

  @Column("nvarchar", { name: "GhiChu", nullable: true, length: 150 })
  ghiChu: string | null;

  @Column("varchar", { name: "UserLevel1", nullable: true, length: 50 })
  userLevel1: string | null;

  @Column("varchar", { name: "UserLevel2", nullable: true, length: 50 })
  userLevel2: string | null;

  @Column("varchar", { name: "UserLevel3", nullable: true, length: 50 })
  userLevel3: string | null;

  @Column("varchar", { name: "UserLevel4", nullable: true, length: 50 })
  userLevel4: string | null;

  @Column("varchar", { name: "UserLevel5", nullable: true, length: 50 })
  userLevel5: string | null;

  @Column("bit", { name: "isLevelEnd4", nullable: true, default: () => "1" })
  isLevelEnd4: boolean | null;

  @Column("bit", { name: "isLevelEnd5", nullable: true, default: () => "1" })
  isLevelEnd5: boolean | null;

  @Column("varchar", { name: "ToApproveID", nullable: true, length: 50 })
  toApproveId: string | null;

  @Column("varchar", { name: "ToApproveID2", nullable: true, length: 50 })
  toApproveId2: string | null;

  @Column("varchar", { name: "ToUser1", nullable: true, length: 50 })
  toUser1: string | null;

  @Column("varchar", { name: "ToUser2", nullable: true, length: 50 })
  toUser2: string | null;

  @Column("varchar", { name: "TableName", nullable: true, length: 50 })
  tableName: string | null;

  @Column("varchar", { name: "ToTableName", nullable: true, length: 50 })
  toTableName: string | null;

  @Column("nvarchar", { name: "FilterEx", nullable: true, length: 100 })
  filterEx: string | null;

  @Column("varchar", { name: "FormID", nullable: true, length: 50 })
  formId: string | null;
}
