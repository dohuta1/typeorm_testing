import {
  Column,
  Entity,
  Index,
  JoinColumn,
  ManyToOne,
  OneToMany
} from "typeorm";
import { ArInvoiceCouponTbl } from "./ArInvoiceCouponTbl";
import { ArInvoiceDetailTbl } from "./ArInvoiceDetailTbl";
import { ArInvoiceLogTbl } from "./ArInvoiceLogTbl";
import { ArInvoiceStatusTbl } from "./ArInvoiceStatusTbl";
import { CfObjectTbl } from "./CfObjectTbl";
import { ArVatInvoiceTbl } from "./ArVatInvoiceTbl";

@Index("IX_AR_InvoiceTbl", ["documentDate"], {})
@Index("PK_InvoiceTbl", ["documentId"], { unique: true })
@Entity("AR_InvoiceTbl", { schema: "dbo" })
export class ArInvoiceTbl {
  @Column("varchar", { primary: true, name: "DocumentID", length: 30 })
  documentId: string;

  @Column("datetime", { name: "DocumentDate" })
  documentDate: Date;

  @Column("varchar", { name: "EmployeeID", nullable: true, length: 50 })
  employeeId: string | null;

  @Column("varchar", { name: "ManagerID", nullable: true, length: 50 })
  managerId: string | null;

  @Column("nvarchar", { name: "Memo", nullable: true, length: 200 })
  memo: string | null;

  @Column("nvarchar", { name: "Notes", nullable: true, length: 200 })
  notes: string | null;

  @Column("nvarchar", { name: "Receiver", nullable: true, length: 100 })
  receiver: string | null;

  @Column("varchar", { name: "CurrencyID", length: 3, default: () => "'VND'" })
  currencyId: string;

  @Column("float", {
    name: "RateExchange",
    precision: 53,
    default: () => "(1)"
  })
  rateExchange: number;

  @Column("varchar", { name: "PaymentTypeID", nullable: true, length: 50 })
  paymentTypeId: string | null;

  @Column("varchar", { name: "PaymentTermID", nullable: true, length: 50 })
  paymentTermId: string | null;

  @Column("datetime", { name: "DueDate", nullable: true })
  dueDate: Date | null;

  @Column("varchar", { name: "PONo", nullable: true, length: 50 })
  poNo: string | null;

  @Column("varchar", { name: "ContractID", nullable: true, length: 50 })
  contractId: string | null;

  @Column("varchar", { name: "RefDoc", nullable: true, length: 50 })
  refDoc: string | null;

  @Column("decimal", {
    name: "BaseTotal",
    nullable: true,
    precision: 18,
    scale: 0
  })
  baseTotal: number | null;

  @Column("varchar", { name: "RevAccID", length: 50, default: () => "(131)" })
  revAccId: string;

  @Column("varchar", { name: "VATAccID", length: 50, default: () => "(33311)" })
  vatAccId: string;

  @Column("bit", { name: "isLock", default: () => "(0)" })
  isLock: boolean;

  @Column("varchar", { name: "InvoiceNo", nullable: true, length: 50 })
  invoiceNo: string | null;

  @Column("nvarchar", { name: "SearchField", nullable: true, length: 2000 })
  searchField: string | null;

  @Column("varchar", { name: "UserCreate", nullable: true, length: 30 })
  userCreate: string | null;

  @Column("varchar", { name: "UserUpdate", nullable: true, length: 30 })
  userUpdate: string | null;

  @Column("datetime", { name: "DateUpdate", nullable: true })
  dateUpdate: Date | null;

  @Column("datetime", { name: "DateCreate", nullable: true })
  dateCreate: Date | null;

  @Column("int", { name: "Status", default: () => "(1)" })
  status: number;

  @Column("varchar", { name: "LinkID", nullable: true, length: 50 })
  linkId: string | null;

  @Column("varchar", { name: "VATAccID2", nullable: true, length: 50 })
  vatAccId2: string | null;

  @Column("decimal", {
    name: "AmountTotal",
    nullable: true,
    precision: 28,
    scale: 0
  })
  amountTotal: number | null;

  @Column("decimal", {
    name: "VATAmountTotal",
    nullable: true,
    precision: 28,
    scale: 0
  })
  vatAmountTotal: number | null;

  @Column("varchar", { name: "ImpExpID", nullable: true, length: 10 })
  impExpId: string | null;

  @Column("bit", { name: "isExport", nullable: true, default: () => "(0)" })
  isExport: boolean | null;

  @Column("varchar", { name: "CostCenterID", nullable: true, length: 50 })
  costCenterId: string | null;

  @Column("varchar", { name: "CostID", nullable: true, length: 50 })
  costId: string | null;

  @Column("varchar", { name: "VATAccExID", nullable: true, length: 50 })
  vatAccExId: string | null;

  @Column("varchar", { name: "DocBatch", nullable: true, length: 50 })
  docBatch: string | null;

  @Column("decimal", {
    name: "CouponAmount",
    nullable: true,
    precision: 18,
    scale: 0
  })
  couponAmount: number | null;

  @Column("decimal", {
    name: "DiemTichLuy",
    nullable: true,
    precision: 18,
    scale: 1
  })
  diemTichLuy: number | null;

  @Column("int", { name: "LanIn", nullable: true })
  lanIn: number | null;

  @Column("nvarchar", { name: "LocationID", nullable: true, length: 50 })
  locationId: string | null;

  @Column("nvarchar", { name: "Attach", nullable: true, length: 100 })
  attach: string | null;

  @Column("datetime", { name: "AttachDate", nullable: true })
  attachDate: Date | null;

  @Column("nvarchar", { name: "NoiDungHoaDon", nullable: true, length: 500 })
  noiDungHoaDon: string | null;

  @Column("nvarchar", { name: "NguoiMuaHangHD", nullable: true, length: 250 })
  nguoiMuaHangHd: string | null;

  @Column("varchar", {
    name: "ChietKhau1AccID",
    length: 50,
    default: () => "(3388)"
  })
  chietKhau1AccId: string;

  @Column("varchar", {
    name: "ChietKhau2AccID",
    length: 50,
    default: () => "(1311)"
  })
  chietKhau2AccId: string;

  @Column("varchar", {
    name: "ChietKhau3AccID",
    length: 50,
    default: () => "(3388)"
  })
  chietKhau3AccId: string;

  @Column("varchar", {
    name: "ChietKhau4AccID",
    length: 50,
    default: () => "(3388)"
  })
  chietKhau4AccId: string;

  @Column("nvarchar", { name: "TenKhachLe", nullable: true, length: 250 })
  tenKhachLe: string | null;

  @Column("nvarchar", { name: "MSTKhachLe", nullable: true, length: 50 })
  mstKhachLe: string | null;

  @Column("varchar", { name: "BranchID", length: 50 })
  branchId: string;

  @Column("bit", { name: "isLocal", default: () => "(0)" })
  isLocal: boolean;

  @Column("varchar", { name: "InvoiceKey", nullable: true, length: 50 })
  invoiceKey: string | null;

  @Column("varchar", { name: "RefInvoiceKey", nullable: true, length: 50 })
  refInvoiceKey: string | null;

  @Column("nvarchar", { name: "ObjectNameHD", nullable: true, length: 250 })
  objectNameHd: string | null;

  @Column("nvarchar", { name: "AddressHD", nullable: true, length: 250 })
  addressHd: string | null;

  @Column("nvarchar", { name: "TaxCodeHD", nullable: true, length: 50 })
  taxCodeHd: string | null;

  @Column("varchar", { name: "UID", nullable: true, length: 50 })
  uid: string | null;

  @OneToMany(
    () => ArInvoiceCouponTbl,
    arInvoiceCouponTbl => arInvoiceCouponTbl.document
  )
  arInvoiceCouponTbls: ArInvoiceCouponTbl[];

  @OneToMany(
    () => ArInvoiceDetailTbl,
    arInvoiceDetailTbl => arInvoiceDetailTbl.document
  )
  arInvoiceDetailTbls: ArInvoiceDetailTbl[];

  @OneToMany(
    () => ArInvoiceLogTbl,
    arInvoiceLogTbl => arInvoiceLogTbl.document
  )
  arInvoiceLogTbls: ArInvoiceLogTbl[];

  @ManyToOne(
    () => ArInvoiceStatusTbl,
    arInvoiceStatusTbl => arInvoiceStatusTbl.arInvoiceTbls,
    { onUpdate: "CASCADE" }
  )
  @JoinColumn([{ name: "StatusID", referencedColumnName: "statusId" }])
  status2: ArInvoiceStatusTbl;

  @ManyToOne(
    () => CfObjectTbl,
    cfObjectTbl => cfObjectTbl.arInvoiceTbls,
    { onUpdate: "CASCADE" }
  )
  @JoinColumn([{ name: "ObjectID", referencedColumnName: "objectId" }])
  object: CfObjectTbl;

  @OneToMany(
    () => ArVatInvoiceTbl,
    arVatInvoiceTbl => arVatInvoiceTbl.invoice
  )
  arVatInvoiceTbls: ArVatInvoiceTbl[];
}
