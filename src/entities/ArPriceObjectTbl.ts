import { Column, Entity, Index, JoinColumn, ManyToOne } from "typeorm";
import { ArPriceTbl } from "./ArPriceTbl";
import { CfObjectTbl } from "./CfObjectTbl";

@Index("PK_AR_PriceObjectTbl", ["userAutoId"], { unique: true })
@Entity("AR_PriceObjectTbl", { schema: "dbo" })
export class ArPriceObjectTbl {
  @Column("varchar", { primary: true, name: "UserAutoID", length: 40 })
  userAutoId: string;

  @Column("nvarchar", { name: "Notes", nullable: true, length: 100 })
  notes: string | null;

  @Column("varchar", { name: "ParentID", nullable: true, length: 50 })
  parentId: string | null;

  @ManyToOne(
    () => ArPriceTbl,
    arPriceTbl => arPriceTbl.arPriceObjectTbls,
    { onDelete: "CASCADE", onUpdate: "CASCADE" }
  )
  @JoinColumn([{ name: "DocumentID", referencedColumnName: "documentId" }])
  document: ArPriceTbl;

  @ManyToOne(
    () => CfObjectTbl,
    cfObjectTbl => cfObjectTbl.arPriceObjectTbls,
    { onUpdate: "CASCADE" }
  )
  @JoinColumn([{ name: "ObjectID", referencedColumnName: "objectId" }])
  object: CfObjectTbl;
}
