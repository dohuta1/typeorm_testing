import { Column, Entity, Index, JoinColumn, ManyToOne } from "typeorm";
import { PcBoMTbl } from "./PcBoMTbl";

@Index("IX_PC_BomItemTbl", ["bomid", "itemId"], { unique: true })
@Index("PK_PC_BomItemTbl", ["userAutoId"], { unique: true })
@Entity("PC_BomItemTbl", { schema: "dbo" })
export class PcBomItemTbl {
  @Column("varchar", { primary: true, name: "UserAutoID", length: 50 })
  userAutoId: string;

  @Column("varchar", { name: "BOMID", length: 50 })
  bomid: string;

  @Column("varchar", { name: "ItemID", length: 50 })
  itemId: string;

  @Column("decimal", {
    name: "Quantity",
    precision: 18,
    scale: 4,
    default: () => "0"
  })
  quantity: number;

  @Column("decimal", {
    name: "HeSo",
    precision: 28,
    scale: 4,
    default: () => "1"
  })
  heSo: number;

  @ManyToOne(
    () => PcBoMTbl,
    pcBoMTbl => pcBoMTbl.pcBomItemTbls,
    { onDelete: "CASCADE", onUpdate: "CASCADE" }
  )
  @JoinColumn([{ name: "BOMID", referencedColumnName: "bomid" }])
  bom: PcBoMTbl;
}
