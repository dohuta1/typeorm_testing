import { Column, Entity, Index } from "typeorm";

@Index("PK_CS_ReceiptTypeTbl", ["documentType"], { unique: true })
@Entity("CS_ReceiptTypeTbl", { schema: "dbo" })
export class CsReceiptTypeTbl {
  @Column("varchar", { primary: true, name: "DocumentType", length: 50 })
  documentType: string;

  @Column("nvarchar", { name: "DocumentTypeName", nullable: true, length: 100 })
  documentTypeName: string | null;

  @Column("varchar", { name: "Prefix", nullable: true, length: 10 })
  prefix: string | null;

  @Column("varchar", { name: "CashAccountID", nullable: true, length: 50 })
  cashAccountId: string | null;

  @Column("bit", { name: "isBank", nullable: true })
  isBank: boolean | null;

  @Column("varchar", { name: "DocumentMask", nullable: true, length: 50 })
  documentMask: string | null;
}
