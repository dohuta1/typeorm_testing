import { Column, Entity, Index } from "typeorm";

@Index("PK_HR_DepartmentListTbl", ["phongBan"], { unique: true })
@Entity("HR_DepartmentListTbl", { schema: "dbo" })
export class HrDepartmentListTbl {
  @Column("nvarchar", { primary: true, name: "PhongBan", length: 50 })
  phongBan: string;

  @Column("nvarchar", { name: "TenPhongBan", nullable: true, length: 250 })
  tenPhongBan: string | null;

  @Column("nvarchar", { name: "GhiChu", nullable: true, length: 50 })
  ghiChu: string | null;
}
