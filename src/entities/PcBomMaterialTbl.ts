import { Column, Entity, Index, JoinColumn, ManyToOne } from "typeorm";
import { PcBoMTbl } from "./PcBoMTbl";

@Index("PK_PC_BomMaterialTbl", ["userAutoId"], { unique: true })
@Entity("PC_BomMaterialTbl", { schema: "dbo" })
export class PcBomMaterialTbl {
  @Column("varchar", { primary: true, name: "UserAutoID", length: 50 })
  userAutoId: string;

  @Column("varchar", { name: "MasterItemID", length: 50 })
  masterItemId: string;

  @Column("varchar", { name: "ItemID", length: 50 })
  itemId: string;

  @Column("decimal", {
    name: "Quantity",
    precision: 18,
    scale: 4,
    default: () => "0"
  })
  quantity: number;

  @Column("bit", { name: "isFix", default: () => "0" })
  isFix: boolean;

  @Column("bit", { name: "isWaste", default: () => "0" })
  isWaste: boolean;

  @Column("decimal", {
    name: "HaoHut",
    nullable: true,
    precision: 28,
    scale: 4,
    default: () => "0"
  })
  haoHut: number | null;

  @Column("decimal", {
    name: "PhePham",
    nullable: true,
    precision: 28,
    scale: 4,
    default: () => "0"
  })
  phePham: number | null;

  @Column("decimal", {
    name: "HeSo",
    precision: 28,
    scale: 4,
    default: () => "1"
  })
  heSo: number;

  @ManyToOne(
    () => PcBoMTbl,
    pcBoMTbl => pcBoMTbl.pcBomMaterialTbls,
    { onDelete: "CASCADE", onUpdate: "CASCADE" }
  )
  @JoinColumn([{ name: "BOMID", referencedColumnName: "bomid" }])
  bom: PcBoMTbl;
}
