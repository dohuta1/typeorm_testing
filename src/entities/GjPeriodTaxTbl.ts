import { Column, Entity, Index } from "typeorm";

@Index("PK_GJ_PeriodTaxTbl", ["setupId"], { unique: true })
@Entity("GJ_PeriodTaxTbl", { schema: "dbo" })
export class GjPeriodTaxTbl {
  @Column("varchar", { primary: true, name: "SetupID", length: 50 })
  setupId: string;

  @Column("varchar", { name: "STT", length: 50 })
  stt: string;

  @Column("nvarchar", { name: "Chitieu", length: 150 })
  chitieu: string;

  @Column("varchar", { name: "MaSoGiaTri", nullable: true, length: 50 })
  maSoGiaTri: string | null;

  @Column("decimal", {
    name: "GiaTriHH",
    nullable: true,
    precision: 28,
    scale: 0
  })
  giaTriHh: number | null;

  @Column("varchar", { name: "MaSoThue", nullable: true, length: 50 })
  maSoThue: string | null;

  @Column("decimal", {
    name: "ThueGTGT",
    nullable: true,
    precision: 28,
    scale: 0
  })
  thueGtgt: number | null;

  @Column("bit", { name: "isBold", default: () => "0" })
  isBold: boolean;
}
