import { Column, Entity, Index, JoinColumn, ManyToOne } from "typeorm";
import { ArPhieuGiamGiaTbl } from "./ArPhieuGiamGiaTbl";

@Index("PK_AR_PhieuGiamGiaDetailTbl", ["userAutoId"], { unique: true })
@Entity("AR_PhieuGiamGiaDetailTbl", { schema: "dbo" })
export class ArPhieuGiamGiaDetailTbl {
  @Column("varchar", { primary: true, name: "UserAutoID", length: 50 })
  userAutoId: string;

  @Column("varchar", { name: "CouponSeri", length: 100 })
  couponSeri: string;

  @Column("decimal", {
    name: "TriGia",
    nullable: true,
    precision: 18,
    scale: 0
  })
  triGia: number | null;

  @Column("nvarchar", { name: "Notes", nullable: true, length: 150 })
  notes: string | null;

  @ManyToOne(
    () => ArPhieuGiamGiaTbl,
    arPhieuGiamGiaTbl => arPhieuGiamGiaTbl.arPhieuGiamGiaDetailTbls,
    { onDelete: "CASCADE", onUpdate: "CASCADE" }
  )
  @JoinColumn([{ name: "DocumentID", referencedColumnName: "documentId" }])
  document: ArPhieuGiamGiaTbl;
}
