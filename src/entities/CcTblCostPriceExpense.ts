import { Column, Entity, Index } from "typeorm";

@Index("PK_CC_TblCostPriceExpense", ["id"], { unique: true })
@Entity("CC_TblCostPriceExpense", { schema: "dbo" })
export class CcTblCostPriceExpense {
  @Column("numeric", { primary: true, name: "ID", precision: 38, scale: 0 })
  id: number;

  @Column("varchar", { name: "PeriodID", nullable: true, length: 7 })
  periodId: string | null;

  @Column("varchar", { name: "CostCenterID", nullable: true, length: 10 })
  costCenterId: string | null;

  @Column("varchar", { name: "CostID", nullable: true, length: 10 })
  costId: string | null;

  @Column("int", { name: "Type", nullable: true })
  type: number | null;

  @Column("varchar", { name: "ItemID", nullable: true, length: 50 })
  itemId: string | null;

  @Column("nvarchar", { name: "Memo", nullable: true, length: 255 })
  memo: string | null;

  @Column("varchar", { name: "Unit", nullable: true, length: 50 })
  unit: string | null;

  @Column("decimal", {
    name: "Quantity",
    nullable: true,
    precision: 28,
    scale: 4
  })
  quantity: number | null;

  @Column("decimal", {
    name: "Amount",
    nullable: true,
    precision: 28,
    scale: 2
  })
  amount: number | null;

  @Column("varchar", { name: "AccountID", nullable: true, length: 25 })
  accountId: string | null;

  @Column("bit", { name: "isReturn", nullable: true })
  isReturn: boolean | null;

  @Column("float", { name: "Tmp", nullable: true, precision: 53 })
  tmp: number | null;

  @Column("bit", { name: "isPrevPeriod", nullable: true, default: () => "0" })
  isPrevPeriod: boolean | null;
}
