import { Column, Entity, Index, JoinColumn, ManyToOne } from "typeorm";
import { ArPromotionTbl } from "./ArPromotionTbl";

@Index("PK_AR_PromotionDenyTbl", ["userAutoId"], { unique: true })
@Entity("AR_PromotionDenyTbl", { schema: "dbo" })
export class ArPromotionDenyTbl {
  @Column("varchar", {
    primary: true,
    name: "UserAutoID",
    length: 50,
    default: () => "newid()"
  })
  userAutoId: string;

  @Column("varchar", { name: "PromotionDenyID", length: 30 })
  promotionDenyId: string;

  @Column("nvarchar", { name: "Notes", nullable: true, length: 250 })
  notes: string | null;

  @ManyToOne(
    () => ArPromotionTbl,
    arPromotionTbl => arPromotionTbl.arPromotionDenyTbls,
    { onDelete: "CASCADE", onUpdate: "CASCADE" }
  )
  @JoinColumn([{ name: "DocumentID", referencedColumnName: "documentId" }])
  document: ArPromotionTbl;
}
