import { Column, Entity, Index, JoinColumn, OneToOne } from "typeorm";
import { CsReceiptTbl } from "./CsReceiptTbl";

@Index("PK_CS_InputTbl", ["inputId"], { unique: true })
@Entity("CS_InputTbl", { schema: "dbo" })
export class CsInputTbl {
  @Column("varchar", { primary: true, name: "InputID", length: 30 })
  inputId: string;

  @Column("nvarchar", { name: "TrangThai", nullable: true, length: 50 })
  trangThai: string | null;

  @Column("datetime", { name: "NgayDuyet", nullable: true })
  ngayDuyet: Date | null;

  @Column("varchar", { name: "NguoiDuyet", nullable: true, length: 50 })
  nguoiDuyet: string | null;

  @Column("int", { name: "MaTrangThai", default: () => "(0)" })
  maTrangThai: number;

  @OneToOne(
    () => CsReceiptTbl,
    csReceiptTbl => csReceiptTbl.csInputTbl,
    { onDelete: "CASCADE", onUpdate: "CASCADE" }
  )
  @JoinColumn([{ name: "InputID", referencedColumnName: "documentId" }])
  input: CsReceiptTbl;
}
