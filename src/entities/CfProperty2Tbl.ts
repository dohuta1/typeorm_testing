import { Column, Entity, Index } from "typeorm";

@Index("PK_CF_Property2Tbl", ["userAutoId"], { unique: true })
@Entity("CF_Property2Tbl", { schema: "dbo" })
export class CfProperty2Tbl {
  @Column("varchar", { primary: true, name: "UserAutoID", length: 40 })
  userAutoId: string;

  @Column("nvarchar", { name: "Property2", nullable: true, length: 50 })
  property2: string | null;

  @Column("varchar", { name: "ItemID", nullable: true, length: 50 })
  itemId: string | null;

  @Column("varchar", { name: "CategoryID", nullable: true, length: 50 })
  categoryId: string | null;

  @Column("varchar", { name: "ItemGroupID", nullable: true, length: 50 })
  itemGroupId: string | null;

  @Column("nvarchar", { name: "Notes", nullable: true, length: 50 })
  notes: string | null;
}
