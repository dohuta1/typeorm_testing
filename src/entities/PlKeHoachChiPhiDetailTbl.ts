import { Column, Entity, Index, JoinColumn, ManyToOne } from "typeorm";
import { PlKeHoachChiPhiTbl } from "./PlKeHoachChiPhiTbl";

@Index("PK_PL_KeHoachChiPhiDetailTbl", ["userAutoId"], { unique: true })
@Entity("PL_KeHoachChiPhiDetailTbl", { schema: "dbo" })
export class PlKeHoachChiPhiDetailTbl {
  @Column("varchar", { primary: true, name: "UserAutoID", length: 50 })
  userAutoId: string;

  @Column("nvarchar", { name: "STT", length: 50 })
  stt: string;

  @Column("nvarchar", { name: "KhoanMuc", length: 250 })
  khoanMuc: string;

  @Column("decimal", {
    name: "Thang01",
    nullable: true,
    precision: 28,
    scale: 2
  })
  thang01: number | null;

  @Column("decimal", {
    name: "Thang02",
    nullable: true,
    precision: 28,
    scale: 2
  })
  thang02: number | null;

  @Column("decimal", {
    name: "Thang03",
    nullable: true,
    precision: 28,
    scale: 2
  })
  thang03: number | null;

  @Column("decimal", {
    name: "Thang04",
    nullable: true,
    precision: 28,
    scale: 2
  })
  thang04: number | null;

  @Column("decimal", {
    name: "Thang05",
    nullable: true,
    precision: 28,
    scale: 2
  })
  thang05: number | null;

  @Column("decimal", {
    name: "Thang06",
    nullable: true,
    precision: 28,
    scale: 2
  })
  thang06: number | null;

  @Column("decimal", {
    name: "Thang07",
    nullable: true,
    precision: 28,
    scale: 2
  })
  thang07: number | null;

  @Column("decimal", {
    name: "Thang08",
    nullable: true,
    precision: 28,
    scale: 2
  })
  thang08: number | null;

  @Column("decimal", {
    name: "Thang09",
    nullable: true,
    precision: 28,
    scale: 2
  })
  thang09: number | null;

  @Column("decimal", {
    name: "Thang10",
    nullable: true,
    precision: 28,
    scale: 2
  })
  thang10: number | null;

  @Column("decimal", {
    name: "Thang11",
    nullable: true,
    precision: 28,
    scale: 2
  })
  thang11: number | null;

  @Column("decimal", {
    name: "Thang12",
    nullable: true,
    precision: 28,
    scale: 2
  })
  thang12: number | null;

  @Column("nvarchar", { name: "GhiChu", nullable: true, length: 250 })
  ghiChu: string | null;

  @Column("varchar", { name: "CostID", nullable: true, length: 50 })
  costId: string | null;

  @Column("varchar", { name: "CostCenterID", nullable: true, length: 50 })
  costCenterId: string | null;

  @Column("varchar", { name: "AccountID", nullable: true, length: 50 })
  accountId: string | null;

  @ManyToOne(
    () => PlKeHoachChiPhiTbl,
    plKeHoachChiPhiTbl => plKeHoachChiPhiTbl.plKeHoachChiPhiDetailTbls,
    { onDelete: "CASCADE", onUpdate: "CASCADE" }
  )
  @JoinColumn([{ name: "MaKeHoach", referencedColumnName: "maKeHoach" }])
  maKeHoach: PlKeHoachChiPhiTbl;
}
