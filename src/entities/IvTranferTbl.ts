import {
  Column,
  Entity,
  Index,
  JoinColumn,
  ManyToOne,
  OneToMany
} from "typeorm";
import { IvTranferDetailTbl } from "./IvTranferDetailTbl";
import { CfObjectTbl } from "./CfObjectTbl";

@Index("PK_TranferTbl", ["documentId"], { unique: true })
@Entity("IV_TranferTbl", { schema: "dbo" })
export class IvTranferTbl {
  @Column("varchar", { primary: true, name: "DocumentID", length: 30 })
  documentId: string;

  @Column("datetime", { name: "DocumentDate" })
  documentDate: Date;

  @Column("nvarchar", { name: "Memo", nullable: true, length: 200 })
  memo: string | null;

  @Column("nvarchar", { name: "Notes", nullable: true, length: 200 })
  notes: string | null;

  @Column("bit", { name: "isLock", default: () => "(0)" })
  isLock: boolean;

  @Column("varchar", { name: "FromStoreHouseID", length: 50 })
  fromStoreHouseId: string;

  @Column("varchar", { name: "ToStoreHouseID", length: 50 })
  toStoreHouseId: string;

  @Column("nvarchar", { name: "SearchField", nullable: true, length: 2000 })
  searchField: string | null;

  @Column("varchar", { name: "UserCreate", nullable: true, length: 30 })
  userCreate: string | null;

  @Column("varchar", { name: "UserUpdate", nullable: true, length: 30 })
  userUpdate: string | null;

  @Column("datetime", { name: "DateUpdate", nullable: true })
  dateUpdate: Date | null;

  @Column("datetime", { name: "DateCreate", nullable: true })
  dateCreate: Date | null;

  @Column("int", { name: "Status", default: () => "(1)" })
  status: number;

  @Column("varchar", { name: "LinkID", nullable: true, length: 50 })
  linkId: string | null;

  @Column("decimal", {
    name: "BaseTotal",
    nullable: true,
    precision: 28,
    scale: 0
  })
  baseTotal: number | null;

  @Column("varchar", { name: "ImpExpID", nullable: true, length: 10 })
  impExpId: string | null;

  @Column("varchar", { name: "DocBatch", nullable: true, length: 50 })
  docBatch: string | null;

  @Column("varchar", { name: "BranchID", length: 50 })
  branchId: string;

  @Column("varchar", { name: "EmployeeID", nullable: true, length: 50 })
  employeeId: string | null;

  @OneToMany(
    () => IvTranferDetailTbl,
    ivTranferDetailTbl => ivTranferDetailTbl.document
  )
  ivTranferDetailTbls: IvTranferDetailTbl[];

  @ManyToOne(
    () => CfObjectTbl,
    cfObjectTbl => cfObjectTbl.ivTranferTbls,
    { onUpdate: "CASCADE" }
  )
  @JoinColumn([{ name: "ObjectID", referencedColumnName: "objectId" }])
  object: CfObjectTbl;
}
