import { Column, Entity, Index, JoinColumn, ManyToOne } from "typeorm";
import { EqMoveTbl } from "./EqMoveTbl";
import { EqEquipTbl } from "./EqEquipTbl";

@Index("PK_EQ_MoveDetailTbl", ["userAutoId"], { unique: true })
@Entity("EQ_MoveDetailTbl", { schema: "dbo" })
export class EqMoveDetailTbl {
  @Column("varchar", { primary: true, name: "UserAutoID", length: 40 })
  userAutoId: string;

  @Column("decimal", {
    name: "Quantity",
    nullable: true,
    precision: 18,
    scale: 2,
    default: () => "(1)"
  })
  quantity: number | null;

  @Column("varchar", { name: "DepartmentID", nullable: true, length: 50 })
  departmentId: string | null;

  @Column("varchar", { name: "ObjectID", nullable: true, length: 50 })
  objectId: string | null;

  @Column("varchar", { name: "CostCenterID", nullable: true, length: 50 })
  costCenterId: string | null;

  @Column("varchar", { name: "CostID", nullable: true, length: 50 })
  costId: string | null;

  @Column("nvarchar", { name: "Notes", nullable: true, length: 150 })
  notes: string | null;

  @ManyToOne(
    () => EqMoveTbl,
    eqMoveTbl => eqMoveTbl.eqMoveDetailTbls,
    { onDelete: "CASCADE", onUpdate: "CASCADE" }
  )
  @JoinColumn([{ name: "DocumentID", referencedColumnName: "documentId" }])
  document: EqMoveTbl;

  @ManyToOne(
    () => EqEquipTbl,
    eqEquipTbl => eqEquipTbl.eqMoveDetailTbls,
    { onUpdate: "CASCADE" }
  )
  @JoinColumn([{ name: "EquipID", referencedColumnName: "equipId" }])
  equip: EqEquipTbl;
}
