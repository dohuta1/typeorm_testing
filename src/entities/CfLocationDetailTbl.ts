import { Column, Entity, Index, JoinColumn, ManyToOne } from "typeorm";
import { CfLocationTbl } from "./CfLocationTbl";

@Index("PK_CF_LocationDetailTbl", ["userAutoId"], { unique: true })
@Entity("CF_LocationDetailTbl", { schema: "dbo" })
export class CfLocationDetailTbl {
  @Column("varchar", { primary: true, name: "UserAutoID", length: 50 })
  userAutoId: string;

  @Column("nvarchar", { name: "QuanHuyen", nullable: true, length: 50 })
  quanHuyen: string | null;

  @Column("int", { name: "STT", nullable: true })
  stt: number | null;

  @Column("nvarchar", { name: "GhiChu", nullable: true, length: 50 })
  ghiChu: string | null;

  @ManyToOne(
    () => CfLocationTbl,
    cfLocationTbl => cfLocationTbl.cfLocationDetailTbls,
    { onDelete: "CASCADE", onUpdate: "CASCADE" }
  )
  @JoinColumn([{ name: "LocationID", referencedColumnName: "locationId" }])
  location: CfLocationTbl;
}
