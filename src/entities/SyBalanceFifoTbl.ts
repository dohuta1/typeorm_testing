import { Column, Entity, Index, JoinColumn, ManyToOne } from "typeorm";
import { CfItemTbl } from "./CfItemTbl";
import { CfStoreHouseTbl } from "./CfStoreHouseTbl";

@Index("IX_SY_BalanceFIFOTbl_DocumentDate", ["documentDate"], {})
@Index("PK_SY_BalanceFIFOTbl", ["userAutoId"], { unique: true })
@Entity("SY_BalanceFIFOTbl", { schema: "dbo" })
export class SyBalanceFifoTbl {
  @Column("varchar", { primary: true, name: "UserAutoID", length: 50 })
  userAutoId: string;

  @Column("varchar", { name: "Lot", nullable: true, length: 50 })
  lot: string | null;

  @Column("varchar", { name: "InvAccID", length: 50 })
  invAccId: string;

  @Column("decimal", { name: "Quantity", precision: 28, scale: 8 })
  quantity: number;

  @Column("decimal", {
    name: "Quantity2",
    nullable: true,
    precision: 28,
    scale: 8
  })
  quantity2: number | null;

  @Column("nvarchar", { name: "Property", nullable: true, length: 50 })
  property: string | null;

  @Column("nvarchar", { name: "Property2", nullable: true, length: 50 })
  property2: string | null;

  @Column("decimal", {
    name: "UnitPrice",
    nullable: true,
    precision: 28,
    scale: 8
  })
  unitPrice: number | null;

  @Column("decimal", {
    name: "Amount",
    nullable: true,
    precision: 28,
    scale: 0
  })
  amount: number | null;

  @Column("varchar", { name: "DocumentID", nullable: true, length: 50 })
  documentId: string | null;

  @Column("datetime", { name: "DocumentDate", nullable: true })
  documentDate: Date | null;

  @Column("bit", { name: "isCompleted", nullable: true, default: () => "(0)" })
  isCompleted: boolean | null;

  @ManyToOne(
    () => CfItemTbl,
    cfItemTbl => cfItemTbl.syBalanceFifoTbls,
    { onUpdate: "CASCADE" }
  )
  @JoinColumn([{ name: "ItemID", referencedColumnName: "itemId" }])
  item: CfItemTbl;

  @ManyToOne(
    () => CfStoreHouseTbl,
    cfStoreHouseTbl => cfStoreHouseTbl.syBalanceFifoTbls,
    { onUpdate: "CASCADE" }
  )
  @JoinColumn([{ name: "StorehouseID", referencedColumnName: "storeHouseId" }])
  storehouse: CfStoreHouseTbl;
}
