import { Column, Entity, Index, JoinColumn, ManyToOne } from "typeorm";
import { CfItemTbl } from "./CfItemTbl";
import { IvTranferTbl } from "./IvTranferTbl";

@Index("IX_IV_TranferDetailTbl_DocumentID", ["documentId"], {})
@Index("PK_IV_TranferDetailTbl", ["userAutoId"], { unique: true })
@Entity("IV_TranferDetailTbl", { schema: "dbo" })
export class IvTranferDetailTbl {
  @Column("varchar", { primary: true, name: "UserAutoID", length: 40 })
  userAutoId: string;

  @Column("varchar", { name: "DocumentID", length: 30 })
  documentId: string;

  @Column("varchar", { name: "Lot", nullable: true, length: 50 })
  lot: string | null;

  @Column("datetime", { name: "ExpireDate", nullable: true })
  expireDate: Date | null;

  @Column("decimal", { name: "Quantity", precision: 28, scale: 4 })
  quantity: number;

  @Column("decimal", {
    name: "UnitPrice",
    nullable: true,
    precision: 28,
    scale: 2
  })
  unitPrice: number | null;

  @Column("decimal", {
    name: "Amount",
    nullable: true,
    precision: 28,
    scale: 0
  })
  amount: number | null;

  @Column("varchar", { name: "InvAccID", length: 50 })
  invAccId: string;

  @Column("bit", { name: "isFix", default: () => "(0)" })
  isFix: boolean;

  @Column("nvarchar", { name: "Notes", nullable: true, length: 100 })
  notes: string | null;

  @Column("decimal", {
    name: "Quantity2",
    nullable: true,
    precision: 28,
    scale: 4
  })
  quantity2: number | null;

  @Column("nvarchar", { name: "Property", nullable: true, length: 50 })
  property: string | null;

  @Column("nvarchar", { name: "Property2", nullable: true, length: 50 })
  property2: string | null;

  @Column("varchar", { name: "ToInvAccID", nullable: true, length: 50 })
  toInvAccId: string | null;

  @Column("varchar", { name: "CostCenterID", nullable: true, length: 50 })
  costCenterId: string | null;

  @Column("varchar", { name: "CostID", nullable: true, length: 50 })
  costId: string | null;

  @Column("float", { name: "UnitFactor", nullable: true, precision: 53 })
  unitFactor: number | null;

  @Column("varchar", { name: "ParentID", nullable: true, length: 50 })
  parentId: string | null;

  @ManyToOne(
    () => CfItemTbl,
    cfItemTbl => cfItemTbl.ivTranferDetailTbls,
    { onUpdate: "CASCADE" }
  )
  @JoinColumn([{ name: "ItemID", referencedColumnName: "itemId" }])
  item: CfItemTbl;

  @ManyToOne(
    () => IvTranferTbl,
    ivTranferTbl => ivTranferTbl.ivTranferDetailTbls,
    { onDelete: "CASCADE", onUpdate: "CASCADE" }
  )
  @JoinColumn([{ name: "DocumentID", referencedColumnName: "documentId" }])
  document: IvTranferTbl;
}
