import { Column, Entity, Index, JoinColumn, ManyToOne } from "typeorm";
import { CfBankContractTbl } from "./CfBankContractTbl";
import { CfCostCenterTbl } from "./CfCostCenterTbl";
import { CsReceiptTbl } from "./CsReceiptTbl";
import { CfCostTbl } from "./CfCostTbl";

@Index("IX_CS_ReceiptDetailTbl_DocumentID", ["documentId"], {})
@Index("PK_CS_ReceiptDetailTbl", ["userAutoId"], { unique: true })
@Entity("CS_ReceiptDetailTbl", { schema: "dbo" })
export class CsReceiptDetailTbl {
  @Column("varchar", { primary: true, name: "UserAutoID", length: 40 })
  userAutoId: string;

  @Column("varchar", { name: "DocumentID", length: 30 })
  documentId: string;

  @Column("varchar", { name: "AccountID", length: 50 })
  accountId: string;

  @Column("nvarchar", { name: "Memo", nullable: true, length: 200 })
  memo: string | null;

  @Column("decimal", {
    name: "SourceAmount",
    precision: 28,
    scale: 2,
    default: () => "(0)"
  })
  sourceAmount: number;

  @Column("decimal", {
    name: "Amount",
    precision: 28,
    scale: 0,
    default: () => "(0)"
  })
  amount: number;

  @Column("varchar", { name: "VATID", nullable: true, length: 50 })
  vatid: string | null;

  @Column("decimal", {
    name: "VATPercent",
    nullable: true,
    precision: 18,
    scale: 2
  })
  vatPercent: number | null;

  @Column("decimal", {
    name: "VATAmount",
    nullable: true,
    precision: 28,
    scale: 0
  })
  vatAmount: number | null;

  @Column("decimal", {
    name: "TotalAmount",
    nullable: true,
    precision: 28,
    scale: 0
  })
  totalAmount: number | null;

  @Column("varchar", { name: "RefDoc", nullable: true, length: 50 })
  refDoc: string | null;

  @Column("varchar", { name: "ItemID", nullable: true, length: 50 })
  itemId: string | null;

  @Column("decimal", {
    name: "Quantity",
    nullable: true,
    precision: 18,
    scale: 2
  })
  quantity: number | null;

  @Column("decimal", {
    name: "UnitPrice",
    nullable: true,
    precision: 18,
    scale: 0
  })
  unitPrice: number | null;

  @Column("varchar", { name: "ObjectID", nullable: true, length: 50 })
  objectId: string | null;

  @Column("varchar", { name: "DiffAccID", nullable: true, length: 50 })
  diffAccId: string | null;

  @Column("varchar", { name: "ContractID", nullable: true, length: 50 })
  contractId: string | null;

  @Column("varchar", { name: "ObjectID2", nullable: true, length: 50 })
  objectId2: string | null;

  @Column("varchar", { name: "RefDoc2", nullable: true, length: 50 })
  refDoc2: string | null;

  @Column("varchar", { name: "ParentID", nullable: true, length: 50 })
  parentId: string | null;

  @Column("varchar", { name: "CSType", nullable: true, length: 50 })
  csType: string | null;

  @Column("varchar", { name: "EmployeeID", nullable: true, length: 50 })
  employeeId: string | null;

  @ManyToOne(
    () => CfBankContractTbl,
    cfBankContractTbl => cfBankContractTbl.csReceiptDetailTbls,
    { onUpdate: "CASCADE" }
  )
  @JoinColumn([
    { name: "BankContractID", referencedColumnName: "bankContractId" }
  ])
  bankContract: CfBankContractTbl;

  @ManyToOne(
    () => CfCostCenterTbl,
    cfCostCenterTbl => cfCostCenterTbl.csReceiptDetailTbls,
    { onUpdate: "CASCADE" }
  )
  @JoinColumn([{ name: "CostCenterID", referencedColumnName: "costCenterId" }])
  costCenter: CfCostCenterTbl;

  @ManyToOne(
    () => CsReceiptTbl,
    csReceiptTbl => csReceiptTbl.csReceiptDetailTbls,
    { onDelete: "CASCADE", onUpdate: "CASCADE" }
  )
  @JoinColumn([{ name: "DocumentID", referencedColumnName: "documentId" }])
  document: CsReceiptTbl;

  @ManyToOne(
    () => CfCostTbl,
    cfCostTbl => cfCostTbl.csReceiptDetailTbls,
    { onUpdate: "CASCADE" }
  )
  @JoinColumn([{ name: "CostID", referencedColumnName: "costId" }])
  cost: CfCostTbl;
}
