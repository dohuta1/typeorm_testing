import { Column, Entity, Index } from "typeorm";

@Index("PK_SY_ApproveCountTbl", ["userAutoId"], { unique: true })
@Entity("SY_ApproveCountTbl", { schema: "dbo" })
export class SyApproveCountTbl {
  @Column("varchar", { primary: true, name: "UserAutoID", length: 50 })
  userAutoId: string;

  @Column("varchar", { name: "UserName", length: 50 })
  userName: string;

  @Column("decimal", { name: "STT", precision: 18, scale: 0 })
  stt: number;

  @Column("varchar", { name: "SourceID", length: 50 })
  sourceId: string;

  @Column("varchar", { name: "DocumentID", length: 50 })
  documentId: string;

  @Column("datetime", { name: "DocumentDate", default: () => "getdate()" })
  documentDate: Date;

  @Column("datetime", { name: "ApproveDate" })
  approveDate: Date;
}
