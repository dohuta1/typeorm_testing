import { Column, Entity, Index } from "typeorm";

@Index("PK_CF_PropertyTbl", ["userAutoId"], { unique: true })
@Entity("CF_PropertyTbl", { schema: "dbo" })
export class CfPropertyTbl {
  @Column("varchar", { primary: true, name: "UserAutoID", length: 40 })
  userAutoId: string;

  @Column("nvarchar", { name: "Property", nullable: true, length: 50 })
  property: string | null;

  @Column("varchar", { name: "ItemID", nullable: true, length: 50 })
  itemId: string | null;

  @Column("varchar", { name: "CategoryID", nullable: true, length: 50 })
  categoryId: string | null;

  @Column("varchar", { name: "ItemGroupID", nullable: true, length: 50 })
  itemGroupId: string | null;

  @Column("nvarchar", { name: "Notes", nullable: true, length: 50 })
  notes: string | null;
}
