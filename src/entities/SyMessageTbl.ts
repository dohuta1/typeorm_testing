import { Column, Entity, Index } from "typeorm";

@Index("PK__SY_Messa__6B2329654BDDB434", ["autoId"], { unique: true })
@Entity("SY_MessageTbl", { schema: "dbo" })
export class SyMessageTbl {
  @Column("varchar", {
    primary: true,
    name: "AutoID",
    length: 50,
    default: () => "newid()"
  })
  autoId: string;

  @Column("varchar", { name: "FromUser", length: 20 })
  fromUser: string;

  @Column("nvarchar", { name: "Content", length: 500 })
  content: string;

  @Column("varchar", { name: "ToUser", length: 500 })
  toUser: string;

  @Column("datetime", { name: "DateCreate", default: () => "getdate()" })
  dateCreate: Date;

  @Column("datetime", { name: "DateUpdate", nullable: true })
  dateUpdate: Date | null;

  @Column("bit", { name: "isRead", default: () => "(0)" })
  isRead: boolean;

  @Column("bit", { name: "isSendDelete", default: () => "(0)" })
  isSendDelete: boolean;

  @Column("bit", { name: "isRecieveDelete", default: () => "(0)" })
  isRecieveDelete: boolean;

  @Column("int", { name: "GroupID", nullable: true })
  groupId: number | null;

  @Column("int", { name: "DetailID", nullable: true })
  detailId: number | null;
}
