import { Column, Entity, Index, JoinColumn, ManyToOne } from "typeorm";
import { ArOrderTbl } from "./ArOrderTbl";

@Index("PK_AR_OrderLogTbl", ["userAutoId"], { unique: true })
@Entity("AR_OrderLogTbl", { schema: "dbo" })
export class ArOrderLogTbl {
  @Column("varchar", { primary: true, name: "UserAutoID", length: 40 })
  userAutoId: string;

  @Column("int", { name: "StatusID", nullable: true })
  statusId: number | null;

  @Column("nvarchar", { name: "StatusName", nullable: true, length: 100 })
  statusName: string | null;

  @Column("datetime", { name: "StatusDate", nullable: true })
  statusDate: Date | null;

  @Column("nvarchar", { name: "UserName", nullable: true, length: 50 })
  userName: string | null;

  @ManyToOne(
    () => ArOrderTbl,
    arOrderTbl => arOrderTbl.arOrderLogTbls,
    { onDelete: "CASCADE", onUpdate: "CASCADE" }
  )
  @JoinColumn([{ name: "DocumentID", referencedColumnName: "documentId" }])
  document: ArOrderTbl;
}
