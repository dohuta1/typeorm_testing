import { Column, Entity, Index, OneToMany } from "typeorm";
import { SlBonusSalaryTbl } from "./SlBonusSalaryTbl";
import { SlPunishSalaryTbl } from "./SlPunishSalaryTbl";

@Index("PK_KD_TargetTypeTbl", ["targetTypeId"], { unique: true })
@Entity("KD_TargetTypeTbl", { schema: "dbo" })
export class KdTargetTypeTbl {
  @Column("varchar", { primary: true, name: "TargetTypeID", length: 10 })
  targetTypeId: string;

  @Column("nvarchar", { name: "TargetTypeName", length: 150 })
  targetTypeName: string;

  @Column("varchar", { name: "UserCreate", nullable: true, length: 30 })
  userCreate: string | null;

  @Column("varchar", { name: "UserUpdate", nullable: true, length: 30 })
  userUpdate: string | null;

  @Column("datetime", { name: "DateUpdate", nullable: true })
  dateUpdate: Date | null;

  @Column("datetime", { name: "DateCreate", nullable: true })
  dateCreate: Date | null;

  @OneToMany(
    () => SlBonusSalaryTbl,
    slBonusSalaryTbl => slBonusSalaryTbl.targetType
  )
  slBonusSalaryTbls: SlBonusSalaryTbl[];

  @OneToMany(
    () => SlPunishSalaryTbl,
    slPunishSalaryTbl => slPunishSalaryTbl.targetType
  )
  slPunishSalaryTbls: SlPunishSalaryTbl[];
}
