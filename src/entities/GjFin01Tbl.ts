import { Column, Entity, Index } from "typeorm";

@Index("PK_GJ_Fin01Tbl", ["codeId"], { unique: true })
@Entity("GJ_Fin01Tbl", { schema: "dbo" })
export class GjFin01Tbl {
  @Column("varchar", { primary: true, name: "CodeID", length: 50 })
  codeId: string;

  @Column("varchar", { name: "GroupID", length: 13 })
  groupId: string;

  @Column("nvarchar", { name: "CodeName", nullable: true, length: 200 })
  codeName: string | null;

  @Column("nvarchar", { name: "CodeName2", nullable: true, length: 200 })
  codeName2: string | null;

  @Column("bit", { name: "PrintTF", nullable: true })
  printTf: boolean | null;

  @Column("bit", { name: "isBold", nullable: true })
  isBold: boolean | null;

  @Column("int", { name: "CodeLevel", nullable: true })
  codeLevel: number | null;

  @Column("decimal", { name: "ID", nullable: true, precision: 28, scale: 4 })
  id: number | null;

  @Column("decimal", { name: "Type", nullable: true, precision: 28, scale: 4 })
  type: number | null;
}
