import { Column, Entity, Index, JoinColumn, ManyToOne } from "typeorm";
import { GjAllocateSetupTbl } from "./GjAllocateSetupTbl";

@Index("PK_GJ_AllocatePeriodSetupTbl", ["userAutoId"], { unique: true })
@Entity("GJ_AllocatePeriodSetupTbl", { schema: "dbo" })
export class GjAllocatePeriodSetupTbl {
  @Column("varchar", { primary: true, name: "UserAutoID", length: 50 })
  userAutoId: string;

  @Column("varchar", { name: "PeriodID", length: 50 })
  periodId: string;

  @Column("bit", { name: "isStopAllocate", nullable: true })
  isStopAllocate: boolean | null;

  @Column("decimal", {
    name: "FixAllocateAmount",
    nullable: true,
    precision: 28,
    scale: 0
  })
  fixAllocateAmount: number | null;

  @Column("varchar", { name: "CostID", nullable: true, length: 50 })
  costId: string | null;

  @Column("varchar", { name: "CostCenterID", nullable: true, length: 50 })
  costCenterId: string | null;

  @Column("varchar", { name: "ObjectID", nullable: true, length: 50 })
  objectId: string | null;

  @Column("varchar", { name: "ExpenseAccID", nullable: true, length: 50 })
  expenseAccId: string | null;

  @Column("varchar", { name: "PrepayAccountID", nullable: true, length: 50 })
  prepayAccountId: string | null;

  @Column("nvarchar", { name: "Notes", nullable: true, length: 150 })
  notes: string | null;

  @ManyToOne(
    () => GjAllocateSetupTbl,
    gjAllocateSetupTbl => gjAllocateSetupTbl.gjAllocatePeriodSetupTbls,
    { onDelete: "CASCADE", onUpdate: "CASCADE" }
  )
  @JoinColumn([{ name: "AllocateID", referencedColumnName: "allocateId" }])
  allocate: GjAllocateSetupTbl;
}
