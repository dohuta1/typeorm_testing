import { Column, Entity, Index, JoinColumn, ManyToOne } from "typeorm";
import { SyUser } from "./SyUser";

@Index("PK_SY_UserBranch", ["userAutoId"], { unique: true })
@Entity("SY_UserBranch", { schema: "dbo" })
export class SyUserBranch {
  @Column("varchar", { primary: true, name: "UserAutoID", length: 50 })
  userAutoId: string;

  @Column("varchar", { name: "BranchID", nullable: true, length: 50 })
  branchId: string | null;

  @Column("bit", { name: "isDefault", nullable: true })
  isDefault: boolean | null;

  @Column("nvarchar", { name: "Notes", nullable: true, length: 50 })
  notes: string | null;

  @ManyToOne(
    () => SyUser,
    syUser => syUser.syUserBranches,
    { onDelete: "CASCADE", onUpdate: "CASCADE" }
  )
  @JoinColumn([{ name: "UserName", referencedColumnName: "userName" }])
  userName: SyUser;
}
