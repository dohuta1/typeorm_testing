import { Column, Entity, Index, JoinColumn, ManyToOne } from "typeorm";
import { ArSalesTargetTbl } from "./ArSalesTargetTbl";

@Index("PK__AR_SalesTargetDetail2Tbl", ["userAutoId"], { unique: true })
@Entity("AR_SalesTargetDetail2Tbl", { schema: "dbo" })
export class ArSalesTargetDetail2Tbl {
  @Column("varchar", { primary: true, name: "UserAutoID", length: 50 })
  userAutoId: string;

  @Column("varchar", { name: "ItemID", nullable: true, length: 50 })
  itemId: string | null;

  @Column("decimal", {
    name: "Quantity",
    nullable: true,
    precision: 18,
    scale: 2
  })
  quantity: number | null;

  @Column("nvarchar", { name: "Notes", nullable: true, length: 50 })
  notes: string | null;

  @ManyToOne(
    () => ArSalesTargetTbl,
    arSalesTargetTbl => arSalesTargetTbl.arSalesTargetDetail2Tbls,
    { onDelete: "CASCADE", onUpdate: "CASCADE" }
  )
  @JoinColumn([
    { name: "SalesTargetID", referencedColumnName: "salesTargetId" }
  ])
  salesTarget: ArSalesTargetTbl;
}
