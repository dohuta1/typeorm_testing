import { Column, Entity, Index, JoinColumn, ManyToOne } from "typeorm";
import { ArSalesTargetTbl } from "./ArSalesTargetTbl";

@Index("PK__AR_Sales__55ACAE2321D1A078", ["userAutoId"], { unique: true })
@Entity("AR_SalesTargetDetailTbl", { schema: "dbo" })
export class ArSalesTargetDetailTbl {
  @Column("varchar", { primary: true, name: "UserAutoID", length: 50 })
  userAutoId: string;

  @Column("int", { name: "ItemGroupCode", nullable: true })
  itemGroupCode: number | null;

  @Column("decimal", {
    name: "DoanhSo",
    nullable: true,
    precision: 18,
    scale: 0
  })
  doanhSo: number | null;

  @Column("nvarchar", { name: "Notes", nullable: true, length: 50 })
  notes: string | null;

  @ManyToOne(
    () => ArSalesTargetTbl,
    arSalesTargetTbl => arSalesTargetTbl.arSalesTargetDetailTbls,
    { onDelete: "CASCADE", onUpdate: "CASCADE" }
  )
  @JoinColumn([
    { name: "SalesTargetID", referencedColumnName: "salesTargetId" }
  ])
  salesTarget: ArSalesTargetTbl;
}
