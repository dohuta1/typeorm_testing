import { Column, Entity, Index } from "typeorm";

@Index("PK_CF_CurrencyTbl", ["currencyId"], { unique: true })
@Entity("CF_CurrencyTbl", { schema: "dbo" })
export class CfCurrencyTbl {
  @Column("varchar", { primary: true, name: "CurrencyID", length: 3 })
  currencyId: string;

  @Column("nvarchar", { name: "CurrencyName", length: 100 })
  currencyName: string;

  @Column("nvarchar", { name: "CurrencyName2", nullable: true, length: 100 })
  currencyName2: string | null;

  @Column("bit", { name: "isMultiply", default: () => "1" })
  isMultiply: boolean;

  @Column("float", { name: "RateExchange", precision: 53 })
  rateExchange: number;

  @Column("varchar", { name: "UserCreate", nullable: true, length: 30 })
  userCreate: string | null;

  @Column("varchar", { name: "UserUpdate", nullable: true, length: 30 })
  userUpdate: string | null;

  @Column("datetime", { name: "DateUpdate", nullable: true })
  dateUpdate: Date | null;

  @Column("datetime", { name: "DateCreate", nullable: true })
  dateCreate: Date | null;
}
