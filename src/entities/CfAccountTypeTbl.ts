import { Column, Entity, Index } from "typeorm";

@Index("PK_CF_AccTypeTbl", ["accountTypeId"], { unique: true })
@Entity("CF_AccountTypeTbl", { schema: "dbo" })
export class CfAccountTypeTbl {
  @Column("nvarchar", { primary: true, name: "AccountTypeID", length: 20 })
  accountTypeId: string;

  @Column("nvarchar", { name: "AccountTypeName", nullable: true, length: 70 })
  accountTypeName: string | null;

  @Column("nvarchar", { name: "AccountTypeNameSE", nullable: true, length: 70 })
  accountTypeNameSe: string | null;

  @Column("nvarchar", { name: "AcctKindID", nullable: true, length: 2 })
  acctKindId: string | null;

  @Column("varchar", { name: "UserCreate", nullable: true, length: 30 })
  userCreate: string | null;

  @Column("varchar", { name: "UserUpdate", nullable: true, length: 30 })
  userUpdate: string | null;

  @Column("datetime", { name: "DateUpdate", nullable: true })
  dateUpdate: Date | null;

  @Column("datetime", { name: "DateCreate", nullable: true })
  dateCreate: Date | null;
}
