import {
  Column,
  Entity,
  Index,
  JoinColumn,
  ManyToOne,
  OneToMany
} from "typeorm";
import { IvInOutDetailTbl } from "./IvInOutDetailTbl";
import { CfObjectTbl } from "./CfObjectTbl";
import { IvVatInvoiceTbl } from "./IvVatInvoiceTbl";

@Index("PK_InOutTbl", ["documentId"], { unique: true })
@Entity("IV_InOutTbl", { schema: "dbo" })
export class IvInOutTbl {
  @Column("varchar", { primary: true, name: "DocumentID", length: 30 })
  documentId: string;

  @Column("datetime", { name: "DocumentDate" })
  documentDate: Date;

  @Column("nvarchar", { name: "Memo", nullable: true, length: 200 })
  memo: string | null;

  @Column("nvarchar", { name: "Notes", nullable: true, length: 200 })
  notes: string | null;

  @Column("bit", { name: "isLock", default: () => "(0)" })
  isLock: boolean;

  @Column("nvarchar", { name: "Deliver", nullable: true, length: 100 })
  deliver: string | null;

  @Column("varchar", { name: "CurrencyID", length: 3, default: () => "'VND'" })
  currencyId: string;

  @Column("float", {
    name: "RateExchange",
    precision: 53,
    default: () => "(1)"
  })
  rateExchange: number;

  @Column("decimal", {
    name: "BaseTotal",
    nullable: true,
    precision: 28,
    scale: 0
  })
  baseTotal: number | null;

  @Column("varchar", { name: "PayAccID", length: 50, default: () => "(3311)" })
  payAccId: string;

  @Column("varchar", { name: "VATAccID", length: 50, default: () => "(13311)" })
  vatAccId: string;

  @Column("varchar", { name: "PaymentTypeID", nullable: true, length: 50 })
  paymentTypeId: string | null;

  @Column("varchar", { name: "PaymentTermID", nullable: true, length: 50 })
  paymentTermId: string | null;

  @Column("datetime", { name: "DueDate", nullable: true })
  dueDate: Date | null;

  @Column("varchar", { name: "PONo", nullable: true, length: 50 })
  poNo: string | null;

  @Column("varchar", { name: "ContractID", nullable: true, length: 50 })
  contractId: string | null;

  @Column("varchar", { name: "LinkID", nullable: true, length: 50 })
  linkId: string | null;

  @Column("int", { name: "Status", default: () => "(1)" })
  status: number;

  @Column("bit", { name: "CalPrice", default: () => "(1)" })
  calPrice: boolean;

  @Column("bit", { name: "CalCost", default: () => "(0)" })
  calCost: boolean;

  @Column("bit", { name: "DirectPrice", default: () => "(0)" })
  directPrice: boolean;

  @Column("varchar", { name: "InvoiceNo", nullable: true, length: 50 })
  invoiceNo: string | null;

  @Column("varchar", { name: "DocBatch", nullable: true, length: 50 })
  docBatch: string | null;

  @Column("nvarchar", { name: "SearchField", nullable: true, length: 2000 })
  searchField: string | null;

  @Column("varchar", { name: "UserCreate", nullable: true, length: 30 })
  userCreate: string | null;

  @Column("varchar", { name: "UserUpdate", nullable: true, length: 30 })
  userUpdate: string | null;

  @Column("datetime", { name: "DateUpdate", nullable: true })
  dateUpdate: Date | null;

  @Column("datetime", { name: "DateCreate", nullable: true })
  dateCreate: Date | null;

  @Column("varchar", { name: "BranchID", length: 50 })
  branchId: string;

  @OneToMany(
    () => IvInOutDetailTbl,
    ivInOutDetailTbl => ivInOutDetailTbl.document
  )
  ivInOutDetailTbls: IvInOutDetailTbl[];

  @ManyToOne(
    () => CfObjectTbl,
    cfObjectTbl => cfObjectTbl.ivInOutTbls,
    { onUpdate: "CASCADE" }
  )
  @JoinColumn([{ name: "ObjectID", referencedColumnName: "objectId" }])
  object: CfObjectTbl;

  @OneToMany(
    () => IvVatInvoiceTbl,
    ivVatInvoiceTbl => ivVatInvoiceTbl.inOut
  )
  ivVatInvoiceTbls: IvVatInvoiceTbl[];
}
