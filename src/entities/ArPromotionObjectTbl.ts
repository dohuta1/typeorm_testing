import { Column, Entity, Index, JoinColumn, ManyToOne } from "typeorm";
import { ArPromotionTbl } from "./ArPromotionTbl";
import { CfObjectTbl } from "./CfObjectTbl";

@Index("PK_AR_PromotionObjectTbl", ["userAutoId"], { unique: true })
@Entity("AR_PromotionObjectTbl", { schema: "dbo" })
export class ArPromotionObjectTbl {
  @Column("varchar", { primary: true, name: "UserAutoID", length: 40 })
  userAutoId: string;

  @Column("nvarchar", { name: "Notes", nullable: true, length: 100 })
  notes: string | null;

  @ManyToOne(
    () => ArPromotionTbl,
    arPromotionTbl => arPromotionTbl.arPromotionObjectTbls,
    { onDelete: "CASCADE", onUpdate: "CASCADE" }
  )
  @JoinColumn([{ name: "DocumentID", referencedColumnName: "documentId" }])
  document: ArPromotionTbl;

  @ManyToOne(
    () => CfObjectTbl,
    cfObjectTbl => cfObjectTbl.arPromotionObjectTbls,
    { onDelete: "CASCADE", onUpdate: "CASCADE" }
  )
  @JoinColumn([{ name: "ObjectID", referencedColumnName: "objectId" }])
  object: CfObjectTbl;
}
