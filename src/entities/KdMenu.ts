import { Column, Entity, Index } from "typeorm";

@Index("PK_KD_Menu", ["menuId"], { unique: true })
@Entity("KD_Menu", { schema: "dbo" })
export class KdMenu {
  @Column("varchar", { primary: true, name: "MenuID", length: 50 })
  menuId: string;

  @Column("nvarchar", { name: "VN", length: 200 })
  vn: string;

  @Column("nvarchar", { name: "EN", nullable: true, length: 200 })
  en: string | null;

  @Column("int", { name: "MenuType" })
  menuType: number;

  @Column("int", { name: "IconIndex", nullable: true })
  iconIndex: number | null;

  @Column("varchar", { name: "FormName", nullable: true, length: 100 })
  formName: string | null;

  @Column("varchar", { name: "Parent", nullable: true, length: 50 })
  parent: string | null;

  @Column("bit", { name: "isBeginGroup", default: () => "(0)" })
  isBeginGroup: boolean;

  @Column("bit", { name: "isDisable", default: () => "(0)" })
  isDisable: boolean;

  @Column("bit", { name: "isBold", default: () => "(0)" })
  isBold: boolean;

  @Column("bit", { name: "isNotCheckPermission", nullable: true })
  isNotCheckPermission: boolean | null;

  @Column("varchar", { name: "Para", nullable: true, length: 50 })
  para: string | null;

  @Column("nvarchar", { name: "ReportPara", nullable: true, length: 200 })
  reportPara: string | null;

  @Column("nvarchar", { name: "SubReportPara", nullable: true, length: 200 })
  subReportPara: string | null;

  @Column("varchar", { name: "MenuKey", nullable: true, length: 100 })
  menuKey: string | null;

  @Column("varchar", { name: "ShortKey", nullable: true, length: 50 })
  shortKey: string | null;

  @Column("bit", { name: "ShowDialog", nullable: true })
  showDialog: boolean | null;

  @Column("nvarchar", { name: "Filter", nullable: true, length: 150 })
  filter: string | null;

  @Column("nvarchar", { name: "CH", nullable: true, length: 200 })
  ch: string | null;
}
