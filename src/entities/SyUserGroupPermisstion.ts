import { Column, Entity, Index } from "typeorm";

@Index("PK_SY_UserGroupPermisstion", ["id"], { unique: true })
@Entity("SY_UserGroupPermisstion", { schema: "dbo" })
export class SyUserGroupPermisstion {
  @Column("varchar", {
    primary: true,
    name: "ID",
    length: 50,
    default: () => "newid()"
  })
  id: string;

  @Column("varchar", { name: "UserGroupID", length: 20 })
  userGroupId: string;

  @Column("varchar", { name: "MenuID", length: 50 })
  menuId: string;

  @Column("bit", { name: "IsRun" })
  isRun: boolean;

  @Column("bit", { name: "IsAdd" })
  isAdd: boolean;

  @Column("bit", { name: "IsUpdate" })
  isUpdate: boolean;

  @Column("bit", { name: "IsDelete" })
  isDelete: boolean;

  @Column("bit", { name: "isManager", nullable: true })
  isManager: boolean | null;

  @Column("bit", { name: "isAdmin" })
  isAdmin: boolean;

  @Column("bit", { name: "isAutoLock", nullable: true, default: () => "(0)" })
  isAutoLock: boolean | null;

  @Column("bit", { name: "isHideAmount", nullable: true })
  isHideAmount: boolean | null;

  @Column("bit", { name: "isLockDoc", nullable: true })
  isLockDoc: boolean | null;

  @Column("bit", { name: "isUnLockDoc", nullable: true })
  isUnLockDoc: boolean | null;
}
