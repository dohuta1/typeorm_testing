import { Column, Entity, Index, JoinColumn, OneToOne } from "typeorm";
import { CsPaymentTbl } from "./CsPaymentTbl";

@Index("PK_CS_outputTbl", ["outputId"], { unique: true })
@Entity("CS_OutputTbl", { schema: "dbo" })
export class CsOutputTbl {
  @Column("varchar", { primary: true, name: "OutputID", length: 30 })
  outputId: string;

  @Column("nvarchar", { name: "TrangThai", nullable: true, length: 50 })
  trangThai: string | null;

  @Column("datetime", { name: "NgayDuyet", nullable: true })
  ngayDuyet: Date | null;

  @Column("varchar", { name: "NguoiDuyet", nullable: true, length: 50 })
  nguoiDuyet: string | null;

  @Column("int", { name: "MaTrangThai", default: () => "(0)" })
  maTrangThai: number;

  @OneToOne(
    () => CsPaymentTbl,
    csPaymentTbl => csPaymentTbl.csOutputTbl,
    { onDelete: "CASCADE", onUpdate: "CASCADE" }
  )
  @JoinColumn([{ name: "OutputID", referencedColumnName: "documentId" }])
  output: CsPaymentTbl;
}
