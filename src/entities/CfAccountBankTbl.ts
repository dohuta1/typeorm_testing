import { Column, Entity, Index } from "typeorm";

@Index("PK_CF_AccountBankTbl", ["accountNo"], { unique: true })
@Entity("CF_AccountBankTbl", { schema: "dbo" })
export class CfAccountBankTbl {
  @Column("varchar", { primary: true, name: "AccountNo", length: 50 })
  accountNo: string;

  @Column("nvarchar", { name: "BankName", length: 100 })
  bankName: string;

  @Column("nvarchar", { name: "BankName2", nullable: true, length: 100 })
  bankName2: string | null;

  @Column("varchar", { name: "ObjectID", nullable: true, length: 50 })
  objectId: string | null;

  @Column("nvarchar", { name: "Notes", nullable: true, length: 50 })
  notes: string | null;

  @Column("varchar", { name: "UserCreate", nullable: true, length: 30 })
  userCreate: string | null;

  @Column("varchar", { name: "UserUpdate", nullable: true, length: 30 })
  userUpdate: string | null;

  @Column("datetime", { name: "DateUpdate", nullable: true })
  dateUpdate: Date | null;

  @Column("datetime", { name: "DateCreate", nullable: true })
  dateCreate: Date | null;

  @Column("nvarchar", { name: "Province", nullable: true, length: 150 })
  province: string | null;

  @Column("varchar", { name: "CashAccountID", nullable: true, length: 50 })
  cashAccountId: string | null;

  @Column("nvarchar", { name: "BankBranch", nullable: true, length: 100 })
  bankBranch: string | null;

  @Column("nvarchar", { name: "ObjectNameUNC", nullable: true, length: 250 })
  objectNameUnc: string | null;
}
