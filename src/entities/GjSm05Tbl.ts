import { Column, Entity, Index } from "typeorm";

@Index("PK_GJ_SM05Tbl", ["codeId"], { unique: true })
@Entity("GJ_SM05Tbl", { schema: "dbo" })
export class GjSm05Tbl {
  @Column("varchar", { primary: true, name: "CodeID", length: 50 })
  codeId: string;

  @Column("varchar", { name: "GroupID", length: 13 })
  groupId: string;

  @Column("nvarchar", { name: "CodeName", nullable: true, length: 200 })
  codeName: string | null;

  @Column("nvarchar", { name: "CodeName2", nullable: true, length: 200 })
  codeName2: string | null;

  @Column("decimal", {
    name: "Amount01",
    nullable: true,
    precision: 28,
    scale: 0
  })
  amount01: number | null;

  @Column("decimal", {
    name: "Amount02",
    nullable: true,
    precision: 28,
    scale: 0
  })
  amount02: number | null;

  @Column("decimal", {
    name: "Amount03",
    nullable: true,
    precision: 28,
    scale: 0
  })
  amount03: number | null;

  @Column("decimal", {
    name: "Amount04",
    nullable: true,
    precision: 28,
    scale: 0
  })
  amount04: number | null;

  @Column("decimal", {
    name: "Amount05",
    nullable: true,
    precision: 28,
    scale: 0
  })
  amount05: number | null;

  @Column("decimal", {
    name: "Amount06",
    nullable: true,
    precision: 28,
    scale: 0
  })
  amount06: number | null;

  @Column("decimal", {
    name: "AmountTotal",
    nullable: true,
    precision: 28,
    scale: 0
  })
  amountTotal: number | null;

  @Column("varchar", { name: "Formular", nullable: true, length: 100 })
  formular: string | null;

  @Column("bit", { name: "PrintTF", nullable: true })
  printTf: boolean | null;

  @Column("bit", { name: "isBold", nullable: true })
  isBold: boolean | null;

  @Column("varchar", { name: "AmtType", nullable: true, length: 50 })
  amtType: string | null;

  @Column("int", { name: "Type", nullable: true })
  type: number | null;

  @Column("bit", { name: "Negative", nullable: true })
  negative: boolean | null;

  @Column("varchar", { name: "Acc01", nullable: true, length: 50 })
  acc01: string | null;

  @Column("varchar", { name: "Acc2_01", nullable: true, length: 50 })
  acc2_01: string | null;

  @Column("varchar", { name: "Acc02", nullable: true, length: 50 })
  acc02: string | null;

  @Column("varchar", { name: "Acc2_02", nullable: true, length: 50 })
  acc2_02: string | null;

  @Column("varchar", { name: "Acc03", nullable: true, length: 50 })
  acc03: string | null;

  @Column("varchar", { name: "Acc2_03", nullable: true, length: 50 })
  acc2_03: string | null;

  @Column("varchar", { name: "Acc04", nullable: true, length: 50 })
  acc04: string | null;

  @Column("varchar", { name: "Acc2_04", nullable: true, length: 50 })
  acc2_04: string | null;

  @Column("varchar", { name: "Acc05", nullable: true, length: 50 })
  acc05: string | null;

  @Column("varchar", { name: "Acc2_05", nullable: true, length: 50 })
  acc2_05: string | null;

  @Column("varchar", { name: "Acc06", nullable: true, length: 50 })
  acc06: string | null;

  @Column("varchar", { name: "Acc2_06", nullable: true, length: 50 })
  acc2_06: string | null;

  @Column("varchar", { name: "Acc07", nullable: true, length: 50 })
  acc07: string | null;

  @Column("varchar", { name: "Acc2_07", nullable: true, length: 50 })
  acc2_07: string | null;

  @Column("decimal", {
    name: "Amount07",
    nullable: true,
    precision: 28,
    scale: 0
  })
  amount07: number | null;
}
