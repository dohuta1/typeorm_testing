import { Column, Entity, Index } from "typeorm";

@Index("PK_IS_OutputTbl", ["outputId"], { unique: true })
@Entity("IS_OutputTbl", { schema: "dbo" })
export class IsOutputTbl {
  @Column("nvarchar", { primary: true, name: "OutputID", length: 50 })
  outputId: string;

  @Column("nvarchar", { name: "TrangThai", nullable: true, length: 50 })
  trangThai: string | null;

  @Column("datetime", { name: "NgayXuat", nullable: true })
  ngayXuat: Date | null;

  @Column("varchar", { name: "NguoiDuyet", nullable: true, length: 50 })
  nguoiDuyet: string | null;

  @Column("int", { name: "MaTrangThai", default: () => "(0)" })
  maTrangThai: number;

  @Column("datetime", { name: "NgayGiao", nullable: true })
  ngayGiao: Date | null;

  @Column("nvarchar", { name: "NhanVienGiao", nullable: true, length: 50 })
  nhanVienGiao: string | null;
}
