import { Column, Entity, Index, OneToMany } from "typeorm";
import { SyUserBranch } from "./SyUserBranch";
import { SyUserStorehouse } from "./SyUserStorehouse";

@Index("PK_SY_User", ["userName"], { unique: true })
@Entity("SY_User", { schema: "dbo" })
export class SyUser {
  @Column("varchar", { primary: true, name: "UserName", length: 100 })
  userName: string;

  @Column("nvarchar", { name: "HoTen", length: 150 })
  hoTen: string;

  @Column("nvarchar", { name: "TenNgan", length: 100 })
  tenNgan: string;

  @Column("varchar", { name: "Password", nullable: true, length: 200 })
  password: string | null;

  @Column("bit", { name: "Manager", nullable: true })
  manager: boolean | null;

  @Column("bit", { name: "Disable" })
  disable: boolean;

  @Column("varchar", { name: "UserGroupID", nullable: true, length: 50 })
  userGroupId: string | null;

  @Column("datetime", { name: "TempDate", nullable: true })
  tempDate: Date | null;

  @Column("varchar", { name: "UserAuthority", nullable: true, length: 100 })
  userAuthority: string | null;

  @Column("varchar", { name: "BranchID", nullable: true, length: 50 })
  branchId: string | null;

  @Column("varchar", { name: "CeoID", nullable: true, length: 50 })
  ceoId: string | null;

  @Column("varchar", { name: "ManagerID", nullable: true, length: 50 })
  managerId: string | null;

  @Column("varchar", { name: "EmployeeID", nullable: true, length: 50 })
  employeeId: string | null;

  @Column("varchar", { name: "CostCenterID", nullable: true, length: 50 })
  costCenterId: string | null;

  @Column("varchar", { name: "StoreHouseID", nullable: true, length: 50 })
  storeHouseId: string | null;

  @Column("varchar", { name: "StartupForm", nullable: true, length: 50 })
  startupForm: string | null;

  @Column("bit", {
    name: "DisableAutoBackup",
    nullable: true,
    default: () => "(0)"
  })
  disableAutoBackup: boolean | null;

  @OneToMany(
    () => SyUserBranch,
    syUserBranch => syUserBranch.userName
  )
  syUserBranches: SyUserBranch[];

  @OneToMany(
    () => SyUserStorehouse,
    syUserStorehouse => syUserStorehouse.userName
  )
  syUserStorehouses: SyUserStorehouse[];
}
