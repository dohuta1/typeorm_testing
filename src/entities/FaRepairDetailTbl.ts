import { Column, Entity, Index, JoinColumn, ManyToOne } from "typeorm";
import { FaRepairTbl } from "./FaRepairTbl";
import { FaAssetTbl } from "./FaAssetTbl";

@Index("PK_FA_RepairDetailTbl", ["userAutoId"], { unique: true })
@Entity("FA_RepairDetailTbl", { schema: "dbo" })
export class FaRepairDetailTbl {
  @Column("varchar", { primary: true, name: "UserAutoID", length: 40 })
  userAutoId: string;

  @Column("nvarchar", { name: "NoiDung", nullable: true, length: 150 })
  noiDung: string | null;

  @Column("decimal", {
    name: "Quantity",
    nullable: true,
    precision: 18,
    scale: 2,
    default: () => "(1)"
  })
  quantity: number | null;

  @Column("decimal", {
    name: "Amount",
    nullable: true,
    precision: 18,
    scale: 2
  })
  amount: number | null;

  @Column("nvarchar", { name: "Notes", nullable: true, length: 150 })
  notes: string | null;

  @ManyToOne(
    () => FaRepairTbl,
    faRepairTbl => faRepairTbl.faRepairDetailTbls,
    { onDelete: "CASCADE", onUpdate: "CASCADE" }
  )
  @JoinColumn([{ name: "DocumentID", referencedColumnName: "documentId" }])
  document: FaRepairTbl;

  @ManyToOne(
    () => FaAssetTbl,
    faAssetTbl => faAssetTbl.faRepairDetailTbls,
    { onUpdate: "CASCADE" }
  )
  @JoinColumn([{ name: "AssetID", referencedColumnName: "assetId" }])
  asset: FaAssetTbl;
}
