import { Column, Entity, Index, JoinColumn, ManyToOne } from "typeorm";
import { GjCfSetupTbl } from "./GjCfSetupTbl";

@Index("PK_GJ_CashFlowSetupDetailTbl", ["userAutoId"], { unique: true })
@Entity("GJ_CFSetupDetailTbl", { schema: "dbo" })
export class GjCfSetupDetailTbl {
  @Column("varchar", { primary: true, name: "UserAutoID", length: 50 })
  userAutoId: string;

  @Column("varchar", { name: "DebitAccIDs", nullable: true, length: 150 })
  debitAccIDs: string | null;

  @Column("varchar", { name: "CreditAccIDs", nullable: true, length: 150 })
  creditAccIDs: string | null;

  @Column("varchar", { name: "SumID", nullable: true, length: 50 })
  sumId: string | null;

  @Column("bit", { name: "ReverseSign", default: () => "0" })
  reverseSign: boolean;

  @Column("bit", { name: "SoDuong", default: () => "0" })
  soDuong: boolean;

  @Column("int", { name: "Type", default: () => "0" })
  type: number;

  @Column("bit", { name: "IsObject", default: () => "0" })
  isObject: boolean;

  @Column("int", { name: "BalanceType", default: () => "0" })
  balanceType: number;

  @Column("varchar", { name: "CSType", nullable: true, length: 50 })
  csType: string | null;

  @ManyToOne(
    () => GjCfSetupTbl,
    gjCfSetupTbl => gjCfSetupTbl.gjCfSetupDetailTbls,
    { onDelete: "CASCADE", onUpdate: "CASCADE" }
  )
  @JoinColumn([{ name: "SetupID", referencedColumnName: "setupId" }])
  setup: GjCfSetupTbl;
}
