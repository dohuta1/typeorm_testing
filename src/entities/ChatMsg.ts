import { Column, Entity, Index, PrimaryGeneratedColumn } from "typeorm";

@Index("PK_ChatMsg", ["id"], { unique: true })
@Entity("ChatMsg", { schema: "dbo" })
export class ChatMsg {
  @PrimaryGeneratedColumn({
    type: "numeric",
    name: "ID"
    // precision: 18,
    // scale: 0
  })
  id: number;

  @Column("varchar", { name: "FromUser", nullable: true, length: 50 })
  fromUser: string | null;

  @Column("varchar", { name: "ToUser", nullable: true, length: 50 })
  toUser: string | null;

  @Column("bit", { name: "isPublic", nullable: true })
  isPublic: boolean | null;

  @Column("datetime", { name: "MsgDate", nullable: true })
  msgDate: Date | null;

  @Column("nvarchar", { name: "Msg", nullable: true, length: 500 })
  msg: string | null;

  @Column("nvarchar", { name: "MsgText", nullable: true, length: 500 })
  msgText: string | null;

  @Column("bit", { name: "isView", nullable: true })
  isView: boolean | null;

  @Column("varchar", { name: "FromUserDelete", nullable: true, length: 50 })
  fromUserDelete: string | null;

  @Column("varchar", { name: "ToUserDelete", nullable: true, length: 50 })
  toUserDelete: string | null;

  @Column("bit", { name: "isSelect", nullable: true })
  isSelect: boolean | null;
}
