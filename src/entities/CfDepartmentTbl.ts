import { Column, Entity, Index, OneToMany } from "typeorm";
import { FaAssetTbl } from "./FaAssetTbl";

@Index("PK_CF_DepartmentTbl", ["departmentId"], { unique: true })
@Entity("CF_DepartmentTbl", { schema: "dbo" })
export class CfDepartmentTbl {
  @Column("varchar", { primary: true, name: "DepartmentID", length: 50 })
  departmentId: string;

  @Column("nvarchar", { name: "DepartmentName", length: 100 })
  departmentName: string;

  @Column("nvarchar", { name: "Note", nullable: true, length: 100 })
  note: string | null;

  @Column("varchar", { name: "UserCreate", nullable: true, length: 30 })
  userCreate: string | null;

  @Column("varchar", { name: "UserUpdate", nullable: true, length: 30 })
  userUpdate: string | null;

  @Column("datetime", { name: "DateUpdate", nullable: true })
  dateUpdate: Date | null;

  @Column("datetime", { name: "DateCreate", nullable: true })
  dateCreate: Date | null;

  @Column("varchar", { name: "ParentDepartmentID", nullable: true, length: 50 })
  parentDepartmentId: string | null;

  @OneToMany(
    () => FaAssetTbl,
    faAssetTbl => faAssetTbl.department
  )
  faAssetTbls: FaAssetTbl[];
}
