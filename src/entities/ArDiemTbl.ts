import { Column, Entity, Index, OneToMany } from "typeorm";
import { ArDiemDetailTbl } from "./ArDiemDetailTbl";
import { ArPromotionTbl } from "./ArPromotionTbl";

@Index("PK_AR_DiemTbl", ["documentId"], { unique: true })
@Entity("AR_DiemTbl", { schema: "dbo" })
export class ArDiemTbl {
  @Column("varchar", { primary: true, name: "DocumentID", length: 30 })
  documentId: string;

  @Column("datetime", { name: "FromDate" })
  fromDate: Date;

  @Column("datetime", { name: "ToDate", nullable: true })
  toDate: Date | null;

  @Column("int", { name: "Loai", default: () => "(0)" })
  loai: number;

  @Column("nvarchar", { name: "Memo", nullable: true, length: 200 })
  memo: string | null;

  @Column("bit", { name: "isLock", default: () => "(0)" })
  isLock: boolean;

  @Column("varchar", { name: "UserCreate", nullable: true, length: 30 })
  userCreate: string | null;

  @Column("varchar", { name: "UserUpdate", nullable: true, length: 30 })
  userUpdate: string | null;

  @Column("datetime", { name: "DateUpdate", nullable: true })
  dateUpdate: Date | null;

  @Column("datetime", { name: "DateCreate", nullable: true })
  dateCreate: Date | null;

  @Column("bit", { name: "isDisable", default: () => "(0)" })
  isDisable: boolean;

  @Column("varchar", { name: "LinkID", nullable: true, length: 50 })
  linkId: string | null;

  @OneToMany(
    () => ArDiemDetailTbl,
    arDiemDetailTbl => arDiemDetailTbl.document
  )
  arDiemDetailTbls: ArDiemDetailTbl[];

  @OneToMany(
    () => ArPromotionTbl,
    arPromotionTbl => arPromotionTbl.diemTichLuy
  )
  arPromotionTbls: ArPromotionTbl[];
}
