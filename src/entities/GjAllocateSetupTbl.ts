import { Column, Entity, Index, OneToMany } from "typeorm";
import { GjAllocatePeriodSetupTbl } from "./GjAllocatePeriodSetupTbl";
import { GjAllocatePeriodTbl } from "./GjAllocatePeriodTbl";

@Index("PK_GJ_AllocateSetupTbl", ["allocateId"], { unique: true })
@Entity("GJ_AllocateSetupTbl", { schema: "dbo" })
export class GjAllocateSetupTbl {
  @Column("varchar", { primary: true, name: "AllocateID", length: 50 })
  allocateId: string;

  @Column("nvarchar", { name: "AllocateName", length: 100 })
  allocateName: string;

  @Column("datetime", { name: "AllocateDate" })
  allocateDate: Date;

  @Column("varchar", { name: "RefDoc", nullable: true, length: 50 })
  refDoc: string | null;

  @Column("varchar", { name: "RefSourceID", nullable: true, length: 3 })
  refSourceId: string | null;

  @Column("varchar", { name: "ItemID", nullable: true, length: 50 })
  itemId: string | null;

  @Column("varchar", { name: "AccountID", length: 50, default: () => "(1421)" })
  accountId: string;

  @Column("varchar", { name: "ExpenseAccID", length: 50 })
  expenseAccId: string;

  @Column("decimal", {
    name: "AllocateAmount",
    precision: 28,
    scale: 0,
    default: () => "(0)"
  })
  allocateAmount: number;

  @Column("decimal", {
    name: "OpenAllocate",
    nullable: true,
    precision: 28,
    scale: 0,
    default: () => "(0)"
  })
  openAllocate: number | null;

  @Column("int", { name: "MonthUse", default: () => "(3)" })
  monthUse: number;

  @Column("varchar", { name: "CostCenterID", nullable: true, length: 50 })
  costCenterId: string | null;

  @Column("varchar", { name: "CostID", nullable: true, length: 50 })
  costId: string | null;

  @Column("nvarchar", { name: "Notes", nullable: true, length: 100 })
  notes: string | null;

  @Column("datetime", {
    name: "DateCreate",
    nullable: true,
    default: () => "getdate()"
  })
  dateCreate: Date | null;

  @Column("datetime", {
    name: "DateUpdate",
    nullable: true,
    default: () => "getdate()"
  })
  dateUpdate: Date | null;

  @Column("varchar", { name: "UserCreate", nullable: true, length: 20 })
  userCreate: string | null;

  @Column("varchar", { name: "UserUpdate", nullable: true, length: 20 })
  userUpdate: string | null;

  @Column("varchar", { name: "ObjectID", nullable: true, length: 50 })
  objectId: string | null;

  @Column("datetime", { name: "StopDate", nullable: true })
  stopDate: Date | null;

  @Column("varchar", { name: "LinkID", nullable: true, length: 50 })
  linkId: string | null;

  @Column("varchar", { name: "BranchID", length: 50 })
  branchId: string;

  @Column("datetime", { name: "NgayChungTu", nullable: true })
  ngayChungTu: Date | null;

  @OneToMany(
    () => GjAllocatePeriodSetupTbl,
    gjAllocatePeriodSetupTbl => gjAllocatePeriodSetupTbl.allocate
  )
  gjAllocatePeriodSetupTbls: GjAllocatePeriodSetupTbl[];

  @OneToMany(
    () => GjAllocatePeriodTbl,
    gjAllocatePeriodTbl => gjAllocatePeriodTbl.allocate
  )
  gjAllocatePeriodTbls: GjAllocatePeriodTbl[];
}
