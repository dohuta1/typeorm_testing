import { Column, Entity, Index, OneToMany } from "typeorm";
import { ArObjectGroupAgencyTbl } from "./ArObjectGroupAgencyTbl";
import { ArObjectGroupDetailTbl } from "./ArObjectGroupDetailTbl";
import { ArObjectGroupEmployeeTbl } from "./ArObjectGroupEmployeeTbl";
import { ArObjectGroupUserNameTbl } from "./ArObjectGroupUserNameTbl";

@Index("IX_AR_ObjectGroupTbl", ["objectGroupName"], { unique: true })
@Index("PK_AR_ObjectGroupTbl", ["objectGroupCode"], { unique: true })
@Entity("AR_ObjectGroupTbl", { schema: "dbo" })
export class ArObjectGroupTbl {
  @Column("int", { primary: true, name: "ObjectGroupCode" })
  objectGroupCode: number;

  @Column("nvarchar", { name: "ObjectGroupName", length: 50 })
  objectGroupName: string;

  @Column("nvarchar", { name: "Memo", nullable: true, length: 150 })
  memo: string | null;

  @Column("bit", { name: "isDisable", default: () => "(0)" })
  isDisable: boolean;

  @Column("varchar", { name: "UserCreate", nullable: true, length: 30 })
  userCreate: string | null;

  @Column("varchar", { name: "UserUpdate", nullable: true, length: 30 })
  userUpdate: string | null;

  @Column("datetime", { name: "DateUpdate", nullable: true })
  dateUpdate: Date | null;

  @Column("datetime", { name: "DateCreate", nullable: true })
  dateCreate: Date | null;

  @Column("varchar", { name: "BranchID", length: 50 })
  branchId: string;

  @OneToMany(
    () => ArObjectGroupAgencyTbl,
    arObjectGroupAgencyTbl => arObjectGroupAgencyTbl.objectGroupCode
  )
  arObjectGroupAgencyTbls: ArObjectGroupAgencyTbl[];

  @OneToMany(
    () => ArObjectGroupDetailTbl,
    arObjectGroupDetailTbl => arObjectGroupDetailTbl.objectGroupCode
  )
  arObjectGroupDetailTbls: ArObjectGroupDetailTbl[];

  @OneToMany(
    () => ArObjectGroupEmployeeTbl,
    arObjectGroupEmployeeTbl => arObjectGroupEmployeeTbl.objectGroupCode
  )
  arObjectGroupEmployeeTbls: ArObjectGroupEmployeeTbl[];

  @OneToMany(
    () => ArObjectGroupUserNameTbl,
    arObjectGroupUserNameTbl => arObjectGroupUserNameTbl.objectGroupCode
  )
  arObjectGroupUserNameTbls: ArObjectGroupUserNameTbl[];
}
