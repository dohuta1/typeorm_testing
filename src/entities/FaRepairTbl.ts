import { Column, Entity, Index, OneToMany } from "typeorm";
import { FaRepairDetailTbl } from "./FaRepairDetailTbl";

@Index("PK_FA_RepairTbl", ["documentId"], { unique: true })
@Entity("FA_RepairTbl", { schema: "dbo" })
export class FaRepairTbl {
  @Column("varchar", { primary: true, name: "DocumentID", length: 30 })
  documentId: string;

  @Column("datetime", { name: "DocumentDate" })
  documentDate: Date;

  @Column("nvarchar", { name: "NguoiGiao", nullable: true, length: 150 })
  nguoiGiao: string | null;

  @Column("nvarchar", { name: "NguoiNhan", nullable: true, length: 150 })
  nguoiNhan: string | null;

  @Column("nvarchar", { name: "Memo", nullable: true, length: 150 })
  memo: string | null;

  @Column("nvarchar", { name: "Notes", nullable: true, length: 150 })
  notes: string | null;

  @Column("bit", { name: "isLock" })
  isLock: boolean;

  @Column("varchar", { name: "UserCreate", nullable: true, length: 30 })
  userCreate: string | null;

  @Column("varchar", { name: "UserUpdate", nullable: true, length: 30 })
  userUpdate: string | null;

  @Column("datetime", { name: "DateUpdate", nullable: true })
  dateUpdate: Date | null;

  @Column("datetime", { name: "DateCreate", nullable: true })
  dateCreate: Date | null;

  @OneToMany(
    () => FaRepairDetailTbl,
    faRepairDetailTbl => faRepairDetailTbl.document
  )
  faRepairDetailTbls: FaRepairDetailTbl[];
}
