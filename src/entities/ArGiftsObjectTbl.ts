import { Column, Entity, Index, JoinColumn, ManyToOne } from "typeorm";
import { ArGiftsTbl } from "./ArGiftsTbl";

@Index("PK_AR_GiftsObjectTbl", ["userAutoId"], { unique: true })
@Entity("AR_GiftsObjectTbl", { schema: "dbo" })
export class ArGiftsObjectTbl {
  @Column("varchar", { primary: true, name: "UserAutoID", length: 40 })
  userAutoId: string;

  @Column("varchar", { name: "ObjectID", nullable: true, length: 50 })
  objectId: string | null;

  @ManyToOne(
    () => ArGiftsTbl,
    arGiftsTbl => arGiftsTbl.arGiftsObjectTbls,
    { onDelete: "CASCADE", onUpdate: "CASCADE" }
  )
  @JoinColumn([{ name: "DocumentID", referencedColumnName: "documentId" }])
  document: ArGiftsTbl;
}
