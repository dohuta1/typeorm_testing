import {
  Column,
  Entity,
  Index,
  JoinColumn,
  ManyToOne,
  OneToMany
} from "typeorm";
import { ApReturnDetailTbl } from "./ApReturnDetailTbl";
import { CfObjectTbl } from "./CfObjectTbl";

@Index("PK_ReturnTbl", ["documentId"], { unique: true })
@Entity("AP_ReturnTbl", { schema: "dbo" })
export class ApReturnTbl {
  @Column("varchar", { primary: true, name: "DocumentID", length: 30 })
  documentId: string;

  @Column("datetime", { name: "DocumentDate" })
  documentDate: Date;

  @Column("nvarchar", { name: "Memo", nullable: true, length: 200 })
  memo: string | null;

  @Column("nvarchar", { name: "Notes", nullable: true, length: 200 })
  notes: string | null;

  @Column("varchar", { name: "CurrencyID", length: 3, default: () => "'VND'" })
  currencyId: string;

  @Column("float", { name: "RateExchange", precision: 53, default: () => "1" })
  rateExchange: number;

  @Column("decimal", {
    name: "BaseTotal",
    nullable: true,
    precision: 18,
    scale: 0
  })
  baseTotal: number | null;

  @Column("varchar", { name: "PayAccID", length: 50, default: () => "331" })
  payAccId: string;

  @Column("varchar", { name: "VATAccID", length: 50, default: () => "1331" })
  vatAccId: string;

  @Column("varchar", { name: "ExpenseAccID", nullable: true, length: 50 })
  expenseAccId: string | null;

  @Column("bit", { name: "isLock" })
  isLock: boolean;

  @Column("nvarchar", { name: "SearchField", nullable: true, length: 2000 })
  searchField: string | null;

  @Column("varchar", { name: "InvoiceNo", nullable: true, length: 50 })
  invoiceNo: string | null;

  @Column("varchar", { name: "UserCreate", nullable: true, length: 30 })
  userCreate: string | null;

  @Column("varchar", { name: "UserUpdate", nullable: true, length: 30 })
  userUpdate: string | null;

  @Column("datetime", { name: "DateUpdate", nullable: true })
  dateUpdate: Date | null;

  @Column("datetime", { name: "DateCreate", nullable: true })
  dateCreate: Date | null;

  @Column("int", { name: "Status", default: () => "1" })
  status: number;

  @Column("varchar", { name: "LinkID", nullable: true, length: 50 })
  linkId: string | null;

  @Column("varchar", { name: "ContractID", nullable: true, length: 50 })
  contractId: string | null;

  @Column("decimal", {
    name: "AmountTotal",
    nullable: true,
    precision: 28,
    scale: 0
  })
  amountTotal: number | null;

  @Column("decimal", {
    name: "VATAmountTotal",
    nullable: true,
    precision: 28,
    scale: 0
  })
  vatAmountTotal: number | null;

  @Column("varchar", { name: "ImpExpID", nullable: true, length: 10 })
  impExpId: string | null;

  @Column("varchar", { name: "DocBatch", nullable: true, length: 50 })
  docBatch: string | null;

  @Column("nvarchar", { name: "Attach", nullable: true, length: 100 })
  attach: string | null;

  @Column("datetime", { name: "AttachDate", nullable: true })
  attachDate: Date | null;

  @Column("varchar", { name: "BranchID", length: 50 })
  branchId: string;

  @Column("varchar", { name: "InvoiceKey", nullable: true, length: 50 })
  invoiceKey: string | null;

  @Column("varchar", { name: "RefInvoiceKey", nullable: true, length: 50 })
  refInvoiceKey: string | null;

  @Column("varchar", { name: "UID", nullable: true, length: 50 })
  uid: string | null;

  @OneToMany(
    () => ApReturnDetailTbl,
    apReturnDetailTbl => apReturnDetailTbl.document
  )
  apReturnDetailTbls: ApReturnDetailTbl[];

  @ManyToOne(
    () => CfObjectTbl,
    cfObjectTbl => cfObjectTbl.apReturnTbls,
    { onUpdate: "CASCADE" }
  )
  @JoinColumn([{ name: "ObjectID", referencedColumnName: "objectId" }])
  object: CfObjectTbl;
}
