import { Column, Entity, Index, OneToMany } from "typeorm";
import { ArPriceDetailTbl } from "./ArPriceDetailTbl";
import { ArPriceObjectTbl } from "./ArPriceObjectTbl";

@Index("PK_AR_PriceTbl", ["documentId"], { unique: true })
@Entity("AR_PriceTbl", { schema: "dbo" })
export class ArPriceTbl {
  @Column("varchar", { primary: true, name: "DocumentID", length: 30 })
  documentId: string;

  @Column("datetime", { name: "FromDate" })
  fromDate: Date;

  @Column("datetime", { name: "ToDate", nullable: true })
  toDate: Date | null;

  @Column("varchar", { name: "ObjectGroupID", nullable: true, length: 50 })
  objectGroupId: string | null;

  @Column("nvarchar", { name: "LocationID", nullable: true, length: 50 })
  locationId: string | null;

  @Column("varchar", { name: "ZoneID", nullable: true, length: 50 })
  zoneId: string | null;

  @Column("bit", { name: "isZoneID", default: () => "(0)" })
  isZoneId: boolean;

  @Column("varchar", { name: "ObjectID", nullable: true, length: 50 })
  objectId: string | null;

  @Column("varchar", { name: "EmployeeID", nullable: true, length: 50 })
  employeeId: string | null;

  @Column("nvarchar", { name: "Opponent", nullable: true, length: 200 })
  opponent: string | null;

  @Column("nvarchar", { name: "TranferMode", nullable: true, length: 100 })
  tranferMode: string | null;

  @Column("nvarchar", { name: "PaymentMode", nullable: true, length: 100 })
  paymentMode: string | null;

  @Column("nvarchar", { name: "Feedback", nullable: true, length: 100 })
  feedback: string | null;

  @Column("nvarchar", { name: "Memo", nullable: true, length: 200 })
  memo: string | null;

  @Column("bit", { name: "isLock", default: () => "(0)" })
  isLock: boolean;

  @Column("bit", { name: "isStandard", default: () => "(0)" })
  isStandard: boolean;

  @Column("varchar", { name: "UserCreate", nullable: true, length: 30 })
  userCreate: string | null;

  @Column("varchar", { name: "UserUpdate", nullable: true, length: 30 })
  userUpdate: string | null;

  @Column("datetime", { name: "DateUpdate", nullable: true })
  dateUpdate: Date | null;

  @Column("datetime", { name: "DateCreate", nullable: true })
  dateCreate: Date | null;

  @Column("bit", { name: "isDisable", default: () => "(0)" })
  isDisable: boolean;

  @Column("varchar", { name: "LinkID", nullable: true, length: 50 })
  linkId: string | null;

  @OneToMany(
    () => ArPriceDetailTbl,
    arPriceDetailTbl => arPriceDetailTbl.document
  )
  arPriceDetailTbls: ArPriceDetailTbl[];

  @OneToMany(
    () => ArPriceObjectTbl,
    arPriceObjectTbl => arPriceObjectTbl.document
  )
  arPriceObjectTbls: ArPriceObjectTbl[];
}
