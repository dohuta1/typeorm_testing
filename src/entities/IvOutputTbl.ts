import {
  Column,
  Entity,
  Index,
  JoinColumn,
  ManyToOne,
  OneToMany
} from "typeorm";
import { IvOutputCouponTbl } from "./IvOutputCouponTbl";
import { IvOutputDetailTbl } from "./IvOutputDetailTbl";
import { CfObjectTbl } from "./CfObjectTbl";

@Index("PK_OutputTbl", ["documentId"], { unique: true })
@Entity("IV_OutputTbl", { schema: "dbo" })
export class IvOutputTbl {
  @Column("varchar", { primary: true, name: "DocumentID", length: 30 })
  documentId: string;

  @Column("datetime", { name: "DocumentDate" })
  documentDate: Date;

  @Column("nvarchar", { name: "Memo", nullable: true, length: 200 })
  memo: string | null;

  @Column("nvarchar", { name: "Notes", nullable: true, length: 200 })
  notes: string | null;

  @Column("bit", { name: "isLock", default: () => "(0)" })
  isLock: boolean;

  @Column("varchar", { name: "OutputType", length: 20, default: () => "'XS'" })
  outputType: string;

  @Column("nvarchar", { name: "SearchField", nullable: true, length: 2000 })
  searchField: string | null;

  @Column("varchar", { name: "UserCreate", nullable: true, length: 30 })
  userCreate: string | null;

  @Column("varchar", { name: "UserUpdate", nullable: true, length: 30 })
  userUpdate: string | null;

  @Column("datetime", { name: "DateUpdate", nullable: true })
  dateUpdate: Date | null;

  @Column("datetime", { name: "DateCreate", nullable: true })
  dateCreate: Date | null;

  @Column("int", { name: "Status", default: () => "(1)" })
  status: number;

  @Column("bit", { name: "CalPrice", default: () => "(1)" })
  calPrice: boolean;

  @Column("bit", { name: "CalCost", default: () => "(0)" })
  calCost: boolean;

  @Column("varchar", { name: "LinkID", nullable: true, length: 50 })
  linkId: string | null;

  @Column("bit", { name: "isPacket", default: () => "(0)" })
  isPacket: boolean;

  @Column("decimal", {
    name: "BaseTotal",
    nullable: true,
    precision: 28,
    scale: 0
  })
  baseTotal: number | null;

  @Column("varchar", { name: "DocBatch", nullable: true, length: 50 })
  docBatch: string | null;

  @Column("nvarchar", { name: "NguoiNhan", nullable: true, length: 100 })
  nguoiNhan: string | null;

  @Column("varchar", { name: "ProductOrderID", nullable: true, length: 50 })
  productOrderId: string | null;

  @Column("varchar", { name: "BranchID", length: 50 })
  branchId: string;

  @Column("nvarchar", { name: "NguoiGiao", nullable: true, length: 100 })
  nguoiGiao: string | null;

  @OneToMany(
    () => IvOutputCouponTbl,
    ivOutputCouponTbl => ivOutputCouponTbl.document
  )
  ivOutputCouponTbls: IvOutputCouponTbl[];

  @OneToMany(
    () => IvOutputDetailTbl,
    ivOutputDetailTbl => ivOutputDetailTbl.document
  )
  ivOutputDetailTbls: IvOutputDetailTbl[];

  @ManyToOne(
    () => CfObjectTbl,
    cfObjectTbl => cfObjectTbl.ivOutputTbls,
    { onUpdate: "CASCADE" }
  )
  @JoinColumn([{ name: "ObjectID", referencedColumnName: "objectId" }])
  object: CfObjectTbl;
}
