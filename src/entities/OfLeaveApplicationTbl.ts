import { Column, Entity, Index } from "typeorm";

@Index("PK_OF_LeaveApplicationTbl", ["documentId"], { unique: true })
@Entity("OF_LeaveApplicationTbl", { schema: "dbo" })
export class OfLeaveApplicationTbl {
  @Column("varchar", { primary: true, name: "DocumentID", length: 50 })
  documentId: string;

  @Column("datetime", { name: "DocumentDate" })
  documentDate: Date;

  @Column("varchar", { name: "NguoiXinNghi", length: 50 })
  nguoiXinNghi: string;

  @Column("decimal", {
    name: "SoNgayXinNghi",
    precision: 18,
    scale: 1,
    default: () => "1"
  })
  soNgayXinNghi: number;

  @Column("int", { name: "LoaiNghiPhep", nullable: true, default: () => "0" })
  loaiNghiPhep: number | null;

  @Column("datetime", { name: "NgayBD" })
  ngayBd: Date;

  @Column("nvarchar", { name: "GhiChu", nullable: true, length: 250 })
  ghiChu: string | null;

  @Column("nvarchar", { name: "YKien", nullable: true, length: 250 })
  yKien: string | null;

  @Column("bit", { name: "isLock", default: () => "0" })
  isLock: boolean;

  @Column("datetime", { name: "DateCreate", nullable: true })
  dateCreate: Date | null;

  @Column("datetime", { name: "DateUpdate", nullable: true })
  dateUpdate: Date | null;

  @Column("varchar", { name: "UserCreate", nullable: true, length: 20 })
  userCreate: string | null;

  @Column("varchar", { name: "UserUpdate", nullable: true, length: 20 })
  userUpdate: string | null;
}
