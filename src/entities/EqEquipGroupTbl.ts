import { Column, Entity, Index, OneToMany } from "typeorm";
import { EqEquipTbl } from "./EqEquipTbl";

@Index("PK_EQ_EquipGroupTbl", ["equipGroupId"], { unique: true })
@Entity("EQ_EquipGroupTbl", { schema: "dbo" })
export class EqEquipGroupTbl {
  @Column("varchar", { primary: true, name: "EquipGroupID", length: 50 })
  equipGroupId: string;

  @Column("nvarchar", { name: "EquipGroupName", length: 150 })
  equipGroupName: string;

  @Column("nvarchar", { name: "Notes", nullable: true, length: 150 })
  notes: string | null;

  @Column("bit", { name: "isDefault", nullable: true })
  isDefault: boolean | null;

  @OneToMany(
    () => EqEquipTbl,
    eqEquipTbl => eqEquipTbl.equipGroup
  )
  eqEquipTbls: EqEquipTbl[];
}
