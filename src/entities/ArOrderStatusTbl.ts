import { Column, Entity, Index, OneToMany } from "typeorm";
import { ArOrderTbl } from "./ArOrderTbl";

@Index("PK__AR_Statu__C8EE20433AC76B74", ["statusId"], { unique: true })
@Entity("AR_OrderStatusTbl", { schema: "dbo" })
export class ArOrderStatusTbl {
  @Column("int", { primary: true, name: "StatusID" })
  statusId: number;

  @Column("nvarchar", { name: "StatusName", length: 50 })
  statusName: string;

  @Column("int", { name: "BackColor", nullable: true })
  backColor: number | null;

  @Column("int", { name: "ForeColor", nullable: true })
  foreColor: number | null;

  @OneToMany(
    () => ArOrderTbl,
    arOrderTbl => arOrderTbl.status
  )
  arOrderTbls: ArOrderTbl[];
}
