import { Column, Entity, Index, JoinColumn, ManyToOne } from "typeorm";
import { HrPersonTbl } from "./HrPersonTbl";

@Index("PK_HR_PersonKTKLTbl", ["userAutoId"], { unique: true })
@Entity("HR_PersonKTKLTbl", { schema: "dbo" })
export class HrPersonKtklTbl {
  @Column("varchar", { primary: true, name: "UserAutoID", length: 50 })
  userAutoId: string;

  @Column("datetime", { name: "Date", nullable: true })
  date: Date | null;

  @Column("nvarchar", { name: "NoiDungKTKL", nullable: true, length: 250 })
  noiDungKtkl: string | null;

  @Column("nvarchar", { name: "GhiChu", nullable: true, length: 250 })
  ghiChu: string | null;

  @ManyToOne(
    () => HrPersonTbl,
    hrPersonTbl => hrPersonTbl.hrPersonKtklTbls,
    { onDelete: "CASCADE", onUpdate: "CASCADE" }
  )
  @JoinColumn([{ name: "PersonID", referencedColumnName: "personId" }])
  person: HrPersonTbl;
}
