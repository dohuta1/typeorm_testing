import { Column, Entity, Index, JoinColumn, ManyToOne } from "typeorm";
import { CfObjectTbl } from "./CfObjectTbl";
import { ArObjectGroupTbl } from "./ArObjectGroupTbl";

@Index("PK_AR_ObjectGroupEmployeeTbl", ["userAutoId"], { unique: true })
@Entity("AR_ObjectGroupEmployeeTbl", { schema: "dbo" })
export class ArObjectGroupEmployeeTbl {
  @Column("varchar", { primary: true, name: "UserAutoID", length: 50 })
  userAutoId: string;

  @Column("int", { name: "ItemGroupCode", default: () => "(1)" })
  itemGroupCode: number;

  @Column("varchar", { name: "ManagerID", length: 50 })
  managerId: string;

  @Column("datetime", { name: "StartDate", nullable: true })
  startDate: Date | null;

  @Column("datetime", { name: "EndDate", nullable: true })
  endDate: Date | null;

  @Column("bit", { name: "isDisable", nullable: true, default: () => "(0)" })
  isDisable: boolean | null;

  @Column("nvarchar", { name: "Note", nullable: true, length: 50 })
  note: string | null;

  @ManyToOne(
    () => CfObjectTbl,
    cfObjectTbl => cfObjectTbl.arObjectGroupEmployeeTbls,
    { onUpdate: "CASCADE" }
  )
  @JoinColumn([{ name: "EmployeeID", referencedColumnName: "objectId" }])
  employee: CfObjectTbl;

  @ManyToOne(
    () => ArObjectGroupTbl,
    arObjectGroupTbl => arObjectGroupTbl.arObjectGroupEmployeeTbls,
    { onDelete: "CASCADE", onUpdate: "CASCADE" }
  )
  @JoinColumn([
    { name: "ObjectGroupCode", referencedColumnName: "objectGroupCode" }
  ])
  objectGroupCode: ArObjectGroupTbl;
}
