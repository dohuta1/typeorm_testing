import "reflect-metadata";
import { createConnection } from "typeorm";
import { ArOrderTbl } from "./entities/ArOrderTbl";

// NOTE: typeorm does not allow no-primary-column entities, so we must assign a temporary decoration to first column in SyInitSetup.ts, SySynDate.ts/
// NOTE: typeorm does not support precision and scale attribute in numeric type, so we comment them in ChatMsg.ts, SyFmtFldTbl.ts,

createConnection()
  .then(async connection => {
    // create rpository
    const orderRepository = connection.getRepository(ArOrderTbl);
    // get order by id
    const order = await orderRepository.findByIds(["DH0819/0218"]);
    console.log("TCL: order", order);
  })
  .catch(error => console.log(error));
